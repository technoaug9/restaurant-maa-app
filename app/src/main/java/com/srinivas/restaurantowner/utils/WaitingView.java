package com.srinivas.restaurantowner.utils;

import android.app.Activity;

public class WaitingView {
	private static CustomProgressDialog progressDialog = null;
	public static void startProgressDialog(Activity mContext){
		if (progressDialog == null || progressDialog.getWindow() == null){
			progressDialog = CustomProgressDialog.createDialog(mContext);
			progressDialog.setMessage("");
		}
		
    	progressDialog.show();
	}
	
	public  static  void stopProgressDialog(){
		
		if (progressDialog != null&&progressDialog.getWindow()!=null){
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
