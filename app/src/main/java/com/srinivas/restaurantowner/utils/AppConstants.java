package com.srinivas.restaurantowner.utils;

/**
 * Created by rahul.kumar on 20/8/15.
 */
public class AppConstants
{

    public static String USER_EMAIL_ID="user_email_id";
    public static String USER_PASSWORD="user_password";
    public static int POSITION_DELETE=-1;
    public static String PRODUCT_ACTIVITY_KEY="product_activity_key";
    public static String INGREDIENT_ACTIVITY_KEY="ingredient_activity_key";
    public static String FRIEND_PROFILE="FriendProfile";
    public static String SCAN_RESULT_KEY="scan_result_key";
    public static String BARCODE_SCAN_KEY="barcode_scan_key";
    public static String QR_SCAN_KEY="qr_scan_key";
    public static String TERM_AND_POLICY_KEY="term_and_policy_key";
    public static String TERM_SIGN_UP_KEY="term_sign_up_key";
    public static String TERM_INFO_KEY="term_info_key";
    public static String MANAGE_PROFILE_KEY="manage_profile";
    public static String ADD_MEMBER="add_member";
    public static String ADD_CHEF="add_chef";
    public static String CHEF_REGISTRATION="chef_registration";

    public static String PRE_ISLOGIN="isLogin";//if isLogin==>1 ie already logged in. else no one is logged in


    public static String USER_TYPE = "user_type";
    public static String USER_TYPE_NORMAL = "normal";
    public static String USER_TYPE_OWNER = "owner";
    public static String USER_TYPE_CHEF = "chef";
    public static String MANAGE_MENU_PRODUCT="manage_menu_product";
    public static String NFC="nfc";
    public static String HOME="home";
    public static String NFCActivity="nfcactivity";
    public static String DELETENFC="deletenfc";
    public static String QRSCANNER="qrscanner";
    public static String NOPRODUCT="NOPRODUCT";
    public static String READNFC="readnfc";
    public static String CONTENTPRESENT="Contentpresent";
    public static String NAVIGATEQRACTIVITY="navigateqyactivity";
    public static String QRCODEADAPTER="qrcodeadapter";
    public static String QRSCREENNAVI="qrscreen";
    public static String IS_NFC = "is_nfc";
    public static String ADD_NFC = "add_nfc_adapter";
    public static String IS_QRSCANNER="QRSCANNER";
    public static String NFCDISSMISS="Nfcchipdismiss";


    public  static String TVDELETEQR="tvdeleteqr";
    public static String TVQRREADER="tvqrreader";
    public static String REM_ME_CHECKED="remember";
    public static String CHEFPRODUCTADAPTER="productname";
    public static String MODIFY="modifydialog";
    public static String DELETEDIALOFNFC="deletedialofnfc";
    public static String NFCCHIPSCANNER="nfcchipscanner";
    public static String AGAINNFCCHIP="againnfchip";
    public static String ADDNFCSCANNER="addnfcscanner";

    //Userid genrate by signup

    public final static String USER_ID="userId";
    public final static String Member_ID="memberId";
    public final static String IS_LOGIN="isLogin";
    public final static String IS_VERIFIED="isVerified";
    public final static String AddMember = "member";

    public static String USER_TYPE_RESTAURANT_OWNER = "user_type_restaurant_owner";
    public static String USER_TYPE_GENERALUSER = "user_type_general_user";
}
