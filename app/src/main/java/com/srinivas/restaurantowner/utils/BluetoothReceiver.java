package com.srinivas.restaurantowner.utils;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by ravi.shah on 8/1/16.
 */
public class BluetoothReceiver extends BroadcastReceiver{
    public final static String TAG = "BluetoothReciever";

    public void onReceive(Context context, Intent intent)
    {
        Log.d(TAG, "Bluetooth Intent Recieved");

        final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        String action = intent.getAction();

        if (BluetoothDevice.ACTION_ACL_CONNECTED.equalsIgnoreCase(action))
        {
            Log.d(TAG, "Connected to " + device.getName());
            Toast.makeText(context, "BT Connected", Toast.LENGTH_SHORT).show();
        }

        if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equalsIgnoreCase(action))
        {
            Log.d(TAG, "Disconnected from " + device.getName());
            Toast.makeText(context, "BT Disconnected", Toast.LENGTH_SHORT).show();
        }
    }}