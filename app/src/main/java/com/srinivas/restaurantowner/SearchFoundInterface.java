package com.srinivas.restaurantowner;

/**
 * Created by shashank.kapsime on 8/2/16.
 */
public interface SearchFoundInterface {

    public void onMatchFound();
}
