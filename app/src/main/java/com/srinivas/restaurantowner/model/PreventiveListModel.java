package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class PreventiveListModel {
    public int getIvUserImg() {
        return ivUserImg;
    }

    public void setIvUserImg(int ivUserImg) {
        this.ivUserImg = ivUserImg;
    }

    public String getTvUserName() {
        return tvUserName;
    }

    public void setTvUserName(String tvUserName) {
        this.tvUserName = tvUserName;
    }

    public String getTvUserAge() {
        return tvUserAge;
    }

    public void setTvUserAge(String tvUserAge) {
        this.tvUserAge = tvUserAge;
    }

    int ivUserImg;
    String tvUserName;
    String tvUserAge;
}
