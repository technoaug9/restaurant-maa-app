package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class ScanResultRelativeModel {
    String tvName;

    public String getTvAdvice() {
        return tvAdvice;
    }

    public void setTvAdvice(String tvAdvice) {
        this.tvAdvice = tvAdvice;
    }

    public String getTvRelativeIngrediants() {
        return tvRelativeIngrediants;
    }

    public void setTvRelativeIngrediants(String tvRelativeIngrediants) {
        this.tvRelativeIngrediants = tvRelativeIngrediants;
    }

    public String getTvName() {
        return tvName;
    }

    public void setTvName(String tvName) {
        this.tvName = tvName;
    }

    String tvAdvice;
    String tvRelativeIngrediants;
}
