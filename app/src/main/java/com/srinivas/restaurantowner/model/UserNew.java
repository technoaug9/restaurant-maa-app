package com.srinivas.restaurantowner.model;

import java.io.Serializable;

/**
 * Created by shashank.kapsime on 5/11/15.
 */
public class UserNew implements Serializable {

    public String name;
    public String email;
    public String dob;
    public String id;
    public ImageURL image;
    public String subject;
    public String description;
    public String user_type;
    public String phone_no;
    public String restaurant_name;

}
