package com.srinivas.restaurantowner.model;

import android.widget.ImageView;

/**
 * Created by vinay.tripathi on 21/9/15.
 */
public class CreateFormModel {

    String ProductName;

    public ImageView getImage() {
        return Image;
    }

    public void setImage(ImageView image) {
        Image = image;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    ImageView Image;

}

