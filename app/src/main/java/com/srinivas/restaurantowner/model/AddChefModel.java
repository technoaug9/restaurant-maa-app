package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 21/9/15.
 */
public class AddChefModel {

    public String getTvuserfrndName() {
        return tvuserfrndName;
    }

    public void setTvuserfrndName(String tvuserfrndName) {
        this.tvuserfrndName = tvuserfrndName;
    }

    public int getIvUserFrndImage() {
        return ivUserFrndImage;
    }

    public void setIvUserFrndImage(int ivUserFrndImage) {
        this.ivUserFrndImage = ivUserFrndImage;
    }

    private String tvuserfrndName;
    private int ivUserFrndImage;
    private int foodimages;

    public int getFoodimages() {
        return foodimages;
    }

    public void setFoodimages(int foodimages) {
        this.foodimages = foodimages;
    }
}
