package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 21/9/15.
 */
public class ManageMenuModel {
    public String getTvMenuIngredients() {
        return tvMenuIngredients;
    }

    public void setTvMenuIngredients(String tvMenuIngredients) {
        this.tvMenuIngredients = tvMenuIngredients;
    }

    public int getIvCross() {
        return ivCross;
    }

    public void setIvCross(int ivCross) {
        this.ivCross = ivCross;
    }

    private String tvMenuIngredients;
    private int ivCross;
}
