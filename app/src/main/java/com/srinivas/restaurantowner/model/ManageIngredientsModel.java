package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 21/9/15.
 */
public class ManageIngredientsModel {
    public String getTvManageIngredients() {
        return tvManageIngredients;
    }

    public void setTvManageIngredients(String tvManageIngredients) {
        this.tvManageIngredients = tvManageIngredients;
    }

    private String tvManageIngredients;

    public int getIvDeleteItem() {
        return ivDeleteItem;
    }

    public void setIvDeleteItem(int ivDeleteItem) {
        this.ivDeleteItem = ivDeleteItem;
    }

    private int ivDeleteItem;
}
