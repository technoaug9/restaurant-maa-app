package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SearchProductUserVisibilityModel {
    public String getTvSearchProductVisibility() {
        return tvSearchProductVisibility;
    }

    public void setTvSearchProductVisibility(String tvSearchProductVisibility) {
        this.tvSearchProductVisibility = tvSearchProductVisibility;
    }

    String tvSearchProductVisibility;

    }
