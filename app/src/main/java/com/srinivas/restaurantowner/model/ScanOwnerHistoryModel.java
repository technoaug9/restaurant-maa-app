package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 23/9/15.
 */
public class ScanOwnerHistoryModel
{
    public String getTvTime() {
        return tvTime;
    }

    public void setTvTime(String tvTime) {
        this.tvTime = tvTime;
    }

    public String getTvDate() {
        return tvDate;
    }

    public void setTvDate(String tvDate) {
        this.tvDate = tvDate;
    }

    public String getTvMilk() {
        return tvMilk;
    }

    public void setTvMilk(String tvMilk) {
        this.tvMilk = tvMilk;
    }

    private String tvTime;
    private String tvDate;
    private String tvMilk;
}
