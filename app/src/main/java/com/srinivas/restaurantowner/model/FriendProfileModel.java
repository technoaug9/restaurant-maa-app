package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class FriendProfileModel {
    public int getIvProductImg() {
        return ivProductImg;
    }

    public void setIvProductImg(int ivProductImg) {
        this.ivProductImg = ivProductImg;
    }

    public String getTvProductName() {
        return tvProductName;
    }

    public void setTvProductName(String tvProductName) {
        this.tvProductName = tvProductName;
    }

    public int getIvDeleteProduct() {
        return ivDeleteProduct;
    }

    public void setIvDeleteProduct(int ivDeleteProduct) {
        this.ivDeleteProduct = ivDeleteProduct;
    }

    int ivProductImg;
    String tvProductName;
    int ivDeleteProduct;

}
