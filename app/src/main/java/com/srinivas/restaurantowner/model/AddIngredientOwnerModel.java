package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 19/8/15.
 */
public class AddIngredientOwnerModel {
    public String getTvProductName() {
        return tvProductName;
    }

    public void setTvProductName(String tvProductName) {
        this.tvProductName = tvProductName;
    }

    public int getIvProductImg() {
        return ivProductImg;
    }

    public void setIvProductImg(int ivProductImg) {
        this.ivProductImg = ivProductImg;
    }

    String tvProductName;
    int ivProductImg;
}
