package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SearchProductThirdModel {
    String tvSearchProductUser;
    String tvSearchProductAdvice;

    public String getTvSearchProductIngredients() {
        return tvSearchProductIngredients;
    }

    public void setTvSearchProductIngredients(String tvSearchProductIngredients) {
        this.tvSearchProductIngredients = tvSearchProductIngredients;
    }

    public String getTvSearchProductUser() {
        return tvSearchProductUser;
    }

    public void setTvSearchProductUser(String tvSearchProductUser) {
        this.tvSearchProductUser = tvSearchProductUser;
    }

    public String getTvSearchProductAdvice() {
        return tvSearchProductAdvice;
    }

    public void setTvSearchProductAdvice(String tvSearchProductAdvice) {
        this.tvSearchProductAdvice = tvSearchProductAdvice;
    }

    String tvSearchProductIngredients;
}
