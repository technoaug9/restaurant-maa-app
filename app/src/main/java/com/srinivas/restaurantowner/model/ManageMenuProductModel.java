package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 22/9/15.
 */
public class ManageMenuProductModel {
    public String getTvIngredientName() {
        return tvIngredientName;
    }

    public void setTvIngredientName(String tvIngredientName) {
        this.tvIngredientName = tvIngredientName;
    }

    private String tvIngredientName;

    public int getIvDelete() {
        return ivDelete;
    }

    public void setIvDelete(int ivDelete) {
        this.ivDelete = ivDelete;
    }

    private int ivDelete;
}
