package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 21/9/15.
 */
public class SelectNfcChipModel {

    private int ivProductImg;
    private String rowHeader;

    public String getTvProductName() {
        return tvProductName;
    }

    public void setTvProductName(String tvProductName) {
        this.tvProductName = tvProductName;
    }

    public int getIvProductImg() {
        return ivProductImg;
    }

    public void setIvProductImg(int ivProductImg) {
        this.ivProductImg = ivProductImg;
    }

    public String getTvProductUniqueCode() {
        return tvProductUniqueCode;
    }

    public void setTvProductUniqueCode(String tvProductUniqueCode) {
        this.tvProductUniqueCode = tvProductUniqueCode;

    }

    private String  tvProductName;
    private String tvProductUniqueCode;
   // private String rowHeader;

    public String getRowHeader() {
        return rowHeader;
    }

    public void setRowHeader(String rowHeader) {
        this.rowHeader = rowHeader;
    }
}
