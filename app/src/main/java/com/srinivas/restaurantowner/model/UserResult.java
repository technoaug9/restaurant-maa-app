package com.srinivas.restaurantowner.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shashank.kapsime on 24/10/15.
 */
public class UserResult implements Serializable {

    public String id;
    public String name;
    public String image;
    public String state;
    public List<String> harmful_ingredients;
}
