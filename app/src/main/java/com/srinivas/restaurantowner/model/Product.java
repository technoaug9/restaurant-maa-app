package com.srinivas.restaurantowner.model;

import com.srinivas.restaurantowner.Services.IngredientList;
import java.io.Serializable;
import java.util.List;
/**
 * Created by shashank.kapsime on 27/10/15.
 */
public class Product implements Serializable
{
    public String id;
    public String name;
    public String user_id;
    public String product_name;
    public String history_type;
    public String created_at;
    public String updated_at;
    public String qr_code;
    public String nfc_code;
    public String image;
    public List<IngredientList> ingredients;
    public User user;
}
