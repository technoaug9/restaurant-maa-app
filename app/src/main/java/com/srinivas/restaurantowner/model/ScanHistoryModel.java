package com.srinivas.restaurantowner.model;

/**
 * Created by madhuri.chaudhari on 19/8/15.
 */
public class ScanHistoryModel {
    public String time;
    public String date;
    public String nameOfProduct;
    public String scanResult;
    public String shefName;
    public String assign;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public String getScanResult() {
        return scanResult;
    }

    public void setScanResult(String scanResult) {
        this.scanResult = scanResult;
    }
    public String getShefName() {
        return shefName;
    }

    public void setShefName(String shefName) {
        this.shefName = shefName;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }





}
