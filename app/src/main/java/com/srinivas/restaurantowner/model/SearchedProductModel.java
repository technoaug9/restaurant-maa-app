package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SearchedProductModel {
    public String getTvIngredientList() {
        return tvIngredientList;
    }

    public void setTvIngredientList(String tvIngredientList) {
        this.tvIngredientList = tvIngredientList;
    }

    String tvIngredientList;
}
