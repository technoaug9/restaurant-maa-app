package com.srinivas.restaurantowner.model;

import java.io.Serializable;

/**
 * Created by vinay.tripathi on 25/9/15.
 */
public class User
        implements Serializable {

    public String name;
    public String email;
    public String dob;
    public String id;
    public String image;
    public String subject;
    public String description;
    public String user_type;
    public String phone_no;
    public String restaurant_name;


}
