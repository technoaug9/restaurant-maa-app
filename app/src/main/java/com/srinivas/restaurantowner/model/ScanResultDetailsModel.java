package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 18/8/15.
 */
public class ScanResultDetailsModel {
    public String getTvIngredientsName() {
        return tvIngredientsName;
    }

    public void setTvIngredientsName(String tvIngredientsName) {
        this.tvIngredientsName = tvIngredientsName;
    }

    String tvIngredientsName;
    }
