package com.srinivas.restaurantowner.model;

/**
 * Created by priya.singh on 19/8/15.
 */
public class ProductModel
{
    public String getTvProductName()
    {
        return tvProductName;
    }

    public void setTvProductName(String tvProductName)
    {
        this.tvProductName = tvProductName;
    }

    public int getIvProductImg()
    {
        return ivProductImg;
    }

    public void setIvProductImg(int ivProductImg)
    {
        this.ivProductImg = ivProductImg;
    }


    String tvProductName;
    public boolean isHeardeView;
    public boolean isFooterView;
    int ivProductImg;

    public int getIvCross() {
        return ivCross;
    }

    public void setIvCross(int ivCross) {
        this.ivCross = ivCross;
    }

    int ivCross;
}
