package com.srinivas.restaurantowner.model;

import java.io.Serializable;

/**
 * Created by shashank.kapsime on 3/11/15.
 */
public class ProductListModel implements Serializable {

    public String upc;
    public String brand;
    public String product_name;
    public String image;
}
