package com.srinivas.restaurantowner.model;

/**
 * Created by vinay.tripathi on 10/10/15.
 */
public class Scanhistriyusermodel {
    public String time;
    public String date;
    public String nameOfProduct;
    public String scanResult;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public String getScanResult() {
        return scanResult;
    }

    public void setScanResult(String scanResult) {
        this.scanResult = scanResult;
    }



}
