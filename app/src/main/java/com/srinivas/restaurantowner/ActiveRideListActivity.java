/*
package com.srinivas.barcodescanner;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apex.free.adapter.RideListSectionAdapter;
import com.apex.free.delegates.OnResponseHandler;
import com.apex.free.model.FavouriteList;
import com.apex.free.model.LoadMoreListView;
import com.apex.free.model.NotificationCount;
import com.apex.free.model.LoadMoreListView.OnLoadMoreListener;
import com.apex.free.model.RideParser;
import com.apex.free.utils.CommonUtils;
import com.apex.free.utils.Constants;
import com.apex.free.webapi.ResponseHandler;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;

import de.greenrobot.event.EventBus;

public class ActiveRideListActivity extends Activity implements OnResponseHandler{
	private LoadMoreListView lvActiveRide;
	private RideListSectionAdapter adapter;
	private ArrayList<FavouriteList> listValues;
	private RideParser myRideParser;
	private OnResponseHandler onResponseHandler;
	private int pageNumber=0, maxPageNumber=20;
	private ImageView imgBack, imgLogo, imgFlag, imgEdit;
	private TextView tvCount;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_active_ride_list);
		onResponseHandler = (OnResponseHandler) this;
		listValues  = new ArrayList<FavouriteList>();
		getIds();
		imgBack.setOnClickListener(listener);
		imgLogo.setOnClickListener(listener);
		imgEdit.setOnClickListener(listener);
		imgFlag.setOnClickListener(listener);
		try
		{
			EventBus.getDefault().register(this);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void onEventMainThread(NotificationCount event)
	{ 
		if(event != null){
			 CommonUtils.savePreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT, event.totalCount);
			tvCount.setVisibility(View.VISIBLE);
			tvCount.setText(event.totalCount);
		}

	}

	
	@Override
	protected void onStart() {
		super.onStart();
		CommonUtils.setScreenTrack("Active Ride List Screen", ActiveRideListActivity.this);	
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);	
	}


	private OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			int id = v.getId();

			switch (id) {
			case R.id.img_back:
				finish();
				break;

			case R.id.img_logo:
				finish();
				break;

			case R.id.img_flag:
				tvCount.setVisibility(View.GONE);
				CommonUtils.savePreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT, "");
				Intent intent = new Intent(ActiveRideListActivity.this,	NotificationActivity.class);
				startActivity(intent);
				break;

			case R.id.img_edit:

				CommonUtils.showHeaderDialog(ActiveRideListActivity.this);

				break;

			default:
				break;
			}

		}
	};




	private void getIds() {
		lvActiveRide = (LoadMoreListView) findViewById(R.id.list_rides);
		imgBack = (ImageView) findViewById(R.id.img_back);
		imgLogo = (ImageView) findViewById(R.id.img_logo);
		imgFlag = (ImageView) findViewById(R.id.img_flag);
		imgEdit = (ImageView) findViewById(R.id.img_edit);


		tvCount = (TextView)findViewById(R.id.tvCount);
		if(CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT)!= null && CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT).length() > 0){
			tvCount.setVisibility(View.VISIBLE);
			tvCount.setText(CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT));
		}else{
			tvCount.setVisibility(View.GONE);
		}


		adapter = new RideListSectionAdapter(ActiveRideListActivity.this, listValues,1);
		lvActiveRide.setAdapter(adapter);

		getServiceInfo();

		lvActiveRide.setOnItemClickListener(new OnItemClickListener() 
		{

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent in = new Intent(ActiveRideListActivity.this, ActiveRideActivity.class);
				in.putExtra("key",listValues.get(arg2).getRideID());
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left,	R.anim.slide_out_left);

			}
		});


		lvActiveRide.setOnLoadMoreListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {

				if(maxPageNumber < pageNumber){

					lvActiveRide.onLoadMoreComplete();

				}else{
					if(CommonUtils.isOnline(ActiveRideListActivity.this)){

						getServiceInfo();

					}else{

						Toast.makeText(ActiveRideListActivity.this,getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
					}
				}

			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
		if(CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT)!= null && CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT).length() > 0){
			tvCount.setVisibility(View.VISIBLE);
			tvCount.setText(CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.NOTIFICATION_COUNT));
		}else{
			tvCount.setVisibility(View.GONE);
		}
	}

	private void getServiceInfo() {

		if(CommonUtils.isOnline(ActiveRideListActivity.this)){
			AsyncHttpClient client = new AsyncHttpClient();
			client.addHeader(Constants.CONTENT_TYPE, Constants.CONTENT_TYPE_APPLICATION_JSON);
			client.addHeader(Constants.ACCEPT,Constants.CONTENT_TYPE_APPLICATION_JSON);

			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("userID", CommonUtils.getPreferences(ActiveRideListActivity.this, Constants.PREF_UID));
				jsonObject.put("pageNumber", "0");
				jsonObject.put("pageSize", "20");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			StringEntity entity = null;
			try {
				entity = new StringEntity(jsonObject.toString(),"utf-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			System.out.println("request : " + jsonObject.toString());

			client.post(ActiveRideListActivity.this, Constants.ACTIVERIDELIST, entity,
					Constants.CONTENT_TYPE_APPLICATION_JSON, new ResponseHandler(ActiveRideListActivity.this,
							Constants.ACTIVERIDELIST, onResponseHandler));


		}

	}


	@Override
	public void onSuccess(int statusCode, String apiName, JSONObject response) {
		lvActiveRide.onLoadMoreComplete();
		myRideParser = new Gson().fromJson(response.toString(), RideParser.class);
		if(!CommonUtils.CheckIfNull(myRideParser.activeRidesList)){

			pageNumber= Integer.parseInt(myRideParser.pagination.pageNumber);
			maxPageNumber=Integer.parseInt(myRideParser.pagination.maxPageNumber);




			for (int i = 0; i < myRideParser.activeRidesList.size(); i++) {

				myRideParser.activeRidesList.get(i).setListCategory("");

			}

			listValues.addAll(myRideParser.activeRidesList);
			adapter.onRefeshData(listValues);
			pageNumber=pageNumber+1;


		}

	}


	@Override
	public void onFailure(int statusCode, String apiName, String responseMessage) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onFailure(int statusCode, String apiName,
			JSONArray errorResponse) {
		// TODO Auto-generated method stub

	}


	@Override
	public void onFailure(int statusCode, String apiName,
			JSONObject errorResponse) {
		// TODO Auto-generated method stub

	}

}
*/
