package com.srinivas.restaurantowner;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Created by shashank.kapsime on 26/10/15.
 */
public class BarcodeOwnerApp extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(getApplicationContext());
    }
}
