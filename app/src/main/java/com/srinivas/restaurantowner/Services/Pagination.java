package com.srinivas.restaurantowner.Services;

import java.io.Serializable;

/**
 * Created by vinay.tripathi on 28/10/15.
 */
public class Pagination implements Serializable {
   public String page_no;
   public String per_page;
   public String max_page_size;
   public String total_no_records;
}



