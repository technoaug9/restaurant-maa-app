package com.srinivas.restaurantowner.Services;

import java.io.Serializable;

/**
 * Created by vinay.tripathi on 5/10/15.
 */
public class IngredientList implements Serializable
{
    public String id;
    public String ingredient_name;
    public String product_name;
    public String user_id;
    public String member_id;
    public String created_at;
    public String updated_at;
    public boolean isCrossVisible;

}
