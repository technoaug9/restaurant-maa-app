package com.srinivas.restaurantowner.Services;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vinay.tripathi on 23/10/15.
 */
public class ScanHistoryList implements Serializable {

    public String product_name;
    public String result;
    public String user_id;
    public String created_at;
    public List<String> unsafe_users;
}


/*
"product_name": "Potato Chips Cheesy Garlic Bread",
        "result": "safe",
        "unsafe_users": [],
        "user_id": 136,
        "created_at": "2015-10-23T10:24:33.187Z"*/
