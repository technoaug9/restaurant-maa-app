package com.srinivas.restaurantowner.Services;

import com.srinivas.restaurantowner.ServiceModel.AddMember;
import com.srinivas.restaurantowner.ServiceModel.PreventiveList;
import com.srinivas.restaurantowner.ServiceModel.SearchHistory;
import com.srinivas.restaurantowner.model.MemberResult;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.model.ProductListModel;
import com.srinivas.restaurantowner.model.User;
import com.srinivas.restaurantowner.model.UserResult;

import java.util.List;


/**
 * Created by vinay.tripathi on 25/9/15.
 */
public class ServiceResponse  {
    public String response_code;
    public String response_message;
    public User user;
    public String userID;
    public String user_id;
    public String isVerified;
    public List<IngredientList> id;
    public List<PreventiveList> preventive_list;
    public List<IngredientList> ingredient_list;
    public List<SearchHistory> scan_histroy;
    public AddMember member;
    public String description;


    public String is_exist;
    public String product_name;
    public String image;
    public List<String> ingredients;
    public List<MemberResult> members_result;
    public UserResult user_result;
  //  public Product product;

   public List<Product> product;
   public List<ProductListModel> products;



    public List<ChefList> chefs;
    public Pagination pagination;
    public ChefDetail chef;


}
