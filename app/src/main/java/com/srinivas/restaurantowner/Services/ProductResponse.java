package com.srinivas.restaurantowner.Services;

import com.srinivas.restaurantowner.model.Product;

import java.io.Serializable;

/**
 * Created by shashank.kapsime on 29/10/15.
 */
public class ProductResponse implements Serializable {

    public String response_code;
    public String response_message;
    public Product product;
}
