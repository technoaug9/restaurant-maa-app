package com.srinivas.restaurantowner.Services;

/**
 * Created by vinay.tripathi on 25/9/15.
 */
public class RequestURL
{
    //Local
    // public static final String BASE_URL = "http://172.16.1.95:3000/";

    //Staging
    //public static final String BASE_URL = "https://barcode-scan.herokuapp.com/";

    //Production
    public static final String BASE_URL = "http://youngcart.in/";
  // public static final String BASE_URL = "http://ec2-52-32-147-137.us-west-2.compute.amazonaws.com/";

    public static final String NEW_SIGN_UP = BASE_URL + "index.php/owner/ownersave";

    public static final String SOCIAL_NEW_SIGN_UP = BASE_URL + "social_login/checksignup";
    //public static final String CHECK_USER_AUTH = BASE_URL + "users/check_user_auth.json";
    public static final String CHECK_USER_AUTH = BASE_URL + "social_login/checklogin";

    public static final String NEW_LOGIN = BASE_URL + "ownerlogin/checklogin";
    //public static final String NEW_LOGIN = BASE_URL + "log_in.json";
    public static final String Log_Out = BASE_URL + "ownerlogin/logout";
    public static final String Forgot_Password = BASE_URL + "forgot_password";
    public static final String Access_Code = BASE_URL + "access_code";
    public static final String Update_Password = BASE_URL + "update_password";
    public static final String Add_Member = BASE_URL + "add_member";
    public static final String Select_Member = BASE_URL + "select_member";
    public static final String Edit_Member = BASE_URL + "edit_member";
    public static final String Remove_Member = BASE_URL + "remove_member";
    public static final String Manage_Profile = BASE_URL + "manage_profile";
    public static final String Edit_Profile = BASE_URL + "owner/updateownerprofile";

    //public static final String Edit_Profile = BASE_URL + "edit_profile";
    public static final String About_Us = BASE_URL + "about_us";
    public static final String Add_Preventive = BASE_URL + "add_preventive";
    public static final String Remove_Preventive = BASE_URL + "remove_preventive";
    public static final String DELETE_PRODUCT = BASE_URL +"products/"+ "delete_product";

    public static final String Search_Ingredients = BASE_URL + "ingredients/search";
    //public static final String Search_Ingredients = BASE_URL + "search_ingredients";

    public static final String Ingredients_list = BASE_URL + "ingredients_list";
    public static final String Scan_Upc = BASE_URL + "scan_upc";
    public static final String Search_Product_By_Name = BASE_URL + "search_product_by_name";
    public static final String Scan_History = BASE_URL + "scan_history";
    public static final String Recent_Searched_Product = BASE_URL + "recent_searched_product";
    public static final String Term_Condition=BASE_URL + "pages/terms";
    //http://youngcart.in/pages/terms
    public static final String About_us=BASE_URL + "pages/howitworks";
    public static final String Subscription=BASE_URL + "pages/subscription";
//http://youngcart.in/pages/subscription
    public static final String Contact_Us=BASE_URL + "pages/contact";
   // public static final String Contact_Us=BASE_URL + "contact_us";
    public static final String My_Profile=BASE_URL + "owner/ownerprofile";
    //public static final String PRODUCTS=BASE_URL + "products";

    public static final String PRODUCTS=BASE_URL + "owner/createproduct";

    public static final String Create_Chef=BASE_URL + "owner/createchef";

    //public static final String All_Chefs=BASE_URL + "owner/chefslist";
   // public static final String Create_Chef=BASE_URL + "chefs";
    //conection timeout
    public static int CONNECTION_TIME_OUT = 20000;
    //header content type
    public static String CONTENT_TYPE = "Content-Type";
    //header
    public static String APPLICATION_JSON = "application/json";
    //sucess code
    public static String SUCCESS_CODE = "200";


}
