package com.srinivas.restaurantowner.Services;

import org.json.JSONObject;

/**
 * This interface is for handling the web service response
 */
public interface AsyncResponse {

    void onSuccess(JSONObject response, String apiName);

    void onEmptyResponse(JSONObject response, String apiName);

    void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject errorResponse);

    void onFinish();
}
