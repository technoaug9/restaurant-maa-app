package com.srinivas.restaurantowner.Services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.utils.Log;

import org.json.JSONObject;

public class CommonResponseHandler extends JsonHttpResponseHandler {

    private final String TAG = getClass().getSimpleName();
    private int responseCode;
    private ProgressDialog pdialog;
    private Context context;
    private String apiName;
    private AsyncResponse asyncResponse;
    private boolean isProgressBarVisible = true;

    public CommonResponseHandler(Context mContext, AsyncResponse asyncResponse, String apiName) {
        super();
        this.context = mContext;
        this.asyncResponse = asyncResponse;
        this.apiName = apiName;

    }

    public CommonResponseHandler(Context mContext, AsyncResponse asyncResponse, String apiName, boolean isProgressBarVisible) {
        super();
        this.context = mContext;
        this.asyncResponse = asyncResponse;
        this.apiName = apiName;
        this.isProgressBarVisible = isProgressBarVisible;
        Log.e(TAG, "Request URL isProgressBarVisiblel**********************" + isProgressBarVisible);

    }

    public void isProgressBarVisisble(boolean isProgressBarVisible) {

        this.isProgressBarVisible = isProgressBarVisible;
    }

    @Override
    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, JSONObject response) {
        super.onSuccess(statusCode, headers, response);
        Log.e(TAG, "Request URL" + apiName);
        Log.e(TAG, "Response" + response);

        try {
            if (response.has("response_code")) {
                responseCode = response.getInt("response_code");
                Log.e(TAG, "responseCode" + responseCode);
            }

            if (response.has("response_message")) {

                Log.e(TAG, "response_message" + response.getString("response_message"));

            }

            if (responseCode == 200) {
                asyncResponse.onSuccess(response, response.getString("response_message"));
            }else {
                asyncResponse.onSuccess(response, response.getString("response_message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }




     /*   responseCode = response.optInt("200");
        response.optString(new ServiceResponse().response_message);
        if (responseCode == 200) {
            if (asyncResponse != null) {
                asyncResponse.onSuccess(response, apiName);
            }

        } else if (responseCode == 201) {

        } else {
            if (asyncResponse != null && responseCode == 0) {
                asyncResponse.onEmptyResponse(response, apiName);
            }
        }*/
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isProgressBarVisible) {
            pdialog = new ProgressDialog(context);
            pdialog.setMessage(context.getString(R.string.please_wait));
            pdialog.setCancelable(true);
            if (pdialog != null && !((Activity) context).isFinishing()) {
                pdialog.show();
            }

        }
    }

    @Override
    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject errorResponse) {
        super.onFailure(statusCode, headers, throwable, errorResponse);
        if (!isProgressBarVisible) {
            if (pdialog != null && !((Activity) context).isFinishing()) {
                pdialog.dismiss();
            }
        }
        Log.e(TAG, "On error being calleeddddddddddddd**********************" + apiName);
        if (asyncResponse != null) {
            asyncResponse.onFailure(statusCode, headers, throwable, errorResponse);
            throwable.printStackTrace();
        }

        Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onFinish() {
        super.onFinish();
        if (pdialog != null && !((Activity) context).isFinishing()) {
            pdialog.dismiss();
        }

    }


}
