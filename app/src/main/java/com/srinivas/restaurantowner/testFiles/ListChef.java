
package com.srinivas.restaurantowner.testFiles;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ListChef {

    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("chefs")
    @Expose
    private List<Chef> chefs = new ArrayList<Chef>();
    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    /**
     * 
     * @return
     *     The responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * 
     * @param responseCode
     *     The response_code
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * 
     * @return
     *     The responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * 
     * @param responseMessage
     *     The response_message
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * 
     * @return
     *     The chefs
     */
    public List<Chef> getChefs() {
        return chefs;
    }

    /**
     * 
     * @param chefs
     *     The chefs
     */
    public void setChefs(List<Chef> chefs) {
        this.chefs = chefs;
    }

    /**
     * 
     * @return
     *     The pagination
     */
    public Pagination getPagination() {
        return pagination;
    }

    /**
     * 
     * @param pagination
     *     The pagination
     */
    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
