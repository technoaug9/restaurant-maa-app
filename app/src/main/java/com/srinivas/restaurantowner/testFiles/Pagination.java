
package com.srinivas.restaurantowner.testFiles;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pagination {

    @SerializedName("max_page_size")
    @Expose
    private String maxPageSize;
    @SerializedName("page_no")
    @Expose
    private String pageNo;
    @SerializedName("per_page")
    @Expose
    private String perPage;
    @SerializedName("total_no_records")
    @Expose
    private Integer totalNoRecords;

    /**
     * 
     * @return
     *     The maxPageSize
     */
    public String getMaxPageSize() {
        return maxPageSize;
    }

    /**
     * 
     * @param maxPageSize
     *     The max_page_size
     */
    public void setMaxPageSize(String maxPageSize) {
        this.maxPageSize = maxPageSize;
    }

    /**
     * 
     * @return
     *     The pageNo
     */
    public String getPageNo() {
        return pageNo;
    }

    /**
     * 
     * @param pageNo
     *     The page_no
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * 
     * @return
     *     The perPage
     */
    public String getPerPage() {
        return perPage;
    }

    /**
     * 
     * @param perPage
     *     The per_page
     */
    public void setPerPage(String perPage) {
        this.perPage = perPage;
    }

    /**
     * 
     * @return
     *     The totalNoRecords
     */
    public Integer getTotalNoRecords() {
        return totalNoRecords;
    }

    /**
     * 
     * @param totalNoRecords
     *     The total_no_records
     */
    public void setTotalNoRecords(Integer totalNoRecords) {
        this.totalNoRecords = totalNoRecords;
    }

}
