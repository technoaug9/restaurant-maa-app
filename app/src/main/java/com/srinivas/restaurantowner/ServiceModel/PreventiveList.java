package com.srinivas.restaurantowner.ServiceModel;

import java.io.Serializable;

/**
 * Created by surya on 30/9/15.
 */
public class PreventiveList implements Serializable {
    public String id;
    public String name;
    public boolean isCrossVisible;
}
