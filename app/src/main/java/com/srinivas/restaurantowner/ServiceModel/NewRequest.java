package com.srinivas.restaurantowner.ServiceModel;

import com.srinivas.restaurantowner.model.User;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * Created by shashank.kapsime on 2/11/15.
 */
public class NewRequest implements Serializable {

    public String signup_type;
    public String email;
    public String name;
    public String password;
    public String password_confirmation;
    public String user_type;
    public String signup_try;
    public String provider_id;
    public String provider_name;
    public String access_code;
    public File image;
    public String dob;
    public String user_id;
    public String subject;
    public String description;
    public String DOB;
    public User user;
    public String member_id;
    public String upc;
    public String ingredient;
    public String id;
    public String preventive_id;
    public String restaurant_name;
    public String history_type;
    public String phone_no;
    public String qr_code;
    public String nfc_code;
    public List<String> product_ingredients_attributes;

}
