package com.srinivas.restaurantowner.ServiceModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shashank.kapsime on 24/10/15.
 */
public class SearchHistory implements Serializable {

    public String created_at;
    public String result;
    public List<String> unsafe_users;
    public String user_id;
    public String strListOfUsers;
    public String upc_code;
    public String code_type;

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String product_name;
    public String image;

    public String getUpc_code() {
        return upc_code;
    }

    public void setUpc_code(String upc_code) {
        this.upc_code = upc_code;
    }

    public String getCode_type() {
        return code_type;
    }

    public void setCode_type(String code_type) {
        this.code_type = code_type;
    }
}
