package com.srinivas.restaurantowner.activity.owner;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.imageutils.CropImage;
import com.srinivas.restaurantowner.imageutils.TakePictureUtils;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by vinay.tripathi on 9/10/15.
 */
public class ChefProfileActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader, tvUpload, tvDelete;
    private ImageView ivProfileImage, ivImageLeft, ivImageRight;
    private EditText etChefName, etEmailId, etContactNumber, etUserID, etPassword;
    private String imageRealPath;
    private Boolean isEditable = false;

    private String MobilePattern = "[0-9]{10}";
    private View.OnClickListener yesButtonListener;
    private Dialog mDialog;
    private String ChefId = "";
    private Context context = ChefProfileActivity.this;
    private String TAG = ChefProfileActivity.class.getSimpleName();
    private File file;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_profile);
        getId();
        setListener();
        makeNonEditable();
        etChefName.setFocusable(true);
        etEmailId.setFocusable(true);
        etContactNumber.setFocusable(true);
        etUserID.setFocusable(true);
        etPassword.setFocusable(true);

        CommonUtils.hideSoftKeyboard(this);

        if (getIntent() != null && getIntent().hasExtra("chefId") && getIntent().getStringExtra("chefId").length() > 0) {
            ChefId = getIntent().getStringExtra("chefId");
            Log.e(":::::::Chef Profile::::", "" + ChefId);
        }

        if (CommonUtils.isOnline(context)) {

            if (ChefId.length() > 0 ) {

                getChefDetail();

            }

        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }

        ivImageRight.setImageResource(R.drawable.edit_icon);
        context = ChefProfileActivity.this;
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        tvUpload.setOnClickListener(this);
        tvDelete.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvUpload = (TextView) findViewById(R.id.tvUpload);
        tvDelete = (TextView) findViewById(R.id.tvDelete);
        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);
        etChefName = (EditText) findViewById(R.id.etChefName);
        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etContactNumber = (EditText) findViewById(R.id.etContactNumber);
        etUserID = (EditText) findViewById(R.id.etUserID);
        etPassword = (EditText) findViewById(R.id.etPassword);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                if (isEditable) {
                    makeNonEditable();
                } else if (!isEditable) {
                    finish();
                }
                break;
            case R.id.ivImageRight:

                if (!isEditable) {
                    makeEditable();

                } else if (isEditable) {
                   /* finishAffinity();
                    if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                        startActivity(new Intent(this, ChefHomeActivity.class));
                    } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                        startActivity(new Intent(this, HomeActivity.class));
                    } else {
                        startActivity(new Intent(this, HomeActivity.class));
                    }
*/
                    Intent intent = new Intent(this, HomeActivity.class);
                    startActivity(intent);
                    break;

                }
                break;
            case R.id.tvUpload:
                if (isAllPermissionGranted()) {
                    Log.e("Permission granted: ", "Button Click performed Action");
                    CommonUtils.showcameradialogs(ChefProfileActivity.this);
                } else {
                    if (ContextCompat.checkSelfPermission(ChefProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChefProfileActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    } else if (ContextCompat.checkSelfPermission(ChefProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChefProfileActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }
                break;
            case R.id.tvDelete:

                if (tvDelete.getText().toString().equalsIgnoreCase("Delete")) {
                    showCustomDialog(context);
                } else if (tvDelete.getText().toString().equalsIgnoreCase("Save")) {
                    if (checkValidation()) {
                        makeNonEditable();

                        if (CommonUtils.isOnline(context)) {
                            if (file == null) {
                                //when not upload image  then request image in string
                                ServiceRequest request = new ServiceRequest();
                                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                                // request.password = etPassword.getText().toString().trim();
                                request.name = etChefName.getText().toString().trim();
                                request.phone_no = etContactNumber.getText().toString().trim();
                                request.image = "";

                                chefProfileUpdate(request);
                                //chefProfileUpdate();
                            } else {
                                // when   upload image then iamge in file(use multipart)
                                chefProfileUpdate();
                            }

                        } else {
                            CommonUtils.showAlert("Please check your internet connection.", context);
                        }

                    }
                }

                break;
        }

    }

    /**
     * Code for Marshmallow permission
     *
     * @return
     */

    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(ChefProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(ChefProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChefProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(ChefProfileActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(ChefProfileActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChefProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(ChefProfileActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(ChefProfileActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Chef Profile>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void getChefDetail() {

        String id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String chefid = ChefId;
        //  String url = "http://172.16.1.95:5000//chefs/" + chefid + "?user_id=" + id;
        //String url = RequestURL.BASE_URL + "/chefs/" + chefid + "?user_id=" + id;
        String url = RequestURL.BASE_URL + "owner/chefprofile?chef_id=" + chefid + "&user_id=" + id;

        Log.e(TAG, "url>>>>>" + url);
        Ion.with(context)
                .load(url)

                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        etChefName.setText(response.chef.name != null && !response.chef.name.equalsIgnoreCase("") ? response.chef.name : "");
                        tvHeader.setText(response.chef.name != null && response.chef.name.length() > 0 ? response.chef.name : "");
                        etContactNumber.setText(response.chef.phone_no != null && response.chef.phone_no.length() > 0 ? response.chef.phone_no : "");
                        tvHeader.setText(response.chef.name != null && response.chef.name.length() > 0 ? response.chef.name : "");
                        etEmailId.setText(response.chef.email != null && !response.chef.email.equalsIgnoreCase("") ? response.chef.email : "");
                        // null not check for null image
                        Picasso.with(context).load(response.chef.image)
                                .placeholder(R.drawable.profile_icon).into(ivProfileImage);

                    } else {
                        CommonUtils.showToast(ChefProfileActivity.this, response.response_message);


                    }
                } else {
                    Toast.makeText(ChefProfileActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Chef Profile Update multipart>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.*/
    private void chefProfileUpdate() {
        String encodedImage="";
        String chefid = ChefId;
        // String url = RequestURL.BASE_URL + "chefs/" + chefid + "/update";
        String url = RequestURL.BASE_URL + "owner/updatechef?chef_id=" + chefid;
        Log.e(TAG, "url>>>>>" + url);
        if(file!=null) {
            Bitmap bm = BitmapFactory.decodeFile(file.getPath());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
           encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        }


        Log.e(TAG, "encodedImage" + encodedImage);
        Ion.with(context)
                .load(url)
                .setMultipartParameter("chef_id", chefid)
                .setMultipartParameter("name", etChefName.getText().toString().trim())
                .setMultipartParameter("phone_no", etContactNumber.getText().toString().trim())
                .setMultipartParameter("image", encodedImage)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));

                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        CommonUtils.showToast(ChefProfileActivity.this, response.response_message);
                        finish();
                    } else {
                        CommonUtils.showToast(ChefProfileActivity.this, response.response_message);
                        finish();
                    }
                } else {
                    Toast.makeText(ChefProfileActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Chef Profile Update>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.*/
    private void chefProfileUpdate(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        //   String url = RequestURL.Edit_Member;
        String chefid = ChefId;
        //String url = RequestURL.BASE_URL + "chefs/" + chefid + "/update";
        String url = RequestURL.BASE_URL + "owner/updatechef";
        Log.e(TAG, "url>>>>>" + url);
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        requestJson.addProperty("chef_id",chefid);
        Log.e(TAG, "req data update " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        Toast.makeText(context, "Profile Updated", Toast.LENGTH_SHORT).show();
                        makeNonEditable();
                        finish();
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                            finish();
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });

    }


    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void deleteChefApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        //String url = RequestURL.Delete_Chef;
        //String url = RequestURL.BASE_URL + "/chefs/delete_chef";

        String url = RequestURL.BASE_URL + "owner/deletechef";
        Log.e(TAG, "url>>>>>" + url);

        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e(TAG, " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        CommonUtils.showToast(context, "Chef deleted successfully");
                        finish();

                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }

    private boolean checkValidation() {
        if (etChefName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter chef name.", null, View.GONE);
            etChefName.requestFocus();
            return false;
        } else if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please check your email to verify.", null, View.GONE);
            etEmailId.requestFocus();
            return false;
        } else if (!checkEmailId(etEmailId.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            etEmailId.requestFocus();
            //etEmailId.setText("");
            return false;
        } else if (checkEmailId(etContactNumber.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter contact number.", null, View.GONE);
            etEmailId.requestFocus();
            //etEmailId.setText("");
            return false;
        } else if (!etContactNumber.getText().toString().matches(MobilePattern)) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid 10 digit phone number", null, View.GONE);
            return false;
        }
        return true;
    }

    private boolean checkEmailId(String emailId) {
        return Patterns.EMAIL_ADDRESS.matcher(emailId).matches();
    }

    private void makeEditable() {
        etChefName.setEnabled(true);
        etEmailId.setEnabled(false);
        etContactNumber.setEnabled(true);

        etUserID.setEnabled(true);

        etPassword.setEnabled(false);

        isEditable = true;

        tvUpload.setVisibility(View.VISIBLE);

        ivImageRight.setImageResource(R.drawable.home_btn);

        tvDelete.setText("Save");
        tvDelete.setBackgroundResource(R.drawable.login_btn);


    }


    private void makeNonEditable() {
        etChefName.setEnabled(false);
        etEmailId.setEnabled(false);
        etContactNumber.setEnabled(false);
        etUserID.setEnabled(false);
        etPassword.setEnabled(false);
        isEditable = false;
        ivImageRight.setImageResource(R.drawable.edit_icon);
        tvUpload.setVisibility(View.INVISIBLE);
        tvDelete.setText("Delete");
        tvDelete.setBackgroundResource(R.drawable.sign_up);
    }

    @Override
    public void onBackPressed() {
        if (isEditable) {
            makeNonEditable();
        } else if (!isEditable) {
            finish();
        }
    }

    private void showCustomDialog(final Context mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);
        tvCode.setVisibility(View.GONE);
        tvMsg.setText("Are you sure you want to delete\nthis chef ");

        tvTestchip.setText("Yes");
        tvOk.setText("No");


        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();

            }
        });

        tvTestchip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.cancel();
                if (CommonUtils.isOnline(mContext)) {
                    String chefid = ChefId;
                    ServiceRequest request = new ServiceRequest();
                    request.user_id = CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
                    request.id = chefid;
                    deleteChefApi(request);
                } else {
                    CommonUtils.showAlert("Please check your internet connection.", mContext);
                }
            }
        });

        mDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case TakePictureUtils.PICK_GALLERY:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), CommonUtils.imageNameLocal + ".png"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        TakePictureUtils.startCropImage(ChefProfileActivity.this, CommonUtils.imageNameLocal + ".png");

                    } catch (Exception e) {

                        Toast.makeText(ChefProfileActivity.this, "Error in picture", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage(ChefProfileActivity.this, CommonUtils.imageNameLocal + ".png");
                    break;

                case TakePictureUtils.CROP_FROM_CAMERA:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);

                    if (path == null) {
                        return;
                    }
                    imageRealPath = path;
                    Log.e("Image Real Path", imageRealPath);

                    file = new File(imageRealPath);
                    //Picasso.with(MemberProfileActivity.this).load(new File(path)).into(ivProfileImage);
                    ivProfileImage.setImageBitmap(BitmapFactory.decodeFile(imageRealPath));
                    break;
            }
        }
    }
}
