package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.owner.ManageMenuProductActivity;
import com.srinivas.restaurantowner.adapter.enduser.AddIngredientOwnerAdapter;
import com.srinivas.restaurantowner.adapter.enduser.SearchAdapter;
import com.srinivas.restaurantowner.model.ProductModel;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by priya.singh on 23/9/15.
 */
public class AddIngredientOwnerSearchActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader, tvAddIngredients;
    private ImageView ivImageLeft, ivImageRight;
    private EditText etSearch;
    private ListView lvOwner;
    private RecyclerView rvOwner;
    private ArrayAdapter<String> dataAdapter;
    public List<String> arrSeracheLast;
    public List<String> arrSeracheFirst;
    private TextView tvNFC, tvQR;
    private Intent intent;
    private LinearLayoutManager linearLayoutManager;
    private Context context = AddIngredientOwnerSearchActivity.this;
    private RecyclerView.Adapter productAdapter;
    public List<ProductModel> data;
    private List<IngredientList> ingredientLists;
    private String TAG = AddIngredientOwnerSearchActivity.class.getSimpleName();
    private SearchAdapter searchAdapter;
    private String productID = "";
    private boolean isMatchFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient_owner_search);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("Add Ingredients");
        ivImageRight.setImageResource(R.drawable.home_btn);


        if (getIntent() != null) {
            if (getIntent().hasExtra("productID")) {
                productID = getIntent().getStringExtra("productID");
                Log.e(TAG, ">>>>>productID>>>>activity" + productID);
            }
        }

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                CommonUtils.hide_keyboard(AddIngredientOwnerSearchActivity.this);
                lvOwner.setVisibility(View.VISIBLE);
                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    request.ingredient = etSearch.getText().toString();
                    searchIngredientsList(request);
                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }

                return true;
            }
        });


    }


    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        tvAddIngredients.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvNFC.setOnClickListener(this);
        tvQR.setOnClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        etSearch = (EditText) findViewById(R.id.etSearch);
        lvOwner = (ListView) findViewById(R.id.lvOwner);
        rvOwner = (RecyclerView) findViewById(R.id.rvOwner);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvOwner.setLayoutManager(linearLayoutManager);
        tvAddIngredients = (TextView) findViewById(R.id.tvAddIngredients);
        tvNFC = (TextView) findViewById(R.id.tvNFC);
        tvQR = (TextView) findViewById(R.id.tvQR);
        ingredientLists = new ArrayList<IngredientList>();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.tvAddIngredients:

                if (ingredientLists != null && ingredientLists.size() > 0) {
                    IngredientList ingredientListModel1 = null;
                    for (int i = 0; i < ingredientLists.size(); i++) {
                        ingredientListModel1 = new IngredientList();
                        ingredientListModel1.id = ingredientLists.get(i).id;
                        ingredientListModel1.ingredient_name = ingredientLists.get(i).ingredient_name;
                        ManageMenuProductActivity.ingredientListsManageMenu.add(ingredientListModel1);
                    }

                    if (ingredientListModel1 != null) {
                        ManageMenuProductActivity.notifyRecyclerView();
                    }

                    finish();
                } else {
                    CommonUtils.hide_keyboard(AddIngredientOwnerSearchActivity.this);
                    lvOwner.setVisibility(View.VISIBLE);
                    if (etSearch.getText().toString().trim().length() > 0) {
                        if (CommonUtils.isOnline(context)) {
                            ServiceRequest request = new ServiceRequest();
                            request.ingredient = etSearch.getText().toString();
                            searchIngredientsList(request);
                        } else {
                            CommonUtils.showToast(context, getString(R.string.please_check_internet));
                        }
                    } else {
                        CommonUtils.showToast(context, "Please enter text to search");
                    }

                }

                break;
            case R.id.ivImageRight:
                intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
        }
    }


    private void searchIngredientsList(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait..");
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        android.util.Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        String url = RequestURL.Search_Ingredients;
        Ion.with(context)
                .load(url)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {
                        android.util.Log.i("Tag", " response data " + new Gson().toJson(response));
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                if (response.ingredient_list != null) {
                                    if (response.ingredient_list.size() > 0) {
                                        lvOwner.setVisibility(View.VISIBLE);
                                        android.util.Log.e(TAG, ":::LIST SIZE:::" + ingredientLists.size());

                                        for (int i = 0; i < response.ingredient_list.size(); i++) {
                                            if (etSearch.getText().toString().trim().equalsIgnoreCase(response.ingredient_list.get(i).ingredient_name)) {
                                                searchAdapter = new SearchAdapter(context, response.ingredient_list, true, i);
                                                lvOwner.setAdapter(searchAdapter);
                                                lvOwner.setSelection(i);
                                                isMatchFound = true;
                                                break;
                                            }
                                        }

                                        if (!isMatchFound) {
                                            searchAdapter = new SearchAdapter(context, response.ingredient_list);
                                            lvOwner.setAdapter(searchAdapter);
                                        }

                                        isMatchFound = false;

                                        lvOwner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view,
                                                                    int position, long id) {
                                                lvOwner.setVisibility(View.GONE);

                                                IngredientList ingredientListModel = new IngredientList();
                                                ingredientListModel.ingredient_name = response.ingredient_list.get(position).ingredient_name;
                                                ingredientListModel.id = response.ingredient_list.get(position).id;
                                                ingredientLists.add(ingredientListModel);

                                                productAdapter = new AddIngredientOwnerAdapter(getApplication(), ingredientLists);
                                                rvOwner.setAdapter(productAdapter);
                                            }
                                        });
                                    } else {
                                        lvOwner.setVisibility(View.GONE);
                                        CommonUtils.showToast(context, "No result");
                                    }
                                }


                            } else {
                                CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                            }

                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }


}