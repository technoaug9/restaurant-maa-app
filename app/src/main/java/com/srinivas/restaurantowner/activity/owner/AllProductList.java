package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.adapter.owner.AllProductAdapter;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.model.ProductModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;


public class AllProductList extends Activity implements View.OnClickListener {
    private ExpandableListView expandview;
    private TextView tvHeader;
    private LinearLayout singlerow;
    private TextView tv_line;
    private ProductModel productModel;
    private ArrayList<Product> list;
    private AllProductAdapter adapter;
    private ImageView ivImageRight;
    private Intent intent;
    private Context context = AllProductList.this;
    private ImageView ivImageLeft;
    private String TAG = AllProductList.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_product_list);
        setIds();
        setListener();
        if (CommonUtils.isOnline(context)) {
            allProductApi();
        } else {
            CommonUtils.showAlert(getString(R.string.please_check_internet), context);
        }

    }

    private void setIds() {
        expandview = (ExpandableListView) findViewById(R.id.expnadview);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tv_line = (TextView) findViewById(R.id.tv_line);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        singlerow = (LinearLayout) findViewById(R.id.singlerow);
        tvHeader.setText("All Product List");


    }

    private void setListener() {
        tvHeader.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageRight:
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
        }
    }

    private void allProductApi() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...");
        String id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String url = RequestURL.BASE_URL + "owner/allproducts?user_id=" + id+"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);
        //http://youngcart.in/owner/allproducts?user_id=2
        Log.e(TAG, "url:>>>>>>>>>" + url);
        Ion.with(context)
                .load(url)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Log.i("Tag", " response data " + new Gson().toJson(response));
                Log.e("scanowner", "res = " + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.product != null && response.product.size() > 0) {

                            list = (ArrayList<Product>) response.product;
                            adapter = new AllProductAdapter(AllProductList.this, list);
                            expandview.setAdapter(adapter);
                        }
                    } else {
                        CommonUtils.showToast(AllProductList.this, response.response_message);
                    }
                } else {
                    Log.e(TAG, " error message  " + e.toString());
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }


}
