package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.enduser.ScanResultDetailsAdapter;
import com.srinivas.restaurantowner.model.ScanResultDetailsModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.List;

public class ScanResultDetailsActivity extends Activity implements View.OnClickListener {

    private RecyclerView rvScanDetails;
    private LinearLayoutManager linearLayoutManager;
    private Context context = ScanResultDetailsActivity.this;
    private ScanResultDetailsAdapter scanResultDetailsAdapter;
    public List<ScanResultDetailsModel> data;
    String[] ingredientsName = new String[]{"Cashew", "Cocca", "Coconut"};
    private ImageView ivImageRight, ivImageLeft, ivProfile;
    private TextView tvHeader, tvName, tvWarnMsg;
    private String memberID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_result_details);
        tvWarnMsg = (TextView) findViewById(R.id.tvWarnMsg);
        tvWarnMsg.setVisibility(View.VISIBLE);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);

        tvHeader.setText("Scan Result");

        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);

        if (getIntent() != null && getIntent().hasExtra("memberId")) {
            memberID = getIntent().getStringExtra("memberId");
        }

        if (memberID.length() > 0) {
            if (CommonUtils.isOnline(context)) {
                ServiceRequest request = new ServiceRequest();

                Log.e("TAG", "memberID>>>>>>" + memberID);

                request.member_id = memberID;
                memberProfileAPI(request);

            } else {
                CommonUtils.showAlert("Please check your internet connection.", context);
            }
        } else {

            if (CommonUtils.isOnline(context)) {
                ServiceRequest request = new ServiceRequest();
                Log.e("TAG", "userID>>>>>>");

                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                myProfileAPI(request);

            } else {
                CommonUtils.showAlert("Please check your internet connection.", context);
            }

        }


    }


    private void myProfileAPI(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.My_Profile;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e("TAG", "URL>>>>>>" + url);
        Log.e("TAG", "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>()

                {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {


                        if (response.user.image != null && !response.user.image.equalsIgnoreCase("")) {

                            Picasso.with(context).load(response.user.image).placeholder(R.drawable.img).into(ivProfile);
                        }


                        if (response.user.name != null && response.user.name.length() > 0) {

                            tvName.setText(response.user.name);
                        }

                        if (response.preventive_list != null) {

                            scanResultDetailsAdapter = new ScanResultDetailsAdapter(getApplication(), response.preventive_list);
                            rvScanDetails.setAdapter(scanResultDetailsAdapter);

                        }


                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }


    private void memberProfileAPI(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Select_Member;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("TAG", "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {


                        if (response.user.id != null && response.user.id.length() > 0) {


                            if (response.user.image != null && !response.user.image.equalsIgnoreCase("")) {

                                Picasso.with(context).load(response.user.image).placeholder(R.drawable.img).into(ivProfile);
                            }


                            if (response.user.name != null && response.user.name.length() > 0) {

                                tvName.setText(response.user.name);
                            }

                            if (response.preventive_list != null) {

                                scanResultDetailsAdapter = new ScanResultDetailsAdapter(getApplication(), response.preventive_list);
                                rvScanDetails.setAdapter(scanResultDetailsAdapter);

                            }


                        }


                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {

                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }


    private void getId() {

        rvScanDetails = (RecyclerView) findViewById(R.id.rvScanDetails);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvName = (TextView) findViewById(R.id.tvName);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvScanDetails.setLayoutManager(linearLayoutManager);
    }

    private void setListener() {

        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);

                finish();

                break;

            case R.id.ivImageRight:

                finishAffinity();

                startActivity(new Intent(this, HomeActivity.class));

                break;
        }
    }
}
