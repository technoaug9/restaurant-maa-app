package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class NFCScannerActivity extends Activity implements View.OnClickListener {
    private ImageView ivImageLeft, ivImageRight;
    private Intent intent;
    private TextView tvHeader;


    // list of NFC technologies detected:
    private final String[][] techList = new String[][]{
            new String[]{
                    NfcA.class.getName(),
                    NfcB.class.getName(),
                    NfcF.class.getName(),
                    NfcV.class.getName(),
                    IsoDep.class.getName(),
                    MifareClassic.class.getName(),
                    MifareUltralight.class.getName(), Ndef.class.getName()
            }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfcscanner);

        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("NFC Chip Scanner");

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            // creating pending intent:
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            // creating intent receiver for NFC events:
            IntentFilter filter = new IntentFilter();
            filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
            filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
            filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
            // enabling foreground dispatch for getting intent from NFC event:
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);

            if (nfcAdapter != null) {
                if (nfcAdapter.isEnabled()) {
                    nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{filter}, this.techList);
                } else {
                    CommonUtils.showToast(NFCScannerActivity.this, "Please enable nfc in settings");
                    finish();
                }
            } else {
                CommonUtils.showToast(NFCScannerActivity.this, "Sorry! your device doesn't support NFC");
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // disabling foreground dispatch:
        try {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            if (nfcAdapter != null) {
                if (nfcAdapter.isEnabled()) {
                    nfcAdapter.disableForegroundDispatch(this);
                } else {
                    CommonUtils.showToast(NFCScannerActivity.this, "Please enable nfc in settings");
                    finish();
                }
            } else {
                CommonUtils.showToast(NFCScannerActivity.this, "Sorry! your device doesn't support NFC");
                finish();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        try {
            if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
                String id = "" + ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));

                if (id != null && id.length() > 0) {
                    Log.e("", "NFC Value>>>>> " + id);
                    Intent intentNFC = new Intent(this, ScanResultsActivity.class);
                    intentNFC.putExtra("upc", id);
                    intentNFC.putExtra("codeType", "nfc");
                    startActivity(intentNFC);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private String ByteArrayToHexString(byte[] inarray) {
        int i, j, in;
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        String out = "";

        for (j = 0; j < inarray.length; ++j) {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    private void getId() {
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.ivImageRight:
                intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nfcscanner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
