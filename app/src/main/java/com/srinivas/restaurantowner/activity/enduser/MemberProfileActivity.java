package com.srinivas.restaurantowner.activity.enduser;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.imageutils.CropImage;
import com.srinivas.restaurantowner.imageutils.TakePictureUtils;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class MemberProfileActivity extends Activity implements View.OnClickListener {

    private TextView tvUpload, tvSetPreventive, tvHeader, tvEdit, tvAge;

    private EditText etName, etAge;

    private ImageView ivProfileImage, ivImageLeft, ivImageRight;

    private Boolean isEditable = false;
    private String imageName;
    private String imageString;
    private Dialog mDialog;
    private String imageRealPath;
    private Context context;
    private String TAG = MemberProfileActivity.class.getSimpleName();
    private String memberId = "";
    public File file;
    private String nameOfMember = "";
    private Long userSelectedTimeStamp = 0L;
    private boolean ageValidation = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_profile);
        mDialog = new Dialog(this,
                android.R.style.Theme_Translucent_NoTitleBar);
        context = MemberProfileActivity.this;
        getId();
        setListener();
        makeNonEditable();
        etName.setFocusable(true);
        CommonUtils.hideSoftKeyboard(this);
        if (getIntent() != null && getIntent().hasExtra("memberId") && getIntent().getStringExtra("memberId").length() > 0) {
            memberId = getIntent().getStringExtra("memberId");
            //Log.e(":::::::Member Profile::::", "" + memberId);
        }
        if (CommonUtils.isOnline(context)) {
            ServiceRequest request = new ServiceRequest();
            if (memberId.length() > 0) {
                request.member_id = memberId;
            }
            memberProfileAPI(request);
        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }


    }


    private void getId() {

        tvUpload = (TextView) findViewById(R.id.tvUpload);
        tvSetPreventive = (TextView) findViewById(R.id.tvSetPreventive);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvEdit = (TextView) findViewById(R.id.tvEdit);

        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);

        etName = (EditText) findViewById(R.id.etName);
        etAge = (EditText) findViewById(R.id.etAge);
        tvAge = (TextView) findViewById(R.id.tvAge);
        ivImageRight.setImageResource(R.drawable.delete_icon);

        tvHeader.setText("");

    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvSetPreventive.setOnClickListener(this);
        tvUpload.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        etAge.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                tvAge.setText("Age");
                if (isEditable) {
                    makeNonEditable();
                } else if (!isEditable) {
                    finish();
                }

                break;

            case R.id.ivImageRight:
                tvAge.setText("Age");
                if (isEditable) {
                    makeNonEditable();


                    if (CommonUtils.isOnline(context)) {

                        if (file == null) {
                            //when not upload image  then request image in string
                            ServiceRequest request = new ServiceRequest();
                            request.member_id = memberId;
                            request.name = etName.getText().toString().trim();
                            request.dob = String.valueOf(userSelectedTimeStamp);
                            request.image = "";
                            editMemberApi(request);
                        } else {
                            // when   upload image then iamge in file(use multipart)
                            editMemberApi();
                        }

                    } else {
                        CommonUtils.showAlert("Please check your internet connection.", context);
                    }
                } else {
                    showCustomDialogLogout(this, "Do you want to delete " + nameOfMember + "'s profile", View.VISIBLE, mDialog);
                }
                break;

            case R.id.tvSetPreventive:

                startActivity(new Intent(this, FriendProfileActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .putExtra("memberID", memberId));

                break;

            case R.id.tvEdit:
                tvAge.setText("Age");
                makeEditable();
                break;

            case R.id.tvUpload:
                if(isAllPermissionGranted()){
                    CommonUtils.showcameradialogs(MemberProfileActivity.this);
                }else{
                    if(ContextCompat.checkSelfPermission(MemberProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MemberProfileActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    }else if(ContextCompat.checkSelfPermission(MemberProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(MemberProfileActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }

                //showCustomDialog(this);

                break;
            case R.id.etAge:
                if (isEditable) {

                    dialogDateOfBirth((Activity) context, etAge);
                }
                break;
        }

    }

    /**
     * Code for Marshmallow permission
     * @return
     */

    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(MemberProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(MemberProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MemberProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(MemberProfileActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(MemberProfileActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MemberProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(MemberProfileActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(MemberProfileActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }



    public void showCustomDialogLogout(final Activity mContext, String msg, int visibilityAlert, final Dialog mDialog) {

        if (mContext == null) {
            com.srinivas.restaurantowner.utils.Log.e("TAG", "mContext is null");
        }

        LayoutInflater inflater = LayoutInflater.from(mContext);

        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);

        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.dialog_alertlogout, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvNo = (TextView) mDialog.findViewById(R.id.tvNo);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);
        TextView tvAlert = (TextView) mDialog.findViewById(R.id.tvAlert);
        View vViewTw = (View) mDialog.findViewById(R.id.vViewTw);

        if (visibilityAlert == View.VISIBLE) {
            tvAlert.setVisibility(visibilityAlert);
            vViewTw.setVisibility(View.GONE);
        } else {
            tvAlert.setVisibility(View.GONE);
            vViewTw.setVisibility(View.VISIBLE);
        }
        if (msg.trim().length() != 0)
            tvMsg.setText(msg);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.cancel();
            }
        });


        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.cancel();
                if (CommonUtils.isOnline(mContext)) {
                    ServiceRequest request = new ServiceRequest();
                    request.user_id = CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
                    request.member_id = memberId;
                    removeMemberApi(request);
                } else {
                    CommonUtils.showAlert("Please check your internet connection.", mContext);
                }
            }
        });

        mDialog.show();
    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>Remove Member>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void removeMemberApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Remove_Member;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        CommonUtils.showToast(context, response.response_message);
                        finish();

                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Member Profile>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void memberProfileAPI(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Select_Member;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        if (response.user.id != null && response.user.id.length() > 0) {
                            etName.setText(response.user.name != null && !response.user.name.equalsIgnoreCase("") ? response.user.name : "");
                            tvHeader.setText(response.user.name != null && response.user.name.length() > 0 ? response.user.name : "");

                            try {
                                nameOfMember = response.user.name;
                            } catch (Exception e1) {
                            }
                            if (response.user.image != null && !response.user.image.equalsIgnoreCase("")) {
                                Picasso.with(context).load(response.user.image)
                                        .placeholder(R.drawable.img_profile).into(ivProfileImage);
                            }


                            if (response.user.dob != null && response.user.dob.length() > 0) {

                                try {

                                    Log.e(TAG, "serverTimeStamp>>>>" + response.user.dob);
                                    long timestampLong = Long.parseLong(response.user.dob);

                                    userSelectedTimeStamp = Long.valueOf(response.user.dob);

                                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                                    cal.setTimeInMillis(timestampLong * 1000);
                                    String date = DateFormat.format("dd-MM-yyyy", cal).toString();

                                    String dateArray[] = date.split("-");

                                    Log.e(TAG, "serverTimeStampin_YEARS>>>>" + dateArray[2]);
                                    final Calendar currentCalender = Calendar.getInstance();
                                    int mYear = currentCalender.get(Calendar.YEAR);

                                    int age = mYear - Integer.valueOf(dateArray[2]);

                                    etAge.setText(String.valueOf(String.valueOf(age)));


                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }

                            }

                        }


                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>Edit Member(when use multipart file)>>>>>>>>>>>>>*/
    private void editMemberApi() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Edit_Member;
        Bitmap bm= BitmapFactory.decodeFile(file.getPath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Ion.with(context)
                .load(url)
                .setMultipartParameter("member_id", memberId)
                .setMultipartParameter("name", etName.getText().toString().trim())
                .setMultipartParameter("dob", String.valueOf(userSelectedTimeStamp))
                .setMultipartParameter("image", encodedImage)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                            makeNonEditable();
                            finish();
                        }
                    }
                } else {
                    Toast.makeText(context, "no response", Toast.LENGTH_SHORT).show();
                    if (e != null) {

                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });


    }

    /*>>>>>>>>>>>>>>>>>>>Edit Member Profile (when not  update image)>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void editMemberApi(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Edit_Member;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        Toast.makeText(context, "Profile Updated", Toast.LENGTH_SHORT).show();
                        makeNonEditable();
                        finish();
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });

    }

    private void dialogDateOfBirth(Activity mContext, final EditText etAge) {
        // Process to get Current Date
        final Calendar currentCalender = Calendar.getInstance();
        int mYear = currentCalender.get(Calendar.YEAR);
        int mMonth = currentCalender.get(Calendar.MONTH);
        int mDay = currentCalender.get(Calendar.DAY_OF_MONTH);
        // Launch Date Picker Dialog
        DatePickerDialog dpd =
                new DatePickerDialog(mContext,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar userAge = new GregorianCalendar(year, monthOfYear, dayOfMonth);

                                int diff = currentCalender.get(Calendar.YEAR) - userAge.get(Calendar.YEAR);


                                Long tsLong = userAge.getTimeInMillis() / 1000;

                                Log.e(TAG, "userSelectedTimeStamp>>>>>" + tsLong);

                                userSelectedTimeStamp = tsLong;


                                if (diff > 100) {

                                    ageValidation = false;

                                } else {
                                    ageValidation = true;
                                }

                                if (userAge.get(Calendar.MONTH) > currentCalender.get(Calendar.MONTH) ||
                                        (userAge.get(Calendar.MONTH) == currentCalender.get(Calendar.MONTH) && userAge.get(Calendar.DATE) > currentCalender.get(Calendar.DATE))) {
                                    diff--;
                                }
                                if (diff <= 1) {
                                    etAge.setText(String.valueOf(diff + " " + ""));
                                    //tvAge.setText("DOB");
                                } else {
                                    etAge.setText(String.valueOf(diff + " " + ""));
                                    //tvAge.setText("DOB");
                                }

                                // tvAge.setText(String.valueOf(diff+ " " +"years"));

                            }
                        }, mYear, mMonth, mDay);

        dpd.show();
        dpd.getDatePicker().setMaxDate(currentCalender.getTimeInMillis());
        dpd.setCancelable(false);
    }

    private void makeEditable() {

        etName.setEnabled(true);

        etAge.setEnabled(true);

        isEditable = true;

        tvUpload.setVisibility(View.VISIBLE);

        tvSetPreventive.setVisibility(View.INVISIBLE);

        tvEdit.setVisibility(View.INVISIBLE);

        ivImageRight.setImageResource(R.drawable.save_icon);

    }


    private void makeNonEditable() {
        etName.setEnabled(false);

        etAge.setEnabled(false);

        isEditable = false;


        tvUpload.setVisibility(View.INVISIBLE);

        tvSetPreventive.setVisibility(View.VISIBLE);

        tvEdit.setVisibility(View.VISIBLE);

        ivImageRight.setImageResource(R.drawable.delete_icon);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case TakePictureUtils.PICK_GALLERY:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), CommonUtils.imageNameLocal + ".png"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        TakePictureUtils.startCropImage(MemberProfileActivity.this, CommonUtils.imageNameLocal + ".png");

                    } catch (Exception e) {

                        Toast.makeText(MemberProfileActivity.this, "Error in picture", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage(MemberProfileActivity.this, CommonUtils.imageNameLocal + ".png");
                    break;

                case TakePictureUtils.CROP_FROM_CAMERA:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    imageRealPath = path;
                    Log.e("Image Real Path", imageRealPath);

                    file = new File(imageRealPath);
                    //Picasso.with(MemberProfileActivity.this).load(new File(path)).into(ivProfileImage);
                    ivProfileImage.setImageBitmap(BitmapFactory.decodeFile(imageRealPath));
                    break;
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (isEditable) {
            makeNonEditable();
        } else if (!isEditable) {
            finish();
        }
    }
}
