package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.owner.NFCActivity;
import com.srinivas.restaurantowner.activity.owner.QRActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class NfcChip  extends Activity implements View.OnClickListener
{
    private LinearLayout ll_nfcdialog;
    private View.OnClickListener tesChipListener,okButtonListener;
    private Dialog mDialog;
    private TextView   tvHeader;
    private Intent intent;
    private String nfc;
    private Context context;
    private ImageView ivImageLeft, ivImageRight;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_chip);
        setIds();
        setListeners();
        ivImageLeft.setImageResource(R.drawable.esc);
        context = NfcChip.this;

       /* CommonUtils.savePreference(NfcChip.this, AppConstants.IS_NFC, true);
        CommonUtils.savePreference(NfcChip.this, AppConstants.IS_QRSCANNER, false);*/


        if(getIntent()!=null&&getIntent().getStringExtra("nfclist")!=null)
        {
            showCustomDialog(NfcChip.this);
        }

        if (CommonUtils.getPreferencesBoolean(context, AppConstants.ADD_NFC)) {
            showCustomDialog(NfcChip.this);
            Log.e("","**********");
        }
    }





    private void setListeners()
    {
        ll_nfcdialog.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    private void setIds()
    {
        ll_nfcdialog= (LinearLayout) findViewById(R.id.ll_nfcdialog);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
    }



    private void showCustomDialog(final Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg= (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);
        tvOk.setVisibility(View.GONE);
        view.setVisibility(View.GONE);

        tvTestchip.setGravity(Gravity.CENTER);

        tvHeader= (TextView) findViewById(R.id.   tvHeader);
        tvHeader.setText("");
        if(getIntent()!=null&&getIntent().getStringExtra("addnfc")!=null){
            tvMsg.setText(getString(R.string.product_has_been_edit));

            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(NfcChip.this, QRActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );;
                    startActivity(intent);
                    mDialog.dismiss();

                }
            });

            tvTestchip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(NfcChip.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );
                    startActivity(intent);
                   mDialog.dismiss();
                }
            });
        }


        tvTestchip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDialog.dismiss();
                ll_nfcdialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showCustomDialogforchip(NfcChip.this);
                    }
                });

            }
        });
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(NfcChip.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );;
                startActivity(intent);
               mDialog.dismiss();

            }
        });
        mDialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
            case R.id.ivImageRight:
                startActivity(new Intent(context,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;

            case R.id.ll_nfcdialog:

                if (getIntent() != null && getIntent().getStringExtra("NFC") != null) {
                    intent = new Intent(NfcChip.this, QrScanner.class);
                    intent.putExtra("modifydialog", AppConstants.MODIFY);
                    startActivity(intent);

                  //  showCustomDialogmodifyNfc(NfcChip.this);
                    Log.e("nfcvalue", "*************8");

                } else if (getIntent() != null && getIntent().getStringExtra("DeleteNfc") != null) {
                   // showCustomDialogmodifyNfc(NfcChip.this);
                    intent = new Intent(NfcChip.this, QrScanner.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("deletedialofnfc", AppConstants.DELETEDIALOFNFC);
                    startActivity(intent);

                } else if (getIntent() != null && getIntent().getStringExtra("QrScanner") != null) {
                    intent = new Intent(NfcChip.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);;
                    intent.putExtra("Noproduct", AppConstants.NOPRODUCT);
                    startActivity(intent);

                } else if (getIntent() != null && getIntent().getStringExtra("ReadNfc") != null) {
                    intent = new Intent(NfcChip.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);;
                    intent.putExtra("Contentpresent", AppConstants.CONTENTPRESENT);
                    startActivity(intent);
                }
                else if (getIntent() != null && getIntent().getStringExtra("nfcchipsvcanner") != null) {
                    intent = new Intent(NfcChip.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);;
                    intent.putExtra("againnfcchip", AppConstants.AGAINNFCCHIP);
                    startActivity(intent);
                }
                else {

                    showCustomDialog(NfcChip.this);
                }
                break;
        }
    }
    private void showCustomDialogmodifyNfc(Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.chip_contains_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvYes= (TextView) mDialog.findViewById(R.id.tvYes);
        TextView tvmsg= (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvmodigymsg= (TextView) mDialog.findViewById(R.id.tvmodigymsg);
        tvmsg.setText("Contents present in the NFC chip.");
        tvOk.setText("No");
        tvmodigymsg.setVisibility(View.VISIBLE);
        tvYes.setVisibility(View.VISIBLE);

        if(getIntent()!=null&&getIntent().getStringExtra("DeleteNfc")!=null){
            tvmodigymsg.setText(getString(R.string.are_you_sure_you_want_delete_product_nfc));
            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();


                }
            });
            tvYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    ll_nfcdialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            intent=new Intent(NfcChip.this,QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );;
                            intent.putExtra("Nfcchipdismiss",AppConstants.NFCDISSMISS);
                            startActivity(intent);


                        }
                    });
                }
            });


        }
        else {

            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(NfcChip.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );;
                    startActivity(intent);
                  mDialog.dismiss();


                }
            });

            tvYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(NfcChip.this, SelectProductFromNfc.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );;
                    startActivity(intent);
                   mDialog.dismiss();

                }
            });
        }
        mDialog.show();
    }


    private void showCustomDialogforchip(final Activity mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.chip_contains_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(NfcChip.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP );;
                startActivity(intent);
               mDialog.dismiss();

            }
        });

        mDialog.show();
    }
}