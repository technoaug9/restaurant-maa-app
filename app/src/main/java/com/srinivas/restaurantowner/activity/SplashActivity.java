package com.srinivas.restaurantowner.activity;
import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
public class SplashActivity extends Activity {

    private static final String TAG = SplashActivity.class.getSimpleName() ;
    private Context mContext;

    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

                    if(ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }else{
                        Log.e("Permission granted: ","Case 2 performed Action");

                        Intent intent;
                        if (CommonUtils.getIntPreferences(SplashActivity.this, AppConstants.PRE_ISLOGIN) == 1) {
                            intent = new Intent(SplashActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            intent = new Intent(SplashActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }

                } else {
                    if(!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)){
                            Toast.makeText(mContext,"You have denied camera permission, Now camera will not work for this app!",Toast.LENGTH_LONG).show();
                        Log.e("Permission denied Camera: ","Case 2 performed Action");

                        Intent intent;
                        if (CommonUtils.getIntPreferences(SplashActivity.this, AppConstants.PRE_ISLOGIN) == 1) {
                            intent = new Intent(SplashActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            intent = new Intent(SplashActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }else{
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    }
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED ) {

                    if(ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    }else{
                        Log.e("Permission granted: ","Case 2 performed Action");


                        Intent intent;
                        if (CommonUtils.getIntPreferences(SplashActivity.this, AppConstants.PRE_ISLOGIN) == 1) {
                            intent = new Intent(SplashActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            intent = new Intent(SplashActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }


                } else {

                    if(!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                        Toast.makeText(mContext,"You have denied the storage permission now camera will not use storage!",Toast.LENGTH_LONG).show();
                        Log.e("Permission denied storage: ","Case 2 performed Action");

                        Intent intent;
                        if (CommonUtils.getIntPreferences(SplashActivity.this, AppConstants.PRE_ISLOGIN) == 1) {
                            intent = new Intent(SplashActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        } else {
                            intent = new Intent(SplashActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }

                    }else{
                        ActivityCompat.requestPermissions(SplashActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mContext = SplashActivity.this;

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isAllPermissionGranted()){

            Log.e("Permission Granted","onResume runs");
            Intent intent;
            if (CommonUtils.getIntPreferences(SplashActivity.this, AppConstants.PRE_ISLOGIN) == 1) {
                intent = new Intent(SplashActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                intent = new Intent(SplashActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

        }else {
            if(ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(SplashActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }else if(ContextCompat.checkSelfPermission(SplashActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(SplashActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        2);
            }

        }
    }
}
