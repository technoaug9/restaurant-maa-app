package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;

/**
 * Created by vivek.pareek on 15/10/15.
 */
public class ScanNfcActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader,tvMsg,tvCode,tvTestchip,tvOk;
    private Context mContext;
    private ImageView ivImageLeft, ivImageRight;
    private LinearLayout ll_nfcdialog;
    Dialog dialog_nfc_first,dialog_nfc_second;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_chip);
        setIds();
        setListeners();
        ivImageLeft.setImageResource(R.drawable.esc);
        mContext = ScanNfcActivity.this;
        counter=0;

       /* CommonUtils.savePreference(NfcChip.this, AppConstants.IS_NFC, true);
        CommonUtils.savePreference(NfcChip.this, AppConstants.IS_QRSCANNER, false);*/



    }





    private void setListeners()
    {

        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        ll_nfcdialog.setOnClickListener(this);
    }

    private void setIds()
    {
        ll_nfcdialog= (LinearLayout) findViewById(R.id.ll_nfcdialog);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
            case R.id.ivImageRight:

            /*    finishAffinity();
                if (CommonUtils.getPreferences(mContext, AppConstants.USER_TYPE)
                        .equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(mContext, AppConstants.USER_TYPE)
                        .equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }*/

                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.ll_nfcdialog:

                if(counter==0)
                {

                    dialog_nfc_first= CommonUtils.customDialog(this, R.layout.fragment_nfc_program_first_dialog);
                    getIdDialogNfcFirst();

                    setListnerDialogNfcFirst();




                    counter=1;

                }
                else {


                    dialog_nfc_second= CommonUtils.customDialog(this, R.layout.chip_contains_dialog);
                    getIdDialogNfcSecond();

                    setListnerDialogNfcSecond();

                    counter=0;
                }

                break;

            //Click event on Nfc First Dialog
            case R.id.tvTestchip:
                dialog_nfc_first.dismiss();
                break;

            //Click event on Nfc Second Dialog
            case R.id.tvOk:
                dialog_nfc_second.dismiss();
                Intent intent1 = new Intent(this, NfcOwnerActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                //dialog_nfc_second.dismiss();
                break;
        }

    }

    private void setListnerDialogNfcSecond()
    {
        tvOk.setOnClickListener(this);
    }

    private void setListnerDialogNfcFirst() {
        tvTestchip.setOnClickListener(this);
    }

    private void getIdDialogNfcFirst() {
        tvMsg=(TextView)dialog_nfc_first.findViewById(R.id.tvMsg);
        tvCode=(TextView)dialog_nfc_first.findViewById(R.id.tvCode);
        tvTestchip=(TextView)dialog_nfc_first.findViewById(R.id.tvTestchip);




    }
    private void getIdDialogNfcSecond() {
        tvOk=(TextView)dialog_nfc_second.findViewById(R.id.tvOk);





    }
}
