package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.owner.RestaurantRegistrationActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private TextView tvRememberMe, tvForgotPassword, tvSignUp, tvLoginWithFacebook, tvLoginWithGooglePlus, tvLogin;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText etEmailId, etPassword;
    private boolean isRemember = false;
    private AccessToken accessToken;
    private CallbackManager callbackManager;
    private Context context = LoginActivity.this;
    private String socialMediaType = "";
    ServiceRequest savedata;
    private static final int RC_SIGN_IN = 0;

    private boolean mIntentInProgress;
    private LinearLayout llLoginWithFacebook, llLoginWithGooglePlus;
    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        getId();
        test();
        isRememberMe();
        CommonUtils.printKeyHash(this);
        setListener();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.facebook.samples.loginhowto",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            //  handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                  //  handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            getProfileInformation(acct);


        } else {
            // Signed out, show unauthenticated UI.
            // CommonUtils.showToast(,getBaseContext(),"couldnt connect to Gmail");

        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }


    private void test() {

        etEmailId.setText("owner@owner.com");
        etPassword.setText("12341234");
    }


    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (socialMediaType.equalsIgnoreCase("facebook")) {
            callbackManager.onActivityResult(requestCode, responseCode, intent);
        } else if (socialMediaType.equalsIgnoreCase("googlePlus") && requestCode == RC_SIGN_IN) {
            Log.e("ONACTIVTY", "***************");
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(intent);
            handleSignInResult(result);
        }
    }

    private void isRememberMe() {

        String emailId = CommonUtils.getPreferencesString(this, AppConstants.USER_EMAIL_ID);
        String password = CommonUtils.getPreferencesString(this, AppConstants.USER_PASSWORD);

        if (emailId.length() == 0 && password.length() == 0) {
            etEmailId.setText("");
            etPassword.setText("");
            isRemember = false;
            tvRememberMe.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0, 0, 0);
        } else {
            etEmailId.setText(emailId);
            etPassword.setText(password);
            isRemember = true;
            tvRememberMe.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon_sel, 0, 0, 0);
        }
    }

    public void getId() {
        tvRememberMe = (TextView) findViewById(R.id.tvRememberMe);
        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvLoginWithFacebook = (TextView) findViewById(R.id.tvLoginWithFacebook);
        tvLoginWithGooglePlus = (TextView) findViewById(R.id.tvLoginWithGooglePlus);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvLoginWithGooglePlus = (TextView) findViewById(R.id.tvLoginWithGooglePlus);
        llLoginWithFacebook = (LinearLayout) findViewById(R.id.llLoginWithFacebook);
        llLoginWithGooglePlus = (LinearLayout) findViewById(R.id.llLoginWithGooglePlus);
    }

    private void setListener() {
        tvLogin.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        tvRememberMe.setOnClickListener(this);
        llLoginWithFacebook.setOnClickListener(this);
        llLoginWithGooglePlus.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLogin:
                if (checkValidation()) {
                    if (CommonUtils.isOnline(context)) {
                        ServiceRequest request = new ServiceRequest();
                        request.email = etEmailId.getText().toString();
                        request.password = etPassword.getText().toString();
                        callLoginApi(request);
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
                break;
            case R.id.tvSignUp:
                Intent intent = new Intent(this, RestaurantRegistrationActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvForgotPassword:
                intent = new Intent(this, ForgotActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvRememberMe:
                if (isRemember) {
                    tvRememberMe.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0, 0, 0);
                    isRemember = false;
                } else {
                    tvRememberMe.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon_sel, 0, 0, 0);
                    isRemember = true;
                }
                break;

            case R.id.llLoginWithFacebook:
                socialMediaType = "facebook";
                loginWithFacebook();
                break;
            case R.id.llLoginWithGooglePlus:
                CommonUtils.showToast(context, "Please wait while we connect you to google plus");
                socialMediaType = "googlePlus";
                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
                signIn();
                break;
            default:
                break;
        }

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void loginWithFacebook() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = AccessToken.getCurrentAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        String id = response.getJSONObject().optString("id");
                        String user_name = response.getJSONObject().optString("name");
                        String email = object.optString("email");
                        if (email != null && email.length() > 0) {
                            email = response.getJSONObject().optString("email");
                            if (CommonUtils.isOnline(context)) {
                                savedata = new ServiceRequest();
                                savedata.signup_type = "social_auth";
                                savedata.email = email;
                                savedata.provider_id = id;
                                savedata.provider_name = "facebook";
                                savedata.name = user_name;
                                callCheckUserAuthAPI(savedata);
                            } else {
                                CommonUtils.showToast(context, getString(R.string.please_check_internet));
                            }
                        } else {
                            Toast.makeText(context, "Sorry! Your email is set private in facebook. Please make it public to use", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,name,gender,email,picture.width(200)");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplication(), "Cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(getApplication(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void rememberMe() {
        if (isRemember) {
            CommonUtils.savePreferencesString(this, AppConstants.USER_EMAIL_ID, etEmailId.getText().toString().trim());
            CommonUtils.savePreferencesString(this, AppConstants.USER_PASSWORD, etPassword.getText().toString().trim());
        } else {
            CommonUtils.savePreferencesString(this, AppConstants.USER_EMAIL_ID, "");
            CommonUtils.savePreferencesString(this, AppConstants.USER_PASSWORD, "");
        }
    }

    private boolean checkValidation() {

        if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter Username.", null, View.GONE);
            etEmailId.requestFocus();
            return false;
        } else if (etPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter password.", null, View.GONE);
            etPassword.requestFocus();
            return false;
        } else if (etPassword.getText().toString().trim().length() < 6) {
            CommonUtils.showCustomDialogOk(this, "Password must be of 6 characters.", null, View.GONE);
            etPassword.requestFocus();
            return false;

        }
        return true;
    }

    //***********************MANUAL LOGIN FROM APP*****************************
    private void callLoginApi(ServiceRequest request) {
       // final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        showProgressDialog();
        String url = RequestURL.NEW_LOGIN;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));
        Log.e(TAG, " url " + url);
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e(TAG, " response data " + new Gson().toJson(response));
                hideProgressDialog();
                Log.e("Login", "res = " + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.user.id != null && response.user.id.length() > 0) {
                            CommonUtils.savePreferencesString(context, AppConstants.USER_ID, response.user.id);
                        }
                        if (response.user.user_type != null && response.user.user_type.length() > 0) {

                            if (response.user.user_type.equalsIgnoreCase(AppConstants.USER_TYPE_OWNER) || response.user.user_type.equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                                CommonUtils.savePreferencesString(context, AppConstants.USER_TYPE, response.user.user_type);
                                CommonUtils.saveIntPreferences(context, AppConstants.PRE_ISLOGIN, 1);
                                Intent intent = new Intent(context, HomeActivity.class);
                                startActivity(intent);
                                rememberMe();
                                finish();
                            } else {
                                CommonUtils.showAlertTitle(getString(R.string.alert), getString(R.string.invalid_crediential), context);
                            }

                        }
                    } else if (response.response_code != null && response.response_code.equalsIgnoreCase("500")) {
                        Log.e("500", "***********");
                        CommonUtils.showAlert(response.response_message, context);
                    } else {
                        CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                    }
                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }

//****************PROFILE INFO OF GOOGLE PLUS*****************

    public void getProfileInformation(GoogleSignInAccount acct) {

        Log.e("getprofileinformation", "***************");

        try {
            if (acct != null) {
                acct.getDisplayName();
                acct.getEmail();
                acct.getPhotoUrl();
                acct.getId();


                String personName = acct.getDisplayName();
                ;
                String personPhotoUrl = "";
                if (acct.getPhotoUrl() != null) {
                    personPhotoUrl = acct.getPhotoUrl().toString();
                    ;
                }
                String email = acct.getEmail();
                ;
                String id = acct.getId();
                ;
                Log.e(TAG, "Name: " + personName + ", plusProfile: "
                        + ", email: " + email
                        + ", Image: " + personPhotoUrl);
                if (email != null && email.length() > 0) {
                    if (CommonUtils.isOnline(context)) {
                        Log.e(TAG, email + "");
                        savedata = new ServiceRequest();
                        savedata.signup_type = "social_auth";
                        savedata.name = personName;
                        savedata.email = email;
                        savedata.provider_id = id;
                        savedata.provider_name = "google+";
                        callCheckUserAuthAPI(savedata);
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                } else {
                    CommonUtils.showAlert("Email is not valid", context);
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


//****************************** checking user already registered through fb and g+ ****************************////

    private void callCheckUserAuthAPI(final ServiceRequest savedata) {

        //final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
       showProgressDialog();
        String url = RequestURL.CHECK_USER_AUTH;
        savedata.user_type = AppConstants.USER_TYPE_OWNER;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(savedata);
        Log.e("Tag", "req data " + new Gson().toJson(requestJson));
        Log.e("Tag", " url >>> " + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e("Tag", " response data " + new Gson().toJson(response));
               /* if (progressDialog != null) {
                    progressDialog.dismiss();
                }*/
                hideProgressDialog();
                if (response != null) {

                    if (response.is_exist != null && response.is_exist.length() > 0) {

                        if (response.is_exist.equalsIgnoreCase("true")) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                if (response.user.id != null && response.user.id.length() > 0) {
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_ID, response.user.id);
                                }
                                if (response.user.user_type != null && response.user.user_type.length() > 0) {
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_TYPE, response.user.user_type);
                                    CommonUtils.saveIntPreferences(context, AppConstants.PRE_ISLOGIN, 1);
                                    Intent intent = new Intent(context, HomeActivity.class);
                                    startActivity(intent);
                                    rememberMe();
                                    finish();
                                }

                                //DONE

                            } else if (response.response_code != null && response.response_code.equalsIgnoreCase("500")) {
                                Log.e("500", "***********");
                                CommonUtils.showAlert(response.response_message, context);
                            } else {
                                CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                            }


                        } else {
                            // signup pe bhejo
                            Intent intent1 = new Intent(context, RestaurantRegistrationActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent1.putExtra("saveData", savedata);
                            startActivity(intent1);
                        }
                    }

                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });


    }

    @Override
    public void onConnected(Bundle bundle) {

        // getProfileInformation();

        Log.e("onConnected", "***************");


    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        if (!mIntentInProgress && connectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                startIntentSenderForResult(connectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (Exception e) {
                mIntentInProgress = true;
                mGoogleApiClient.connect();
            }
        }
    }
}
