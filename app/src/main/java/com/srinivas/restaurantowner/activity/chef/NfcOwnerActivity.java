package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponseProduct;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.owner.NFCScannerOwnerActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

/**
 * Created by vivek.pareek on 15/10/15.
 */
public class NfcOwnerActivity extends Activity implements View.OnClickListener {

    private TextView tvModifyNfc, tvProgramNfc, tvDeleteNfc, tvReadNfc, tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private Context context;
    private Intent intent;
    private Dialog mDialog;
    private String action = "";
    private NfcAdapter nfcAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        CommonUtils.hideSoftKeyboard(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        context = NfcOwnerActivity.this;
        getId();
        setListener();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);


    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvModifyNfc = (TextView) findViewById(R.id.tvModifyNfc);
        tvProgramNfc = (TextView) findViewById(R.id.tvProgramNfc);
        tvDeleteNfc = (TextView) findViewById(R.id.tvDeleteNfc);
        tvReadNfc = (TextView) findViewById(R.id.tvReadNfc);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);

        tvHeader.setText("NFC");
    }

    private void setListener() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        tvModifyNfc.setOnClickListener(this);
        tvProgramNfc.setOnClickListener(this);
        tvDeleteNfc.setOnClickListener(this);
        tvReadNfc.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;

            case R.id.ivImageRight:
                startActivity(new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;
            case R.id.tvProgramNfc:

                intent = new Intent(this, NFCScannerOwnerActivity.class);
                intent.putExtra("action", "new");
                startActivity(intent);
                break;
            case R.id.tvReadNfc:

                if (nfcAdapter != null) {

                    if (nfcAdapter.isEnabled()) {
                        action = "read";
                        intent = new Intent(this, NFCScannerOwnerActivity.class);
                        startActivityForResult(intent, 2);
                    } else {
                        CommonUtils.showToast(NfcOwnerActivity.this, "Please enable nfc in settings");
                    }
                } else {
                    CommonUtils.showToast(NfcOwnerActivity.this, "Sorry! your device doesn't support NFC");
                }


                break;
            case R.id.tvModifyNfc:

                if (nfcAdapter != null) {
                    if (nfcAdapter.isEnabled()) {
                        action = "modify";
                        intent = new Intent(this, NFCScannerOwnerActivity.class);
                        startActivityForResult(intent, 2);
                    } else {
                        CommonUtils.showToast(NfcOwnerActivity.this, "Please enable nfc in settings");
                    }
                } else {
                    CommonUtils.showToast(NfcOwnerActivity.this, "Sorry! your device doesn't support NFC");
                }


                break;
            case R.id.tvDeleteNfc:
                if (nfcAdapter != null) {

                    if (nfcAdapter.isEnabled()) {
                        action = "delete";
                        intent = new Intent(this, NFCScannerOwnerActivity.class);
                        startActivityForResult(intent, 2);
                    } else {
                        CommonUtils.showToast(NfcOwnerActivity.this, "Please enable nfc in settings");
                    }

                } else {
                    CommonUtils.showToast(NfcOwnerActivity.this, "Sorry! your device doesn't support NFC");
                }


                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            String id = "";
            try {
                id = data.getStringExtra("code");
                Log.e("", "ID " + id);
            } catch (Exception e) {
                id = "";
                e.printStackTrace();
            }

            if (action.equalsIgnoreCase("modify")) {
                if (CommonUtils.isOnline(NfcOwnerActivity.this)) {
                    testAssignProductServiceCall(id, "nfc");
                } else {
                    CommonUtils.showToast(NfcOwnerActivity.this, getString(R.string.please_check_internet));
                }
            } else if (action.equalsIgnoreCase("delete")) {
                if (CommonUtils.isOnline(context)) {
                    testAssignProductServiceCall(id, "nfc");
                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }
            } else if (action.equalsIgnoreCase("read")) {
                if (CommonUtils.isOnline(context)) {
                    testAssignProductServiceCall(id, "nfc");
                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }
            }
        }
    }

    private void testAssignProductServiceCall(String barCodeValue, final String type) {
        final ProgressDialog progressDialog = ProgressDialog.show(NfcOwnerActivity.this, "", "Please wait..");
        String userId = CommonUtils.getPreferencesString(NfcOwnerActivity.this, AppConstants.USER_ID);

        //http://172.16.1.95:5000//products/get_product_based_on_code?user_id=2&code=12345678&code_type=nfc
        String url = RequestURL.BASE_URL + "/products/get_product_based_on_code?user_id=" + userId + "&code=" + barCodeValue + "&code_type=" + type;

        Log.e("", "url" + url);

        Ion.with(NfcOwnerActivity.this)
                .load(url)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                if (response.product != null) {
                                    modifyhowCustomDialog(NfcOwnerActivity.this, response.product.name, response.product.nfc_code, response.product.id);
                                }
                            } else {
                                okDialog(NfcOwnerActivity.this, response.response_message);
                            }
                        } else {
                            Toast.makeText(NfcOwnerActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    private void okDialog(final Context context, String msg) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert_ok, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);

        tvMsg.setText(msg);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void modifyhowCustomDialog(final Context context, String productName, final String productCode, final String productID) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_qr_nfc_product_modify, null);
        mDialog.setContentView(dialoglayout);

        TextView tvContents = (TextView) mDialog.findViewById(R.id.tvContents);
        TextView tvProductName = (TextView) mDialog.findViewById(R.id.tvProductName);
        TextView tvProductCode = (TextView) mDialog.findViewById(R.id.tvProductCode);
        TextView tvSurityStatment = (TextView) mDialog.findViewById(R.id.tvSurityStatment);
        TextView tvNo = (TextView) mDialog.findViewById(R.id.tvNo);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);

        Log.e("", "productName " + productName);
        Log.e("", "productCode " + productCode);


        tvContents.setText("Contents present in the NFC code image");
        tvProductName.setText("Product name: " + productName);
        tvProductCode.setText("Unique code: " + productCode);

        if (action.equalsIgnoreCase("modify")) {
            tvNo.setText("No");
            tvYes.setText("Yes");
            tvSurityStatment.setText("Are you sure you want to modify the NFC code image?");
        } else if (action.equalsIgnoreCase("delete")) {
            tvNo.setText("No");
            tvYes.setText("Yes");
            tvSurityStatment.setText("Are you sure you want to delete the product from the NFC code image?");
        } else if (action.equalsIgnoreCase("read")) {
            tvNo.setText("Modify");
            tvYes.setText("Delete");
            tvSurityStatment.setText("Do you want to modify or delete the NFC code image from here?");
        }


        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if (action.equalsIgnoreCase("read")) {
                    action = "modify";
                    Intent intent = new Intent(NfcOwnerActivity.this, SelectProductFromNfc.class);
                    intent.putExtra("barCodeValue", productCode);
                    intent.putExtra("modify_product", "modifyProduct");
                    intent.putExtra("product_id", productID);
                    intent.putExtra("type", "nfc");
                    startActivity(intent);
                }
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if (action.equalsIgnoreCase("modify")) {
                    Intent intent = new Intent(NfcOwnerActivity.this, SelectProductFromNfc.class);
                    intent.putExtra("barCodeValue", productCode);
                    intent.putExtra("modify_product", "modifyProduct");
                    intent.putExtra("product_id", productID);
                    intent.putExtra("type", "nfc");
                    startActivity(intent);
                } else if (action.equalsIgnoreCase("delete")) {
                    deleteProductServiceCall(productID);
                } else if (action.equalsIgnoreCase("read")) {
                    action = "delete";
                    deleteProductServiceCall(productID);
                }

            }
        });
        mDialog.show();
    }


    private void deleteProductServiceCall(String id) {
        final ProgressDialog progressDialog = ProgressDialog.show(NfcOwnerActivity.this, "", "Please wait..");

        JsonObject jsonRequest = new JsonObject();
        jsonRequest.addProperty("user_id", CommonUtils.getPreferencesString(NfcOwnerActivity.this, AppConstants.USER_ID));
        jsonRequest.addProperty("id", id);
        jsonRequest.addProperty("nfc_code", "");
        jsonRequest.addProperty("operation_type", "deleted from NFC");

        String url = RequestURL.BASE_URL + "products/assign_code_to_product";

        Log.e("", "request" + jsonRequest.toString());
        Log.e("", "url" + url);

        Ion.with(NfcOwnerActivity.this)
                .load(url)
                .setJsonObjectBody(jsonRequest)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                Toast.makeText(NfcOwnerActivity.this, response.response_message, Toast.LENGTH_SHORT).show();
                                showCustomDialog(NfcOwnerActivity.this);
                            } else {
                                Toast.makeText(NfcOwnerActivity.this, response.response_message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(NfcOwnerActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void showCustomDialog(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);

        tvCode.setVisibility(View.GONE);
        tvMsg.setText(context.getString(R.string.edit_nfc));
        view.setVisibility(View.GONE);
        tvOk.setVisibility(View.GONE);
        tvTestchip.setGravity(Gravity.CENTER);
        tvTestchip.setText("Test The NFC");


        tvTestchip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(NfcOwnerActivity.this, NFCScannerOwnerActivity.class);
                intent.putExtra("action", "test");
                startActivity(intent);
                finish();


            }
        });

        mDialog.show();
    }

}
