package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponseProduct;
import com.srinivas.restaurantowner.activity.HomeActivity;

import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class NewQrProgramActivity extends Activity implements View.OnClickListener
{

    private ImageView ivImageRight,ivImageLeft, ivNextArrow;
    private TextView tvHeader, tvQRDetail, tvDetail ;
    private String barCodeValue="";
    private String type = "qr";

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_qr_activity);

        getId();
        setListener();

        try {
            if(getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equalsIgnoreCase("nfc")){
                tvQRDetail.setText("NFC Contains the unique id \""+ getIntent().getStringExtra("barCodeValue") + "\"");
                tvDetail.setText("Tap the icon to assign a product into the NFC Code image");
                barCodeValue = getIntent().getStringExtra("barCodeValue");
                type = "nfc";
            }else{
                tvQRDetail.setText("QR Contains the unique id \""+ getIntent().getStringExtra("barCodeValue") + "\"");
                tvDetail.setText("Tap the icon to assign a product into the QR Code image");
                barCodeValue = getIntent().getStringExtra("barCodeValue");
                type = "qr";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("","userID "+ CommonUtils.getPreferencesString(NewQrProgramActivity.this, AppConstants.USER_ID));

    }



    private void getId()
    {
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivNextArrow=(ImageView) findViewById(R.id.ivNextArrow);
        tvQRDetail=(TextView) findViewById(R.id.tvQRDetail);
        tvDetail=(TextView) findViewById(R.id.tvDetail);

    }
    private void setListener()
    {
       ivImageRight.setOnClickListener(this);
       ivImageLeft.setOnClickListener(this);
        ivNextArrow.setOnClickListener(this);
    }



    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ivImageRight:
                finishAffinity();
                if(CommonUtils.getPreferences(NewQrProgramActivity.this, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, HomeActivity.class));
                }else if (CommonUtils.getPreferences(NewQrProgramActivity.this, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                else{
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;
            case R.id.ivImageLeft:
                    finish();
                break;

            case R.id.ivNextArrow:

                if(getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equalsIgnoreCase("nfc")){
                    testAssignProductServiceCall(barCodeValue, "nfc");
                }else{
                    testAssignProductServiceCall(barCodeValue, "qr");
                }

                break;

        }

    }


    private void testAssignProductServiceCall(final String barCodeValue, final String type)
    {
        final ProgressDialog progressDialog = ProgressDialog.show(NewQrProgramActivity.this, "", "Please wait..");
        String userId= CommonUtils.getPreferencesString(NewQrProgramActivity.this, AppConstants.USER_ID);
// ServiceRequest request = new ServiceRequest();
        /*request.upc = getIntent().getStringExtra("upc");
        request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        request.history_type = "scan";
        request.code_type = codeType;
        Log.e("barcodeRequest-->" + codeType, request.toString());*/
      //Scan_Upc
        String url = RequestURL.BASE_URL + "owner/qrcodesearch?user_id="+userId+"&code="+barCodeValue+"&code_type="+type+"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);

        Log.v("QrCoderequestData", "url" + url);

        Ion.with(NewQrProgramActivity.this)
                .load(url)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            Log.i("Tag", " response data " + new Gson().toJson(response));
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                CommonUtils.showToast(NewQrProgramActivity.this, response.response_message);

                                if (response.product != null) {
                                    if(type.equalsIgnoreCase("nfc")){
                                        detailProductshowCustomDialog(NewQrProgramActivity.this, response.product.name, response.product.nfc_code);
                                    }else{
                                        detailProductshowCustomDialog(NewQrProgramActivity.this, response.product.name, response.product.qr_code);
                                    }

                                }

                            } else {
                                CommonUtils.showToast(NewQrProgramActivity.this, response.response_message);
                                //CommonUtils.savePreference(NewQrProgramActivity.this, AppConstants.IS_NFC, false);
                                Intent intent = new Intent(NewQrProgramActivity.this, SelectProductFromNfc.class);
                                intent.putExtra("barCodeValue", barCodeValue);
                                intent.putExtra("type", type);
                                startActivity(intent);
                                finish();

                            }
                        } else {
                            Log.e("Response error",e.toString());
                            Toast.makeText(NewQrProgramActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void detailProductshowCustomDialog(final Context context, String productName, String productCode) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_nfc_pushed_product_modify, null);
        mDialog.setContentView(dialoglayout);

        TextView tvCantains = (TextView) mDialog.findViewById(R.id.tvCantains);
        TextView tvProductName = (TextView) mDialog.findViewById(R.id.tvPushedProductNameModify);
        TextView tvProductCode = (TextView) mDialog.findViewById(R.id.tvPushedProductCodeModify);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvPushedProductOkModify);

        if(getIntent().getStringExtra("type")!=null && getIntent().getStringExtra("type").equalsIgnoreCase("nfc")){
            tvCantains.setText("The NFC code contains");
            tvProductName.setText("Product name: "+productName);
            tvProductCode.setText("Unique code: "+productCode);
        }else{
            tvCantains.setText("The QR code contains");
            tvProductName.setText("Product name: "+productName);
            tvProductCode.setText("Unique code: "+productCode);
        }

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                NewQrProgramActivity.this.finish();


            }
        });

        mDialog.show();
    }
}
