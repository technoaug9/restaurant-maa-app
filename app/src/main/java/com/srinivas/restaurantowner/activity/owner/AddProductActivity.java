package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.adapter.owner.AddproductAdapter;

import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.LoadMoreListView;
import com.srinivas.restaurantowner.utils.SwipListView;

import java.util.ArrayList;
import java.util.List;

public class AddProductActivity extends Activity implements View.OnClickListener {

    private SwipListView swipListView;
    private AddproductAdapter adapter;
    private Context context = AddProductActivity.this;
    private List<Product> list;
   // private TextView tvAddNewMember;
    private TextView tvHeader, tvAddProduct;
    private ImageView ivAdd;
    //private TextView tvAddNewMember;
    private Intent intent;
    private Boolean isEditable = false;
    private View.OnClickListener yesButtonListener;
    private Dialog mDialog;
    private ImageView ivImageRight, ivImageLeft;
    private boolean isLoading = true;
    private boolean isLoadMorePullToRefresh, isResume;
    private String TAG = AddProductActivity.class.getSimpleName();
    private EditText etSearch;
    private int page_number = 1;
    private int maxPages = 20;
    private String ID = "", productAddedFor = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        list = new ArrayList<Product>();
        getIDs();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
        makeNonEditable();
        tvHeader.setText("My Products");


        if (getIntent() != null) {

            if (getIntent().hasExtra("chefId") && getIntent().getStringExtra("chefId").length() > 0) {
                ID = getIntent().getStringExtra("chefId");
                productAddedFor = "chef_id";
                Log.e(TAG, ">>>chefId<<<<<" + ID);
            }

            if (getIntent().hasExtra("chefName") && getIntent().getStringExtra("chefName").length() > 0) {
                tvHeader.setText(getIntent().getStringExtra("chefName") + "'s Products");
            }

            if (getIntent().hasExtra("from") && (getIntent().getStringExtra("from").equalsIgnoreCase("ManageMenu") || getIntent().getStringExtra("from").equalsIgnoreCase("Home"))) {
                ID = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                Log.e(TAG, ">>>userID<<<<<" + ID);
                productAddedFor = "user_id";
            }
        } else {
            ID = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
            Log.e(TAG, ">>>userID<<<<<" + ID);
            productAddedFor = "user_id";
        }


        mDialog = new Dialog(this,
                android.R.style.Theme_Translucent_NoTitleBar);

        adapter = new AddproductAdapter(AddProductActivity.this, list);
        swipListView.setAdapter(adapter);

        page_number = 1;

        if (CommonUtils.isOnline(context)) {

            if (ID.length() > 0) {
                callProductListOfUserAPI(ID);
            }
        } else {

            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }

        swipListView.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (maxPages < page_number) {
                    swipListView.onLoadMoreComplete();
                } else {
                    if (CommonUtils.isOnline(context)) {

                        if (ID.length() > 0) {
                            callProductListOfUserAPI(ID);
                        }

                    } else {

                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
            }
        });


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                adapter.getFilter().filter(charSequence.toString());

                Log.e(TAG, "Char   " +charSequence.toString());

                // mAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        page_number = 1;

        if (CommonUtils.isOnline(context)) {

            if (ID.length() > 0) {
                callProductListOfUserAPI(ID);
            }
        } else {


            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }
    }


    private void callProductListOfUserAPI(String id) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...");

        //String url = RequestURL.BASE_URL + "owner/productlist?user_id=" + id +"&user_type=" + CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE) + "&page=" + page_number + "&per_page=10";
        String url = RequestURL.BASE_URL + "owner/myproducts?user_id=" + id +"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE)+"&page=" + page_number + "&per_page=10";
        Log.e(TAG, "url_chef>>>>>" + url);


        Ion.with(context)
                .load(url)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                swipListView.onLoadMoreComplete();
                                if (response.pagination.page_no != null && response.pagination.page_no.length() > 0) {
                                    page_number = Integer.parseInt(response.pagination.page_no);
                                }
                                if (response.pagination.max_page_size != null && response.pagination.max_page_size.length() > 0) {
                                    maxPages = Integer.parseInt(response.pagination.max_page_size);
                                }
                                if (response.product != null && response.product.size() > 0) {

                                    if (page_number == 1) {
                                        list.clear();
                                    }
                                    list.addAll(response.product);
                                    adapter.notifyDataSetChanged();
                                }
                                adapter.notifyDataSetChanged();
                                page_number = page_number + 1;


                            } else {
                                CommonUtils.showToast(context, response.response_message);
                            }
                        } else {
                            Log.e(TAG, " error message  " + e.toString());
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });


    }

    public void getIDs() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        //tvAddNewMember = (TextView) findViewById(R.id.tvAddNewMember);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        tvAddProduct = (TextView) findViewById(R.id.tvAddProduct);
        tvAddProduct.setVisibility(View.VISIBLE);
        swipListView = (SwipListView) findViewById(R.id.lvCreateAForm);
        etSearch = (EditText) findViewById(R.id.etSearch);
        list = new ArrayList<Product>();

    }

    public void setListeners() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvAddProduct.setOnClickListener(this);
    }

    private void makeNonEditable() {
        isEditable = false;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isEditable) {
            makeNonEditable();
            finish();
        }
    }

    private void makeEditable() {
        isEditable = true;

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                if (isEditable) {
                    makeNonEditable();
                } else if (!isEditable) {
                    finish();
                }
                break;
            case R.id.ivImageRight:
                CommonUtils.hideSoftKeyboard(this);
                finishAffinity();
                if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;
            case R.id.tvAddProduct:
                intent = new Intent(this, AddProductIngredientList.class)
                        .putExtra("ID", ID)
                        .putExtra("productAddedFor", productAddedFor)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
    }

}
