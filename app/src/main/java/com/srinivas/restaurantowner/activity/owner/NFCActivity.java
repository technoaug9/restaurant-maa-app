package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.activity.chef.NfcChip;
import com.srinivas.restaurantowner.activity.chef.SelectProductFromNfc;
import com.srinivas.restaurantowner.R;

import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.enduser.ManageIngredientsActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;


public class NFCActivity extends Activity implements View.OnClickListener {

    private  TextView tvModifyNfc, tvProgramNfc,tvDeleteNfc,tvReadNfc, tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private Context context;
    private Intent intent;
    private Dialog mDialog;
    private View.OnClickListener ok;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        CommonUtils.hideSoftKeyboard(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);
        context = NFCActivity.this;

        getId();
        setListener();
        mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);

        if(CommonUtils.getPreferencesString(this,AppConstants.NFC).equalsIgnoreCase("NFC"))
        {
            CommonUtils.showCustomDialogOk(this, getString(R.string.close_nfc_chef), null, View.GONE);
            ok = new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    intent=new Intent(context, ManageIngredientsActivity.class);
                    startActivity(intent);
                    mDialog.dismiss();
                }
            };
        }
    }

    private void setListener()
    {

        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        tvModifyNfc.setOnClickListener(this);
        tvProgramNfc.setOnClickListener(this);
        tvDeleteNfc.setOnClickListener(this);
        tvReadNfc.setOnClickListener(this);
    }

    private void getId() {
        tvHeader=(TextView) findViewById(R.id.tvHeader);
        tvModifyNfc=(TextView) findViewById(R.id.tvModifyNfc);
        tvProgramNfc=(TextView) findViewById(R.id.tvProgramNfc);
        tvDeleteNfc=(TextView) findViewById(R.id.tvDeleteNfc);
        tvReadNfc=(TextView) findViewById(R.id.tvReadNfc);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);

        tvHeader.setText("NFC");
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;
            case R.id.tvProgramNfc:
                intent = new Intent(this, SelectProductFromNfc.class).putExtra("from","nfc_modify");
                CommonUtils.savePreference(context, AppConstants.IS_NFC, true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            case R.id.tvModifyNfc:
                intent=new Intent(this, NfcChip.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("NFC",AppConstants.NFC);
                startActivity(intent);
                break;
            case R.id.tvDeleteNfc:
                intent=new Intent(this, NfcChip.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("DeleteNfc",AppConstants.DELETENFC);
                startActivity(intent);
                break;
            case R.id.tvReadNfc:
                intent=new Intent(this,NfcChip.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("ReadNfc",AppConstants.READNFC);
                startActivity(intent);
                break;
            case R.id.ivImageRight :
                finishAffinity();
              /*  if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;*/
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
             break;



        }
    }
}
