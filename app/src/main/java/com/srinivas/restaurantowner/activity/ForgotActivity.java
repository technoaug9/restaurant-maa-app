package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
//import com.srinivas.barcodescanner.activity.enduser.VerificationCodeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;


public class ForgotActivity extends Activity implements View.OnClickListener {
    private Button btn_done;
    private Context context;
    private EditText etEmailId;
    private Intent intent;
    private TextView tvDone, tvHeader;
    private ImageView ivImageLeft, ivImageRight;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        context = ForgotActivity.this;
        getID();
        setListeners();

        etEmailId.addTextChangedListener(new TextWatcher()
        {
            public void afterTextChanged(Editable s) {
                if (s.length() == 1 && s.equals(" ")) {
                    etEmailId.setText("");
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (count == 1 && s.toString().matches(" ")) {
                    String result = s.toString().replace(" ", "");
                    etEmailId.setText(result);
                }
                if (count > 0) {
                }
            }
        });
        tvHeader.setText("Forgot Password");
        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setVisibility(View.INVISIBLE);
    }

    private void getID() {

        tvDone = (TextView) findViewById(R.id.tvDone);
        tvHeader = (TextView) findViewById(R.id.tvHeader);

        etEmailId = (EditText) findViewById(R.id.etEmailId);


        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);


    }

    private void setListeners() {

        tvDone.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDone:
                if (doValidation()) {

                    if (CommonUtils.isOnline(context)) {
                        ServiceRequest request = new ServiceRequest();
                        request.email = etEmailId.getText().toString();
                        callForgotPasswordApi(request);


                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
                break;

            case R.id.ivImageLeft:

                finish();

                break;
        }
    }
    private void callForgotPasswordApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Forgot_Password;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        Log.i("Tag", " url " + url);
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data " + new Gson().toJson(response));
                progressDialog.dismiss();
                Log.e("Login", "res = " + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        showAlert();

                    } else {
                        CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                    }
                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }
    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(R.string.access_code_sent_to_your_email)
                .setCancelable(false)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                               // startActivity(new Intent(ForgotActivity.this, VerificationCodeActivity.class));
                                finish();
                            }
                        });

        try {
            AlertDialog alert11 = builder.create();
            alert11.show();
            Button buttonbackground = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
            buttonbackground.setTextColor(context.getResources().getColor(R.color.skyblue));
            buttonbackground.setTextSize(20);
            TextView textView = (TextView) alert11.findViewById(android.R.id.message);
            textView.setTextSize(20);
            textView.setTextColor(context.getResources().getColor(R.color.black));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean doValidation()
    {

        if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter email.", null, View.GONE);

            etEmailId.requestFocus();
            return false;
        } else if (!(CommonUtils.checkEmailId(etEmailId.getText().toString().trim()))) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            etEmailId.requestFocus();
            return false;

        } else {
            return true;
        }
    }

}