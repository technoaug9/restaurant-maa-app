package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.Log;

/**
 * Created by vinay.tripathi on 17/10/15.
 */
public class ModifyScanQrActivity extends Activity implements View.OnClickListener {
    public  static int count;
    Dialog dialog_nfc_modify,dialog_nfc_after_yes_modify,dialog_nfc_after_testchip_modify;
    private TextView tvHeader,tvYesModify,tvNoModify,tvContentsModify,tvSurityStatmentModify;
    private TextView tvPushedCodeModify,tvTestTheChipModify;
    private TextView tvPushedProductNameModify,tvPushedProductCodeModify,tvPushedProductOkModify;
    private Context context;
    private ImageView ivImageLeft, ivImageRight,iv_Scanner;
    private LinearLayout ll_nfcdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_image);
        setIds();
        tvHeader.setText("Scan");
        setListeners();
        if (getIntent().getStringExtra("from")!=null && getIntent().getStringExtra("from").length()>0){
            Log.e("ModifyScanNfcActivity", "<<<<<<>ModifyScanNfcActivity>>>>>>>" + getIntent().getStringExtra("from"));
            count=1;
        }
        else
        {
            count = 0;
        }
        ivImageRight.setVisibility(View.GONE);
        context = ModifyScanQrActivity.this;
    }

    private void setIds() {
        iv_Scanner= (ImageView) findViewById(R.id.iv_Scanner);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        tvHeader=(TextView) findViewById(R.id.tvHeader);

    }
    private void setListeners() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        iv_Scanner.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
            case R.id.ivImageRight:
                startActivity(new Intent(context,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;
            case R.id.iv_Scanner:
                if (count==0)
                {
                    dialog_nfc_modify = CommonUtils.customDialog(this, R.layout.dialog_nfc_modify);
                    getIdDialogNfcModify();
                    setListnerDialogNfcModify();
                }
                else if (count==1){
                    dialog_nfc_after_yes_modify=CommonUtils.customDialog(this, R.layout.dialog_nfc_successfull_pushed_modify);
                    getIdDialogNfcAfterYesModify();
                    setListnerDialogNfcAfterYesModify();
                }

                else {

                    dialog_nfc_after_testchip_modify=CommonUtils.customDialog(this, R.layout.dialog_nfc_pushed_product_modify);
                    getIdDialogNfcAfterTestChipModify();

                    setListnerDialogNfcAfterTestChipModify();

                }
                break;

            //Click event on Nfc Modify Dialog
            case R.id.tvNoModify:
                dialog_nfc_modify.dismiss();

                break;


            case R.id.tvYesModify:
                dialog_nfc_modify.dismiss();

                Intent intent1 = new Intent(this, SelectProductQRModify.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);

                count=1;

                break;

            case R.id.tvTestTheChipModify:
                dialog_nfc_after_yes_modify.dismiss();
                count=2;

                break;

            case R.id.tvPushedProductOkModify:
                dialog_nfc_after_testchip_modify.dismiss();
                Intent intent2 = new Intent(this, QrOwnerActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                count=0;
                break;
        }


    }

    private void getIdDialogNfcAfterTestChipModify() {
        tvPushedProductNameModify=(TextView)dialog_nfc_after_testchip_modify.findViewById(R.id.tvPushedProductNameModify);
        tvPushedProductCodeModify=(TextView)dialog_nfc_after_testchip_modify.findViewById(R.id.tvPushedProductCodeModify);
        tvPushedProductOkModify=(TextView)dialog_nfc_after_testchip_modify.findViewById(R.id.tvPushedProductOkModify);


    }
    private void setListnerDialogNfcAfterTestChipModify() {
        tvPushedProductOkModify.setOnClickListener(this);
    }

    private void getIdDialogNfcAfterYesModify() {
        tvPushedCodeModify=(TextView)dialog_nfc_after_yes_modify.findViewById(R.id.tvPushedCodeModify);
        tvTestTheChipModify=(TextView)dialog_nfc_after_yes_modify.findViewById(R.id.tvTestTheChipModify);
    }
    private void setListnerDialogNfcAfterYesModify()
    {
        tvTestTheChipModify.setOnClickListener(this);
    }
    private void setListnerDialogNfcModify() {
        tvNoModify.setOnClickListener(this);
        tvYesModify.setOnClickListener(this);
    }
    private void getIdDialogNfcModify() {
        tvNoModify=(TextView)dialog_nfc_modify.findViewById(R.id.tvNoModify);
        tvYesModify=(TextView)dialog_nfc_modify.findViewById(R.id.tvYesModify);
        tvContentsModify=(TextView)dialog_nfc_modify.findViewById(R.id.tvContentsModify);
        tvSurityStatmentModify=(TextView)dialog_nfc_modify.findViewById(R.id.tvSurityStatmentModify);
        tvContentsModify.setText("Contents present in the QR\ncode image.");
        tvSurityStatmentModify.setText("Are you sure you want to modify\nthe QR code image?");

    }
}



