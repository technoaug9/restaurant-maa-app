package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;

/**
 * Created by vinay.tripathi on 16/10/15.
 */
public class ProgramScanQrListActivity extends Activity implements View.OnClickListener {


    private ImageView ivImageRight, ivImageLeft;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_qr_activity);

        getId();

    }

    private void getId() {
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
    }

    private void setListener() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageRight:
                startActivity(new Intent(ProgramScanQrListActivity.this,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;
            case R.id.ivImageLeft:
                finish();
                break;
        }
    }
}
