package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;



public class TermPolicyActivity extends Activity implements View.OnClickListener {
    private TextView tvTermCondition, tvHeader;
    private Context context;
    private ImageView ivImageLeft, ivImageRight;
    private String user_type = AppConstants.USER_TYPE_NORMAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_plolicy);
        user_type = getIntent().getStringExtra(AppConstants.USER_TYPE);
        context = TermPolicyActivity.this;
        getID();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
        if (CommonUtils.isOnline(context)) {

            termpolicyApi();


        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }


        ivImageRight.setVisibility(View.GONE);
        if (user_type != null) {
            if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE) != null && !CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(""))
                user_type = CommonUtils.getPreferences(context, AppConstants.USER_TYPE);
            {
                if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_NORMAL)) {
                    ivImageRight.setVisibility(View.VISIBLE);
                } else if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_OWNER)) {
                    ivImageRight.setVisibility(View.VISIBLE);
                } else {
                    ivImageRight.setVisibility(View.VISIBLE);
                }

            }

        }


    }

    private void getID() {
        tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft.setImageResource(R.drawable.back_btn);
        tvHeader.setText("Terms & Policy");
    }
    private void setListeners() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTermCondition:
                break;
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;
            case R.id.ivImageRight:

                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void termpolicyApi() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        Log.e("", "Call Service");

    String Url = RequestURL.Term_Condition;
   //  String Url = "https://barcode-scan.herokuapp.com/terms_conditions";
        Gson json = new Gson();
        JsonObject responsejson = new JsonObject();

        Log.i("Tag", "req data " + new Gson().toJson(responsejson));
        Log.i("Tag", " url " + Url);
        Ion.with(context).load("GET", Url)./*setJsonObjectBody(responsejson).*/as(new TypeToken<ServiceResponse>() {
        }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data " + new Gson().toJson(response));
                if (progressDialog != null)
                {
                    progressDialog.dismiss();
                }
                //Log.e("Login", "res = " + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        tvTermCondition.setText(response.description);


                    } else {
                        CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                    }
                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }


}
