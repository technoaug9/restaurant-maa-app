package com.srinivas.restaurantowner.activity.owner;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.loopj.android.http.AsyncHttpClient;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.AsyncResponse;
import com.srinivas.restaurantowner.Services.CommonResponseHandler;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.chef.NfcOwnerActivity;
import com.srinivas.restaurantowner.activity.chef.QrOwnerActivity;
/*import com.srinivas.barcodescanner.adapter.enduser.FlashTextAdapter;
import com.srinivas.barcodescanner.adapter.enduser.ProductAdapter;
import com.srinivas.barcodescanner.adapter.enduser.SearchAdapter;
import com.srinivas.barcodescanner.adapter.enduser.SearchProductAdapter;*/
import com.srinivas.restaurantowner.adapter.enduser.FlashTextAdapter;
import com.srinivas.restaurantowner.adapter.enduser.SearchAdapter;
import com.srinivas.restaurantowner.adapter.enduser.SearchProductAdapter;
import com.srinivas.restaurantowner.imageutils.CropImage;
import com.srinivas.restaurantowner.imageutils.TakePictureUtils;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

/**
 * Created by vinay.tripathi on 21/9/15.
 */
public class AddProductIngredientList extends Activity implements View.OnClickListener, AsyncResponse {
    private SearchProductAdapter searchProductAdapter;
    private ArrayAdapter<String> dataAdapter;
    private TextView tvSearchProduct, tvHeader, tvUpload, tvSave;
    private EditText et, etProductList;
    private LinearLayout ll_disable;
    //private AutoCompleteTextView etSearch;
    private Intent intent;
    private ListView rvProducts;
    private LinearLayoutManager linearLayoutManager;
    private FlashTextAdapter productAdapter;
    public TextView tvAddIngredients, tvSearchedProduct, tvPreventiveList, tvNFC, tvQR;
    private ImageView ivImageLeft, ivImageRight, ivFoodImage;
    private String imageRealPath;
    private ArrayAdapter adapter;
    private Context context = AddProductIngredientList.this;
    private final String TAG = AddProductIngredientList.class.getSimpleName();
    private View headerView, footerView;
    private IngredientList ingredientListModel;
    private List<IngredientList> ingredientList, ingredientLists;
    private File file;
    private AsyncResponse asyncResponse;
    private String ID = "", productAddedFor = "";

    private ListView lvProducts;
    private EditText etSearchNew;

    private ProgressDialog progressDialog = null;
    private boolean isMatchFound = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_ingredient);
        asyncResponse = this;
        getId();
        setListener();
        tvHeader.setText("Add Product");
        CommonUtils.hideSoftKeyboard(this);
        ivImageRight.setImageResource(R.drawable.save_icon);

        if (getIntent() != null) {
            if (getIntent().hasExtra("ID") && getIntent().getStringExtra("ID").length() > 0) {
                ID = getIntent().getStringExtra("ID");
            }
            if (getIntent().hasExtra("productAddedFor") && getIntent().getStringExtra("productAddedFor").length() > 0) {
                productAddedFor = getIntent().getStringExtra("productAddedFor");
            }
        }


        etSearchNew.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                CommonUtils.hide_keyboard(AddProductIngredientList.this);
                android.util.Log.e(TAG, "searched");
                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    request.ingredient = etSearchNew.getText().toString().trim();
                    searchIngredientsList(request);

                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }
                android.util.Log.e(TAG, "searched_inside");

                return true;
            }
        });

        etSearchNew.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lvProducts.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ingredientList = new ArrayList<>();
        ingredientLists = new ArrayList<>();

        lvProducts.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        lvProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e(TAG, "itemSelected" + adapterView.getItemAtPosition(i));
                ingredientListModel = new IngredientList();
                CommonUtils.hide_keyboard(AddProductIngredientList.this);
                etSearchNew.setText("");
              /*  for (int j = 0; i < ingredientList.size(); j++) {
                    if (String.valueOf(adapterView.getItemAtPosition(i)).equalsIgnoreCase(ingredientList.get(j).ingredient_name)) {
                        ingredientListModel.ingredient_name = ingredientList.get(j).ingredient_name;
                        ingredientListModel.id = ingredientList.get(j).id;
                        ingredientLists.add(ingredientListModel);
                        break;
                    }
                }*/

                ingredientListModel.ingredient_name = ingredientList.get(i).ingredient_name;
                ingredientListModel.id = ingredientList.get(i).id;
                ingredientLists.add(ingredientListModel);

                productAdapter = new FlashTextAdapter(context, ingredientLists);
                rvProducts.setAdapter(productAdapter);
                productAdapter.notifyDataSetChanged();


            }
        });

    }


    private void searchIngredientsList(ServiceRequest request) {

        try {
            progressDialog = ProgressDialog.show(context, "", "Please wait...");
        } catch (Exception e) {
            e.printStackTrace();
        }


        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));
        String url = RequestURL.Search_Ingredients;
        Log.e(TAG, "URL>>>>>>" + url);
        Ion.with(context)
                .load(url)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }

                        if (response != null) {
                            Log.e(TAG, " response data " + new Gson().toJson(response));
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                List<String> list;
                                if (response.ingredient_list != null) {
                                    list = new ArrayList<String>();

                                    if (response.ingredient_list.size() > 0) {
                                        lvProducts.setVisibility(View.VISIBLE);
                                        ingredientList.clear();
                                        ingredientList = response.ingredient_list;
                                       /* for (int i = 0; i < response.ingredient_list.size(); i++) {
                                            list.add(response.ingredient_list.get(i).ingredient_name);
                                        }*/

                                        for (int i = 0; i < response.ingredient_list.size(); i++) {
                                            if (etSearchNew.getText().toString().trim().equalsIgnoreCase(response.ingredient_list.get(i).ingredient_name)) {
                                                SearchAdapter searchAdapter = new SearchAdapter(context, response.ingredient_list, true, i);
                                                lvProducts.setAdapter(searchAdapter);
                                                lvProducts.setSelection(i);
                                                isMatchFound = true;
                                                break;
                                            }
                                        }

                                        if (!isMatchFound) {
                                            SearchAdapter searchAdapter = new SearchAdapter(context, response.ingredient_list);
                                            lvProducts.setAdapter(searchAdapter);
                                        }

                                        isMatchFound = false;


                                       /* adapter = new ArrayAdapter(context, R.layout.row_list_product, R.id.textView1, list);
                                        lvProducts.setAdapter(adapter);*/
                                        //etSearch.setThreshold(1);
                                    } else {
                                        lvProducts.setVisibility(View.GONE);
                                        CommonUtils.showToast(context, "The ingredient you are looking for is not available.");
                                    }


                                }

                            } else {
                                CommonUtils.showToast(context, response.response_message);
                            }

                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }


    private void getId() {

        headerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.add_product_ingredient_list_header, null, false);
        footerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.add_product_ingredient_list_footer, null, false);

        rvProducts = (ListView) findViewById(R.id.rvProducts_list);

        rvProducts.addHeaderView(headerView);
        rvProducts.addFooterView(footerView);
        rvProducts.setAdapter(null);

        //etSearch = (AutoCompleteTextView) headerView.findViewById(R.id.etSearch);
        tvUpload = (TextView) headerView.findViewById(R.id.tvUpload);
        etProductList = (EditText) headerView.findViewById(R.id.etProductList);
        ivFoodImage = (ImageView) headerView.findViewById(R.id.ivFoodImage);

        etSearchNew = (EditText) headerView.findViewById(R.id.etSearchNew);
        lvProducts = (ListView) headerView.findViewById(R.id.lvProducts);

        tvSave = (TextView) footerView.findViewById(R.id.tvSave);
        tvNFC = (TextView) footerView.findViewById(R.id.tvNFC);
        tvQR = (TextView) footerView.findViewById(R.id.tvQR);


        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvAddIngredients = (TextView) findViewById(R.id.tvAddIngredients);

    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvUpload.setOnClickListener(this);
        tvQR.setOnClickListener(this);
        tvNFC.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;

            case R.id.ivImageRight:

                if (isValidate() && ID.length() > 0) {
                    if (CommonUtils.isOnline(context)) {

                        if (file == null) {
                            addProduct();
                        } else {
                            new MultiPartAsync().execute();
                        }


                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
                break;

            case R.id.tvAddIngredients:
                finish();
                break;
            case R.id.tvUpload:
                if (isAllPermissionGranted()) {
                    android.util.Log.e("Permission granted: ", "Button Click performed Action");
                    CommonUtils.showcameradialogs(AddProductIngredientList.this);
                } else {
                    if (ContextCompat.checkSelfPermission(AddProductIngredientList.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddProductIngredientList.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    } else if (ContextCompat.checkSelfPermission(AddProductIngredientList.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddProductIngredientList.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }
                break;
            case R.id.tvSave:
                intent = new Intent(this, AddProductActivity.class);
                startActivity(intent);
                break;
            case R.id.tvNFC:
                intent = new Intent(this, NfcOwnerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvQR:
                intent = new Intent(this, QrOwnerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ;
                startActivity(intent);
                break;
        }

    }

    /**
     * Code for Marshmallow permission check
     *
     * @return
     */

    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(AddProductIngredientList.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(AddProductIngredientList.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AddProductIngredientList.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(AddProductIngredientList.this);
                    android.util.Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(AddProductIngredientList.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(AddProductIngredientList.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(AddProductIngredientList.this);
                    android.util.Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(AddProductIngredientList.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    private class MultiPartAsync extends AsyncTask<Void, Void, String> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Please wait...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            String responseMessage = "";
            try {
                responseMessage = addProductWithMultiPartApi();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseMessage;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            Toast.makeText(context, aVoid, Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    private String addProductWithMultiPartApi() throws Exception {

        String responseMessage = "";
        try {

            HttpClient httpClient = new DefaultHttpClient();

            String url = RequestURL.PRODUCTS;
            Log.e(TAG, "url>>>>" + url);
            HttpPost postRequest = new HttpPost(url);

            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();

            reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            Bitmap bm = BitmapFactory.decodeFile(file.getPath());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            Log.e(TAG, "encodedImage" + encodedImage);
            String key = "";

            key = productAddedFor;
            Log.e(TAG,"userid"+CommonUtils.getPreferences(context,AppConstants.USER_ID));
            if (key.equalsIgnoreCase("user_id")) {

                reqEntity.addTextBody("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
                reqEntity.addTextBody("image", encodedImage);
                reqEntity.addTextBody("name", etProductList.getText().toString().trim());
                reqEntity.addTextBody("qr_code", "");
                reqEntity.addTextBody("nfc_code", "");
                reqEntity.addTextBody("user_type",CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE));
                Log.e("Add Product Req Data", " User Id :" +CommonUtils.getPreferencesString(context, AppConstants.USER_ID +" image :") +encodedImage + "  name :" +etProductList.getText().toString().trim() + "  qr_code :" +"" +"  nfc_code" + "");
                List<String> listIds = new ArrayList<>();
                listIds.clear();
                for (int i = 0; i < ingredientLists.size(); i++) {
                    listIds.add(ingredientLists.get(i).id);

                }
                Log.e("List ids",listIds.toString());


                for (String mBusinessID : listIds) {
                    reqEntity.addPart("product_ingredients_attributes[]", new StringBody(mBusinessID));
                }
                JSONObject object = new JSONObject();

                Log.e("Add Product Req Data", " User Id :" +CommonUtils.getPreferencesString(context, AppConstants.USER_ID +" image :") +encodedImage + "  name :" +etProductList.getText().toString().trim() + "  qr_code :" +"" +"  nfc_code" + "");
                Log.e("ingredieants", object.toString());
            } else {
                reqEntity.addTextBody("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
                reqEntity.addTextBody("chef_id", ID);
                reqEntity.addTextBody("image", encodedImage);
                reqEntity.addTextBody("name", etProductList.getText().toString().trim());
                reqEntity.addTextBody("qr_code", "");
                reqEntity.addTextBody("nfc_code", "");
                Log.e("Add Product Req Data", " User Id :" +CommonUtils.getPreferencesString(context, AppConstants.USER_ID +" image :") +encodedImage + "  name :" +etProductList.getText().toString().trim() + "  qr_code :" +"" +"  nfc_code" + "");
                // Prepare Category Array

                List<String> listIds = new ArrayList<>();
                listIds.clear();
                for (int i = 0; i < ingredientLists.size(); i++) {
                    listIds.add(ingredientLists.get(i).id);
                }

                Log.e("List ids",listIds.toString());
                JSONObject object = new JSONObject();
                for (String mBusinessID : listIds) {
                    reqEntity.addPart("product_ingredients_attributes[]", new StringBody(mBusinessID));
                    object.put("product_ingredients_attributes[]", new StringBody(mBusinessID));
                }
                Log.e("ingredieants", object.toString());
                Log.e("Add Product Req Data", " User Id :" +CommonUtils.getPreferencesString(context, AppConstants.USER_ID +" image :") +encodedImage + "  name :" +etProductList.getText().toString().trim() + "  qr_code :" +"" +"  nfc_code" + "");
            }


            HttpEntity httpEntity = reqEntity.build();
            postRequest.setEntity(httpEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder mStringBuilder = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                mStringBuilder = mStringBuilder.append(sResponse);
            }

            android.util.Log.e(TAG, "asyncResponse" + mStringBuilder.toString());

            JSONObject jsonObject = new JSONObject((mStringBuilder.toString()));
            if (jsonObject.has("response_message")) {
                Log.e(TAG, "response_message>>>>" + jsonObject.getString("response_message"));
                responseMessage = jsonObject.getString("response_message");
            }
            if (jsonObject.has("response_code")) {
                Log.e(TAG, "response_code>>>>" + jsonObject.getString("response_code"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e(e.getClass().getName(), e.getMessage());
        }

        return responseMessage;
    }


    private void addProduct() {

        List<String> listIds = new ArrayList<>();
        for (int i = 0; i < ingredientLists.size(); i++) {
            listIds.add(ingredientLists.get(i).id);
        }

        try {

            String key = "";
            key = productAddedFor;

            JSONArray jsonArray = new JSONArray(listIds);

            Log.e(TAG, "JSON Array :" + jsonArray);
            JSONObject jsonObject = new JSONObject();

            if (key.equalsIgnoreCase("user_id")) {
                jsonObject.put("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
                jsonObject.put("name", etProductList.getText().toString().trim());
                jsonObject.put("qr_code", "");
                jsonObject.put("nfc_code", "");
                jsonObject.put("image", "");
                jsonObject.put("user_type", CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE));
                jsonObject.put("product_ingredients_attributes", jsonArray);
            } else {
                jsonObject.put("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
                jsonObject.put("chef_id", ID);
                jsonObject.put("name", etProductList.getText().toString().trim());
                jsonObject.put("qr_code", "");
                jsonObject.put("nfc_code", "");
                jsonObject.put("image", "");
                jsonObject.put("user_type", CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE));
                jsonObject.put("product_ingredients_attributes", jsonArray);
            }


            Log.e("Request", "The requeset of login web service is " + jsonObject.toString());
            StringEntity req = new StringEntity(jsonObject.toString(), "UTF-8");
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(30000);
            client.post(context, RequestURL.PRODUCTS, req, RequestURL.APPLICATION_JSON,
                    new CommonResponseHandler(context, asyncResponse, RequestURL.PRODUCTS));
        } catch (Exception e) {

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case TakePictureUtils.PICK_GALLERY:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), CommonUtils.imageNameLocal + ".png"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        TakePictureUtils.startCropImage(AddProductIngredientList.this, CommonUtils.imageNameLocal + ".png");
                    } catch (Exception e) {
                        Toast.makeText(AddProductIngredientList.this, "Error in picture", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage(AddProductIngredientList.this, CommonUtils.imageNameLocal + ".png");
                    break;
                case TakePictureUtils.CROP_FROM_CAMERA:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    file = new File(path);
                    ivFoodImage.setImageBitmap(BitmapFactory.decodeFile(path));
                    break;
            }

        }
    }

    @Override
    public void onSuccess(JSONObject response, String message) {
        Log.e(TAG, "asyncResponse>>>>>" + response);
        CommonUtils.showToast(context, "Product has been added successfully");
        finish();
    }

    @Override
    public void onEmptyResponse(JSONObject response, String apiName) {
    }

    @Override
    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, Throwable throwable, JSONObject errorResponse) {
    }

    @Override
    public void onFinish() {

    }


    private boolean isValidate() {

        if (etProductList.getText().toString().trim().length() == 0) {
            CommonUtils.showToast(context, "Please enter product name");
            etProductList.requestFocus();
            return false;
        } else if (ingredientLists.size() == 0) {
            CommonUtils.showToast(context, "Please select ingredients");
            etSearchNew.requestFocus();
            return false;
        } else {
            return true;
        }
    }
}
