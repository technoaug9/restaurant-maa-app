package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import com.srinivas.restaurantowner.activity.owner.NFCActivity;
import com.srinivas.restaurantowner.activity.owner.QRActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import static android.view.View.OnClickListener;
/**
 * Created by nidhi on 9/10/15.
 */
public class QrScanner extends Activity implements OnClickListener
{
    private Context mContext;
    private Intent intent;
    private View view;
    private ImageView iv_Scanner, ivImageRight, ivImageLeft;
    private TextView tvHeader;
    private String from = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_image);
        mContext = QrScanner.this;
        getIds();
        setListeners();
        tvHeader.setText("Scan");
        ivImageRight.setVisibility(View.INVISIBLE);
        if (getIntent() != null) {
            if (getIntent().getStringExtra("Noproduct") != null)
            {
                showCustomDialogNoproduct(QrScanner.this);
            } else if (getIntent().getStringExtra("Contentpresent") != null)
            {
                showCustomDialogNFC(mContext);

            }else if (getIntent().getStringExtra("from") != null){
                qrscannerdialog(QrScanner.this);
            }
            else if (getIntent().getStringExtra("navigateQractivity") != null) {

            } else if (getIntent().getStringExtra("qrscannerscreen") != null) {

            } else if (getIntent().getStringExtra("QRnavigation") != null) {

                Log.e("Tag", ">>>>>>>>>>>>>>>>" + getIntent().getStringExtra("Noproduct"));

            } else if (getIntent().getStringExtra("MODIFYQR") != null) {

            } else if (getIntent().getStringExtra("TVdeleteqr") != null) {

            } else if (getIntent().getStringExtra("tvREADQR") != null) {

            } else if (getIntent().getStringExtra("addnfcscanner") != null) {

            } else if (getIntent().getStringExtra("noproduct") != null) {


            } else if (getIntent().getStringExtra("modifydialog") != null) {

                showCustomDialogmodifyNfc(QrScanner.this);
                Log.e("nfcvalue", "*************8");

            } else if (getIntent().getStringExtra("deletedialofnfc") != null) {

                showCustomDialogmodifyNfc(QrScanner.this);

            } else if (getIntent().getStringExtra("Nfcchipdismiss") != null) {

                showCustomDialogmodifyNfc(QrScanner.this);

            } else if (getIntent().getStringExtra("deletedialofnfc") != null) {

                showCustomDialogmodifyNfc(QrScanner.this);

            } else if (getIntent().getStringExtra("againnfcchip") != null) {
                showCustomDialog(QrScanner.this);
            }
        } else {
            showCustomDialog(QrScanner.this);
        }

    }

    private void qrscannerdialog(QrScanner qrScanner) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.chip_contains_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);
        TextView tvmsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvmodigymsg = (TextView) mDialog.findViewById(R.id.tvmodigymsg);
        tvmsg.setText(mContext.getString(R.string.qr_code_contains));
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(mContext, QRActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(intent);

                mDialog.dismiss();


            }
        });


        mDialog.show();

    }

    private void showCustomDialogNoproduct(QrScanner QrScanner) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);

        tvMsg.setText("No Product Found");
        tvCode.setVisibility(View.GONE);


        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);

        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);
        tvTestchip.setVisibility(View.GONE);

        tvOk.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (getIntent().getStringExtra("noproduct") != null)
                {
                    intent = new Intent(QrScanner.this, QRActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                else
                {
                    intent = new Intent(QrScanner.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();


    }

    private void setListeners()
    {
        iv_Scanner.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    private void getIds()
    {
        iv_Scanner = (ImageView) findViewById(R.id.iv_Scanner);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
    }

    private void showCustomDialogNFC(final Context mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.chip_contains_dialog, null);


        mDialog.setContentView(dialoglayout);

        TextView tvOk = (TextView) dialoglayout.findViewById(R.id.tvOk);
        TextView tvYes = (TextView) dialoglayout.findViewById(R.id.tvYes);
        TextView tvmodigymsg = (TextView) dialoglayout.findViewById(R.id.tvmodigymsg);

        TextView tvMsg = (TextView) dialoglayout.findViewById(R.id.tvMsg);
        tvYes.setVisibility(View.VISIBLE);
        tvmodigymsg.setVisibility(View.VISIBLE);

        tvMsg.setText("Contents present  in the NFC chip");
        tvOk.setText("Modify");
        tvYes.setText("Delete");

        if (getIntent().getStringExtra("QRnavigation") != null) {
            Log.e("STRING", "************ QRACTIVITY");
            tvmodigymsg.setVisibility(View.VISIBLE);
            tvMsg.setText(getString(R.string.contents_present_qr_code));
            tvmodigymsg.setText(getString(R.string.are_you_sure_qr_code_image));
            tvOk.setText("No");
            tvYes.setText("Yes");
            tvOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intentQrActivity = new Intent(QrScanner.this, QRActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentQrActivity);
                    mDialog.dismiss();
                }
            });


            tvYes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentQrActivity = new Intent(QrScanner.this, SelectProductFromNfc.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("from","nfc_modify");
                    CommonUtils.savePreference(QrScanner.this, AppConstants.IS_QRSCANNER, true);
                    startActivity(intentQrActivity);
                    mDialog.dismiss();
                }
            });
        } else if (getIntent().getStringExtra("tvREADQR") != null) {
            tvMsg.setText("Contents present  in the QR code.");
            tvOk.setText("Modify");
            tvYes.setText("Delete");
            tvmodigymsg.setText("Do you want to modify or delete\nthe QR code image from here?");
            tvOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("QRnavigation", AppConstants.QRSCREENNAVI);
                    startActivity(intent);
                    mDialog.dismiss();

                }
            });
            tvYes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("TVdeleteqr", AppConstants.TVDELETEQR);
                    startActivity(intent);
                    mDialog.dismiss();
                }
            });


        } else if (getIntent().getStringExtra("TVdeleteqr") != null) {
            tvMsg.setText("Contents present  in the QR code.");
            tvmodigymsg.setText("Are you sure you want to delete\nthe product from the\nQR code image?");
            tvOk.setText("No");
            tvYes.setText("Yes");
            tvOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, QRActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    mDialog.dismiss();

                }
            });
            tvYes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  if(getIntent()!=null&&getIntent().getStringExtra("TVdeleteqr")!=null){
                    showCustomDialog(QrScanner.this);
                    mDialog.dismiss();

                }//}
            });
        } else {
            tvmodigymsg.setText("Do you want to modify or delete the NFC chip from here?");

            tvOk.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    ;
                    intent.putExtra("modifydialog", AppConstants.MODIFY);
                    startActivity(intent);
                    mDialog.dismiss();

                /*    intent = new Intent(QrScanner.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    mDialog.dismiss();
*/
                }
            });
            tvYes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    ;
                    intent.putExtra("deletedialofnfc", AppConstants.DELETEDIALOFNFC);
                    startActivity(intent);
                    mDialog.dismiss();

               /*     intent = new Intent(QrScanner.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                   mDialog.dismiss();*/
                }
            });
        }
        mDialog.show();


    }

    private void showCustomDialog(final QrScanner mContext) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
     /*   if(getIntent()!=null&&getIntent().getStringExtra("Noproduct")!=null){
            tvTestchip.setVisibility(View.GONE);

            tvMsg.setText("No Product Found");
            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent=new Intent(QrScannerActivity.this, QRActivity.class);
                    startActivity(intent);
                }
            });
            mDialog.show();
        }*/
       /* if ( getIntent().getStringExtra("Nfcchipdismiss") != null) {
            Log.e("","*************888");
            tvOk.setVisibility(View.VISIBLE);
        }*/
        if (getIntent().getStringExtra("noproduct") != null) {
            tvMsg.setText("No Product Found");
            tvTestchip.setVisibility(View.GONE);
            tvCode.setVisibility(View.GONE);

            tvOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, QRActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
        }
        if (getIntent().getStringExtra("TVdeleteqr") != null) {
            tvMsg.setText("Successfully deleted..");
            tvOk.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            tvCode.setVisibility(View.GONE);
            tvTestchip.setText("Test the QR");
            tvTestchip.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(mContext, QrScanner.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("noproduct", "noproductfound");
                    startActivity(intent);

                }
            });
        } else {
            tvMsg.setText("Successfully deleted..");
            tvCode.setVisibility(View.GONE);
            tvOk.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
           /* tvOk.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });*/
            tvTestchip.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, NfcChip.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("QrScanner", AppConstants.QRSCANNER);
                    startActivity(intent);
                }
            });

        }

        mDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
         /*   case R.id.ivImageRight:
                finishAffinity();
                if (CommonUtils.getPreferences(mContext, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(mContext, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;*/

            case R.id.iv_Scanner:

                if (getIntent().getStringExtra("QRnavigation") != null) {
                    showCustomDialogNFC(mContext);

                } else if (getIntent().getStringExtra("MODIFYQR") != null) {
                    showCustomDialogNFC(mContext);
                } else if (getIntent().getStringExtra("TVdeleteqr") != null) {
                    showCustomDialogNFC(mContext);

                } else if (getIntent().getStringExtra("noproduct") != null) {
                    showCustomDialogNoproduct(QrScanner.this);
                } else if (getIntent().getStringExtra("tvREADQR") != null) {
                    showCustomDialogNFC(mContext);
                } else if (getIntent().getStringExtra("addnfcscanner") != null) {
                    qrscannerdialog(QrScanner.this);
                } else
                {
                    intent = new Intent(QrScanner.this, NewQrProgramActivity.class);
                    startActivity(intent);
                }

                break;

        }
    }

    private void showCustomDialogmodifyNfc(QrScanner qrScanner) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.chip_contains_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);
        TextView tvmsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvmodigymsg = (TextView) mDialog.findViewById(R.id.tvmodigymsg);
        tvmsg.setText("Contents present in the NFC chip.");
        tvOk.setText("No");
        tvmodigymsg.setVisibility(View.VISIBLE);
        // if(getIntent()!=null&&getIntent().getStringExtra("deletedialofnfc")!=null) {
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*     intent = new Intent(QrScanner.this, NfcChip.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                   startActivity(intent);*/
                mDialog.dismiss();


            }
        });

        tvYes.setVisibility(View.VISIBLE);
        if (getIntent().getStringExtra("deletedialofnfc") != null) {
            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, NfcChip.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("DeleteNfc", AppConstants.DELETENFC);
                    startActivity(intent);
                    mDialog.dismiss();


                }
            });
            tvYes.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, NfcChip.class);
                    intent.putExtra("nfcchipsvcanner", AppConstants.DELETEDIALOFNFC);
                    startActivity(intent);
                }
            });

        } else {

            tvOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, NFCActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    mDialog.dismiss();


                }
            });

            tvYes.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(QrScanner.this, SelectProductFromNfc.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .putExtra("from","qr_code_modify");
                    startActivity(intent);
                    mDialog.dismiss();

                }
            });
        }
        mDialog.show();
    }

}



