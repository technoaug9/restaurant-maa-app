package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.adapter.owner.ScanResturantAdapter;
import com.srinivas.restaurantowner.model.ScanHistoryModel;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinay.tripathi on 23/9/15.
 */
public class RestaurantScanHistoryActivity extends Activity {
    private RecyclerView rvDemo;
    private Context context;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<ScanHistoryModel> list;
    private TextView tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_history_owner);
        CommonUtils.hideSoftKeyboard(this);
        list = new ArrayList<ScanHistoryModel>();
        context = RestaurantScanHistoryActivity.this;
        rvDemo = (RecyclerView) findViewById(R.id.rvDemo);

        tvHeader = (TextView) findViewById(R.id.tvHeader);

        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);

        tvHeader.setText("Scan History");

        ivImageLeft.setImageResource(R.drawable.back_btn);

        ivImageRight.setVisibility(View.INVISIBLE);

        rvDemo.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        rvDemo.setLayoutManager(mLayoutManager);
        ScanHistoryModel scanHistoryModel;//=new ScanHistoryModel[4];
        for (int i = 0; i < 4; i++) {
            scanHistoryModel = new ScanHistoryModel();
            scanHistoryModel.setTime("1:01 AM ");
            scanHistoryModel.setDate("27-7-15");

            scanHistoryModel.setNameOfProduct("Milk");
            // scanHistoryModel.setScanResult("Safe");
            list.add(scanHistoryModel);

            if (i == 3) {
                scanHistoryModel.setTime("1:01 AM ");
                scanHistoryModel.setDate("27-7-15");

                scanHistoryModel.setNameOfProduct("Milk");
                // scanHistoryModel.setScanResult("UnSafe");
            }
        }
        // specify an adapter (see also next example)
        mAdapter = new ScanResturantAdapter(this, RestaurantScanHistoryActivity.this.list);
        rvDemo.setAdapter(mAdapter);


        ivImageLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
