package com.srinivas.restaurantowner.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.srinivas.restaurantowner.R;
/*import com.srinivas.barcodescanner.activity.enduser.LaserScannerActivity;
import com.srinivas.barcodescanner.activity.enduser.NFCScannerActivity;
import com.srinivas.barcodescanner.activity.enduser.ScanResultsActivity;*/
import com.srinivas.restaurantowner.scannerbarcode.BarcodeCaptureActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class ScanActivity extends Activity implements View.OnClickListener {
    private static final String TAG = ScanActivity.class.getSimpleName();
    private Context mContext;
    private TextView tvBarcodeScanner, tvQRCodeScanner, tvNfcChipScanner, tvLaserScanner, tvHeader;
    private Intent intent;
    private ImageView ivImageLeft, ivImageRight;
    // private String user_type = AppConstants.USER_TYPE_RESTAURANT_OWNER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        mContext = ScanActivity.this;
        getID();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);

        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);
        tvHeader.setText("Scan");
        ivImageRight.setVisibility(View.INVISIBLE);
    }

    private void getID() {

        tvBarcodeScanner = (TextView) findViewById(R.id.tvBarcodeScanner);
        tvQRCodeScanner = (TextView) findViewById(R.id.tvQRCodeScanner);
        tvNfcChipScanner = (TextView) findViewById(R.id.tvNfcChipScanner);
        tvLaserScanner = (TextView) findViewById(R.id.tvLaserScanner);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvNfcChipScanner = (TextView) findViewById(R.id.tvNfcChipScanner);

    }

    private void setListeners() {

        tvQRCodeScanner.setOnClickListener(this);
        tvBarcodeScanner.setOnClickListener(this);
        tvNfcChipScanner.setOnClickListener(this);
        tvLaserScanner.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tvQRCodeScanner:

                try {

                    Intent intent = new Intent(ScanActivity.this, BarcodeCaptureActivity.class);
                    intent.putExtra("value", 1);
                    startActivityForResult(intent, 1);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR:", Toast.LENGTH_LONG).show();
                }


                break;

            case R.id.tvBarcodeScanner:

                try {

                    Intent intent = new Intent(ScanActivity.this, BarcodeCaptureActivity.class);
                    intent.putExtra("value", 1);
                    startActivityForResult(intent, 1);


                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR:", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.tvNfcChipScanner:
               /* intent = new Intent(this, NFCScannerActivity.class);
                startActivity(intent);*/
                break;

            case R.id.tvLaserScanner:
                //  Toast.makeText(this, "Work in progress", Toast.LENGTH_SHORT).show();

                showCustomDialog(ScanActivity.this);

                break;

            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();

                break;

            case R.id.ivImageRight:

                finishAffinity();

                startActivity(new Intent(this, HomeActivity.class));

                break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && !barCodeValue.equalsIgnoreCase("")) {
                   /* intent = new Intent(this, ScanResultsActivity.class);
                    intent.putExtra("upc", barCodeValue);
                    intent.putExtra("codeType", "barcode");
                    startActivity(intent);
                    Log.e(TAG, "barcode_value>>>>>" + barCodeValue);
                    Toast.makeText(ScanActivity.this, " Scan Successfully", Toast.LENGTH_SHORT).show();*/
                } else {
                    Toast.makeText(ScanActivity.this, "Not Scanned Successfully", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }


        }


    }


    public void showCustomDialog(final Context mContext) {

        final Dialog mDialog = new Dialog(mContext,
                android.R.style.Theme_Translucent_NoTitleBar);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.dialog_alertlogout, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvNo = (TextView) mDialog.findViewById(R.id.tvNo);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);
        TextView tvAlert = (TextView) mDialog.findViewById(R.id.tvAlert);

        tvMsg.setText("Have you paired laser device?");

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                /*intent = new Intent(ScanActivity.this, LaserScannerActivity.class);
                startActivity(intent);*/
            }
        });

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }
        });

        mDialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(isAllPermissionGranted()){
            Log.e("Permission granted: ","Button performed Action");
           // CommonUtils.showcameradialogs(ScanActivity.this);
        }else{
            if(ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(ScanActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }else if(ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(ScanActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        2);
            }
        }
    }

    /**
     * Code for Marshmallow permission check
     * @return
     */
    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                  //  CommonUtils.showcameradialogs(ScanActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(ScanActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                   // CommonUtils.showcameradialogs(ScanActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(ScanActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }
}
