package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.Log;

/**
 * Created by vinay.tripathi on 16/10/15.
 */
public class ProgramScanQrActivity extends Activity implements View.OnClickListener {

    private Context context;
    private Intent intent;
    private View view;
    private ImageView iv_Scanner, ivImageRight, ivImageLeft;
    private TextView tvHeader,tvOk,tvMsg;
     private Dialog dialog_nfc_second;
    int conut;


    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_image);
        context = ProgramScanQrActivity.this;
        getIds();
        setListeners();
        conut=0;
        if (getIntent().getStringExtra("from")!=null && getIntent().getStringExtra("from").length()>0){
            Log.e("ModifyScanNfcActivity", "<<<<<<>ModifyScanNfcActivity>>>>>>>" + getIntent().getStringExtra("from"));
            conut=1;
        }
        tvHeader.setText("Scan");
        ivImageRight.setVisibility(View.INVISIBLE);
    }

    private void setListeners() {
        iv_Scanner.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    private void getIds()
    {
        iv_Scanner = (ImageView) findViewById(R.id.iv_Scanner);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
            case R.id.tvOk:
                dialog_nfc_second.dismiss();
                Intent intent1 = new Intent(this, QrOwnerActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                //dialog_nfc_second.dismiss();
                break;
            case R.id.iv_Scanner:
                if(conut==0)
                {
                    intent = new Intent(ProgramScanQrActivity.this, NewQrProgramActivity.class);
                    startActivity(intent);
                    break;
                }
                else
                {
                    dialog_nfc_second= CommonUtils.customDialog(this, R.layout.chip_contains_dialog);
                    getIdDialogNfcSecond();
                    setListnerDialogNfcSecond();
                    conut=0;
                }
        }
    }
    private void getIdDialogNfcSecond()
    {
        tvOk=(TextView)dialog_nfc_second.findViewById(R.id.tvOk);
        tvMsg=(TextView)dialog_nfc_second.findViewById(R.id.tvMsg);
        tvMsg.setText("The QR Code Contains");


    }
    private void setListnerDialogNfcSecond()
    {
        tvOk.setOnClickListener(this);
    }
}