package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.adapter.owner.ScanhistoryOwnerAdapter;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.model.ScanHistoryModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;


public class ScanHistoryOwnerActivity extends Activity implements View.OnClickListener
{
    private  RecyclerView rvDemo;
    private Context context = ScanHistoryOwnerActivity.this;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Product> list;
    private TextView tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private TextView tvTimeDate, tvNameOfProduct, tvScanResult;
    private String user_type = AppConstants.USER_TYPE_GENERALUSER;
    public  String TAG=ScanHistoryOwnerActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_history_owner);
        CommonUtils.hideSoftKeyboard(this);
        if (CommonUtils.isOnline(context))
        {
            callScanHistoryApi();
        } else {
            CommonUtils.showAlert(getString(R.string.please_check_internet), context);
        }
        getID();
        setListener();
        //user_type = getIntent().getStringExtra(AppConstants.USER_TYPE);
        mLayoutManager = new LinearLayoutManager(this);
        rvDemo.setLayoutManager(mLayoutManager);
        ScanHistoryModel scanHistoryModel;//=new ScanHistoryModel[4];
        mAdapter = new ScanhistoryOwnerAdapter(this, ScanHistoryOwnerActivity.this.list);
        rvDemo.setAdapter(mAdapter);
        ivImageLeft.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }
    private void setListener()
    {

        ivImageLeft.setOnClickListener(this);
    }

    private void getID()
    {
        list = new ArrayList<Product>();
        context = ScanHistoryOwnerActivity.this;
        rvDemo = (RecyclerView) findViewById(R.id.rvDemo);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader.setText("Scan History");
        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setVisibility(View.INVISIBLE);
        tvTimeDate = (TextView) findViewById(R.id.tvChefName);
        tvNameOfProduct = (TextView) findViewById(R.id.tvNameOfProduct);
        tvScanResult = (TextView) findViewById(R.id.tvScanResult);
        rvDemo.setHasFixedSize(true);
    }
    private void callScanHistoryApi()
    {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...");
        String id=CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String url= RequestURL.BASE_URL+"barcode/owner_history?user_id="+id+"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);
        Log.e(TAG, "url:>>>>>>>>>" + url);
        Ion.with(context)
                .load(url)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                Log.e("scanowner", "res = " + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.product != null && response.product.size() > 0) {

                            list = response.product;
                            mAdapter = new ScanhistoryOwnerAdapter(ScanHistoryOwnerActivity.this, list);
                            rvDemo.setAdapter(mAdapter);
                        }
                    } else {
                        CommonUtils.showToast(ScanHistoryOwnerActivity.this, response.response_message);
                    }
                } else {
                    Log.e(TAG, " error message  " + e.toString());
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ivImageLeft:
                finish();
                break;
        }
    }
}
