package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.enduser.ScanResultRelativeAdapter;
import com.srinivas.restaurantowner.adapter.enduser.SearchedProductAdapter;
import com.srinivas.restaurantowner.model.MemberResult;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class ScanResultsActivity extends Activity implements View.OnClickListener {
    private static final String TAG = ScanResultsActivity.class.getSimpleName();
    private RecyclerView rvScanResultRelative;
    private LinearLayoutManager linearLayoutManager;
    private Context context = ScanResultsActivity.this;
    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayout llIngredientList;
    private Intent intent;
    private ImageView ivArrow;
    private RecyclerView.Adapter scanResultRelativeAdapter;


    private RecyclerView rvIngredientList;
    private RecyclerView.Adapter ingredientAdapter;

    private Boolean aBoolean = false;
    private TextView tvscanProductName, tvHeader;
    private String strtvscanProductName;
    private LinearLayout llSearchedProduct;
    private String key;
    private ImageView ivImageLeft, ivImageRight, ivUserImg;
    private String ProductActivity, ingredientName = "";
    private List<MemberResult> listMemberResult;
    private String codeType = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_results);
        CommonUtils.hideSoftKeyboard(this);

        getId();
        setListener();

        key = getIntent().getStringExtra(AppConstants.SCAN_RESULT_KEY);
        ProductActivity = getIntent().getStringExtra(AppConstants.SCAN_RESULT_KEY);

        if (getIntent() != null && getIntent().hasExtra("ingredientName")
                && getIntent().getStringExtra("ingredientName").length() > 0) {

            ingredientName = getIntent().getStringExtra("ingredientName");

            if (getIntent().hasExtra("fromAdapter") && getIntent().getStringExtra("fromAdapter").length() > 0) {
                if (ingredientName.length() > 0) {

                    if (CommonUtils.isOnline(context)) {
                        ServiceRequest request = new ServiceRequest();
                        request.upc = ingredientName;
                        request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                        request.code_type = "barcode";
                        request.is_after_history = "true";
                        //request.history_type = "";
                        callScanUPCApi(request);

                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
            } else {
                if (ingredientName.length() > 0) {

                    if (CommonUtils.isOnline(context)) {
                        ServiceRequest request = new ServiceRequest();
                        request.upc = ingredientName;
                        request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                        request.code_type = "barcode";
                        //request.history_type = "";
                        callScanUPCApi(request);

                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
            }


        } else if (getIntent() != null && getIntent().hasExtra("upc") && getIntent().getStringExtra("upc").length() > 0) {

            if (getIntent().hasExtra("codeType") && getIntent().getStringExtra("codeType").length() > 0) {

                ingredientName = getIntent().getStringExtra("upc");

                codeType = getIntent().getStringExtra("codeType");

                Log.e(TAG, "code_type>>>>>>>>" + codeType);

                if (ingredientName.length() > 0) {

                    if (CommonUtils.isOnline(context)) {
                        ServiceRequest request = new ServiceRequest();
                        request.upc = ingredientName;
                        request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                        request.history_type = "scan";
                        request.code_type = codeType;
                        callScanUPCApi(request);

                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
            }
        }


        strtvscanProductName = tvscanProductName.getText().toString().trim();
        System.out.println(strtvscanProductName);
        tvHeader.setText("Scan Results");

        if (key != null) {


            if (key.equalsIgnoreCase(AppConstants.BARCODE_SCAN_KEY)) {

            } else if (key.equalsIgnoreCase(AppConstants.QR_SCAN_KEY)) {
                llSearchedProduct.setVisibility(View.VISIBLE);
                llIngredientList.setVisibility(View.VISIBLE);
                tvHeader.setText("Scan Results");
            } else if (ProductActivity.equalsIgnoreCase(AppConstants.PRODUCT_ACTIVITY_KEY)) {
                tvHeader.setText("Searched Products");
            }
        }

        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);
    }

    private void callScanUPCApi(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait..");
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        String url = RequestURL.Scan_Upc;
        Log.e(TAG, "url >>>>> " + url);
        Log.e(TAG, "requestJson >>>>> " + requestJson);

        Ion.with(context)
                .load(url)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        progressDialog.dismiss();
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                if (response.product_name != null && response.product_name.length() > 0) {

                                    tvscanProductName.setText(response.product_name);
                                }

                                if (response.image != null && response.image.length() > 0) {
                                    ivUserImg.setVisibility(View.VISIBLE);
                                    Picasso.with(context).load(response.image).placeholder(R.drawable.img).into(ivUserImg);
                                } else {
                                    ivUserImg.setVisibility(View.INVISIBLE);
                                }

                                if (response.ingredients != null && response.ingredients.size() > 0) {
                                    ingredientAdapter = new SearchedProductAdapter(getApplication(), response.ingredients);
                                    rvIngredientList.setAdapter(ingredientAdapter);
                                }

                                //listMemberResult

                                MemberResult memberResultModel = new MemberResult();
                                if (response.user_result != null) {

                                    memberResultModel.id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);

                                    if (response.user_result.state != null && response.user_result.state.length() > 0) {
                                        memberResultModel.state = response.user_result.state;
                                    }
                                    if (response.user_result.image != null && response.user_result.image.length() > 0) {
                                        memberResultModel.image = response.user_result.image;
                                    }
                                    if (response.user_result.harmful_ingredients != null) {
                                        memberResultModel.harmful_ingredients = response.user_result.harmful_ingredients;
                                    }
                                    if (response.user_result.name != null && response.user_result.name.length() > 0) {
                                        memberResultModel.name = response.user_result.name;
                                    }

                                    listMemberResult.add(memberResultModel);
                                }

                                if (response.members_result != null && response.members_result.size() > 0) {

                                    listMemberResult.addAll(response.members_result);
                                }

                                scanResultRelativeAdapter = new ScanResultRelativeAdapter(ScanResultsActivity.this, listMemberResult);
                                rvScanResultRelative.setAdapter(scanResultRelativeAdapter);


                            } else {
                                okDialog(ScanResultsActivity.this, response.response_message);
                            }
                        } else {
                            okDialog(ScanResultsActivity.this, getString(R.string.server_error));
                        }
                    }
                });
    }

    private void okDialog(final Context context, String msg) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert_ok, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);

        tvMsg.setText(msg);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();
            }
        });

        mDialog.show();
    }

    private void getId() {

        llSearchedProduct = (LinearLayout) findViewById(R.id.llSearchedProduct);
        rvScanResultRelative = (RecyclerView) findViewById(R.id.rvScanResultRelative);
        llIngredientList = (LinearLayout) findViewById(R.id.llIngredientList);
        rvIngredientList = (RecyclerView) findViewById(R.id.rvIngredientList);
        tvscanProductName = (TextView) findViewById(R.id.tvscanProductName);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivUserImg = (ImageView) findViewById(R.id.ivUserImg);
        ivArrow = (ImageView) findViewById(R.id.ivArrow);

        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvIngredientList.setLayoutManager(linearLayoutManager);

        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvScanResultRelative.setLayoutManager(linearLayoutManager);
        listMemberResult = new ArrayList<>();
    }

    private void setListener() {
        llIngredientList.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivArrow:
                if (!aBoolean) {
                    rvIngredientList.setVisibility(View.VISIBLE);
                    ivArrow.setImageResource(R.drawable.drop_up_icon);
                    aBoolean = true;
                } else {
                    rvIngredientList.setVisibility(View.GONE);
                    ivArrow.setImageResource(R.drawable.drop_icon);
                    aBoolean = false;
                }
                break;

            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();

                break;

            case R.id.ivImageRight:

                finishAffinity();

                startActivity(new Intent(this, HomeActivity.class));

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
