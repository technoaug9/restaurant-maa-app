package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.owner.SelectProductToLoadIntoNfcAdapter;
import com.srinivas.restaurantowner.model.SelectProductToLoadIntoNfcModel;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vivek.pareek on 15/10/15.
 */
public class SelectProductToLoadIntoNfcActivity extends Activity implements View.OnClickListener {
    Activity tvPreventiveList;
    TextView tvHeader;
    private EditText etSearch;
    private ImageView ivImageLeft, ivImageRight;


    String[] strtvProductNames = {"Pizza", "Hotdog", "Burger"};
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public List<SelectProductToLoadIntoNfcModel> selectProductToLoadIntoNfcModelListModelList;
    public Context context=SelectProductToLoadIntoNfcActivity.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.addactivityfromnfc);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        selectProductToLoadIntoNfcModelListModelList = getSelectProductToLoadIntoNfcModeldata();
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter =new SelectProductToLoadIntoNfcAdapter(this,selectProductToLoadIntoNfcModelListModelList);
        mRecyclerView.setAdapter(mAdapter);




    }

    private void getId() {
        tvHeader=(TextView) findViewById(R.id.tvHeader);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        etSearch=(EditText) findViewById(R.id.etSearch);
        mRecyclerView = (RecyclerView)findViewById(R.id.rvOwner);
        tvHeader.setText("Select a product to load into NFC");
        tvHeader.setTextSize(15);
        ivImageRight.setImageResource(R.drawable.home_btn);

    }

    private void setListener()
    {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    public List<SelectProductToLoadIntoNfcModel> getSelectProductToLoadIntoNfcModeldata()
    {
        selectProductToLoadIntoNfcModelListModelList = new ArrayList<SelectProductToLoadIntoNfcModel>();
        for (int i = 0; i < strtvProductNames.length; i++)
        {
            SelectProductToLoadIntoNfcModel selectProductToLoadIntoNfcModel = new SelectProductToLoadIntoNfcModel();

            selectProductToLoadIntoNfcModel.setTv_ProductsName(strtvProductNames[i]);
            selectProductToLoadIntoNfcModelListModelList.add(selectProductToLoadIntoNfcModel);
        }

        return selectProductToLoadIntoNfcModelListModelList;
    }

    @Override
    public void onClick(View v)
    {

        switch (v.getId()) {
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.ivImageRight:

              /*  finishAffinity();
                if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE)
                        .equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE)
                        .equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }*/

                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
        }
    }
}
