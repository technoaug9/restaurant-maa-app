package com.srinivas.restaurantowner.activity;

import android.app.Activity;

/**
 * Created by vinay.tripathi on 15/10/15.
 */
public abstract class BaseActivity extends Activity {


    abstract public void getIDs();
    abstract public void setListeners();


}
