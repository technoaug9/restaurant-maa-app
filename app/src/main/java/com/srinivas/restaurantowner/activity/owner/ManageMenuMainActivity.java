package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class ManageMenuMainActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private TextView tvChefProduct, tvMyProduct, tvAllProduct, tvHistory;
    // private Intent intent;
    private Context context = ManageMenuMainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_menu_main);
        getId();
        setlistener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("Manage Menu");
        ivImageRight.setVisibility(View.INVISIBLE);
    }

    private void setlistener() {
        ivImageLeft.setOnClickListener(this);
        tvChefProduct.setOnClickListener(this);
        tvMyProduct.setOnClickListener(this);
        tvAllProduct.setOnClickListener(this);
        tvHistory.setOnClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvChefProduct = (TextView) findViewById(R.id.tvChefProduct);
        tvAllProduct = (TextView) findViewById(R.id.tvAllProduct);
        tvHistory = (TextView) findViewById(R.id.tvHistory);
        tvMyProduct = (TextView) findViewById(R.id.tvMyProduct);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manage_menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                startActivity(new Intent(context, HomeActivity.class));
                break;
            case R.id.tvMyProduct:
                startActivity(new Intent(context, AddProductActivity.class)
                        .putExtra("from","ManageMenu")
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP ));
                break;
            case R.id.tvChefProduct:
                startActivity(new Intent(context, ChefListActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP ));
                break;
            case R.id.tvAllProduct:
                startActivity(new Intent(context, AllProductList.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP ));
                break;
            case R.id.tvHistory:
                startActivity(new Intent(context, ScanHistoryOwnerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP ));
                break;
        }
    }
}
