package com.srinivas.restaurantowner.activity.owner;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.imageutils.CropImage;
import com.srinivas.restaurantowner.imageutils.TakePictureUtils;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class OwnerProfileActivity extends Activity implements View.OnClickListener {

    private TextView tvResturantName, tvName, tvEmailId, tvContactNumber, tvPassword, tvConfirmPassword, tvUpload, tvHeader;

    private EditText etRestaurantName, etName, etEmailId, etContactNumber, etPassword, etConfirmPassword;
    public File file;
    private ImageView ivProfileImage, ivImageLeft, ivImageRight;
    private Boolean isEditable = false;
    private LinearLayout llConfirmPassword, llResturantname;
    private String MobilePattern = "[0-9]{10}";
    private String imageRealPath;
    private Context context = OwnerProfileActivity.this;
    private final String TAG = OwnerProfileActivity.class.getSimpleName();
    private String user_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ownerprofile);
        getId();
        setListener();
        makeNonEditable();
        if (user_type != null) {
            if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE) != null && !CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(""))
                user_type = CommonUtils.getPreferences(context, AppConstants.USER_TYPE);
            {
                if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_OWNER)) {
                    ivImageRight.setVisibility(View.VISIBLE);
                } else if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    ivImageRight.setVisibility(View.VISIBLE);
                    llResturantname.setVisibility(View.GONE);
                }
            }
        }
        ivImageRight.setImageResource(R.drawable.edit_icon);
        tvHeader.setText("My Profile");
        if (CommonUtils.isOnline(context)) {
            ServiceRequest request = new ServiceRequest();
             request.user_type=CommonUtils.getPreferences(context, AppConstants.USER_TYPE);
            request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
            myProfileAPI(request);
        } else {
            CommonUtils.showAlert("Please check your internet connection.", context);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private void myProfileAPI(ServiceRequest request) {
        request.user_type=CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.My_Profile;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "URL>>>>>>" + url);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>()

                {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {


                        if (response.user.image != null && response.user.image.length() > 0) {
                            Picasso.with(context).load(response.user.image)
                                    .placeholder(R.drawable.profile_icon).into(ivProfileImage);
                        } else {
                            ivProfileImage.setImageResource(R.drawable.profile_icon);
                        }
                        if (response.user.name != null && response.user.name.length() > 0) {
                            tvName.setText(response.user.name);
                            etName.setText(response.user.name);
                        } else {
                            tvName.setText("");
                            etName.setText("");
                        }


                        if (response.user.restaurant_name != null && response.user.restaurant_name.length() > 0) {
                            tvResturantName.setText(response.user.restaurant_name);
                            etRestaurantName.setText(response.user.restaurant_name);
                        } else {
                            tvResturantName.setText("");
                            etRestaurantName.setText("");
                        }


                        if (response.user.email != null && response.user.email.length() > 0) {
                            tvEmailId.setText(response.user.email);
                            etEmailId.setText(response.user.email);
                        } else {
                            tvEmailId.setText("");
                            etEmailId.setText("");
                        }


                        if (response.user.phone_no != null && response.user.phone_no.length() > 0) {
                            tvContactNumber.setText(response.user.phone_no);
                            etContactNumber.setText(response.user.phone_no);
                        } else {
                            tvContactNumber.setText("");
                            etContactNumber.setText("");
                        }

                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });


    }


    private void getId() {
        tvResturantName = (TextView) findViewById(R.id.tvResturantName);
        tvName = (TextView) findViewById(R.id.tvName);
        tvEmailId = (TextView) findViewById(R.id.tvEmailId);
        tvContactNumber = (TextView) findViewById(R.id.tvContactNumber);
        tvPassword = (TextView) findViewById(R.id.tvPassword);
        tvConfirmPassword = (TextView) findViewById(R.id.tvConfirmPassword);
        tvUpload = (TextView) findViewById(R.id.tvUpload);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        etRestaurantName = (EditText) findViewById(R.id.etRestaurantName);
        etName = (EditText) findViewById(R.id.etName);
        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etContactNumber = (EditText) findViewById(R.id.etContactNumber);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        llConfirmPassword = (LinearLayout) findViewById(R.id.llConfirmPassword);
        llResturantname = (LinearLayout) findViewById(R.id.llResturantname);
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvUpload.setOnClickListener(this);
    }

    private void makeEditable() {
        tvResturantName.setVisibility(View.GONE);
        tvName.setVisibility(View.GONE);
        tvEmailId.setVisibility(View.GONE);
        tvContactNumber.setVisibility(View.GONE);
        tvPassword.setVisibility(View.GONE);
        // tvConfirmPassword.setVisibility(View.GONE);
        etRestaurantName.setVisibility(View.VISIBLE);
        etName.setVisibility(View.VISIBLE);
        etEmailId.setVisibility(View.VISIBLE);
        etContactNumber.setVisibility(View.VISIBLE);
        etPassword.setVisibility(View.VISIBLE);
        //etConfirmPassword.setVisibility(View.VISIBLE);
        ivImageRight.setImageResource(R.drawable.save_icon);
        llConfirmPassword.setVisibility(View.VISIBLE);
        tvUpload.setVisibility(View.VISIBLE);
        isEditable = true;
        ivImageRight.setImageResource(R.drawable.save_icon);


    }

    private void makeNonEditable() {

        CommonUtils.hideSoftKeyboard(this);
        tvResturantName.setVisibility(View.VISIBLE);
        tvName.setVisibility(View.VISIBLE);
        tvEmailId.setVisibility(View.VISIBLE);
        tvContactNumber.setVisibility(View.VISIBLE);
        tvPassword.setVisibility(View.VISIBLE);
        // tvConfirmPassword.setVisibility(View.VISIBLE);
        etRestaurantName.setVisibility(View.GONE);
        etName.setVisibility(View.GONE);
        etEmailId.setVisibility(View.GONE);
        etContactNumber.setVisibility(View.GONE);
        etPassword.setVisibility(View.GONE);
        tvUpload.setVisibility(View.GONE);
        llConfirmPassword.setVisibility(View.GONE);
        // etConfirmPassword.setVisibility(View.GONE);
        isEditable = false;
        ivImageRight.setImageResource(R.drawable.edit_icon);
    }

    @Override
    public void onBackPressed() {
        if (isEditable) {
            makeNonEditable();
        } else if (!isEditable) {
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                if (isEditable) {
                    makeNonEditable();
                } else if (!isEditable) {
                    finish();
                }
                break;
            case R.id.ivImageRight:

                if (!isEditable) {
                    makeEditable();
                } else if (isEditable) {
                    if (checkValidation()) {


                        if (CommonUtils.isOnline(context)) {

                            if (file == null) {
                                //when not upload image  then request image in string
                                ServiceRequest request = new ServiceRequest();
                                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                                request.name = etName.getText().toString().trim();
                                request.image = "";
                                request.dob = "";
                                request.phone_no = etContactNumber.getText().toString().trim();
                                request.restaurant_name = etRestaurantName.getText().toString().trim();
                                request.user_type =  CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE);
                                editProfileAPI(request);
                            } else {
                                // when   upload image then iamge in file(use multipart)
                                editProfileAPI();
                            }
                        } else {
                            CommonUtils.showAlert("Please check your internet connection.", context);
                        }
                        makeNonEditable();
                        //startActivity(new Intent(this,HomeActivity.class));
                    }
                }
                break;
            case R.id.tvUpload:

                if(isAllPermissionGranted()){
                    CommonUtils.showcameradialogs(OwnerProfileActivity.this);
                }else{
                    if(ContextCompat.checkSelfPermission(OwnerProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(OwnerProfileActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    }else if(ContextCompat.checkSelfPermission(OwnerProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(OwnerProfileActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }
                break;
        }
    }


    /**
     * Code for Marshmallow permission
     * @return
     */

    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(OwnerProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(OwnerProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(OwnerProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(OwnerProfileActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(OwnerProfileActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(OwnerProfileActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(OwnerProfileActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(OwnerProfileActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    ///****************Edit Profile API*****************************************///
    //when user donot update his/her profile image
    private void editProfileAPI(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Edit_Profile;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        //   Toast.makeText(context, "Profile Updated", Toast.LENGTH_SHORT).show();

                        if (CommonUtils.isOnline(context)) {
                            ServiceRequest request = new ServiceRequest();
                            request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                            myProfileAPI(request);
                        } else {
                            CommonUtils.showAlert("Please check your internet connection.", context);
                        }

                        makeNonEditable();
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });

    }

    //*******************Edit Profile API***************************//
    // when user updates his/her profile image
    private void editProfileAPI() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Edit_Profile;
        Log.e(TAG, " url " + url);
        Bitmap bm= BitmapFactory.decodeFile(file.getPath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e(TAG,"encodedImage" +encodedImage);
        Ion.with(context)
                .load(url)
                .setMultipartParameter("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID))
                .setMultipartParameter("" +
                        "" +
                        "name", etName.getText().toString().trim())
                .setMultipartParameter("dob", "")
                .setMultipartParameter("phone_no", etContactNumber.getText().toString().trim())
                .setMultipartParameter("restaurant_name", etRestaurantName.getText().toString().trim())
                .setMultipartParameter("user_type", CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE))
                .setMultipartParameter("image", encodedImage)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e(TAG, " response data" + new Gson().toJson(response));

                Log.e(TAG, " Request data  : " + "user_id" +CommonUtils.getPreferencesString(context, AppConstants.USER_ID) + "name" +etName.getText().toString().trim() + "dob" +"" + "phone_no" +etContactNumber.getText().toString().trim() + "restaurant_name" +etRestaurantName.getText().toString().trim()+ "user_type" +CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE) +"image" +encodedImage);
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);

                            if (CommonUtils.isOnline(context)) {
                                ServiceRequest request = new ServiceRequest();
                                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                                myProfileAPI(request);
                            } else {
                                CommonUtils.showAlert("Please check your internet connection.", context);
                            }

                            makeNonEditable();
                        }
                    }
                } else {
                    Toast.makeText(context, "no response", Toast.LENGTH_SHORT).show();
                    if (e != null) {

                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });


    }


    private boolean checkValidation() {
        /*if (etRestaurantName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter restaurant name.", null, View.GONE);
            etRestaurantName.requestFocus();
            return false;
        }*/
        if (etName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter name.", null, View.GONE);
            etName.requestFocus();
            return false;
        } else if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter email.", null, View.GONE);

            return false;
        } else if (!CommonUtils.checkEmailId(etEmailId.getText().toString())) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            return false;
        } else if (etContactNumber.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter contact number.", null, View.GONE);
            return false;
        } else if (!etContactNumber.getText().toString().matches(MobilePattern)) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid 10 digit phone number", null, View.GONE);
            return false;
        }
            /* else if (etPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter password.", null, View.GONE);
            etPassword.requestFocus();
            return false;
        } else if (etPassword.getText().toString().trim().length() < 8) {
            CommonUtils.showCustomDialogOk(this, "Password must be of 8 characters.", null, View.GONE);
            etPassword.requestFocus();
            //etPassword.setText("");
            return false;
        }   else if (etConfirmPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter confirm password.", null, View.GONE);
            etConfirmPassword.requestFocus();

            return false;
        }   else if (!etPassword.getText().toString().trim().equals(etConfirmPassword.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "New password and confirm password does not matches.", null, View.GONE);
            etConfirmPassword.requestFocus();
            //etConfirmPassword.setText("");
            return false;
        }*/
        return true;
    }


    private boolean checkEmailId(String emailId) {
        return Patterns.EMAIL_ADDRESS.matcher(emailId).matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case TakePictureUtils.PICK_GALLERY:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), CommonUtils.imageNameLocal + ".png"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        TakePictureUtils.startCropImage(OwnerProfileActivity.this, CommonUtils.imageNameLocal + ".png");

                    } catch (Exception e) {

                        Toast.makeText(OwnerProfileActivity.this, "Error in picture", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage(OwnerProfileActivity.this, CommonUtils.imageNameLocal + ".png");
                    break;

                case TakePictureUtils.CROP_FROM_CAMERA:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    imageRealPath = path;
                    Log.e("Image Real Path", imageRealPath);

                    file = new File(imageRealPath);
                    //Picasso.with(OwnerProfileActivity.this).load(new File(path)).into(ivProfileImage);
                    ivProfileImage.setImageBitmap(BitmapFactory.decodeFile(imageRealPath));
                    break;
            }

        }
    }


}
