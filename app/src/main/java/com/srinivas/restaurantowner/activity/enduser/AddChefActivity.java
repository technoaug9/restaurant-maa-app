package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.utils.EndlessRecyclerOnScrollListener;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.ChefList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.owner.ChefRegistrationActivity;
import com.srinivas.restaurantowner.adapter.enduser.AddChefAdapter;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class AddChefActivity extends Activity implements View.OnClickListener {
    private Context context;
    private TextView tvHeader, tvAddChef;
    private ImageView ivImageRight, ivImageLeft;
    private RecyclerView rvAddChef;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView.Adapter addChefAdapter;
    public List<ChefList> chefLists;
    private EndlessRecyclerOnScrollListener rvAddChefScrollListener;
    private int pageNo = 1;
    private Intent intent;
    private String TAG = AddChefActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_chef);
        context = this;
        getId();
        setListener();
        tvHeader.setText("Chef List");
        ivImageRight.setVisibility(View.INVISIBLE);
        linearLayoutManager = new GridLayoutManager(context, 2);
        rvAddChef.setLayoutManager(linearLayoutManager);
        addChefAdapter = new AddChefAdapter(this, chefLists);
        rvAddChef.setAdapter(addChefAdapter);


        rvAddChefScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.e("Load more ", "current page" + current_page);
                Log.e(TAG, "inside-load-more");
                if (addChefAdapter.getItemViewType(chefLists.size() - 1) == 1) {
                    Log.e(TAG, "inside-cond-load-more");
                    chefLists.add(null);
                    addChefAdapter.notifyItemInserted(chefLists.size());


                    if (CommonUtils.isOnline(context)) {
                        // getManageProfileApi(pageNo);
                       // testChefListApi(pageNo);
                        getChefListApi(pageNo);
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }

                }

            }
        };

     //   rvAddChef.addOnScrollListener(rvAddChefScrollListener);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "InsideOnResume..........");
        if (CommonUtils.isOnline(context)) {

            pageNo = 1;
            chefLists.clear();
            Log.e(TAG, "pageNo..........");
            getChefListApi(pageNo);
            //testChefListApi(pageNo);
        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }
    }

    private void setListener() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        tvAddChef.setOnClickListener(this);
    }

    private void getId() {
        chefLists = new ArrayList<>();
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        rvAddChef = (RecyclerView) findViewById(R.id.rvAddChef);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        tvAddChef = (TextView) findViewById(R.id.tvAddChef);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageRight:
                Toast.makeText(getApplicationContext(), "work on progress", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ivImageLeft:
                intent = new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.tvAddChef:
                intent = new Intent(this, ChefRegistrationActivity.class);
                intent.putExtra(AppConstants.ADD_CHEF, AppConstants.CHEF_REGISTRATION);
                startActivity(intent);
                break;
        }
    }


    private void getChefListApi(int pageNumber) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...");
        String id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String url = RequestURL.BASE_URL + "owner/allchefs?user_id=" + id + "&page=" + pageNumber + "&per_page=5";
       //String url = RequestURL.BASE_URL + "chefs?user_id=" + id + "&page=" + pageNumber + "&per_page=5";


        Log.e(TAG, "url>>>>>" + url);
        Ion.with(AddChefActivity.this)
                .load(url)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }

                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {


                                if (response.chefs != null) {
                                    if (pageNo == 1) {
                                        chefLists.clear();
                                    }
                                    if (chefLists.size() > 0) {
                                        chefLists.remove(chefLists.size() - 1);
                                        addChefAdapter.notifyItemRemoved(chefLists.size());
                                    }
                                    pageNo = Integer.parseInt(response.pagination.page_no);
                                    chefLists.addAll(response.chefs);

                                    Log.e(TAG, "listSize>>>>>" + chefLists.size());
                                   addChefAdapter = new AddChefAdapter(AddChefActivity.this, chefLists);
                                    rvAddChef.setAdapter(addChefAdapter);
                                    //addChefAdapter.notifyDataSetChanged();

                                    pageNo++;
                                }
                            } else {
                                CommonUtils.showToast(AddChefActivity.this, response.response_message);
                            }
                        } else {
                            Toast.makeText(AddChefActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            if(e!=null)
                            Log.e(TAG,e.toString());
                        }
                    }
                });
    }

    private void testChefListApi(int pageNumber) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...");
        String id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String url = RequestURL.BASE_URL + "owner/allchefs?user_id=" + id + "&page=" + pageNumber + "&per_page=5";


        Log.e(TAG, "url>>>>>" + url);
        Ion.with(AddChefActivity.this)
                .load(url)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, final JsonObject response) {

                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if(e!=null)
                        Log.v("exception",e.toString());

                        Log.e("test", " response data " + new Gson().toJson(response));

                    }
                });
    }
}
