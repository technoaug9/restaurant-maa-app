package com.srinivas.restaurantowner.activity.owner;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.imageutils.CropImage;
import com.srinivas.restaurantowner.imageutils.TakePictureUtils;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class ChefRegistrationActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader, tvRegister, tvUpload;
    private EditText etChefName, etEmailId, etContactNumber, etUserID, etPassword;
    private String add_chef;
    private ImageView ivImageRight, ivImageLeft, ivProfileImage;
    private Context context = ChefRegistrationActivity.this;
    private Intent intent;
    private String imageString;
    private String MobilePattern = "[0-9]{10}";
    public File file;
    private String imageRealPath;
    private String TAG = ChefRegistrationActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_registration);
        getId();
        CommonUtils.hideSoftKeyboard(this);
        String image = "";
        tvHeader.setText("Chef Registration");
        ivImageRight.setVisibility(View.INVISIBLE);
        seListener();
        Intent intent = getIntent();
        context = ChefRegistrationActivity.this;
        add_chef = intent.getExtras().getString(AppConstants.ADD_CHEF);
    }

    private void seListener() {
        tvRegister.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        tvUpload.setOnClickListener(this);
    }

    private void getId() {
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivProfileImage = (ImageView) findViewById(R.id.ivProfileImage);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        etChefName = (EditText) findViewById(R.id.etChefName);
        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etContactNumber = (EditText) findViewById(R.id.etContactNumber);
        etUserID = (EditText) findViewById(R.id.etUserID);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        tvUpload = (TextView) findViewById(R.id.tvUpload);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvRegister:
                if (checkValidation()) {
                    if (CommonUtils.isOnline(context)) {

                        if (file == null) {
                            //when not upload image  then request image in string
                            ServiceRequest request = new ServiceRequest();
                            request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                            request.email = etEmailId.getText().toString().trim();
                            request.password = etPassword.getText().toString().trim();
                            request.name = etChefName.getText().toString().trim();
                            request.phone_no = etContactNumber.getText().toString().trim();
                            request.image = "";
                            getCreateChefApi(request);
                        } else {
                            // when   upload image then iamge in file(use multipart)
                            getCreateChefApi();
                        }

                    } else {
                        CommonUtils.showAlert("Please check your internet connection.", context);
                    }

                }
                break;
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;
            case R.id.tvUpload:

                if(isAllPermissionGranted()){
                    Log.e("Permission granted: ","Button Click performed Action");
                    CommonUtils.showcameradialogs(ChefRegistrationActivity.this);
                }else{
                    if(ContextCompat.checkSelfPermission(ChefRegistrationActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(ChefRegistrationActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    }else if(ContextCompat.checkSelfPermission(ChefRegistrationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(ChefRegistrationActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }

                break;

        }
    }

    /**
     * Code for Marshmallow permission
     * @return
     */
    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(ChefRegistrationActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(ChefRegistrationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChefRegistrationActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(ChefRegistrationActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(ChefRegistrationActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChefRegistrationActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(ChefRegistrationActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(ChefRegistrationActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }



    private boolean checkValidation() {
        if (etChefName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter chef name.", null, View.GONE);
            etChefName.requestFocus();
            return false;
        } else if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please check your email to verify.", null, View.GONE);
            etEmailId.requestFocus();
            return false;
        } else if (!checkEmailId(etEmailId.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            etEmailId.requestFocus();
            //etEmailId.setText("");
            return false;
        } else if (checkEmailId(etContactNumber.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter contact number.", null, View.GONE);
            etEmailId.requestFocus();
            //etEmailId.setText("");
            return false;
        } else if (!etContactNumber.getText().toString().matches(MobilePattern)) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid 10 digit phone number", null, View.GONE);


            return false;
        } else if (etPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter password.", null, View.GONE);
            etPassword.requestFocus();
            return false;
        } else if (etPassword.getText().toString().trim().length() < 8) {
            CommonUtils.showCustomDialogOk(this, "Password must be of 8 characters.", null, View.GONE);
            etPassword.requestFocus();
            //etPassword.setText("");
            return false;
        }


        return true;
    }


    private boolean checkEmailId(String emailId) {
        return Patterns.EMAIL_ADDRESS.matcher(emailId).matches();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chef_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void chefDialog(String message) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.registration_dialog, null);
        mDialog.setContentView(dialoglayout);

        TextView tvOk = (TextView) dialoglayout.findViewById(R.id.tvOk);
        TextView tvCheckMail = (TextView) dialoglayout.findViewById(R.id.tvCheckMail);
        tvCheckMail.setText(message);


        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*finishAffinity();*/
            /*    if (add_chef.equalsIgnoreCase(AppConstants.CHEF_REGISTRATION)) {
                    Intent intent = new Intent(context, AddChefActivity.class);
                    startActivity(intent);

                } else {
                    startActivity(new Intent(context, HomeActivity.class));
                }*/

                finish();
            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>when not set image create chef>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void getCreateChefApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Create_Chef;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        Log.i("Tag", " url " + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        chefDialog(response.response_message);
                    } else {
                        CommonUtils.showToast(context, response.response_message);
                    }

                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }

    /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>multipart image create chef>>>>>>>>>>>>>>>>>>>>>>>>>>>*/
    private void getCreateChefApi() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Create_Chef;
        Log.e(TAG, "url>>>>>" + url);
        Bitmap bm= BitmapFactory.decodeFile(file.getPath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        final String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        Log.e(TAG,"encodedImage" +encodedImage);
        Ion.with(context)

                .load(url)
                .setMultipartParameter("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID))
                .setMultipartParameter("email", etEmailId.getText().toString().trim())
                .setMultipartParameter("password", etPassword.getText().toString().trim())
                .setMultipartParameter("image", encodedImage)
                .setMultipartParameter("name", etChefName.getText().toString().trim())
                .setMultipartParameter("phone_no", etContactNumber.getText().toString().trim())
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e(TAG, " Req data" + CommonUtils.getPreferencesString(context, AppConstants.USER_ID) +etEmailId.getText().toString().trim() + encodedImage +  etChefName.getText().toString().trim() + etContactNumber.getText().toString().trim());
                Log.e(TAG, " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        chefDialog(response.response_message);
                    } else {
                        CommonUtils.showToast(context, response.response_message);
                    }

                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case TakePictureUtils.PICK_GALLERY:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), CommonUtils.imageNameLocal + ".png"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        TakePictureUtils.startCropImage(ChefRegistrationActivity.this, CommonUtils.imageNameLocal + ".png");

                    } catch (Exception e) {

                        Toast.makeText(ChefRegistrationActivity.this, "Error in picture", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage(ChefRegistrationActivity.this, CommonUtils.imageNameLocal + ".png");
                    break;

                case TakePictureUtils.CROP_FROM_CAMERA:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                   /* if (path == null) {
                        return;
                    }
                    imageRealPath = path;
                    Log.e("Image Real Path", imageRealPath);

                    Picasso.with(ChefRegistrationActivity.this).load(new File(path)).resize(100, 100).into(ivProfileImage);
*/
                    if (path == null) {
                        return;
                    }
                    //path add in file save string image in file
                    file = new File(path);
                    ivProfileImage.setImageBitmap(BitmapFactory.decodeFile(path));
                    Log.e("TAG", "<<<<<<<cameraPath>>>>>>>" + path);
                    break;
            }

        }
    }
}
