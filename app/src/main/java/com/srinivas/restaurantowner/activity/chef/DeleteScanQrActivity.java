package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;

/**
 * Created by vinay.tripathi on 17/10/15.
 */
public class DeleteScanQrActivity extends Activity implements View.OnClickListener
{

    private TextView tvHeader,tvNoDelete,tvYesDelete,tvContents,tvSurityStatment;
    private  TextView tvTestTheChipDelete;
    private  TextView tvOkDelete;
    private Context context;
    private ImageView ivImageLeft, ivImageRight,iv_Scanner;
    private LinearLayout ll_nfcdialog;
    Dialog dialog_nfc_delete,dialog_nfc_delete_yes,dialog_nfc_delete_ok;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_image);
        setIds();
        setListeners();
        tvHeader.setText("Scan");
        ivImageRight.setVisibility(View.GONE);
        context = DeleteScanQrActivity.this;
        counter=0;
    }
    private void setIds()
    {
        iv_Scanner= (ImageView)findViewById(R.id.iv_Scanner);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        tvHeader=(TextView) findViewById(R.id.tvHeader);
    }
    private void setListeners()
    {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        iv_Scanner.setOnClickListener(this);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
            case R.id.ivImageRight:
                startActivity(new Intent(context,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;
            case R.id.iv_Scanner:
                if (counter==0)
                {
                    dialog_nfc_delete = CommonUtils.customDialog(this, R.layout.dialog_nfc_delete);
                    getIdDialogNfcDelete();
                    setListnerDialogNfcDelete();
                }
                else if (counter==1)
                {
                    dialog_nfc_delete_yes = CommonUtils.customDialog(this, R.layout.dialog_nfc_successfully_delete);
                    getIdDialogNfcDeleteYes();
                    setListnerDialogNfcDeleteYes();
                }
                else
                {
                    dialog_nfc_delete_ok = CommonUtils.customDialog(this, R.layout.dialog_nfc_no_product_found_delete);
                    getIdDialogNfcDeleteOk();
                    setListnerDialogNfcDeleteOk();
                }

                break;
            //Click event on Nfc Modify Dialog
            case R.id.tvNoDelete:
                dialog_nfc_delete.dismiss();
                break;


            case R.id.tvYesDelete:
                //dialog_nfc_second.dismiss();
                /*Intent intent1 = new Intent(this, DeleteScanNfcActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);*/
                dialog_nfc_delete.dismiss();
                counter=1;
                break;
            //Click event on Nfc Successfully Delete Dialog
            case R.id.tvTestTheChipDelete:
                dialog_nfc_delete_yes.dismiss();
                counter=2;

                break;
            case R.id.tvOkDelete:
                dialog_nfc_delete_ok.dismiss();
                Intent intent1 = new Intent(this, QrOwnerActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                dialog_nfc_delete_ok.dismiss();
                counter=0;
                break;
        }
    }
    private void getIdDialogNfcDeleteOk() {
        tvOkDelete=(TextView)dialog_nfc_delete_ok.findViewById(R.id.tvOkDelete);
    }
    private void setListnerDialogNfcDeleteOk() {
        tvOkDelete.setOnClickListener(this);
    }
    private void setListnerDialogNfcDeleteYes() {
        tvTestTheChipDelete.setOnClickListener(this);
    }
    private void getIdDialogNfcDeleteYes()
    {
        tvTestTheChipDelete=(TextView)dialog_nfc_delete_yes.findViewById(R.id.tvTestTheChipDelete);
    }

    private void setListnerDialogNfcDelete() {
        tvNoDelete.setOnClickListener(this);
        tvYesDelete.setOnClickListener(this);
    }
    private void getIdDialogNfcDelete()
    {
        tvNoDelete=(TextView)dialog_nfc_delete.findViewById(R.id.tvNoDelete);
        tvYesDelete=(TextView)dialog_nfc_delete.findViewById(R.id.tvYesDelete);
        tvContents=(TextView)dialog_nfc_delete.findViewById(R.id.tvContents);
        tvSurityStatment=(TextView)dialog_nfc_delete.findViewById(R.id.tvSurityStatment);
        tvContents.setText("Contents present in the QR code.");
        tvSurityStatment.setText("Are you sure you want to delete\nthe product from the\nQR code image?");
    }
}



