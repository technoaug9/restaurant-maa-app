package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.Log;

/**
 * Created by vinay.tripathi on 17/10/15.
 */
public class ReadScanQrActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader, tvDelete, tvModify, tvContents, tvSurityStatment, tvSurityStatmentModify, tvContentsModify;
    private TextView tvNoDelete, tvYesDelete;
    private TextView tvNoModify, tvYesModify;
    private TextView tvTestTheChipDelete;
    private TextView tvOkDelete;

    private Context mContext;
    private ImageView ivImageLeft, ivImageRight, iv_Scanner;
    private LinearLayout ll_nfcdialog;
    Dialog dialog_nfc_read, dialog_nfc_modify, dialog_nfc_delete, dialog_nfc_delete_yes, dialog_nfc_delete_ok;
    int countForDelete;
    public static int countForModify;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_image);
        setIds();
        setListeners();
        tvHeader.setText("Scan");
        countForDelete = 0;

        if (getIntent().getStringExtra("from") != null && getIntent().getStringExtra("from").length() > 0) {
            Log.e("ReadScanNfcActivity", "<<<<<<>ReadScanNfcActivity>>>>>>>" + getIntent().getStringExtra("from"));
            countForModify = 1;
        } else {
            countForModify = 0;
        }

        //countForModify=0;
        ivImageRight.setVisibility(View.GONE);
        mContext = ReadScanQrActivity.this;
        //counter=0;

       /* CommonUtils.savePreference(NfcChip.this, AppConstants.IS_NFC, true);
        CommonUtils.savePreference(NfcChip.this, AppConstants.IS_QRSCANNER, false);*/


    }

    private void setIds() {
        iv_Scanner = (ImageView) findViewById(R.id.iv_Scanner);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);

    }

    private void setListeners() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        iv_Scanner.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                onBackPressed();
                break;
            case R.id.ivImageRight:
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
               /* finishAffinity();
                if
                        (CommonUtils.getPreferences(mContext, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                }
                else if (CommonUtils.getPreferences(mContext, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                else
                {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;*/
            case R.id.iv_Scanner:
                if (countForDelete == 0)
                {
                    dialog_nfc_read = CommonUtils.customDialog(this, R.layout.fragment_nfc_read_modify_delete_dialog);
                    getIdDialogNfcRead();

                    setListnerDialogNfcRead();
                } else if (countForDelete == 1) {


                    dialog_nfc_delete_yes = CommonUtils.customDialog(this, R.layout.dialog_nfc_successfully_delete);
                    getIdDialogNfcDeleteYes();

                    setListnerDialogNfcDeleteYes();


                } else {


                    dialog_nfc_delete_ok = CommonUtils.customDialog(this, R.layout.dialog_nfc_no_product_found_delete);
                    getIdDialogNfcDeleteOk();

                    setListnerDialogNfcDeleteOk();
                    //countForDelete=0;
                }


                break;

            //Click event on Nfc Read Dialog
            case R.id.tvModify:
                dialog_nfc_read.dismiss();
                dialog_nfc_modify = CommonUtils.customDialog(this, R.layout.dialog_nfc_modify);
                getIdDialogNfcModify();

                setListnerDialogNfcModify();

                break;


            case R.id.tvDelete:
                //dialog_nfc_second.dismiss();
                /*Intent intent1 = new Intent(this, NfcOwnerActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                dialog_nfc_read.dismiss();*/
                dialog_nfc_read.dismiss();
                dialog_nfc_delete = CommonUtils.customDialog(this, R.layout.dialog_nfc_delete);
                getIdDialogNfcDelete();
                setListnerDialogNfcDelete();
                break;


            case R.id.tvNoDelete:
                dialog_nfc_delete.dismiss();
                countForDelete = 0;

                break;


            case R.id.tvYesDelete:
                //dialog_nfc_second.dismiss();
                /*Intent intent1 = new Intent(this, DeleteScanNfcActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);*/
                dialog_nfc_delete.dismiss();
                countForDelete = 1;


                break;


            //Click event on Nfc Successfully Delete Dialog
            case R.id.tvTestTheChipDelete:
                dialog_nfc_delete_yes.dismiss();
                countForDelete = 2;
                break;

//Click event on Nfc No Product Found Delete Dialog
            case R.id.tvOkDelete:
                dialog_nfc_delete_ok.dismiss();
                Intent intent2 = new Intent(this, QrOwnerActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                //dialog_nfc_delete_ok.dismiss();

                countForDelete = 0;


                break;

            case R.id.tvNoModify:
                dialog_nfc_modify.dismiss();
                countForModify = 0;

                break;

            case R.id.tvYesModify:
                dialog_nfc_modify.dismiss();
                Intent intent1 = new Intent(this, SelectProductQRModify.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);

                countForModify = 1;


                break;


        }


    }

    private void getIdDialogNfcModify() {
        tvNoModify = (TextView) dialog_nfc_modify.findViewById(R.id.tvNoModify);
        tvYesModify = (TextView) dialog_nfc_modify.findViewById(R.id.tvYesModify);
        tvContentsModify = (TextView) dialog_nfc_modify.findViewById(R.id.tvContentsModify);
        tvSurityStatmentModify = (TextView) dialog_nfc_modify.findViewById(R.id.tvSurityStatmentModify);
        tvContentsModify.setText("Contents present in the QR\ncode image.");
        tvSurityStatmentModify.setText("Are you sure you want to modify\nthe QR code image?");

    }

    private void setListnerDialogNfcModify() {
        tvNoModify.setOnClickListener(this);
        tvYesModify.setOnClickListener(this);
    }

    private void getIdDialogNfcDelete() {
        tvNoDelete = (TextView) dialog_nfc_delete.findViewById(R.id.tvNoDelete);
        tvYesDelete = (TextView) dialog_nfc_delete.findViewById(R.id.tvYesDelete);
        tvContents = (TextView) dialog_nfc_delete.findViewById(R.id.tvContents);
        tvSurityStatment = (TextView) dialog_nfc_delete.findViewById(R.id.tvSurityStatment);
        tvContents.setText("Contents present in the QR code.");
        tvSurityStatment.setText("Are you sure you want to delete\nthe product from the\nQR code image?");


    }

    private void setListnerDialogNfcDelete() {
        tvNoDelete.setOnClickListener(this);
        tvYesDelete.setOnClickListener(this);
    }

    private void setListnerDialogNfcDeleteYes() {
        tvTestTheChipDelete.setOnClickListener(this);


    }

    private void getIdDialogNfcDeleteYes() {
        /*tvMsg=(TextView)dialog_nfc_delete_yes.findViewById(R.id.tvMsg);
        tvProductName=(TextView)dialog_nfc_delete_yes.findViewById(R.id.tvProductName);
        tvCode=(TextView)dialog_nfc_delete_yes.findViewById(R.id.tvCode);*/
        tvTestTheChipDelete = (TextView) dialog_nfc_delete_yes.findViewById(R.id.tvTestTheChipDelete);


    }


    private void setListnerDialogNfcRead() {
        tvModify.setOnClickListener(this);
        tvDelete.setOnClickListener(this);
    }

    private void getIdDialogNfcRead() {
        tvModify = (TextView) dialog_nfc_read.findViewById(R.id.tvModify);
        tvDelete = (TextView) dialog_nfc_read.findViewById(R.id.tvDelete);
        tvContents = (TextView) dialog_nfc_read.findViewById(R.id.tvContents);
        tvSurityStatment = (TextView) dialog_nfc_read.findViewById(R.id.tvSurityStatment);
        tvContents.setText("Contents present in the QR code.");
        tvSurityStatment.setText("Do you want to modify or delete\nthe QR code image from here?");


    }


    private void getIdDialogNfcDeleteOk() {
        tvOkDelete = (TextView) dialog_nfc_delete_ok.findViewById(R.id.tvOkDelete);


    }

    private void setListnerDialogNfcDeleteOk() {
        tvOkDelete.setOnClickListener(this);

    }
}
