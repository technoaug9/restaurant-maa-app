package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.PreventiveList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.enduser.FriendProfileAdapter;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class FriendProfileActivity extends Activity implements View.OnClickListener {
    private RecyclerView rvFrndProfile;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    private RecyclerView.Adapter friendProfileAdapter;
    public List<PreventiveList> preventiveLists;
    private TextView tvHeader, tvAdd, tvDelete;
    private Intent intent;
    ProductsActivity tvAddIngredients;
    private ImageView ivImageLeft, ivImageRight;
    private View.OnClickListener yesButtonListener;
    private Dialog mDialog;
    private String TAG = FriendProfileActivity.class.getSimpleName();
    private String memberID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);
        mDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        context = FriendProfileActivity.this;

        if (getIntent() != null && getIntent().hasExtra("memberID") && getIntent().getStringExtra("memberID").length() > 0) {
            memberID = getIntent().getStringExtra("memberID");
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonUtils.isOnline(context)) {
            ServiceRequest request = new ServiceRequest();

            if (memberID.equalsIgnoreCase("") || memberID.length() == 0) {
                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                myProfileAPI(request);
            } else {
                request.member_id = memberID;
                memberProfileAPI(request);
            }

        } else {
            CommonUtils.showAlert("Please check your internet connection.", context);
        }
    }


    private void getId() {
        rvFrndProfile = (RecyclerView) findViewById(R.id.rvFrndProfile);
        tvAdd = (TextView) findViewById(R.id.tvAdd);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvDelete = (TextView) findViewById(R.id.tvDelete);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);
        preventiveLists = new ArrayList<>();
    }

    private void setListener() {
        tvAdd.setOnClickListener(this);
        tvDelete.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        rvFrndProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvAdd:
                intent = new Intent(this, ProductsActivity.class);
                intent.putExtra(AppConstants.FRIEND_PROFILE, AppConstants.INGREDIENT_ACTIVITY_KEY);
                intent.putExtra("memberID", memberID);
                startActivity(intent);
                break;

            case R.id.ivImageLeft:
                finish();

                break;

            case R.id.ivImageRight:

                finishAffinity();

                startActivity(new Intent(this, HomeActivity.class));

                break;

            case R.id.tvDelete:


                if (preventiveLists.size() > 0) {

                    for (int i = 0; i < preventiveLists.size(); i++) {
                        preventiveLists.get(i).isCrossVisible = true;
                    }
                    friendProfileAdapter.notifyDataSetChanged();
                }

                break;
            case R.id.rvFrndProfile:
                rvFrndProfile.setNestedScrollingEnabled(true);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    //*******************My Profile API***************************//
    private void myProfileAPI(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.My_Profile;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "URL>>>>>>" + url);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        String userName = "", userImage = "", userDOB = "";

                        if (response.user.image != null && !response.user.image.equalsIgnoreCase("")) {
                            userImage = response.user.image;
                        }


                        if (response.user.name != null && response.user.name.length() > 0) {
                            tvHeader.setText(response.user.name);
                            userName = response.user.name;
                        }

                        if (response.user.dob != null && response.user.dob.length() > 0) {
                            userDOB = response.user.dob;
                        }


                        if (response.preventive_list != null) {

                            preventiveLists = response.preventive_list;

                            Log.e(TAG, "list_size>>>>" + preventiveLists.size());

                            getFriendProfileAdapter(userName, userDOB, userImage);

                        }


                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }

                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }

    private void getFriendProfileAdapter(String userName, String dob, String image) {
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFrndProfile.setLayoutManager(linearLayoutManager);
        String ID = "", userType = "";

        if (memberID.equalsIgnoreCase("") || memberID.length() == 0) {
            ID = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
            userType = "user";
        } else {
            ID = memberID;
            userType = "member";
        }

        friendProfileAdapter = new FriendProfileAdapter(FriendProfileActivity.this, preventiveLists, userName, dob, image, ID, userType);
        rvFrndProfile.setAdapter(friendProfileAdapter);
    }

    private void memberProfileAPI(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Select_Member;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i(TAG, "req data " + new Gson().toJson(requestJson));
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {


                        if (response.user.id != null && response.user.id.length() > 0) {


                            // tvHeader.setText(response.user.name != null && !response.user.name.equalsIgnoreCase("") ? response.user.name : "");


                            String userName = "", userImage = "", userDOB = "";


                            if (response.user.image != null && !response.user.image.equalsIgnoreCase("")) {
                                userImage = response.user.image;
                            }


                            if (response.user.name != null && response.user.name.length() > 0) {
                                tvHeader.setText(response.user.name);
                                userName = response.user.name;
                            }

                            if (response.user.dob != null && response.user.dob.length() > 0) {
                                userDOB = response.user.dob;
                            }


                            if (response.preventive_list != null) {

                                preventiveLists = response.preventive_list;

                                Log.e(TAG, "list_size>>>>" + preventiveLists.size());

                                // getFriendProfileAdapter();
                                getFriendProfileAdapter(userName, userDOB, userImage);

                            }


                        }


                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {

                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }


}

