package com.srinivas.restaurantowner.activity.chef;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.chef.SelectproductQRModifyAdapter;
import com.srinivas.restaurantowner.model.AddIngredientOwnerModel;
import com.srinivas.restaurantowner.model.SelectNfcChipModel;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinay.tripathi on 17/10/15.
 */
public class SelectProductQRModify extends Activity implements View.OnClickListener
{
    private TextView tvHeader,tvAddIngredients;
    private ImageView ivImageLeft, ivImageRight;
    private EditText etSearch;
    private ListView lvOwner;
    private RecyclerView rvOwner;
    private ArrayAdapter<String> dataAdapter;
    public List<String> arrSeracheLast;
    public List<String> arrSeracheFirst;
    private List<SelectNfcChipModel> selectNfcChipModels;

    private Intent intent;
    private String[] arrFirst=new String[]{"Good Day","Hide & Seek","5 Star","Dairy Milk"};
    private RecyclerView rvProducts;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    public SelectproductQRModifyAdapter productAdapter;
    public List<AddIngredientOwnerModel> data;
    private int[] foodImage = {R.drawable.img,R.drawable.food_img1,R.drawable.food_img, R.drawable.food_img};
    private String[] arrProductsName={"Pizza","HotDog","Burger", "Momos"};
    // private String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addactivityfromnfc);
        context = SelectProductQRModify.this;


        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        getRecycleAdapter();
    }

    public List<SelectNfcChipModel> getData()
    {
        selectNfcChipModels = new ArrayList<>();
        for (int i=0; i<foodImage.length; i++)
        {
            SelectNfcChipModel selectNfcChipModel = new SelectNfcChipModel();
            selectNfcChipModel.setRowHeader(arrProductsName[i]);
            selectNfcChipModel.setIvProductImg(foodImage[i]);
            selectNfcChipModels.add(selectNfcChipModel);
        }
        return selectNfcChipModels;
    }

    private void getRecycleAdapter() {
        linearLayoutManager=new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvOwner.setLayoutManager(linearLayoutManager);
        productAdapter = new SelectproductQRModifyAdapter(this, getData());
        rvOwner.setAdapter(productAdapter);
    }

    private void setListener()
    {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    private void getId() {
        tvHeader=(TextView) findViewById(R.id.tvHeader);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        etSearch=(EditText) findViewById(R.id.etSearch);
        lvOwner=(ListView) findViewById(R.id.lvOwner);
        rvOwner=(RecyclerView) findViewById(R.id.rvOwner);
        tvHeader.setText("Select a poduct to load into QR");
        tvHeader.setTextSize(15);
        ivImageRight.setImageResource(R.drawable.home_btn);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()){
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.ivImageRight:
            /*    finishAffinity();
                if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
*/
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);

                break;
        }
    }
}
