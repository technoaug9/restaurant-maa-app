package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.model.User;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class ContactUsActivity extends Activity implements View.OnClickListener, View.OnTouchListener {
    private TextView tvTermCondition, tvHeader, tvSubmit;
    private Context context;
    private EditText etName, etSubject, etDescription, etContactNumber, etEmailId;
    private ImageView ivImageLeft, ivImageRight;
    private String MobilePattern = "[0-9]{10}";
    private  String TAG=ContactUsActivity.class.getSimpleName();
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_contact);
        context = ContactUsActivity.this;
        getID();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
        etDescription.setOnTouchListener(this);
        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);
        tvHeader.setText("Contact Us");
    }

    private void getID() {

        tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvSubmit = (TextView) findViewById(R.id.tvSubmit);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        etName = (EditText) findViewById(R.id.etName);
        etSubject = (EditText) findViewById(R.id.etSubject);
        etDescription = (EditText) findViewById(R.id.etDescription);
        etContactNumber = (EditText) findViewById(R.id.etContactNumber);
        etEmailId = (EditText) findViewById(R.id.etEmailId);


    }

    private void setListeners() {

        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(view.getId() == R.id.etDescription){
            view.getParent().requestDisallowInterceptTouchEvent(true);
            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTermCondition:

                break;

            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                // onBackPressed();
                finish();
                break;

            case R.id.ivImageRight:
                CommonUtils.hideSoftKeyboard(this);
              /*  finishAffinity();
                if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;*/
                startActivity(new Intent(context,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                break;


            case R.id.tvSubmit:
                CommonUtils.hideSoftKeyboard(this);
                CommonUtils.hideSoftKeyboard(this);
                if (checkValidation()) {
                    if (CommonUtils.isOnline(context)) {
                        ServiceRequest request = new ServiceRequest();
                        request = new ServiceRequest();
                        request.user_type = CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE);
                        request.name = etName.getText().toString();
                        request.subject = etSubject.getText().toString();
                        request.description = etDescription.getText().toString();
                        request.phone_no = etContactNumber.getText().toString().trim();
                        request.email = etEmailId.getText().toString().trim();
                     //   contactUsApi(etName.getText().toString(), etSubject.getText().toString(), etDescription.getText().toString());
                        contactUsApi(request);

                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }

                break;
        }
    }

    private boolean checkValidation() {

        if (etName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter name.", null, View.GONE);
            return false;
        } else if (etContactNumber.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter contact number.", null, View.GONE);
            etEmailId.requestFocus();
            //etEmailId.setText("");
            return false;
        } else if (!etContactNumber.getText().toString().matches(MobilePattern)) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid 10 digit phone number", null, View.GONE);


            etEmailId.requestFocus();

            return false;
        } else if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please check your email to verify.", null, View.GONE);
            etEmailId.requestFocus();
            return false;
        } else if (!CommonUtils.checkEmailId(etEmailId.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            etEmailId.requestFocus();
            //etEmailId.setText("");
            return false;
        } else if (etSubject.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter subject.", null, View.GONE);
            return false;
        } else if (etDescription.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter description.", null, View.GONE);
            return false;
        }

        return true;
    }

    private void contactUsApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        Log.i("", "connecting to server");
        String url = RequestURL.Contact_Us;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i(TAG, "req data " + new Gson().toJson(requestJson));

        Log.i("Tag", " url " + url);
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {

                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        showAlert(response.response_message, context);
                    } else {
                    }
                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }

    public void showAlert(String message, final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)

                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();
                            }
                        });

        try {
            AlertDialog alert11 = builder.create();
            alert11.show();
            Button buttonbackground = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
            buttonbackground.setTextColor(context.getResources().getColor(R.color.skyblue));
            buttonbackground.setTextSize(20);
            TextView textView = (TextView) alert11.findViewById(android.R.id.message);
            textView.setTextSize(20);
            textView.setTextColor(context.getResources().getColor(R.color.black));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
