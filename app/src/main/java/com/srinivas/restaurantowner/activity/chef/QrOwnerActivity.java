package com.srinivas.restaurantowner.activity.chef;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponseProduct;
//import com.srinivas.barcodescanner.activity.enduser.ScanResultsActivity;
import com.srinivas.restaurantowner.scannerbarcode.BarcodeCaptureActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

/**
 * Created by vinay.tripathi on 16/10/15.
 */
public class QrOwnerActivity extends Activity implements View.OnClickListener
{
    private TextView tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private TextView tvReadQR, tvDeleteQR, tvModifyQR, tvProgramQR;
    private Intent intent;
    private String nfc;
    private View.OnClickListener ok;
    private Context context;
    private Dialog mDialog;
    private String action="";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        context = QrOwnerActivity.this;

        getId();
        setlistener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("QR Scan");
        ivImageRight.setVisibility(View.INVISIBLE);

    }

    private void setlistener() {
        ivImageLeft.setOnClickListener(this);
        tvReadQR.setOnClickListener(this);
        tvDeleteQR.setOnClickListener(this);
        tvModifyQR.setOnClickListener(this);
        tvProgramQR.setOnClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvReadQR = (TextView) findViewById(R.id.tvReadQR);
        tvDeleteQR = (TextView) findViewById(R.id.tvDeleteQR);
        tvModifyQR = (TextView) findViewById(R.id.tvModifyQR);
        tvProgramQR = (TextView) findViewById(R.id.tvProgramQR);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.tvProgramQR:
                try {
                /*    Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("cam_mode", "back");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 1);*/
                   /* Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("from","QrOwnerActivity_ProgramQR");
                    startActivity(intent);*/

                    Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class);
                    intent.putExtra("value", 1);
                    startActivityForResult(intent, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR:", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.tvModifyQR:
                action = "modify";

                try {
                   /* Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("cam_mode", "back");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 2);*/
                   /* Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("from","QrOwnerActivity_ModifyQR");
                    startActivity(intent);*/


                    Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class);
                    startActivityForResult(intent, 2);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR:", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.tvDeleteQR:
                action = "delete";

                try {
                  /*  Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("cam_mode", "back");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 3);*/
                   /* Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("from","QrOwnerActivity_DeleteQR");
                    startActivity(intent);*/

                    Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class);

                    startActivityForResult(intent, 3);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR:", Toast.LENGTH_LONG).show();
                }


                break;
            case R.id.tvReadQR:

                action = "read";

                try {
                   /* Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("cam_mode", "back");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                    startActivityForResult(intent, 5);*/
                    /*Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("from","QrOwnerActivity_ReadQR");
                    startActivity(intent);*/

                    Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class);
                    startActivityForResult(intent, 5);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "ERROR:", Toast.LENGTH_LONG).show();
                }

                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && barCodeValue.length()>0) {
                    intent = new Intent(this, NewQrProgramActivity.class);
                    intent.putExtra("barCodeValue", barCodeValue);
                    startActivity(intent);
                    Log.e("", "barcode_value>>>>>" + barCodeValue);
                    Toast.makeText(QrOwnerActivity.this, " Scan Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(QrOwnerActivity.this, "Not Scan Successfully", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }


        }else if (requestCode == 2) {

            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && barCodeValue.length()>0) {
                    Log.e("", "barcode_value>>>>>" + barCodeValue);
                    if (CommonUtils.isOnline(context)) {
                        testAssignProductServiceCall(barCodeValue, "qr");
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                    //Toast.makeText(QrOwnerActivity.this, " Scan Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(QrOwnerActivity.this, "Not Scan Successfully", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }
        }else if (requestCode == 3) {

            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && barCodeValue.length()>0) {
                    Log.e("", "barcode_value>>>>>" + barCodeValue);
                    if (CommonUtils.isOnline(context)) {
                        testAssignProductServiceCall(barCodeValue, "qr");
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                    //Toast.makeText(QrOwnerActivity.this, " Scan Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(QrOwnerActivity.this, "Not Scan Successfully", Toast.LENGTH_SHORT).show();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }
        }else if (requestCode == 4) {
            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && barCodeValue.length()>0) {
                    Log.e("", "barcode_value>>>>>" + barCodeValue);
                    Toast.makeText(QrOwnerActivity.this, " Scan Successfully", Toast.LENGTH_SHORT).show();

                    testAssignProductServiceCall(barCodeValue, "qr");

                } else {
                    Toast.makeText(QrOwnerActivity.this, "Not Scan Successfully", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }
        }else if (requestCode == 5) {
            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && barCodeValue.length()>0) {
                    Log.e("", "barcode_value>>>>>" + barCodeValue);
                    Toast.makeText(QrOwnerActivity.this, " Scan Successfully", Toast.LENGTH_SHORT).show();

                    testAssignProductServiceCall(barCodeValue, "qr");

                } else {
                    Toast.makeText(QrOwnerActivity.this, "Not Scan Successfully", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }
        }
    }

    private void testAssignProductServiceCall(final String barCodeValue, String type)
    {
        final ProgressDialog progressDialog = ProgressDialog.show(QrOwnerActivity.this, "", "Please wait..");
        String userId= CommonUtils.getPreferencesString(QrOwnerActivity.this, AppConstants.USER_ID);

        //http://172.16.1.95:5000//products/get_product_based_on_code?user_id=2&code=12345678&code_type=nfc
        String url = RequestURL.BASE_URL + "owner/qrcodesearch?user_id="+userId+"&code="+barCodeValue+"&code_type="+type+"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);
      //  jsonRequest.addProperty("user_type",CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE));
        Log.e("", "url" + url);

        Ion.with(QrOwnerActivity.this)
                .load(url)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                       // Log.e("response",new JsonObject())
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                CommonUtils.showToast(QrOwnerActivity.this, response.response_message);
                                if (response.product != null) {
                                    modifyhowCustomDialog(QrOwnerActivity.this, response.product.name, response.product.qr_code, response.product.id);
                                }
                            } else {
                                okDialog(QrOwnerActivity.this, response.response_message);
                                CommonUtils.showToast(QrOwnerActivity.this, response.response_message);
                            }
                        } else {
                            Toast.makeText(QrOwnerActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void modifyhowCustomDialog(final Context context, String productName,   final String productCode, final String productID) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_qr_nfc_product_modify, null);
        mDialog.setContentView(dialoglayout);

        TextView tvContents = (TextView) mDialog.findViewById(R.id.tvContents);
        TextView tvProductName = (TextView) mDialog.findViewById(R.id.tvProductName);
        TextView tvProductCode = (TextView) mDialog.findViewById(R.id.tvProductCode);
        TextView tvSurityStatment = (TextView) mDialog.findViewById(R.id.tvSurityStatment);
        TextView tvNo = (TextView) mDialog.findViewById(R.id.tvNo);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);

        Log.e("", "productName " + productName);
        Log.e("", "productCode " + productCode);


        tvContents.setText("Contents present in the QR code image");
        tvProductName.setText("Product name: " + productName);
        tvProductCode.setText("Unique code: " + productCode);

        if(action.equalsIgnoreCase("modify")){
            tvNo.setText("No");
            tvYes.setText("Yes");
            tvSurityStatment.setText("Are you sure you want to modify the QR code image?");
        }else if(action.equalsIgnoreCase("delete")) {
            tvNo.setText("No");
            tvYes.setText("Yes");
            tvSurityStatment.setText("Are you sure you want to delete the product from the QR code image?");
        }else if(action.equalsIgnoreCase("read")) {
            tvNo.setText("Modify");
            tvYes.setText("Delete");
            tvSurityStatment.setText("Do you want to modify or delete the QR code image from here?");
        }


        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if(action.equalsIgnoreCase("read")) {
                    action = "modify";
                    CommonUtils.savePreference(QrOwnerActivity.this, AppConstants.IS_NFC, false);
                    Intent intent = new Intent(QrOwnerActivity.this, SelectProductFromNfc.class);
                    intent.putExtra("barCodeValue", productCode);
                    intent.putExtra("modify_product", "modifyProduct");
                    intent.putExtra("product_id", productID);
                    startActivity(intent);
                }
            }
        });

        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if(action.equalsIgnoreCase("modify")){
                    CommonUtils.savePreference(QrOwnerActivity.this, AppConstants.IS_NFC, false);
                    Intent intent = new Intent(QrOwnerActivity.this, SelectProductFromNfc.class);
                    intent.putExtra("barCodeValue", productCode);
                    intent.putExtra("modify_product", "modifyProduct");
                    intent.putExtra("product_id", productID);
                    startActivity(intent);
                }else if(action.equalsIgnoreCase("delete")) {
                    deleteProductServiceCall(productID);
                }else if(action.equalsIgnoreCase("read")){
                    action = "delete";
                    deleteProductServiceCall(productID);
                }

                }
        });
        mDialog.show();
    }


    private void deleteProductServiceCall(String id)
    {
        final ProgressDialog progressDialog = ProgressDialog.show(QrOwnerActivity.this, "", "Please wait..");

        JsonObject jsonRequest = new JsonObject();
        jsonRequest.addProperty("user_id", CommonUtils.getPreferencesString(QrOwnerActivity.this, AppConstants.USER_ID));
        jsonRequest.addProperty("id", id);
        jsonRequest.addProperty("qr_code", "");
        jsonRequest.addProperty("operation_type", "deleted from QR");
        jsonRequest.addProperty("user_type",CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE));
       // http://youngcart.in/

        String url = RequestURL.BASE_URL + "owner/delete_qrcode";

        Log.e("", "request" + jsonRequest.toString());
        Log.e("", "url" + url);

        Ion.with(QrOwnerActivity.this)
                .load(url)
                .setJsonObjectBody(jsonRequest)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>()
                {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null)
                        {
                            progressDialog.dismiss();
                        }
                        if (response != null)
                        {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                Toast.makeText(QrOwnerActivity.this, response.response_message, Toast.LENGTH_SHORT).show();
                                showCustomDialog(QrOwnerActivity.this);
                            } else {
                                Toast.makeText(QrOwnerActivity.this, response.response_message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void showCustomDialog(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);

        tvCode.setVisibility(View.GONE);
        tvMsg.setText(context.getString(R.string.edit_Qr));
        view.setVisibility(View.GONE);
        tvOk.setVisibility(View.GONE);
        tvTestchip.setGravity(Gravity.CENTER);
        tvTestchip.setText("Test The QR");


        tvTestchip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                try {
                   /* Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                    intent.putExtra("cam_mode", "back");
                    intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
                    startActivityForResult(intent, 4);*/
                   /* Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .putExtra("from","QrOwnerActivity_Dialog");
                    startActivity(intent);*/

                    Intent intent = new Intent(QrOwnerActivity.this, BarcodeCaptureActivity.class);
                    intent.putExtra("value", 4);
                    startActivityForResult(intent, 4);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, "ERROR:", Toast.LENGTH_LONG).show();
                }


            }
        });

        mDialog.show();
    }


    private void okDialog(final Context context, String msg) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert_ok, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);

        tvMsg.setText(msg);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isAllPermissionGranted()){
            Log.e("Permission granted: ","Button performed Action");
            // CommonUtils.showcameradialogs(ScanActivity.this);
        }else{
            if(ContextCompat.checkSelfPermission(QrOwnerActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(QrOwnerActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }else if(ContextCompat.checkSelfPermission(QrOwnerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(QrOwnerActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        2);
            }
        }
    }


    /**
     * Code for Marshmallow permission check
     * @return
     */
    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(QrOwnerActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(QrOwnerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(QrOwnerActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    //  CommonUtils.showcameradialogs(ScanActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(QrOwnerActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(QrOwnerActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    // CommonUtils.showcameradialogs(ScanActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(QrOwnerActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }
}