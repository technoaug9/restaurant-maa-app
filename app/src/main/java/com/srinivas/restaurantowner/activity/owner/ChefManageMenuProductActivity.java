package com.srinivas.restaurantowner.activity.owner;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.Services.ProductResponse;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.activity.enduser.AddIngredientOwnerSearchActivity;
import com.srinivas.restaurantowner.adapter.enduser.ManageMenuProductAdapter;
import com.srinivas.restaurantowner.imageutils.CropImage;
import com.srinivas.restaurantowner.imageutils.TakePictureUtils;
import com.srinivas.restaurantowner.model.ManageMenuProductModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.StringBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;


public class ChefManageMenuProductActivity extends Activity implements View.OnClickListener {
    private static final String TAG = ChefManageMenuProductActivity.class.getSimpleName();
    private ListView rvManageMenuProduct;
    private TextView tvHeader, tvProductName, tvProduct, tvUpload, tvAdd, tvDelete;
    private ImageView ivImageLeft, ivImageRight, ivProfileImage;
    private Intent intent;
    private LinearLayoutManager linearLayoutManager;
    private Context context = ChefManageMenuProductActivity.this;
    private List<ManageMenuProductModel> manageMenuProductModelList;
    private static ManageMenuProductAdapter manageMenuProductAdapter;
    private String[] arrtvIngredientName = {"Olive", "Salt", "Cashew"};
    private int[] arrivDelete = {R.drawable.cross_icon, R.drawable.cross_icon, R.drawable.cross_icon};
    private Boolean isEditable = true;
    private FrameLayout flProduct;
    private String imageRealPath;
    private View.OnClickListener yesButtonListener;
    private String user_type = AppConstants.USER_TYPE_GENERALUSER;
    private Dialog mDialog;
    public static final int TAKE_PICTURE = 1;
    public static final int PICK_GALLERY = 2;
    public static final int CROP_FROM_CAMERA = 3;
    private static final String TEMP_PHOTO_FILE_NAME = "temp";
    private IngredientList ingredientListModel;
    public static List<IngredientList> ingredientListsManageMenu;
    private String productID = "", userID = "", productAddedFor = "";
    private File file;
    private EditText etProductName;
    private View headerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_menu_product);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("Manage Menu");
        ivImageRight.setImageResource(R.drawable.edit_icon);
        ivImageRight.setVisibility(View.INVISIBLE);
        mDialog = new Dialog(this,
                android.R.style.Theme_Translucent_NoTitleBar);

        ingredientListsManageMenu = new ArrayList<>();

        if (getIntent() != null) {
            if (getIntent().hasExtra("productID")) {
                productID = getIntent().getStringExtra("productID");
                com.srinivas.restaurantowner.utils.Log.e(TAG, ">>>>>productID>>>>activity" + productID);
            }

            if (getIntent().hasExtra("userID")) {
                userID = getIntent().getStringExtra("userID");
                Log.e(TAG, ">>>>userID<<<<" + userID);
            }

            if (getIntent().hasExtra("productAddedFor")) {
                productAddedFor = getIntent().getStringExtra("productAddedFor");
                Log.e(TAG, ">>>>productAddedFor<<<<" + productAddedFor);
            }
        }

        if (productID.length() > 0 && userID.length() > 0) {
            callIngredientListForParticularProductAPI(productID, userID);
        }


    }

    private void callIngredientListForParticularProductAPI(String productID, String userId) {

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Please wait...");

        //String url = RequestURL.BASE_URL + "products/" + productID + "?user_id=" + userId+"&user_type=chef";
        String url = RequestURL.BASE_URL + "owner/product/" + "?product_id=" +productID;
        Log.e(TAG, "url>>>>>" + url);

        Ion.with(context)
                .load(url)
                .as(new TypeToken<ProductResponse>() {
                })
                .setCallback(new FutureCallback<ProductResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ProductResponse response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                if (response.product != null) {

                                    if (response.product.name != null && response.product.name.length() > 0) {
                                        tvProduct.setText(response.product.name);
                                        etProductName.setText(response.product.name);
                                    }

                                    if (response.product.image != null && response.product.image != null &&
                                            response.product.image.length() > 0) {
                                        Picasso.with(context).load(response.product.image).placeholder(R.drawable.img).into(ivProfileImage);
                                    } else {
                                        ivProfileImage.setImageResource(R.drawable.img);
                                    }

                                    if (response.product.ingredients != null && response.product.ingredients.size() > 0) {

                                        ingredientListsManageMenu = response.product.ingredients;
                                        manageMenuProductAdapter = new ManageMenuProductAdapter(ChefManageMenuProductActivity.this, ingredientListsManageMenu);
                                        rvManageMenuProduct.setAdapter(manageMenuProductAdapter);
                                    }


                                }

                            } else {
                                CommonUtils.showToast(context, response.response_message);
                            }
                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });


    }


    public static void notifyRecyclerView() {

        if (manageMenuProductAdapter != null) {
            manageMenuProductAdapter.notifyDataSetChanged();
        }
    }


    private void setListener() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        tvUpload.setOnClickListener(this);
        tvDelete.setOnClickListener(this);
        tvAdd.setOnClickListener(this);
    }

    private void getId() {

        headerView = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.activity_manage_menu_product_header, null, false);

        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        rvManageMenuProduct = (ListView) findViewById(R.id.rvManageMenuProduct);

        flProduct = (FrameLayout) headerView.findViewById(R.id.flProduct);
        tvProductName = (TextView) headerView.findViewById(R.id.tvProductName);
        tvProduct = (TextView) headerView.findViewById(R.id.tvProduct);
        tvUpload = (TextView) headerView.findViewById(R.id.tvUpload);
        ivProfileImage = (ImageView) headerView.findViewById(R.id.ivProfileImage);
        etProductName = (EditText) headerView.findViewById(R.id.etProductName);

        rvManageMenuProduct.addHeaderView(headerView);

        tvAdd = (TextView) findViewById(R.id.tvAdd);
        tvAdd.setVisibility(View.GONE);
        tvDelete = (TextView) findViewById(R.id.tvDelete);
        tvDelete.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        // rvManageMenuProduct.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageRight:
                if (isEditable) {
                    makeEditable();
                } else if (!isEditable) {
                    if (CommonUtils.isOnline(context) && userID.length() > 0) {

                        if (file == null) {
                            //when not upload image  then request image in string
                            ServiceRequest request = new ServiceRequest();

                            if (productAddedFor.equalsIgnoreCase("chef_id")) {
                                request.chef_id = userID;
                                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                            } else {
                                request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                            }
                            request.name = etProductName.getText().toString().trim();
                            //request.qr_code = "";
                            request.image = "";
                          //  request.nfc_code = "";
                            List<String> ids = new ArrayList<>();
                            ids.clear();
                            if (ingredientListsManageMenu != null && ingredientListsManageMenu.size() > 0) {
                                for (int i = 0; i < ingredientListsManageMenu.size(); i++) {
                                    ids.add(ingredientListsManageMenu.get(i).id);
                                }
                            }
                            request.product_ingredients_attributes = ids;
                            updateProductApi(request, productID);
                        } else {
                            // when   upload image then iamge in file(use multipart)
                            try {
                                new updateProductAsync().execute();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        CommonUtils.showAlert("Please check your internet connection.", context);
                    }


                }

                break;
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                if (!isEditable) {
                    makeNonEditable();
                } else if (isEditable) {
                    finish();
                }
                break;
            case R.id.tvUpload:
                Log.e("ChefManage Clicked: ", "update button Click performed Action");
                if(isAllPermissionGranted()){
                    Log.e("Permission granted: ", "Button Click performed Action");
                    CommonUtils.showcameradialogs(ChefManageMenuProductActivity.this);
                }else{
                    if(ContextCompat.checkSelfPermission(ChefManageMenuProductActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(ChefManageMenuProductActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                1);
                    }else if(ContextCompat.checkSelfPermission(ChefManageMenuProductActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(ChefManageMenuProductActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                2);
                    }
                }

                break;
            case R.id.tvDelete:
                if (ingredientListsManageMenu != null && ingredientListsManageMenu.size() > 0) {
                    for (int i = 0; i < ingredientListsManageMenu.size(); i++) {
                        ingredientListsManageMenu.get(i).isCrossVisible = true;
                    }
                    manageMenuProductAdapter.notifyDataSetChanged();
                }

                break;
            case R.id.tvAdd:
                intent = new Intent(this, AddIngredientOwnerSearchActivity.class).putExtra("productID", productID);
                startActivity(intent);
                break;
        }
    }

    /**
     * Code for Marshmallow permission check
     * @return
     */

    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(ChefManageMenuProductActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(ChefManageMenuProductActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChefManageMenuProductActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(ChefManageMenuProductActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(ChefManageMenuProductActivity.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChefManageMenuProductActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    CommonUtils.showcameradialogs(ChefManageMenuProductActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(ChefManageMenuProductActivity.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }

    }


    public class updateProductAsync extends AsyncTask<Void, Void, String> {

        private ProgressDialog progressDialog;
        String responseMessage = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(context, "", "Please wait...");
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                if (productID.length() > 0) {
                    responseMessage = updateProductMultipartPost(productID);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            //  Toast.makeText(context, aVoid, Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    public String updateProductMultipartPost(String productID) throws Exception {

        String responseMessage = "";

        try {
            HttpClient httpClient = new DefaultHttpClient();

            String url = RequestURL.BASE_URL + "products/" + productID + "/update";
            Log.e(TAG, "url>>>>" + url);
            HttpPost postRequest = new HttpPost(url);

            MultipartEntityBuilder reqEntity = MultipartEntityBuilder.create();

            reqEntity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            if (productAddedFor.equalsIgnoreCase("chef_id")) {
                reqEntity.addTextBody("chef_id", userID);
            }
            reqEntity.addTextBody("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
            reqEntity.addBinaryBody("image", file);
            reqEntity.addTextBody("name", etProductName.getText().toString().trim());
            reqEntity.addTextBody("qr_code", "");
            reqEntity.addTextBody("nfc_code", "");

            // Prepare Category Array

            List<String> ids = new ArrayList<>();
            ids.clear();
            if (ingredientListsManageMenu != null && ingredientListsManageMenu.size() > 0) {
                for (int i = 0; i < ingredientListsManageMenu.size(); i++) {
                    ids.add(ingredientListsManageMenu.get(i).id);
                }
            }

            for (String mBusinessID : ids) {
                reqEntity.addPart("product_ingredients_attributes[]", new StringBody(mBusinessID));
            }

            HttpEntity httpEntity = reqEntity.build();
            postRequest.setEntity(httpEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String sResponse;
            StringBuilder mStringBuilder = new StringBuilder();

            while ((sResponse = reader.readLine()) != null) {
                mStringBuilder = mStringBuilder.append(sResponse);
            }

            Log.e(TAG, "asyncResponse" + mStringBuilder.toString());

            JSONObject jsonObject = new JSONObject((mStringBuilder.toString()));
            if (jsonObject.has("response_message")) {
                com.srinivas.restaurantowner.utils.Log.e(TAG, "response_message>>>>" + jsonObject.getString("response_message"));
                //  CommonUtils.showToast(context, jsonObject.getString("response_message"));
                responseMessage = jsonObject.getString("response_message");
            }
            if (jsonObject.has("response_code")) {
                com.srinivas.restaurantowner.utils.Log.e(TAG, "response_code>>>>" + jsonObject.getString("response_code"));
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage());
        }
        return responseMessage;
    }


   /* private void updateProductApi() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.BASE_URL + "products/" + productID + "/update";
        Log.e(TAG, "url>>>>" + url);

        Map<String, List<String>> map = new HashMap<>();

        map.clear();
        List<String> ids = new ArrayList<>();
        ids.clear();
        if (ingredientListsManageMenu != null && ingredientListsManageMenu.size() > 0) {
            for (int i = 0; i < ingredientListsManageMenu.size(); i++) {
                ids.add(ingredientListsManageMenu.get(i).id);
            }
        }

        String key = "";
        if (productAddedFor.equalsIgnoreCase("chef_id")) {
            key = "chef_id";
        } else {
            key = "user_id";
        }

        map.put("product_ingredients_attributes", ids);
        Ion.with(context)
                .load(url)
                .setMultipartParameter(key, userID)
                .setMultipartParameter("name", etProductName.getText().toString().trim())
                .setMultipartParameter("qr_code", "")
                .setMultipartParameter("nfc_code", "")
                .setMultipartFile("image", file)
                .setMultipartParameters(map)
                .as(new TypeToken<ProductResponse>() {
                }).setCallback(new FutureCallback<ProductResponse>() {

            public void onCompleted(Exception e, ProductResponse response) {

                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();

                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.response_message != null) {
                            CommonUtils.showToast(context, "Product updated successfully");
                            makeNonEditable();
                        }
                    }
                } else {
                    Toast.makeText(context, "no response", Toast.LENGTH_SHORT).show();
                    if (e != null) {

                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }*/


    private void updateProductApi(ServiceRequest request, String proID) {


        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.BASE_URL + "products/" + proID + "/update";
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i(TAG, "req data " + new Gson().toJson(requestJson));
        Log.e(TAG, "url>>>>>>" + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ProductResponse>() {
                }).setCallback(new FutureCallback<ProductResponse>() {

            public void onCompleted(Exception e, ProductResponse response) {

                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();

                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        Toast.makeText(context, "Product updated", Toast.LENGTH_SHORT).show();
                        makeNonEditable();
                        finish();
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {

                case TakePictureUtils.PICK_GALLERY:
                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());
                        FileOutputStream fileOutputStream = new FileOutputStream(new File(getExternalFilesDir("temp"), CommonUtils.imageNameLocal + ".png"));
                        TakePictureUtils.copyStream(inputStream, fileOutputStream);
                        fileOutputStream.close();
                        inputStream.close();
                        TakePictureUtils.startCropImage(ChefManageMenuProductActivity.this, CommonUtils.imageNameLocal + ".png");

                    } catch (Exception e) {

                        Toast.makeText(ChefManageMenuProductActivity.this, "Error in picture", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case TakePictureUtils.TAKE_PICTURE:
                    TakePictureUtils.startCropImage(ChefManageMenuProductActivity.this, CommonUtils.imageNameLocal + ".png");
                    break;

                case TakePictureUtils.CROP_FROM_CAMERA:
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {
                        return;
                    }
                    imageRealPath = path;
                    Log.e("Image Real Path", imageRealPath);

                    file = new File(imageRealPath);
                    //   Picasso.with(ManageMenuProductActivity.this).load(new File(path)).resize(100, 100).into(ivProfileImage);
                    ivProfileImage.setImageBitmap(BitmapFactory.decodeFile(imageRealPath));
                    break;
            }

        }

    }

    private void makeNonEditable() {
        ivImageRight.setImageResource(R.drawable.edit_icon);
        tvProductName.setVisibility(View.GONE);
        flProduct.setVisibility(View.GONE);
        tvProduct.setVisibility(View.VISIBLE);
        tvDelete.setVisibility(View.GONE);
        tvAdd.setVisibility(View.GONE);
        tvUpload.setVisibility(View.GONE);
        isEditable = true;
    }

    private void makeEditable() {
        ivImageRight.setImageResource(R.drawable.save_icon);
        tvProductName.setVisibility(View.VISIBLE);
        flProduct.setVisibility(View.VISIBLE);
        tvProduct.setVisibility(View.GONE);
        tvDelete.setVisibility(View.VISIBLE);
        tvAdd.setVisibility(View.VISIBLE);
        tvUpload.setVisibility(View.VISIBLE);
        isEditable = false;
    }
}
