package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.adapter.enduser.ManageIngredientsAdapter;
import com.srinivas.restaurantowner.model.ManageIngredientsModel;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class ManageIngredientsActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader;
    private ImageView ivImageRight, ivImageLeft,ivProduct;
    private RecyclerView rvManageIngredients;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    private ManageIngredientsAdapter manageIngredientsAdapter;
    private List<ManageIngredientsModel> manageIngredientsModelList;
    private String[] arrManageItem={"Olive","Salt","Sugar"};

    private View.OnClickListener yesButtonListener;

    private Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_ingredients);
        getId();
        setListener();
        startRecycler();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("Manage Ingredients");
    }

    private void startRecycler() {
        linearLayoutManager=new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvManageIngredients.setLayoutManager(linearLayoutManager);
        manageIngredientsModelList=getData();

        /*yesButtonListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manageIngredientsModelList.remove(AppConstants.POSITION_DELETE);
                manageIngredientsAdapter = new ManageIngredientsAdapter(ManageIngredientsActivity.this, manageIngredientsModelList,true,yesButtonListener,mDialog);
                rvManageIngredients.setAdapter(manageIngredientsAdapter);
                *//*Utility.setListViewHeightBasedOnChildren(rvFrndProfile);*//*
                mDialog.cancel();
            }
        };*/
        manageIngredientsAdapter=new ManageIngredientsAdapter(ManageIngredientsActivity.this,manageIngredientsModelList);
        rvManageIngredients.setAdapter(manageIngredientsAdapter);
    }

    private List<ManageIngredientsModel> getData() {
        manageIngredientsModelList=new ArrayList<>();
        for(int i=0;i<arrManageItem.length;i++){
            ManageIngredientsModel manageIngredientsModel=new ManageIngredientsModel();
            manageIngredientsModel.setTvManageIngredients(arrManageItem[i]);
            manageIngredientsModelList.add(manageIngredientsModel);
        }
        return manageIngredientsModelList;
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivProduct.setOnClickListener(this);
    }

    private void getId() {
        tvHeader=(TextView) findViewById(R.id.tvHeader);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivProduct=(ImageView) findViewById(R.id.ivProduct);
        rvManageIngredients=(RecyclerView) findViewById(R.id.rvManageIngredients);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()){
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;
            case R.id.ivImageRight:
                break;
            case R.id.ivProduct:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
