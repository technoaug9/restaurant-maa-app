package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.chef.NfcOwnerActivity;
import com.srinivas.restaurantowner.activity.chef.QrOwnerActivity;
import com.srinivas.restaurantowner.activity.enduser.AddChefActivity;
import com.srinivas.restaurantowner.activity.owner.AddProductActivity;
import com.srinivas.restaurantowner.activity.owner.ManageMenuMainActivity;
import com.srinivas.restaurantowner.activity.owner.OwnerProfileActivity;
import com.srinivas.restaurantowner.activity.owner.ScanHistoryOwnerActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.Log;

public class HomeActivity extends Activity implements View.OnClickListener {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private Context context = HomeActivity.this;
    private TextView tvScan, tvSearchProduct, tvTool, tvUserSetting, tvInfo, tvHistory, tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private View.OnClickListener yesButtonListener;
    private LinearLayout ll_ResturantHome, ll_UserHome, llChefHome;
    private TextView tvNfc, tvQRCode, tvManageMenu, tvManageChef, tvProfile, tvInfoOwner;
    private TextView tvNfcChef, tvQRCodeChef, tvMyProduct, tvProfileChef, tvHistoryChef, tvInfoChef;
    private Dialog mDialog;
    private String user_type = AppConstants.USER_TYPE_NORMAL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_home);
        mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        getID();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
        Log.v("password",CommonUtils.getPreferencesString(this, AppConstants.USER_PASSWORD));
        if (user_type != null) {
            if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE) != null && !CommonUtils.getPreferences
                    (context, AppConstants.USER_TYPE).equalsIgnoreCase(""))
                user_type = CommonUtils.getPreferences(context, AppConstants.USER_TYPE);
            {
                if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_NORMAL)) {
                    ll_UserHome.setVisibility(View.VISIBLE);
                    ll_ResturantHome.setVisibility(View.GONE);
                    llChefHome.setVisibility(View.GONE);

                } else if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_OWNER)) {
                    ll_ResturantHome.setVisibility(View.VISIBLE);
                    ll_UserHome.setVisibility(View.GONE);
                    llChefHome.setVisibility(View.GONE);
                } else if (user_type.equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    llChefHome.setVisibility(View.VISIBLE);
                    ll_UserHome.setVisibility(View.GONE);
                    ll_ResturantHome.setVisibility(View.GONE);
                }
            }
        }
    }

    private void getID() {
        ll_ResturantHome = (LinearLayout) findViewById(R.id.ll_ResturantHome);
        ll_UserHome = (LinearLayout) findViewById(R.id.ll_UserHome);
        llChefHome = (LinearLayout) findViewById(R.id.llChefHome);
        tvScan = (TextView) findViewById(R.id.tvScan);
        tvSearchProduct = (TextView) findViewById(R.id.tvSearchProduct);
        tvTool = (TextView) findViewById(R.id.tvTool);
        tvUserSetting = (TextView) findViewById(R.id.tvUserSetting);
        tvInfo = (TextView) findViewById(R.id.tvInfo);
        tvHistory = (TextView) findViewById(R.id.tvHistory);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft.setVisibility(View.INVISIBLE);
        ivImageRight.setImageResource(R.drawable.logout_btn);
        /* Owner home Id*/
        tvNfc = (TextView) findViewById(R.id.tvNfc);
        tvManageChef = (TextView) findViewById(R.id.tvManageChef);
        tvManageMenu = (TextView) findViewById(R.id.tvManageMenu);
        tvProfile = (TextView) findViewById(R.id.tvProfile);
        tvInfoOwner = (TextView) findViewById(R.id.tvInfoOwner);
        tvQRCode = (TextView) findViewById(R.id.tvQRCode);
          /* Chef home Id*/
        tvNfcChef = (TextView) findViewById(R.id.tvNfcChef);
        tvQRCodeChef = (TextView) findViewById(R.id.tvQRCodeChef);
        tvMyProduct = (TextView) findViewById(R.id.tvMyProduct);
        tvProfileChef = (TextView) findViewById(R.id.tvProfileChef);
        tvHistoryChef = (TextView) findViewById(R.id.tvHistoryChef);
        tvInfoChef = (TextView) findViewById(R.id.tvInfoChef);
        tvHeader.setText(getString(R.string.home));
    }


    private void setListeners() {
        tvScan.setOnClickListener(this);
        tvSearchProduct.setOnClickListener(this);
        tvTool.setOnClickListener(this);
        tvUserSetting.setOnClickListener(this);
        tvInfo.setOnClickListener(this);
        tvHistory.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
         /* Owner home listener*/
        tvNfc.setOnClickListener(this);
        tvManageChef.setOnClickListener(this);
        tvManageMenu.setOnClickListener(this);
        tvProfile.setOnClickListener(this);
        tvInfoOwner.setOnClickListener(this);
        tvQRCode.setOnClickListener(this);
           /* Chef home listener*/
        tvNfcChef.setOnClickListener(this);
        tvQRCodeChef.setOnClickListener(this);
        tvMyProduct.setOnClickListener(this);
        tvProfileChef.setOnClickListener(this);
        tvHistoryChef.setOnClickListener(this);
        tvInfoChef.setOnClickListener(this);

        yesButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    if (CommonUtils.getPreferencesString(context, AppConstants.USER_ID).length() > 0) {
                        request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                        callLogoutApi(request);
                    }
                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }
            }
        };
    }

    private void callLogoutApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait..");
        String url = RequestURL.Log_Out;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        android.util.Log.e("TagHomeActivity", "req data " + new Gson().toJson(requestJson));
        android.util.Log.e("TagHomeActivity", " url " + url);
        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        CommonUtils.showToast(context, response.response_message);
                        CommonUtils.saveIntPreferences(HomeActivity.this, AppConstants.PRE_ISLOGIN, 0);
                        CommonUtils.savePreferencesString(HomeActivity.this, AppConstants.USER_TYPE, "");
                        CommonUtils.savePreferencesString(HomeActivity.this, AppConstants.USER_ID, "");
                        Intent intent = new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        CommonUtils.showToast(context, getString(R.string.server_error));
                    }
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageRight:
                showCustomDialogLogout(this, getString(R.string.logout_dialog_message),
                        null, yesButtonListener, View.GONE, mDialog);
                break;
            /*User Section*/
            case R.id.tvScan:
                startActivity(new Intent(this, ScanActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
           /* case R.id.tvSearchProduct:
                startActivity(new Intent(this, SearchProductActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvTool:
                startActivity(new Intent(this, PreventiveListActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvUserSetting:
                startActivity(new Intent(this, UserSettingsActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvInfo:
                startActivity(new Intent(this, InfoActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvHistory:
                startActivity(new Intent(this, ScanHistoryUserActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;*/
         /*Owner Section*/
            case R.id.tvNfc:
                startActivity(new Intent(context, NfcOwnerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvQRCode:
                startActivity(new Intent(context, QrOwnerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvManageChef:
                startActivity(new Intent(context, AddChefActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvManageMenu:
                startActivity(new Intent(context, ManageMenuMainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvProfile:
                startActivity(new Intent(context, OwnerProfileActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvInfoOwner:
                startActivity(new Intent(this, InfoActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;

            /*Chef Section*/
            case R.id.tvNfcChef:
                startActivity(new Intent(this, NfcOwnerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvQRCodeChef:
                startActivity(new Intent(this, QrOwnerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvMyProduct:
                startActivity(new Intent(this, AddProductActivity.class)
                        .putExtra("from", "Home")
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvHistoryChef:

                startActivity(new Intent(this, ScanHistoryOwnerActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvProfileChef:
                startActivity(new Intent(this, OwnerProfileActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
            case R.id.tvInfoChef:
                startActivity(new Intent(this, InfoActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                break;
        }
    }

    void showCustomDialogLogout(final Activity mContext, String msg, View.OnClickListener
            noButtonListener, View.OnClickListener yesButtonListener, int visibilityAlert, final Dialog mDialog) {
        if (mContext == null) {
            Log.e("TAG", "mContext is null");
        }
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_alertlogout, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvNo = (TextView) mDialog.findViewById(R.id.tvNo);
        TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);
        TextView tvAlert = (TextView) mDialog.findViewById(R.id.tvAlert);
        View vViewTw = (View) mDialog.findViewById(R.id.vViewTw);
        if (visibilityAlert == View.VISIBLE) {
            tvAlert.setVisibility(visibilityAlert);
            vViewTw.setVisibility(View.GONE);
        } else {
            tvAlert.setVisibility(View.GONE);
            vViewTw.setVisibility(View.VISIBLE);
        }
        if (msg.toString().trim().length() != 0)
            tvMsg.setText(msg);
        if (noButtonListener != null)
            tvNo.setOnClickListener(noButtonListener);
        else {
            tvNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.cancel();
                }
            });
        }
        if (yesButtonListener != null) {
            mDialog.cancel();
            tvYes.setOnClickListener(yesButtonListener);
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }

        }
        mDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }
}
