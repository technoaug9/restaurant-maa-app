package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.MemberListService;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.model.User;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class ResetPasswordActivity extends Activity implements View.OnClickListener {
    private TextView tvPasswordSubmit, tvHeader;
    private EditText etConfirmPassword, etNewPassword;
    private Intent intent;
    private ImageView ivImageLeft, ivImageRight;
    private Context context = ResetPasswordActivity.this;
    private User user;
    private String userID = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);

        if (getIntent() != null && getIntent().hasExtra("userID") && getIntent().getStringExtra("userID").length() > 0) {
            userID = getIntent().getStringExtra("userID");

        }

        tvHeader.setText("Reset Password");
        ivImageRight.setVisibility(View.INVISIBLE);

    }

    private void getId() {
        tvPasswordSubmit = (TextView) findViewById(R.id.tvPasswordSubmit);
        tvHeader = (TextView) findViewById(R.id.tvHeader);

        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);

        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        etNewPassword = (EditText) findViewById(R.id.etNewPassword);
    }

    private void setListener() {

        tvPasswordSubmit.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        String stretNewPassword = etNewPassword.getText().toString().trim();
        String stretConfirmPassword = etConfirmPassword.getText().toString().trim();

        switch (view.getId()) {
            case R.id.tvPasswordSubmit:

                checkValidation();
                break;

            case R.id.ivImageLeft:

                finish();

                break;
        }
    }

    private void checkValidation() {

        if (etNewPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter new password.", null, View.GONE);

            etNewPassword.requestFocus();


        } else if (etNewPassword.getText().toString().trim().length() < 8) {
            CommonUtils.showCustomDialogOk(this, "Password must be of 8 characters.", null, View.GONE);

            etNewPassword.requestFocus();

            //etNewPassword.setText("");


        } else if (etConfirmPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter confirm password.", null, View.GONE);

            etConfirmPassword.requestFocus();


        } else if (!etConfirmPassword.getText().toString().trim().equals(etNewPassword.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "New password and confirm password does not matches.", null, View.GONE);

            etConfirmPassword.requestFocus();

            //etConfirmPassword.setText("");


        } else {
            if (CommonUtils.isOnline(context)) {

                ServiceRequest request = new ServiceRequest();
                if (userID.length() > 0) {
                    request.user_id = userID;
                    request.password = etNewPassword.getText().toString();
                    request.password_confirmation = etConfirmPassword.getText().toString();
                    changePassword(request);
                }
            } else {
                CommonUtils.showToast(context, getString(R.string.please_check_internet));
            }
        }


    }

    private void changePassword(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(ResetPasswordActivity.this, "", "Please wait..");

        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        String url = RequestURL.Update_Password;
        Ion.with(ResetPasswordActivity.this)
                .load(url)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<MemberListService>() {
                })
                .setCallback(new FutureCallback<MemberListService>() {
                    @Override
                    public void onCompleted(Exception e, final MemberListService response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                CommonUtils.showToast(ResetPasswordActivity.this, response.response_message);
                                ResetPasswordActivity.this.finish();
                            } else {
                                CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                            }

                        } else {
                            Toast.makeText(ResetPasswordActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

}
