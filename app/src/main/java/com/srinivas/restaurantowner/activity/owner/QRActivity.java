package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.activity.chef.QrScanner;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

/**
 * Created by vinay.tripathi on 8/10/15.
 */
public class QRActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private TextView tvReadQR, tvDeleteQR, tvModifyQR, tvProgramQR;
    private Intent intent;
    private String nfc;
    private View.OnClickListener ok;
    private Context context;

    private Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        getId();
        setlistener();

        /*CommonUtils.savePreference(QRActivity.this, AppConstants.IS_NFC, false);
        CommonUtils.savePreference(QRActivity.this, AppConstants.IS_QRSCANNER, true);
*/
        CommonUtils.hideSoftKeyboard(this);

        tvHeader.setText("QR Scan");

        ivImageRight.setVisibility(View.INVISIBLE);

        // intent = getIntent();
      /*  // nfc = intent.getExtras().getString(AppConstants.NFC);
        mDialog = new Dialog(this,
        android.R.style.Theme_Translucent_NoTitleBar);
        if(CommonUtils.getPreferencesString(this, AppConstants.NFC).equalsIgnoreCase("NFC")){
        CommonUtils.showCustomDialogOk(this, getString(R.string.close_nfc_chef), null, View.GONE);
        ok=new View.OnClickListener()
        {
@Override
public void onClick(View v) {
        intent=new Intent(context, ManageIngredientsActivity.class);
        startActivity(intent);
                *//*Utility.setListViewHeightBasedOnChildren(rvFrndProfile);*//*
        mDialog.cancel();

        }
        };   */
    }

      /*  if(nfc!=null) {
            if (nfc.equalsIgnoreCase(AppConstants.NFC)) {
                //finishAffinity();
                CommonUtils.showCustomDialogOk(this, "Please bring your phone close to NFC chip.", null, View.GONE);
            }
        }*/

    private void setlistener() {

        ivImageLeft.setOnClickListener(this);
        tvReadQR.setOnClickListener(this);
        tvDeleteQR.setOnClickListener(this);
        tvModifyQR.setOnClickListener(this);
        tvProgramQR.setOnClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvReadQR = (TextView) findViewById(R.id.tvReadQR);
        tvDeleteQR = (TextView) findViewById(R.id.tvDeleteQR);
        tvModifyQR = (TextView) findViewById(R.id.tvModifyQR);
        tvProgramQR = (TextView) findViewById(R.id.tvProgramQR);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v)

    {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.tvProgramQR:
                intent = new Intent(this, QrScanner.class);
                intent.putExtra("navigateQractivity", AppConstants.NAVIGATEQRACTIVITY);
                startActivity(intent);
                break;
            case R.id.tvModifyQR:
                intent = new Intent(this, QrScanner.class);
                intent.putExtra("QRnavigation", AppConstants.QRSCREENNAVI);
                startActivity(intent);
                break;
            case R.id.tvDeleteQR:
                intent = new Intent(this, QrScanner.class);
                intent.putExtra("TVdeleteqr", AppConstants.TVDELETEQR);
                startActivity(intent);
                break;
            case R.id.tvReadQR:
                intent = new Intent(this, QrScanner.class);
                intent.putExtra("tvREADQR", AppConstants.TVQRREADER);
                startActivity(intent);
                break;

     /*       case R.id.ivDeleteNfc:
                intent=new Intent(this, SelectNfcChipActivity.class);
                startActivity(intent);
                break;
            case R.id.ivNewNfc:
                intent=new Intent(this, AddProductIngredientList.class);
                startActivity(intent);
                break;*/

        }
    }
}