package com.srinivas.restaurantowner.activity.chef;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.BaseActivity;
import com.srinivas.restaurantowner.utils.CommonUtils;

/**
 * Created by vinay.tripathi on 15/10/15.
 */
public class QrScannerCameraActivity extends BaseActivity implements View.OnClickListener{

    private Context context=QrScannerCameraActivity.this;
    private Intent intent;
    private View view;
    private ImageView iv_Scanner, ivImageRight, ivImageLeft;
    private TextView tvHeader;
    private String from = "";
    private String TAG=QrScannerCameraActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_image);
        getIDs();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
    }
    @Override
    public void getIDs()
    {
        tvHeader.setText("Scan");
        ivImageRight.setVisibility(View.GONE);
        iv_Scanner = (ImageView) findViewById(R.id.iv_Scanner);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
    }

    @Override
    public void setListeners()
    {
        iv_Scanner.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {

    }
}
