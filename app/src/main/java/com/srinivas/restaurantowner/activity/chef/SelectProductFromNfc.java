package com.srinivas.restaurantowner.activity.chef;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.Services.ServiceResponseProduct;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.owner.NFCScannerOwnerActivity;
import com.srinivas.restaurantowner.adapter.owner.ProductListQRAndNFCAdapter;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.scannerbarcode.BarcodeCaptureActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.LoadMoreListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by priya.singh on 23/9/15.
 */
public class SelectProductFromNfc extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private TextView tvHeader, tvAddIngredients;
    private ImageView ivImageLeft, ivImageRight;
    private Context context;
    private LoadMoreListView lvProductsList;
    private int page_number = 1;
    private int maxPages = 20;
    private List<Product> arlProductList;
    private ProductListQRAndNFCAdapter adapter;
    private EditText etSearch;
    private String barCodeValue = "";
    private String type = "qr";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addproduct_qr);
        context = SelectProductFromNfc.this;
        arlProductList = new ArrayList<Product>();
        if (getIntent().getStringExtra("barCodeValue") != null && getIntent().getStringExtra("barCodeValue").length() > 0) {
            barCodeValue = getIntent().getStringExtra("barCodeValue");
            Log.e("TAG", "barCodeValue>>>>" + barCodeValue);
        }

        if (getIntent().getStringExtra("type") != null) {
            type = getIntent().getStringExtra("type");
        }

        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);

        adapter = new ProductListQRAndNFCAdapter(SelectProductFromNfc.this, arlProductList);
        lvProductsList.setAdapter(adapter);

        if (CommonUtils.isOnline(context)) {
            callProductListOfUserAPI();
        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }

        lvProductsList.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (maxPages < page_number) {
                    lvProductsList.onLoadMoreComplete();
                } else {
                    if (CommonUtils.isOnline(context)) {
                        callProductListOfUserAPI();
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }
                }
            }
        });


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editable.toString());
            }
        });

    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        lvProductsList.setOnItemClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        etSearch = (EditText) findViewById(R.id.etSearch);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        if (type.equalsIgnoreCase("nfc")) {
            tvHeader.setText("Select a poduct to load into NFC");
        } else {
            tvHeader.setText("Select a poduct to load into QR");
        }

        ivImageRight.setImageResource(R.drawable.home_btn);
        lvProductsList = (LoadMoreListView) findViewById(R.id.lvProductsList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                CommonUtils.hide_keyboard(SelectProductFromNfc.this);
                finish();
                break;
            case R.id.ivImageRight:
                finishAffinity();
                if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_RESTAURANT_OWNER)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;
        }
    }

    private void callProductListOfUserAPI() {
        String id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String url = RequestURL.BASE_URL + "owner/myproducts?user_id=" + id + "&page=" + page_number + "&per_page=10"+"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);
        Log.e(getLocalClassName(), "url>>>>>" + url);

        Ion.with(context)
                .load(url)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));

                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                lvProductsList.onLoadMoreComplete();
                                if (response.pagination.page_no != null && response.pagination.page_no.length() > 0) {
                                    page_number = Integer.parseInt(response.pagination.page_no);
                                }
                                if (response.pagination.max_page_size != null && response.pagination.max_page_size.length() > 0) {
                                    maxPages = Integer.parseInt(response.pagination.max_page_size);
                                }
                                if (response.product != null && response.product.size() > 0) {
                                    arlProductList.addAll(response.product);
                                    adapter.notifyDataSetChanged();
                                }
                                adapter.notifyDataSetChanged();
                                page_number = page_number + 1;

                            } else {
                                CommonUtils.showToast(context, response.response_message);
                            }
                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void assignProductServiceCall(String id, String type, String code) {
        final ProgressDialog progressDialog = ProgressDialog.show(SelectProductFromNfc.this, "", "Please wait..");

        JsonObject jsonRequest = new JsonObject();
        jsonRequest.addProperty("user_id", CommonUtils.getPreferencesString(SelectProductFromNfc.this, AppConstants.USER_ID));
        jsonRequest.addProperty("id", id);
        jsonRequest.addProperty(code, barCodeValue);
        jsonRequest.addProperty("operation_type", type);
        jsonRequest.addProperty("user_type",CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE));

        String url = RequestURL.BASE_URL + "owner/qrcode";

        Log.e("", "request" + jsonRequest.toString());
        Log.e("", "url" + url);

        Ion.with(SelectProductFromNfc.this)
                .load(url)
                .setJsonObjectBody(jsonRequest)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                CommonUtils.showToast(context, response.response_message);
                                showCustomDialog(SelectProductFromNfc.this);
                            } else {
                                CommonUtils.showToast(context, response.response_message);
                            }
                        } else {
                            Log.e(getLocalClassName(),e.toString());
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (getIntent().getStringExtra("modify_product") != null && getIntent().getStringExtra("modify_product").length() > 0) {

            if (getIntent().getStringExtra("product_id") != null && getIntent().getStringExtra("product_id").length() > 0) {
                String assignedId = getIntent().getStringExtra("product_id");
                Toast.makeText(getBaseContext(),assignedId,Toast.LENGTH_SHORT).show();
                if (type.equalsIgnoreCase("nfc")) {
                    modifyProductServiceCall(arlProductList.get(position).id, assignedId, "NFC");
                } else {
                    modifyProductServiceCall(arlProductList.get(position).id, assignedId, "QR");
                }

            }

        } else {
            if (CommonUtils.isOnline(context)) {
                if (type.equalsIgnoreCase("nfc")) {
                    assignProductServiceCall(arlProductList.get(position).id, "assigned to NFC", "nfc_code");
                } else {
                    assignProductServiceCall(arlProductList.get(position).id, "assigned to QR", "qr_code");
                }

            } else {
                CommonUtils.showToast(context, getString(R.string.please_check_internet));
            }
        }


    }

    private void showCustomDialog(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);

        tvCode.setVisibility(View.GONE);
        if (type.equalsIgnoreCase("nfc")) {
            tvMsg.setText("Product has been successfully \nadded to the NFC");
        } else {
            tvMsg.setText("Product has been successfully \nadded to the QR");
        }

        view.setVisibility(View.GONE);
        tvOk.setVisibility(View.GONE);
        tvTestchip.setGravity(Gravity.CENTER);

        if (type.equalsIgnoreCase("nfc")) {
            tvTestchip.setText("Test The NFC");
        } else {
            tvTestchip.setText("Test The QR");
        }

        tvTestchip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if (type.equalsIgnoreCase("nfc")) {
                    Intent intent = new Intent(SelectProductFromNfc.this, NFCScannerOwnerActivity.class);
                    intent.putExtra("action", "test");
                    startActivity(intent);
                    finish();
                } else {
                    try {

                        Intent intent = new Intent(SelectProductFromNfc.this, BarcodeCaptureActivity.class);
                        startActivityForResult(intent, 1);
                        /*Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                        intent.putExtra("cam_mode", "back");
                        intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
                        startActivityForResult(intent, 1);*/
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, "ERROR:", Toast.LENGTH_LONG).show();
                    }
                }


            }
        });

        mDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String barCodeValue = data.getStringExtra("SCAN_RESULT");
                if (barCodeValue != null && barCodeValue.length() > 0) {
                    Log.e("", "barcode_value>>>>>" + barCodeValue);
                    Toast.makeText(SelectProductFromNfc.this, " Scan Successfully", Toast.LENGTH_SHORT).show();
                    testAssignProductServiceCall(barCodeValue, "qr");
                } else {
                    Toast.makeText(SelectProductFromNfc.this, "Not Scan Successfully", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.i("result: ", "Press a button to start a scan.");
                Log.i("result: ", "Scan cancelled.");
            }
        }


    }

    private void testAssignProductServiceCall(String barCodeValue, final String type) {
        final ProgressDialog progressDialog = ProgressDialog.show(SelectProductFromNfc.this, "", "Please wait..");
        String userId = CommonUtils.getPreferencesString(SelectProductFromNfc.this, AppConstants.USER_ID);

        //http://172.16.1.95:5000//products/get_product_based_on_code?user_id=2&code=12345678&code_type=nfc
       // String url = RequestURL.BASE_URL + "/products/get_product_based_on_code?user_id=" + userId + "&code=" + barCodeValue + "&code_type=" + type;
      String url=  RequestURL.BASE_URL + "owner/qrcodesearch?user_id="+userId+"&code="+barCodeValue+"&code_type="+type+"&user_type="+CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE);

        Log.e("", "url" + url);

        Ion.with(SelectProductFromNfc.this)
                .load(url)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                CommonUtils.showToast(context, response.response_message);

                                if (response.product != null) {
                                    if (type.equalsIgnoreCase("nfc")) {
                                        detailProductshowCustomDialog(SelectProductFromNfc.this, response.product.name, response.product.nfc_code);
                                    } else {
                                        detailProductshowCustomDialog(SelectProductFromNfc.this, response.product.name, response.product.qr_code);
                                    }

                                }
                            } else {
                                okDialog(SelectProductFromNfc.this, response.response_message);
                            }
                        } else {
                            Log.e(getLocalClassName(),e.toString());
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    private void okDialog(final Context context, String msg) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert_ok, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);

        tvMsg.setText(msg);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }


    private void detailProductshowCustomDialog(final Context context, String productName, String productCode) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_nfc_pushed_product_modify, null);
        mDialog.setContentView(dialoglayout);

        TextView tvCantains = (TextView) mDialog.findViewById(R.id.tvCantains);
        TextView tvProductName = (TextView) mDialog.findViewById(R.id.tvPushedProductNameModify);
        TextView tvProductCode = (TextView) mDialog.findViewById(R.id.tvPushedProductCodeModify);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvPushedProductOkModify);

        tvCantains.setText("The QR code contains");
        tvProductName.setText("Product name: " + productName);
        tvProductCode.setText("Unique code: " + productCode);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                SelectProductFromNfc.this.finish();


            }
        });

        mDialog.show();
    }

    private void modifyProductServiceCall(String id, String assignedId, String type) {
        final ProgressDialog progressDialog = ProgressDialog.show(SelectProductFromNfc.this, "", "Please wait..");

        JsonObject jsonRequest = new JsonObject();
        jsonRequest.addProperty("user_id", CommonUtils.getPreferencesString(SelectProductFromNfc.this, AppConstants.USER_ID));
        jsonRequest.addProperty("id", id);
        if (type.equalsIgnoreCase("nfc")) {
            jsonRequest.addProperty("nfc_code", barCodeValue);
        } else {
            jsonRequest.addProperty("qr_code", barCodeValue);
        }
        jsonRequest.addProperty("user_type",CommonUtils.getPreferences(getBaseContext(),AppConstants.USER_TYPE));

        jsonRequest.addProperty("modification_type", type);
        jsonRequest.addProperty("assigned_product_id", assignedId);

        String url = RequestURL.BASE_URL + "owner/modify_qrcode";

        Log.e("", "request" + jsonRequest.toString());
        Log.e("", "url" + url);

        Ion.with(SelectProductFromNfc.this)
                .load(url)
                .setJsonObjectBody(jsonRequest)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            Log.e(getLocalClassName(), " response data " + new Gson().toJson(response));
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                CommonUtils.showToast(context, response.response_message);
                                showCustomDialog(SelectProductFromNfc.this);
                            } else {
                                CommonUtils.showToast(context, response.response_message);
                            }
                        } else {
                            Log.e(getLocalClassName(),e.toString());
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isAllPermissionGranted()){
            Log.e("Permission granted: ","Button performed Action");
            // CommonUtils.showcameradialogs(ScanActivity.this);
        }else{
            if(ContextCompat.checkSelfPermission(SelectProductFromNfc.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(SelectProductFromNfc.this,
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }else if(ContextCompat.checkSelfPermission(SelectProductFromNfc.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(SelectProductFromNfc.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        2);
            }
        }
    }

    /**
     * Code for Marshmallow permission check
     * @return
     */
    private boolean isAllPermissionGranted() {

        if (ContextCompat.checkSelfPermission(SelectProductFromNfc.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(SelectProductFromNfc.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SelectProductFromNfc.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    //  CommonUtils.showcameradialogs(ScanActivity.this);
                    Log.e("Permission granted: ", "Case 1 performed Action");

                } else {

                    Toast.makeText(SelectProductFromNfc.this, "Permission deny to access camera", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case 2: {
                // If request is cancelled, the result arrays are empty.

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(SelectProductFromNfc.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

                    // opening the camera dialog....
                    // CommonUtils.showcameradialogs(ScanActivity.this);
                    Log.e("Permission granted: ", "Case 2 performed Action");

                } else {

                    Toast.makeText(SelectProductFromNfc.this, "Permission deny to read external storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }



            // other 'case' lines to check for other
            // permissions this app might request
        }

    }
}
