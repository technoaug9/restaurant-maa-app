package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.enduser.SelectNfcChipAdapter;
import com.srinivas.restaurantowner.model.SelectNfcChipModel;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class SelectNfcChipActivity extends Activity implements View.OnClickListener
{
    private TextView tvHeader;
    private ImageView ivImageLeft, ivImageRight;
    private RecyclerView rvSelectNfcChip;
    private SelectNfcChipAdapter selectNfcChipAdapter;
    private List<SelectNfcChipModel> selectNfcChipListModels;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    private int[] arrivProductImg={R.drawable.img,R.drawable.img,R.drawable.img};
    private String[] arrtvProductName={"Pizza", "Hotdog","Burger"};
    private String[] arrtvProductUniqueCode={"0909090","Unique Code","Unique Code"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_nfc_chip);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("Select and push to NFC chip");
        ivImageRight.setImageResource(R.drawable.home_btn);
        getRecyclerAdapter();
    }

    private void getRecyclerAdapter() {
        linearLayoutManager=new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSelectNfcChip.setLayoutManager(linearLayoutManager);
        selectNfcChipListModels=getData();
        selectNfcChipAdapter=new SelectNfcChipAdapter(getApplication(),selectNfcChipListModels);
        rvSelectNfcChip.setAdapter(selectNfcChipAdapter);
    }

    private List<SelectNfcChipModel> getData()
    {
        selectNfcChipListModels= new ArrayList<>();
        for(int i=0;i<arrivProductImg.length;i++){
            SelectNfcChipModel selectNfcChipModel=new SelectNfcChipModel();
            selectNfcChipModel.setIvProductImg(arrivProductImg[i]);
            selectNfcChipModel.setTvProductName(arrtvProductName[i]);
            selectNfcChipModel.setTvProductUniqueCode(arrtvProductUniqueCode[i]);
            selectNfcChipListModels.add(selectNfcChipModel);
        }
        return selectNfcChipListModels;
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    private void getId() {
        tvHeader=(TextView) findViewById(R.id.tvHeader);
        ivImageLeft=(ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight=(ImageView) findViewById(R.id.ivImageRight);
        rvSelectNfcChip=(RecyclerView) findViewById(R.id.rvSelectNfcChip);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_nfc_chip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.ivImageRight:
                Intent intent=new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
        }
    }
}
