package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.ChefList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.adapter.enduser.ChefListAdapter;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.EndlessRecyclerOnScrollListener;
import com.srinivas.restaurantowner.utils.WaitingView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinay.tripathi on 8/10/15.
 */
public class ChefListActivity extends Activity implements View.OnClickListener {
    private Context context = ChefListActivity.this;
    private TextView tvHeader;
    private ImageView ivImageRight, ivImageLeft;
    private RecyclerView rvAddChef;
    private LinearLayoutManager linearLayoutManager;
    private ChefListAdapter chefListAdapter;
    public List<ChefList> chefLists;
    private EndlessRecyclerOnScrollListener rvAddChefScrollListener;
    private int pageNo = 1;
    private Intent intent;
    private String TAG = ChefListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef_list);
        context = this;
        getId();
        //getPreventiveListAdapter();
        setListener();
        tvHeader.setText("Chef List");
        ivImageRight.setVisibility(View.INVISIBLE);
        linearLayoutManager = new GridLayoutManager(context, 2);
        rvAddChef.setLayoutManager(linearLayoutManager);
        chefListAdapter = new ChefListAdapter(this, chefLists);
        rvAddChef.setAdapter(chefListAdapter);
        rvAddChefScrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.e("Load more ", "current page" + current_page);
                Log.e(TAG, "inside-load-more");
                if (chefListAdapter.getItemViewType(chefLists.size() - 1) == 1) {
                    Log.e(TAG, "inside-cond-load-more");
                    chefLists.add(null);
                    chefListAdapter.notifyItemInserted(chefLists.size());
                    if (CommonUtils.isOnline(context)) {
                        //getChefListApi(pageNo);
                    } else {
                        CommonUtils.showToast(context, getString(R.string.please_check_internet));
                    }

                }

            }
        };

        rvAddChef.addOnScrollListener(rvAddChefScrollListener);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CommonUtils.isOnline(context)) {
            pageNo = 1;
            getChefListApi(pageNo);
        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }
    }

    private void setListener() {
        ivImageRight.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);

    }

    private void getId() {
        chefLists = new ArrayList<>();
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        rvAddChef = (RecyclerView) findViewById(R.id.rvAddChef);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageRight:
                Toast.makeText(getApplicationContext(), "work on progress", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ivImageLeft:
                   /* intent=new Intent(context,HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);*/
                finish();
                break;
        }
    }

    private void getChefListApi(int pageNumber) {
        //final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait...");
        WaitingView.startProgressDialog(ChefListActivity.this);
        String id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        String url = RequestURL.BASE_URL + "owner/allchefs?user_id="+id +"&user_type=chef"+  "&page=" + pageNumber + "&per_page=10";
        //Toast.makeText(getBaseContext(),url,Toast.LENGTH_SHORT).show();
        Log.e(TAG, "url>>>>>" + url);
        Ion.with(ChefListActivity.this)
                .load(url)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {

                        WaitingView.stopProgressDialog();

                        Log.e("Tag", " response data " + new Gson().toJson(response));
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {


                                if (response.chefs != null) {
                                    if (pageNo == 1) {
                                        chefLists.clear();
                                    }
                                    if (chefLists.size() > 0) {
                                        chefLists.remove(chefLists.size() - 1);
                                        chefListAdapter.notifyItemRemoved(chefLists.size());
                                    }
                                    pageNo = Integer.parseInt(response.pagination.page_no);
                                    chefLists.addAll(response.chefs);
                                    Log.e(TAG, "listSize>>>>>" + chefLists.size());
                                    ChefListActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            chefListAdapter.notifyDataSetChanged();
                                        }
                                    });
                                    pageNo++;
                                }
                            } else {
                                CommonUtils.showToast(ChefListActivity.this, response.response_message);
                            }
                        } else {
                            Log.e(TAG, " error message  " + e.toString());
                            Toast.makeText(ChefListActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


}
