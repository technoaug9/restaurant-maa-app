package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.nfc.tech.NfcA;
import android.nfc.tech.NfcB;
import android.nfc.tech.NfcF;
import android.nfc.tech.NfcV;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponseProduct;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.activity.chef.NewQrProgramActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class NFCScannerOwnerActivity extends Activity implements View.OnClickListener {
    private ImageView ivImageLeft, ivImageRight;
    private Intent intent;
    private TextView tvHeader;
    private String action = "";


    // list of NFC technologies detected:
    private final String[][] techList = new String[][]{
            new String[]{
                    NfcA.class.getName(),
                    NfcB.class.getName(),
                    NfcF.class.getName(),
                    NfcV.class.getName(),
                    IsoDep.class.getName(),
                    MifareClassic.class.getName(),
                    MifareUltralight.class.getName(), Ndef.class.getName()
            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfcscanner);

        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);
        tvHeader.setText("NFC Chip Scanner");

    }

    @Override
    protected void onResume() {
        super.onResume();
        // creating pending intent:
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        // creating intent receiver for NFC events:
        IntentFilter filter = new IntentFilter();
        filter.addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filter.addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        // enabling foreground dispatch for getting intent from NFC event:
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {
                nfcAdapter.enableForegroundDispatch(this, pendingIntent, new IntentFilter[]{filter}, this.techList);
            } else {
                CommonUtils.showToast(NFCScannerOwnerActivity.this, "Please enable nfc in settings");
                finish();
            }
        } else {
            CommonUtils.showToast(NFCScannerOwnerActivity.this, "Sorry! your device doesn't support NFC");
            finish();
        }


      /*  try {

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        // disabling foreground dispatch:
  /*      try {
            NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
            nfcAdapter.disableForegroundDispatch(this);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter != null) {
            if (nfcAdapter.isEnabled()) {
                nfcAdapter.disableForegroundDispatch(this);
            } else {
                CommonUtils.showToast(NFCScannerOwnerActivity.this, "Please enable nfc in settings");
                finish();
            }
        } else {
            CommonUtils.showToast(NFCScannerOwnerActivity.this, "Sorry! your device doesn't support NFC");
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        try {
            if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
                String id = "" + ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));

                if (id != null && id.length() > 0) {
                    Log.e("", "NFC Value>>>>> " + id);

                    if (getIntent().getStringExtra("action") != null && getIntent().getStringExtra("action").equalsIgnoreCase("new")) {
                        action = "new";
                        intent = new Intent(this, NewQrProgramActivity.class);
                        intent.putExtra("barCodeValue", id);
                        intent.putExtra("type", "nfc");
                        startActivity(intent);
                        finish();
                    } else if (getIntent().getStringExtra("action") != null && getIntent().getStringExtra("action").equalsIgnoreCase("test")) {
                        action = "test";
                        testAssignProductServiceCall(id, "nfc");
                    } else {
                        intent = new Intent();
                        intent.putExtra("code", id);
                        setResult(2, intent);
                        finish();
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void testAssignProductServiceCall(String barCodeValue, final String type) {
        final ProgressDialog progressDialog = ProgressDialog.show(NFCScannerOwnerActivity.this, "", "Please wait..");
        String userId = CommonUtils.getPreferencesString(NFCScannerOwnerActivity.this, AppConstants.USER_ID);

        //http://172.16.1.95:5000//products/get_product_based_on_code?user_id=2&code=12345678&code_type=nfc
        String url = RequestURL.BASE_URL + "/products/get_product_based_on_code?user_id=" + userId + "&code=" + barCodeValue + "&code_type=" + type;

        Log.e("", "url" + url);

        Ion.with(NFCScannerOwnerActivity.this)
                .load(url)
                .as(new TypeToken<ServiceResponseProduct>() {
                })
                .setCallback(new FutureCallback<ServiceResponseProduct>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponseProduct response) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                if (response.product != null) {
                                    detailProductshowCustomDialog(NFCScannerOwnerActivity.this, response.product.name, response.product.nfc_code);
                                }
                            } else {
                                okDialog(NFCScannerOwnerActivity.this, response.response_message);
                            }
                        } else {
                            Toast.makeText(NFCScannerOwnerActivity.this, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void detailProductshowCustomDialog(final Context context, String productName, String productCode) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_nfc_pushed_product_modify, null);
        mDialog.setContentView(dialoglayout);

        TextView tvCantains = (TextView) mDialog.findViewById(R.id.tvCantains);
        TextView tvProductName = (TextView) mDialog.findViewById(R.id.tvPushedProductNameModify);
        TextView tvProductCode = (TextView) mDialog.findViewById(R.id.tvPushedProductCodeModify);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvPushedProductOkModify);

        tvCantains.setText("The NFC code contains");
        tvProductName.setText("Product name: " + productName);
        tvProductCode.setText("Unique code: " + productCode);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                NFCScannerOwnerActivity.this.finish();
            }
        });

        mDialog.show();
    }

    private void okDialog(final Context context, String msg) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.dialog_alert_ok, null);
        mDialog.setContentView(dialoglayout);

        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);

        tvMsg.setText(msg);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();
            }
        });

        mDialog.show();
    }


    private String ByteArrayToHexString(byte[] inarray) {
        int i, j, in;
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        String out = "";

        for (j = 0; j < inarray.length; ++j) {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    private void getId() {
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivImageLeft:
                finish();
                break;
            case R.id.ivImageRight:
                intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nfcscanner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
