package com.srinivas.restaurantowner.activity.enduser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.PreventiveList;
import com.srinivas.restaurantowner.Services.AddPreventiveRequest;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.HomeActivity;
import com.srinivas.restaurantowner.adapter.enduser.ProductAdapter;
import com.srinivas.restaurantowner.adapter.enduser.SearchAdapter;
import com.srinivas.restaurantowner.adapter.enduser.SearchProductAdapter;
import com.srinivas.restaurantowner.model.ProductModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends Activity implements View.OnClickListener {
    private SearchProductAdapter searchProductAdapter;
    private SearchAdapter searchAdapter;
    private ArrayAdapter<String>
            dataAdapter;
    private TextView tvSearchProduct, tvHeader;
    private ListView listView;
    int i;
    private EditText etSearch;
    private LinearLayout ll_disable;
    private Intent intent;
    private RecyclerView rvProducts;
    private LinearLayoutManager linearLayoutManager;
    private Context context = ProductsActivity.this;
    private ProductAdapter productAdapter;
    public List<ProductModel> data;
    public List<String> arrSeracheFirst;
    public List<String> arrSeracheLast;

    public TextView tvAddIngredients, tvSearchedProduct, tvPreventiveList;
    private String friendProfile;
    private ImageView ivImageLeft, ivImageRight;
    private String TAG = ProductsActivity.class.getSimpleName(), memberID = "";
    public List<String> productlist;
    public List<PreventiveList> preventiveLists;


    private List<IngredientList> ingredientLists = new ArrayList<IngredientList>();
    private boolean isMatchFound = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);


        if (getIntent() != null && getIntent().hasExtra(AppConstants.FRIEND_PROFILE) && getIntent()
                .getStringExtra(AppConstants.FRIEND_PROFILE).length() > 0) {
            friendProfile = getIntent().getStringExtra(AppConstants.FRIEND_PROFILE);
            if (friendProfile.equals(AppConstants.PRODUCT_ACTIVITY_KEY)) {
                tvAddIngredients.setVisibility(View.GONE);
                tvSearchedProduct.setVisibility(View.VISIBLE);
                tvPreventiveList.setVisibility(View.GONE);
                ivImageRight.setVisibility(View.GONE);
                tvHeader.setText("Products");
            } else {
                tvAddIngredients.setVisibility(View.VISIBLE);
                tvSearchedProduct.setVisibility(View.GONE);
                tvPreventiveList.setVisibility(View.VISIBLE);
                ivImageRight.setVisibility(View.VISIBLE);
                tvHeader.setText("Ingredient");
            }
        }


        if (getIntent() != null && getIntent().hasExtra("memberID") &&
                getIntent().getStringExtra("memberID").length() > 0) {
            memberID = getIntent().getStringExtra("memberID");
        }


        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                CommonUtils.hide_keyboard(ProductsActivity.this);
                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    request.ingredient = etSearch.getText().toString();
                    searchIngredientsList(request);
                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }

                return true;
            }
        });


    }



    /*..................................IngredientList.............................*/

    private void searchIngredientsList(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait..");
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        String url = RequestURL.Search_Ingredients;
        Ion.with(context)
                .load(url)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                        }
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                                if (response.ingredient_list != null) {
                                    if (response.ingredient_list.size() > 0) {
                                        listView.setVisibility(View.VISIBLE);
                                        ingredientLists = response.ingredient_list;
                                        Log.e(TAG, ":::LIST SIZE:::" + ingredientLists.size());



                                       /* searchAdapter = new SearchAdapter(context, ingredientLists);
                                        listView.setAdapter(searchAdapter);*/

                                        for (int i = 0; i < response.ingredient_list.size(); i++) {
                                            if (etSearch.getText().toString().trim().equalsIgnoreCase(response.ingredient_list.get(i).ingredient_name)) {
                                                searchAdapter = new SearchAdapter(context, response.ingredient_list, true, i);
                                                listView.setAdapter(searchAdapter);
                                                listView.setSelection(i);
                                                isMatchFound = true;
                                                break;
                                            }
                                        }

                                        if (!isMatchFound) {
                                            searchAdapter = new SearchAdapter(context, response.ingredient_list);
                                            listView.setAdapter(searchAdapter);
                                        }
                                        isMatchFound = false;

                                    } else {
                                        listView.setVisibility(View.GONE);
                                        CommonUtils.showToast(context, "No result");
                                    }


                                }


                            } else {
                                CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                            }

                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }


    private void getId() {
        data = new ArrayList<>();
        listView = (ListView) findViewById(R.id.lvProducts);
        rvProducts = (RecyclerView) findViewById(R.id.rvProducts);
        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvProducts.setLayoutManager(linearLayoutManager);
        tvAddIngredients = (TextView) findViewById(R.id.tvAddIngredients);
        tvSearchedProduct = (TextView) findViewById(R.id.tvSearchedProduct);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvPreventiveList = (TextView) findViewById(R.id.tvPreventiveList);
        etSearch = (EditText) findViewById(R.id.etSearch);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        ll_disable = (LinearLayout) findViewById(R.id.ll_disable);
        ingredientLists = new ArrayList<IngredientList>();
    }

    private void setListener() {
        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
        tvAddIngredients.setOnClickListener(this);
        etSearch.setOnClickListener(this);
        ll_disable.setOnClickListener(this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                etSearch.setText("");
                CommonUtils.hide_keyboard(ProductsActivity.this);
                listView.setVisibility(View.INVISIBLE);

                ProductModel productModel = new ProductModel();

                if (ingredientLists != null && ingredientLists.size() > 0) {
                    if (ingredientLists.get(position).ingredient_name != null && ingredientLists.get(position).ingredient_name.length() > 0) {
                        productModel.setTvProductName(ingredientLists.get(position).ingredient_name);
                        data.add(productModel);
                        productAdapter = new ProductAdapter(getApplication(), data);
                        rvProducts.setAdapter(productAdapter);
                        productAdapter.notifyDataSetChanged();
                    }

                }


            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivImageLeft:
                try {
                    CommonUtils.hide_keyboard(ProductsActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();

                break;
            case R.id.ivImageRight:
                finishAffinity();
                CommonUtils.hideSoftKeyboard(this);
                startActivity(new Intent(this, HomeActivity.class));
                break;
            case R.id.tvAddIngredients:

                if (CommonUtils.isOnline(context)) {
                    AddPreventiveRequest request = new AddPreventiveRequest();
                    if (memberID.equalsIgnoreCase("") || memberID.length() == 0) {
                        request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                        request.user_type = "user";
                    } else {
                        request.member_id = memberID;
                        request.user_type = "member";
                    }

                    try {
                        List<String> stringList = new ArrayList<>();
                        for (int i = 0; i < data.size(); i++) {
                            stringList.add(data.get(i).getTvProductName());
                        }

                        request.name = stringList;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (request.name.size() > 0) {
                        addPreventiveApi(request);
                    } else {
                        if (etSearch.getText().toString().trim().length() > 0) {
                            CommonUtils.hide_keyboard(ProductsActivity.this);
                            if (CommonUtils.isOnline(context)) {
                                ServiceRequest requestSearch = new ServiceRequest();
                                requestSearch.ingredient = etSearch.getText().toString();
                                searchIngredientsList(requestSearch);
                            } else {
                                CommonUtils.showToast(context, getString(R.string.please_check_internet));
                            }
                        } else {
                            CommonUtils.showToast(context, "Please enter text to search");
                        }
                    }


                } else {
                    CommonUtils.showToast(context, getString(R.string.please_check_internet));
                }


                break;
            case R.id.etSearch:

                break;
        }
    }

 /*..................................Add preventive.............................*/

    private void addPreventiveApi(AddPreventiveRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Please wait..");
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        String url = RequestURL.Add_Preventive;
        Ion.with(context)
                .load(url)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                })
                .setCallback(new FutureCallback<ServiceResponse>() {
                    @Override
                    public void onCompleted(Exception e, final ServiceResponse response) {
                        Log.i("Tag", " response data " + new Gson().toJson(response));
                        progressDialog.dismiss();
                        if (response != null) {
                            if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                                finish();

                            } else {
                                CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                            }

                        } else {
                            Toast.makeText(context, getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }
                    }
                });

    }

}
