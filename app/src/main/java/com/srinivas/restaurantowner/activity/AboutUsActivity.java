package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.utils.CommonUtils;


public class AboutUsActivity extends Activity implements View.OnClickListener {
    private TextView tvTermCondition, tvHeader;
    private Context context;
    private ImageView ivImageLeft, ivImageRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_plolicy);
        context = AboutUsActivity.this;
        getID();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
        if (CommonUtils.isOnline(context)) {
            aboutusApi();
        } else {
            CommonUtils.showToast(context, getString(R.string.please_check_internet));
        }


        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setImageResource(R.drawable.home_btn);
        tvHeader.setText("How it works");

    }

    private void getID() {
        tvTermCondition = (TextView) findViewById(R.id.tvTermCondition);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
    }

    private void setListeners() {

        ivImageLeft.setOnClickListener(this);
        ivImageRight.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvTermCondition:

                break;

            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;
            case R.id.ivImageRight:
              /*  finishAffinity();
                if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_CHEF)) {
                    startActivity(new Intent(this, ChefHomeActivity.class));
                } else if (CommonUtils.getPreferences(context, AppConstants.USER_TYPE).equalsIgnoreCase(AppConstants.USER_TYPE_NORMAL)) {
                    startActivity(new Intent(this, HomeActivity.class));
                } else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                break;*/
                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
                break;


        }
    }

    private void aboutusApi() {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));

        String URl= RequestURL.About_us;
       // String URl = "http://youngcart.in/pages/howitworks";
        //  String URl = "https://barcode-scan.herokuapp.com/about_us";
        JsonObject responsejson = new JsonObject();
        Log.i("Tag", "req data " + new Gson().toJson(responsejson));
        Log.i("Tag", " url " + URl);
        Ion.with(context).load("GET", URl)./*setJsonObjectBody(responsejson).*/as(new TypeToken<ServiceResponse>() {
        }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data " + new Gson().toJson(response));
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Log.e("Login", "res = " + new Gson().toJson(response));
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        tvTermCondition.setText(response.description);


                    } else {
                        CommonUtils.showAlertTitle(getString(R.string.alert), response.response_message, context);
                    }
                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }


}
