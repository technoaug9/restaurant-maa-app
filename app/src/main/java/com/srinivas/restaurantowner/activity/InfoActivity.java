package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class InfoActivity extends Activity implements View.OnClickListener {
    private Context mContext;
    private LinearLayout tv1, tv2, tv3, tv4;
    private TextView tvHeader;
    private Intent intent;
    private ImageView ivImageLeft, ivImageRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_info);
        mContext = InfoActivity.this;
        getID();
       // setText();
        //setBackground();
        setListeners();
        CommonUtils.hideSoftKeyboard(this);
    }

/*    private void setText() {
       *//* tv1.setText("How it works");
        tv2.setText("Terms & Policy");
        tv3.setText("Contact Us");
        tv4.setText("Subscription");*//*

    }

    private void setBackground() {
        tv1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.about_btn, 0, 0);
        tv2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.doc_icon, 0, 0);
        tv3.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.contact_btn, 0, 0);
        tv4.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sub_btn, 0, 0);


    }*/

    private void getID() {

        tv1 = (LinearLayout) findViewById(R.id.tv1);
        tv2 = (LinearLayout) findViewById(R.id.tv2);
        tv3 = (LinearLayout) findViewById(R.id.tv3);
        tv4 = (LinearLayout) findViewById(R.id.tv4);
        tvHeader = (TextView) findViewById(R.id.tvHeader);

        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader.setText("Info");

        ivImageLeft.setImageResource(R.drawable.back_btn);

        ivImageRight.setVisibility(View.INVISIBLE);


    }

    private void setListeners() {

        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);

        ivImageLeft.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv1:
                intent = new Intent(mContext, AboutUsActivity.class);
                startActivity(intent);
                break;
            case R.id.tv2:
                intent = new Intent(mContext, TermPolicyActivity.class).putExtra(AppConstants.USER_TYPE, AppConstants.USER_TYPE_NORMAL);
                ;

                startActivity(intent);
                break;
            case R.id.tv3:
                intent = new Intent(mContext, ContactUsActivity.class);
                startActivity(intent);
                break;

            case R.id.tv4:
                intent = new Intent(mContext, SubscriptionActivity.class);
                startActivity(intent);
                break;

            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();

                break;


        }
    }


}
