package com.srinivas.restaurantowner.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.owner.RestaurantRegistrationActivity;
import com.srinivas.restaurantowner.model.SpinnerRecyclerModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.List;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private TextView tvSignUp, tvGeneralUser, tvHeader, tvIAgreeYour, tvTermAndCondition, gen, res;
    private EditText etName, etEmailId, etPassword, etConfirmPassword;
    private ImageView ivImageLeft, ivImageRight;
    private boolean isChecked = false;
    private Context context = SignUpActivity.this;
    public List<SpinnerRecyclerModel> data;
    private Boolean spinnerBoolean = false;
    private LinearLayout llGeneralUser;
    public String OwnerName, OwnerEmail, OwnerPassword, OwnerConfPass;
    private ServiceRequest request;
    private String providerID = "";
    private String signuptype = "", providerName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        getId();
        setListener();
        CommonUtils.hideSoftKeyboard(this);

        if (getIntent() != null && getIntent().hasExtra("saveData")) {
            Log.e(TAG, "inside_getIntent>>>>");
            request = (ServiceRequest) getIntent().getSerializableExtra("saveData");
            etName.setText(request.name);
            etEmailId.setText(request.email);
            providerID = request.provider_id;
            signuptype = request.signup_type;
            providerName = request.provider_name;
            Log.e(TAG, "<<<<name>>>>" + etName + "<<<<email>>>>" + etEmailId + "<<<<<<providerID>>>>>" + providerID + "<<<<providerName>>>>" + providerName);

        }


    }

    private void getId() {

        tvSignUp = (TextView) findViewById(R.id.tvSignUp);
        tvGeneralUser = (TextView) findViewById(R.id.tvGeneralUser);
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvIAgreeYour = (TextView) findViewById(R.id.tvIAgreeYour);
        tvTermAndCondition = (TextView) findViewById(R.id.tvTermAndCondition);

        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        llGeneralUser = (LinearLayout) findViewById(R.id.llGeneralUser);
        etName = (EditText) findViewById(R.id.etName);
        etEmailId = (EditText) findViewById(R.id.etEmailId);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        gen = (TextView) findViewById(R.id.gen);
        res = (TextView) findViewById(R.id.res);
        ivImageLeft.setImageResource(R.drawable.back_btn);
        ivImageRight.setVisibility(View.INVISIBLE);
        tvHeader.setText("Sign Up");
        tvGeneralUser.setText("General User");
    }

    private void setListener() {
        tvSignUp.setOnClickListener(this);
        tvGeneralUser.setOnClickListener(this);
        llGeneralUser.setOnClickListener(this);
        tvTermAndCondition.setOnClickListener(this);
        tvIAgreeYour.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
        gen.setOnClickListener(this);
        res.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvSignUp:
                if (checkValidation()) {

                    if (tvGeneralUser.getText().toString().equalsIgnoreCase("General User")) {

                        if (CommonUtils.isOnline(context)) {
                            if (request != null && !TextUtils.isEmpty(request.provider_name)) {
                                if (request.provider_name.equalsIgnoreCase("google+")) {
                                    Log.e(TAG, "name_inside_google+>>>>" + request.provider_name);
                                    request.email = etEmailId.getText().toString().trim();
                                    request.restaurant_name = "";
                                    request.phone_no = "";
                                    request.user_type = AppConstants.USER_TYPE_OWNER;
                                    request.signup_type = "social_auth";
                                    request.provider_id = providerID;
                                    request.provider_name = "google+";
                                    callSocialSignUpApi(request);
                                } else if (request.provider_name.equalsIgnoreCase("facebook")) {
                                    Log.e(TAG, "name_inside_facebook>>>>" + request.provider_name);
                                    request.email = etEmailId.getText().toString().trim();
                                    request.restaurant_name = "";
                                    request.phone_no = "";
                                    request.user_type = AppConstants.USER_TYPE_OWNER;
                                    request.signup_type = "social_auth";
                                    request.provider_id = providerID;
                                    request.provider_name = "facebook";
                                    callSocialSignUpApi(request);
                                }

                            } else {
                                request = new ServiceRequest();
                                request.signup_type = "inapp";
                                request.email = etEmailId.getText().toString().trim();
                                request.name = etName.getText().toString().trim();
                                request.password = etPassword.getText().toString().trim();
                                request.password_confirmation = etConfirmPassword.getText().toString().trim();
                                request.user_type = AppConstants.USER_TYPE_NORMAL;
                                request.restaurant_name = "";
                                request.phone_no = "";
                                getSignupApi(request);
                            }


                        } else {
                            Toast.makeText(context, getString(R.string.please_check_internet), Toast.LENGTH_SHORT).show();
                        }

                    } else {

                     /*   request.email = etEmailId.getText().toString().trim();
                        request.provider_id = providerID;
                        request.provider_name = "facebook";*/

                        Bundle b = new Bundle();
                        b.putString("OwnerEmail", etEmailId.getText().toString().trim());
                        b.putString("OwnerName", etName.getText().toString().trim());

                        b.putString("OwnerPassword", etPassword.getText().toString().trim());
                        b.putString("OwnerConfPass", etConfirmPassword.getText().toString().trim());

                        b.putString("provider_id", providerID);
                        b.putString("provider_name", providerName);

                        Log.e(TAG, "<<<<name>>>>" + etName + "<<<<email>>>>" + etEmailId +
                                "<<<<<<providerID>>>>>" + providerID + "<<<<providerName>>>>" + providerName);

                        Intent in = new Intent(SignUpActivity.this, RestaurantRegistrationActivity.class);
                        in.putExtras(b);
                        startActivity(in);
                    }

                }

                break;

            case R.id.tvGeneralUser:
                if (!spinnerBoolean) {
                    spinnerBoolean = true;

                    if (tvGeneralUser.getText().toString().trim().equalsIgnoreCase("General User")) {
                        res.setVisibility(View.VISIBLE);
                        gen.setVisibility(View.GONE);

                    } else if (tvGeneralUser.getText().toString().trim().equalsIgnoreCase("Restaurant Owner")) {
                        gen.setVisibility(View.VISIBLE);
                        res.setVisibility(View.GONE);
                    }


                } else if (spinnerBoolean) {
                    spinnerBoolean = false;

                    res.setVisibility(View.GONE);
                    gen.setVisibility(View.GONE);

                }

                break;

            case R.id.llGeneralUser:
                if (!spinnerBoolean) {
                    spinnerBoolean = true;
                    if (tvGeneralUser.getText().toString().trim().equalsIgnoreCase("General User")) {
                        res.setVisibility(View.VISIBLE);
                        gen.setVisibility(View.GONE);
                    } else if (tvGeneralUser.getText().toString().trim().equalsIgnoreCase("Restaurant Owner")) {
                        gen.setVisibility(View.VISIBLE);
                        res.setVisibility(View.GONE);
                    }


                } else if (spinnerBoolean) {
                    spinnerBoolean = false;
                    res.setVisibility(View.GONE);
                    gen.setVisibility(View.GONE);

                }


                break;
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();

                break;

            case R.id.tvIAgreeYour:

                if (isChecked) {
                    tvIAgreeYour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon, 0, 0, 0);

                    isChecked = false;

                } else {
                    tvIAgreeYour.setCompoundDrawablesWithIntrinsicBounds(R.drawable.check_icon_sel, 0, 0, 0);

                    isChecked = true;
                }
                break;

            case R.id.tvTermAndCondition:

                startActivity(new Intent(this, TermPolicyActivity.class).putExtra(AppConstants.TERM_AND_POLICY_KEY, AppConstants.TERM_SIGN_UP_KEY));

                break;
            case R.id.gen:
                tvGeneralUser.setText("General User");
                gen.setVisibility(View.GONE);
                spinnerBoolean = false;
                break;
            case R.id.res:
                tvGeneralUser.setText("Restaurant Owner");
                res.setVisibility(View.GONE);
                spinnerBoolean = false;
                break;
        }

    }

    private void callSocialSignUpApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.SOCIAL_NEW_SIGN_UP;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        Log.i("Tag", " url " + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {

                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.response_message.equalsIgnoreCase("You've signed up successfully.You will receive a verification email .")) {
                            showAlert(response.response_message, context);
                        } else {
                            if (response.user != null) {
                                if (!TextUtils.isEmpty(response.user.id)) {
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_ID, response.user.id);
                                }
                                if (!TextUtils.isEmpty(response.user.user_type)) {
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_TYPE, response.user.user_type);
                                    CommonUtils.saveIntPreferences(context, AppConstants.PRE_ISLOGIN, 1);
                                    Intent intent = new Intent(context, HomeActivity.class);
                                    startActivity(intent);
                                }
                            }
                        }


                    } else {
                        CommonUtils.showToast(context, response.response_message);
                    }


                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });

    }

    private void getDataFromForm() {
        OwnerName = etName.getText().toString();
        OwnerEmail = etEmailId.getText().toString();
        OwnerPassword = etPassword.getText().toString();
        OwnerConfPass = etConfirmPassword.getText().toString();
    }

    private boolean checkValidation() {

        if (etName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter name.", null, View.GONE);
            etName.requestFocus();
            return false;
        } else if (etEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter email.", null, View.GONE);
            etEmailId.requestFocus();
            return false;
        } else if (!CommonUtils.checkEmailId(etEmailId.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            etEmailId.requestFocus();

            return false;
        } else if (etPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter password.", null, View.GONE);

            etPassword.requestFocus();

            return false;
        } else if (etPassword.getText().toString().trim().length() < 8) {
            CommonUtils.showCustomDialogOk(this, "Password must be of 8 characters.", null, View.GONE);
            etPassword.requestFocus();

            return false;
        } else if (etConfirmPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter confirm password.", null, View.GONE);
            etConfirmPassword.requestFocus();

            return false;
        } else if (!etPassword.getText().toString().trim().equals(etConfirmPassword.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "New password and confirm password does not matches.", null, View.GONE);
            etConfirmPassword.requestFocus();

            return false;
        } else if (!isChecked) {
            CommonUtils.showCustomDialogOk(this, "Please check terms & policy.", null, View.GONE);
            return false;
        } else {
            return true;
        }

    }

    private void getSignupApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.NEW_SIGN_UP;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        Log.i("Tag", " url " + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {

                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        if (response.response_message.equalsIgnoreCase("You've signed up successfully.You will receive a verification email .")) {
                            showAlert(response.response_message, context);
                        } else {
                            if (response.user != null) {
                                if (!TextUtils.isEmpty(response.user.id)) {
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_ID, response.user.id);
                                }
                                if (!TextUtils.isEmpty(response.user.user_type)) {
                                    CommonUtils.savePreferencesString(context, AppConstants.USER_TYPE, response.user.user_type);
                                    CommonUtils.saveIntPreferences(context, AppConstants.PRE_ISLOGIN, 1);
                                    Intent intent = new Intent(context, HomeActivity.class);
                                    startActivity(intent);
                                }
                            }
                        }


                    } else {
                        CommonUtils.showToast(context, response.response_message);
                    }


                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });
    }

    private void showAlert(String message, final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                finish();

                            }
                        });

        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
