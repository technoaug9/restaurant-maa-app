package com.srinivas.restaurantowner.activity.owner;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.LoginActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

public class RestaurantRegistrationActivity extends Activity implements View.OnClickListener {
    private TextView tvHeader, tvRegister;
    private EditText etRestaurantName, etContactNumber;
    private EditText tvName, tvEmailId, tvPassword, tvConfirmPassword;
    private boolean isChecked = false;
    private ImageView ivImageLeft, ivImageRight;
    private Intent intent;
    private ServiceRequest request;
    private Context context = RestaurantRegistrationActivity.this;
    private String MobilePattern = "[0-9]{10}";
    private String nameString = "", emailString = "", passString = "", confpassString = "";
    private static final String TAG = RestaurantRegistrationActivity.class.getSimpleName();
    private String providerID = "";
    private String signuptype = "", providerName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_registration);
        getId();
        CommonUtils.hideSoftKeyboard(this);
        setListener();
  /*      if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle b = getIntent().getExtras();
            nameString = b.getString("OwnerName");
            emailString = b.getString("OwnerEmail");
            passString = b.getString("OwnerPassword");
            confpassString = b.getString("OwnerConfPass");
            providerID = b.getString("provider_id");
            providerName = b.getString("provider_name");
            tvName.setText("" + nameString);
            tvEmailId.setText("" + emailString);
            tvPassword.setText("" + passString);
            tvConfirmPassword.setText("" + confpassString);
        }*/
        if (getIntent() != null && getIntent().hasExtra("saveData")) {
            Log.e(TAG, "inside_getIntent>>>>");
            request = (ServiceRequest) getIntent().getSerializableExtra("saveData");
            tvName.setText(request.name);
            emailString=request.email;
            nameString=request.name;
            tvEmailId.setText(request.email);
            providerID = request.provider_id;
            signuptype = request.signup_type;
            providerName = request.provider_name;
            Log.e(TAG, "<<<<name>>>>" + tvName + "<<<<email>>>>" + tvEmailId + "<<<<<<providerID>>>>>" + providerID + "<<<<providerName>>>>" + providerName);

        }
    }

    private void setListener() {
        tvRegister.setOnClickListener(this);
        ivImageLeft.setOnClickListener(this);
    }

    private void getId() {
        tvHeader = (TextView) findViewById(R.id.tvHeader);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        tvName = (EditText) findViewById(R.id.tvName);
        tvEmailId = (EditText) findViewById(R.id.tvEmailId);
        tvPassword = (EditText) findViewById(R.id.tvPassword);
        tvConfirmPassword = (EditText) findViewById(R.id.tvConfirmPassword);
        etRestaurantName = (EditText) findViewById(R.id.etRestaurantName);
        etContactNumber = (EditText) findViewById(R.id.etContactNumber);
        ivImageLeft = (ImageView) findViewById(R.id.ivImageLeft);
        ivImageRight = (ImageView) findViewById(R.id.ivImageRight);
        tvHeader.setText("Restaurant Registration");
        ivImageRight.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_restaurant_registration, menu);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvRegister:
                if (checkValidation()) {
                    ServiceRequest request = new ServiceRequest();
                    if (CommonUtils.isOnline(context)) {
                        if (providerName.equalsIgnoreCase("google")) {
                            request.email = emailString;
                            request.restaurant_name = etRestaurantName.getText().toString();
                            request.phone_no = etContactNumber.getText().toString().trim();
                            request.user_type = AppConstants.USER_TYPE_OWNER;
                            request.signup_type = "social_auth";
                            request.provider_id = providerID;
                            request.provider_name = "google+";
                            request.name = nameString;
                            callSocialSignUpApi(request);
                        } else if (providerName.equalsIgnoreCase("facebook")) {
                            request.email = emailString;
                            request.restaurant_name = etRestaurantName.getText().toString();
                            request.phone_no = etContactNumber.getText().toString().trim();
                            request.user_type = AppConstants.USER_TYPE_OWNER;
                            request.signup_type = "social_auth";
                            request.provider_id = providerID;
                            request.name = nameString;
                            request.provider_name = "facebook";
                            callSocialSignUpApi(request);
                            Toast.makeText(getBaseContext(),"in facebook",Toast.LENGTH_SHORT).show();
                        } else {
                            request = new ServiceRequest();
                            request.signup_type = "inapp";
                            request.email = tvEmailId.getText().toString().trim();
                            request.name = tvName.getText().toString().trim();
                            request.password = tvPassword.getText().toString().trim();
                            request.password_confirmation = tvConfirmPassword.getText().toString().trim();
                            request.user_type = AppConstants.USER_TYPE_OWNER;
                            request.restaurant_name = etRestaurantName.getText().toString();
                            request.phone_no = etContactNumber.getText().toString().trim();
                            getSignupApi(request);
                            Toast.makeText(getBaseContext(),"in app",Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(context, getString(R.string.please_check_internet), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.ivImageLeft:
                CommonUtils.hideSoftKeyboard(this);
                finish();
                break;
        }
    }

    private void callSocialSignUpApi(ServiceRequest request) {
        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.SOCIAL_NEW_SIGN_UP;
        Gson gson = new Gson();
        request.password = tvPassword.getText().toString().trim();
        request.password_confirmation = tvConfirmPassword.getText().toString().trim();
        request.phone_no = etContactNumber.getText().toString().trim();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));
        Log.e(TAG, " url " + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e(TAG, " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {

                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        customDialog(response.response_message);

                    } else {
                        CommonUtils.showToast(context, response.response_message);
                    }


                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });

    }


    private void getSignupApi(ServiceRequest request) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.NEW_SIGN_UP;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.i("Tag", "req data " + new Gson().toJson(requestJson));
        Log.i("Tag", " url " + url);

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {

            public void onCompleted(Exception e, ServiceResponse response) {
                Log.i("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {

                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {

                        customDialog(response.response_message);


                    } else {
                        CommonUtils.showToast(context, response.response_message);
                    }


                } else {
                    CommonUtils.showToast(context, getString(R.string.server_error));
                }
            }

        });


    }


    private boolean checkValidation() {

        if (etRestaurantName.getText().toString().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter Restaurant name.", null, View.GONE);
            etRestaurantName.requestFocus();
            return false;
        }
        else if (tvName.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter Name.", null, View.GONE);
            tvName.requestFocus();
            return false;
        }
        else if (tvEmailId.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter Email.", null, View.GONE);
            tvEmailId.requestFocus();
            return false;
        }
        else if (!CommonUtils.checkEmailId(tvEmailId.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid email.", null, View.GONE);
            tvEmailId.requestFocus();

            return false;
        }
        else if (etContactNumber.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter contact number.", null, View.GONE);
            etContactNumber.requestFocus();
            return false;
        } else if (!etContactNumber.getText().toString().matches(MobilePattern)) {
            CommonUtils.showCustomDialogOk(this, "Please enter valid 10 digit phone number", null, View.GONE);
            etContactNumber.requestFocus();
            return false;
        }

        else if (tvPassword.getText().toString().trim().length() < 6) {
            CommonUtils.showCustomDialogOk(this, "Please enter Password.", null, View.GONE);
            tvPassword.requestFocus();
            return false;
        }
        else if (tvConfirmPassword.getText().toString().trim().length() == 0) {
            CommonUtils.showCustomDialogOk(this, "Please enter ConfirmPassword.", null, View.GONE);
            tvConfirmPassword.requestFocus();
            return false;
        }

        else if (!tvPassword.getText().toString().trim().equals(tvConfirmPassword.getText().toString().trim())) {
            CommonUtils.showCustomDialogOk(this, "Password and confirm password does not matches.", null, View.GONE);
            tvConfirmPassword.requestFocus();

            return false;
        }
        return true;
    }


    private void customDialog(String message) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.registration_dialog, null);
        mDialog.setContentView(dialoglayout);

        TextView tvOk = (TextView) dialoglayout.findViewById(R.id.tvOk);
        TextView tvCheckMail = (TextView) dialoglayout.findViewById(R.id.tvCheckMail);

        tvCheckMail.setText(message);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
                intent = new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
