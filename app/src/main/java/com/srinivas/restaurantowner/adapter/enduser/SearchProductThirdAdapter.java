package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.SearchProductThirdModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SearchProductThirdAdapter extends RecyclerView.Adapter<SearchProductThirdAdapter.MyViewHolder> {
    List<SearchProductThirdModel> searchProductThirdModelList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;


    public SearchProductThirdAdapter(Context context, List<SearchProductThirdModel> searchProductThirdModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.searchProductThirdModelList = searchProductThirdModels;

    }

    public void delete(int position) {
        searchProductThirdModelList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_search_product_third, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SearchProductThirdModel current = searchProductThirdModelList.get(position);
        holder.tvSearchProductName.setText(current.getTvSearchProductUser());
        holder.tvSearchProductAdvice.setText(current.getTvSearchProductUser());
        holder.tvSearchProductIngredients.setText(current.getTvSearchProductUser());
        if (position<2) {
            holder.llSearchProduct.setBackgroundColor(Color.parseColor("#ff4a4a"));
        }
    }

    @Override
    public int getItemCount() {
        return searchProductThirdModelList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSearchProductName, tvSearchProductAdvice, tvSearchProductIngredients;
        LinearLayout llSearchProduct;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvSearchProductName = (TextView) itemView.findViewById(R.id.tvSearchProductName);
            tvSearchProductAdvice = (TextView) itemView.findViewById(R.id.tvSearchProductAdvice);
            tvSearchProductIngredients = (TextView) itemView.findViewById(R.id.tvSearchProductIngredients);
            llSearchProduct=(LinearLayout) itemView.findViewById(R.id.llSearchProduct);
        }
    }
}