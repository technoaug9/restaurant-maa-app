package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.utils.Log;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 23/9/15.
 */
public class AddIngredientOwnerAdapter extends RecyclerView.Adapter<AddIngredientOwnerAdapter.MyViewHolder> {
    List<IngredientList> productModel = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private String searchedProduct;
    private Intent intent;


    public AddIngredientOwnerAdapter(Context context, List<IngredientList> productModels) {

        this.context = context;
        inflater = LayoutInflater.from(context);
        Log.e("TAG", "listsize>>>>>>" + productModel.size());
        this.productModel = productModels;
    }

    public void delete(int position) {
        productModel.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_add_ingredients, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        if (productModel.size() > 0) {
            if (productModel.get(position).ingredient_name != null && productModel.get(position).ingredient_name.length() > 0) {
                holder.tvProductName.setText(productModel.get(position).ingredient_name);
            } else {
                holder.tvProductName.setText("");
            }
        }


    }

    @Override
    public int getItemCount() {
        return productModel.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductName;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductsName);
        }
    }
}
