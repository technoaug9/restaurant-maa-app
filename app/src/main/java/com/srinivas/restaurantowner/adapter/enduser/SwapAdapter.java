package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.MemberList;
//import com.srinivas.barcodescanner.activity.enduser.MemberProfileActivity;

import java.util.List;



public class SwapAdapter extends RecyclerView.Adapter<SwapAdapter.ViewHolder> {
    private Context context;
    public List<MemberList> memberLists;
    public List<String> image;

    public SwapAdapter(Context context, List<MemberList> memberLists) {
        this.memberLists = memberLists;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_manage_profile_images, parent, false);
        ViewHolder vh = new ViewHolder(v);
        vh.ivMemberProfileImage = (ImageView) v.findViewById(R.id.ivMemberProfileImage);
        vh.tvMemberName = (TextView) v.findViewById(R.id.tvMemberName);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final MemberList member = memberLists.get(position);

        if (member.name != null) {
            holder.tvMemberName.setText(member.name);
        }

        if (member.image != null && !member.image.equalsIgnoreCase("")) {
            String imageValues = member.image;
            Ion.with(context)
                    .load(imageValues)
                    .withBitmap()
                    .placeholder(R.drawable.img_profile)
                    .error(R.drawable.img_profile)
                    .intoImageView(holder.ivMemberProfileImage);
        }
        holder.ivMemberProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //android.util.Log.e("::::::::::::::::::SWAP ADAPTER::::::::::", "+++++" + String.valueOf(member.id));
                /*Intent intent = new Intent(context, MemberProfileActivity.class);
                intent.putExtra("memberId", String.valueOf(member.id));
                context.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return memberLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivMemberProfileImage;
        private TextView tvMemberName;

        public ViewHolder(View v) {
            super(v);
            ivMemberProfileImage = (ImageView) v.findViewById(R.id.ivMemberProfileImage);
            tvMemberName = (TextView) v.findViewById(R.id.tvMemberName);
        }
    }
}