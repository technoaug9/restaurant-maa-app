package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.ChefList;
import com.srinivas.restaurantowner.activity.owner.ChefProductsActivity;
import com.srinivas.restaurantowner.utils.Log;

import java.util.List;

/**
 * Created by vinay.tripathi on 29/10/15.
 */
public class ChefListAdapter extends RecyclerView.Adapter {
    List<ChefList> chefLists;
    private LayoutInflater inflater;
    private Context context;
    private Intent intent;
    private final int BODY = 1;
    private final int VIEW_PROGRESS = 0;

    public ChefListAdapter(Context context, List<ChefList> chefLists) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.chefLists = chefLists;
        // android.util.Log.e("TAG", "listSize>>>>>" + chefLists.size());

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case BODY:
                Log.e("Adapter", "adapter>>>>>>BODY");
                view = inflater.inflate(R.layout.row_add_chef, parent, false);
                return new MyViewHolder(view);
            case VIEW_PROGRESS:
                Log.e("Adapter", "adapter>>>>>>PROGRESS");
                view = inflater.inflate(R.layout.load_more_footer, parent, false);
                return new ProgressViewHolder(view);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case BODY:
                MyViewHolder viewHolderHeader = (MyViewHolder) holder;

                if (chefLists.get(position) != null && chefLists.size() > 0) {
                    final ChefList chefListModel = chefLists.get(position);
                    if (chefLists != null && chefListModel.image != null) {
                        String imageValues = chefListModel.image;
                        Ion.with(context)
                                .load(imageValues)
                                .withBitmap()
                                .placeholder(R.drawable.img_profile)
                                .error(R.drawable.img_profile)
                                .intoImageView(viewHolderHeader.ivuserFrndImage);

                    }
                    if (chefLists != null && chefLists.size() > 0) {

                        if (chefLists.get(position).name != null && chefLists.get(position).name.length() > 0) {
                            viewHolderHeader.tvUserFrndName.setText(chefLists.get(position).name);
                        }

                        viewHolderHeader.ivuserFrndImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                               // android.util.Log.e("::::::::::::::::::AddChef ADAPTER::::::::::", "+++++" + String.valueOf(chefListModel.id));
                                intent = new Intent(context, ChefProductsActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("chefId", String.valueOf(chefListModel.id));
                                intent.putExtra("chefName", chefListModel.name);
                                context.startActivity(intent);
                            }
                        });
                    }
                }


                break;
            case VIEW_PROGRESS:
                ProgressViewHolder viewHolderProgress = (ProgressViewHolder) holder;
                viewHolderProgress.progressBar.setIndeterminate(true);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return chefLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return chefLists.get(position) != null ? BODY : VIEW_PROGRESS;
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUserFrndName;
        private ImageView ivuserFrndImage;
        private LinearLayout llAddChef;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvUserFrndName = (TextView) itemView.findViewById(R.id.tvUserFrndName);
            ivuserFrndImage = (ImageView) itemView.findViewById(R.id.ivuserFrndImage);
            llAddChef = (LinearLayout) itemView.findViewById(R.id.llAddChef);
        }
    }

    class ProgressViewHolder extends RecyclerView.ViewHolder {

        private ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.load_more_progressBar);
        }
    }
}