package com.srinivas.restaurantowner.adapter.owner;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.activity.chef.SelectProductFromNfc;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.NewQrCodeModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.Collections;
import java.util.List;


public class NewQrCodeAdapter extends RecyclerView.Adapter<NewQrCodeAdapter.ViewHolder>
{

    private Context context;
    private LayoutInflater inflater;
    Intent intent;
    List<NewQrCodeModel> data = Collections.emptyList();
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        // each data item is just a string in this case

        public TextView tv_qrdetail;
        public  LinearLayout linearLayout;
        public ImageView iv_back;
        public ViewHolder(View v)
        {
            super(v);
            tv_qrdetail=(TextView)v.findViewById(R.id.tv_qrdetail);
            iv_back=(ImageView)v.findViewById(R.id.iv_back);
            linearLayout=(LinearLayout)v.findViewById(R.id.list_item);

        }
    }
    public NewQrCodeAdapter(Context context, List<NewQrCodeModel> data)
    {
        this.context = context;
        this.data = data;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_new_qr_code,parent,false);
        final ViewHolder viewHolder=new ViewHolder(v);
        viewHolder.iv_back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                intent=new Intent(context, SelectProductFromNfc.class);
                CommonUtils.savePreference(context, AppConstants.IS_NFC, false);
                context.startActivity(intent);

            }
        });
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
       NewQrCodeModel current = data.get(position);
        holder.tv_qrdetail.setText((CharSequence) current.getTv_qrdetail());

        Log.e("data size",""+data.size());
        switch (position)
        {

             case 1:
                 holder.iv_back.setVisibility(View.VISIBLE);
                 holder.iv_back.setImageResource(R.drawable.arrow_);

                break;

        }
    }


    @Override
    public int getItemCount() {
        return
                data.size();
    }
}
