package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.enduser.ScanResultDetailsActivity;
import com.srinivas.restaurantowner.model.MemberResult;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 18/8/15.
 */
public class ScanResultRelativeAdapter extends RecyclerView.Adapter<ScanResultRelativeAdapter.MyViewHolder> {
    List<MemberResult> scanResultRelativeModelList = Collections.emptyList();
    private LayoutInflater inflater;
    private Activity context;


    public ScanResultRelativeAdapter(Activity context, List<MemberResult> scanResultRelativeModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.scanResultRelativeModelList = scanResultRelativeModels;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_scan_result_relative_single, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        if (scanResultRelativeModelList != null && scanResultRelativeModelList.size() > 0) {
            final MemberResult memberResultModel = scanResultRelativeModelList.get(position);

            if (memberResultModel.name != null && memberResultModel.name.length() > 0) {

                holder.tvName.setText(memberResultModel.name);
            } else {
                holder.tvName.setText("");
            }

            if (memberResultModel.harmful_ingredients != null) {

                if (memberResultModel.harmful_ingredients.size() > 0) {
                    holder.llMore.setVisibility(View.VISIBLE);
                    holder.llRel.setBackgroundResource(R.drawable.red_bar);
                    String strListOfUsers = "";
                    for (int i = 0; i < memberResultModel.harmful_ingredients.size(); i++) {
                        strListOfUsers = memberResultModel.harmful_ingredients.get(i) + ",";
                    }
                    holder.tvRelativeIngrediants.setText("It contains..\n" + memberResultModel.harmful_ingredients.get(0));

                } else {
                    holder.tvRelativeIngrediants.setText("");
                    if (memberResultModel.state != null & memberResultModel.state.equalsIgnoreCase("Ingredients of the product are not available, thats why we are not sure about product")) {
                        holder.llMore.setVisibility(View.GONE);
                        holder.llRel.setBackgroundResource(R.drawable.gray_background);

                    } else {
                        holder.llMore.setVisibility(View.VISIBLE);
                        holder.llRel.setBackgroundResource(R.drawable.green_bar);
                    }
                }
            } else {
                holder.llRel.setBackgroundResource(R.drawable.green_bar);
            }

            if (memberResultModel.state != null && memberResultModel.state.length() > 0) {

                holder.tvAdvice.setText(memberResultModel.state);

            } else {
                holder.tvAdvice.setText("");
            }

            holder.llMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!memberResultModel.state.toString().trim().equalsIgnoreCase("You can eat.")) {
                        if (scanResultRelativeModelList.get(position).id != null && scanResultRelativeModelList.get(position).id.length() > 0) {

                            if (scanResultRelativeModelList.get(position).id.equalsIgnoreCase(CommonUtils.getPreferencesString(context, AppConstants.USER_ID))) {
                                Intent intent = new Intent(context, ScanResultDetailsActivity.class);
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, ScanResultDetailsActivity.class);
                                intent.putExtra("memberId", scanResultRelativeModelList.get(position).id);
                                context.startActivity(intent);
                            }
                        }
                    }


                }
            });

        }

    }


    @Override
    public int getItemCount() {
        return scanResultRelativeModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvAdvice, tvRelativeIngrediants;
        LinearLayout llRel, llMore;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvAdvice = (TextView) itemView.findViewById(R.id.tvAdvice);
            tvRelativeIngrediants = (TextView) itemView.findViewById(R.id.tvRelativeIngrediants);
            llRel = (LinearLayout) itemView.findViewById(R.id.llRel);
            llMore = (LinearLayout) itemView.findViewById(R.id.llMore);
        }
    }
}