package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.PreventiveList;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 18/8/15.
 */
public class ScanResultDetailsAdapter extends RecyclerView.Adapter<ScanResultDetailsAdapter.MyViewHolder> {
    List<PreventiveList> scanResultDetailsModelList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;


    public ScanResultDetailsAdapter(Context context, List<PreventiveList> scanResultDetailsModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.scanResultDetailsModelList = scanResultDetailsModels;
    }

    public void delete(int position) {
        scanResultDetailsModelList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_scan_result_detail_single, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        PreventiveList current = scanResultDetailsModelList.get(position);

        if (current.name != null && current.name.length() > 0) {
            holder.tvIngredientsName.setText(current.name);
        }
    }

    @Override
    public int getItemCount() {
        return scanResultDetailsModelList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvIngredientsName;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvIngredientsName = (TextView) itemView.findViewById(R.id.tvIngredientsName);
        }
    }
}