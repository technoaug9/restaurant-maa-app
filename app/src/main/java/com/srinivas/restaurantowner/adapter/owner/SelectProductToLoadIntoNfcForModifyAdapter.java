package com.srinivas.restaurantowner.adapter.owner;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import com.srinivas.restaurantowner.activity.chef.ModifyScanNfcActivity;
import com.srinivas.restaurantowner.model.SelectProductToLoadIntoNfcModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by vivek.pareek on 16/10/15.
 */
public class SelectProductToLoadIntoNfcForModifyAdapter extends RecyclerView.Adapter<SelectProductToLoadIntoNfcForModifyAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    List<SelectProductToLoadIntoNfcModel> data = Collections.emptyList();
    String fromwhere;
    boolean isFromReadScan;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public TextView tvProductsName;

        public LinearLayout linearLayout;

        public ViewHolder(View v) {
            super(v);

            linearLayout=(LinearLayout)v.findViewById(R.id.llNfcList);
            tvProductsName=(TextView)v.findViewById(R.id.tvProductsName);

        }


    }

    public SelectProductToLoadIntoNfcForModifyAdapter(Context context, List<SelectProductToLoadIntoNfcModel> data) {
        this.context = context;

        this.data = data;
        /*this.isFromReadScan=isFromReadScan;*/
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_add_ingredients, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SelectProductToLoadIntoNfcModel current = data.get(position);


        holder.tvProductsName.setText((CharSequence) current.getTv_ProductsName());




            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ModifyScanNfcActivity.class);
                    //  intent.putExtra(AppConstant.LOGIN_KEY, "login");
                    intent.putExtra("from", "SelectProductToLoadIntoNfcForModifyAdapter");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);

                }
            });





    }

    @Override
    public int getItemCount() {
        return
                data.size();
    }



}


