package com.srinivas.restaurantowner.adapter.owner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductListQRAndNFCAdapter extends BaseAdapter implements Filterable{

	//Fragment context;
	private Activity context1;
	private List<Product> rowItems;
	private List<Product> mStringSearch;
	private LayoutInflater mInflater;
	//private FriendListData rowItem ;


	public ProductListQRAndNFCAdapter(Activity context, List<Product> items) {
		mInflater = LayoutInflater.from(context);
		rowItems = items;
		mStringSearch = items;
		this.context1=context;
	}
	/*private view holder class*/
	private class ViewHolder {

		private TextView tvProductName;
		private ImageView ivProductImage, ivTool, ivDelete;
		private LinearLayout llproductlist;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolderForm = null;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_add_product_list, null);
			viewHolderForm = new ViewHolder();
			viewHolderForm.tvProductName = (TextView) convertView.findViewById(R.id.tvProductName);
			viewHolderForm.llproductlist = (LinearLayout) convertView.findViewById(R.id.llproductlist);
			viewHolderForm.ivProductImage = (ImageView) convertView.findViewById(R.id.ivProductImage);
			viewHolderForm.ivTool = (ImageView) convertView.findViewById(R.id.ivTool);
			viewHolderForm.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
			convertView.setTag(viewHolderForm);

		}
		else {
			viewHolderForm = (ViewHolder) convertView.getTag();
		}

		if (rowItems.get(position).name != null && rowItems.get(position).name.length() > 0) {
			viewHolderForm.tvProductName.setText(rowItems.get(position).name);
		} else {
			viewHolderForm.tvProductName.setText("");
		}

		if (rowItems.get(position).image != null && rowItems.get(position).image != null && rowItems.get(position).image.length() > 0) {
			Picasso.with(context1).load(rowItems.get(position).image).placeholder(R.drawable.img).into(viewHolderForm.ivProductImage);
		}else {
			Picasso.with(context1).load(R.drawable.img).into(viewHolderForm.ivProductImage);
		}

		return convertView;


	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return rowItems.size();
	}


	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if(results.values!=null){
					rowItems = (ArrayList<Product>) results.values;
					Log.e("search", "search");
					notifyDataSetChanged();
				}

			}

			@SuppressLint("DefaultLocale")
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {

				FilterResults results = new FilterResults();
				ArrayList<Product> arlFilteredArrayNames = new ArrayList<Product>();
				constraint = constraint.toString().toLowerCase();
				Log.e("","mStringSearch size "+ mStringSearch.size());

				for (int i = 0; i < mStringSearch.size(); i++) {
					Product dataNames = new Product();
					dataNames.name = mStringSearch.get(i).name;
					dataNames.id = mStringSearch.get(i).id;
					dataNames.image = mStringSearch.get(i).image;
					dataNames.ingredients = mStringSearch.get(i).ingredients;
					String searchData = dataNames.name;
					if (searchData.toLowerCase().contains(constraint.toString()))  {
						arlFilteredArrayNames.add(dataNames);
					}
				}
				results.values = arlFilteredArrayNames;

				return results;
			}
		};

		return filter;
	}

	@Override
	public Object getItem(int position) {
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return rowItems.indexOf(getItem(position));
	}




}
