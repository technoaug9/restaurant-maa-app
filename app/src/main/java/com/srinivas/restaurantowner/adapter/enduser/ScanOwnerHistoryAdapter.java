package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.ScanOwnerHistoryModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 23/9/15.
 */
public class ScanOwnerHistoryAdapter extends RecyclerView.Adapter<ScanOwnerHistoryAdapter.MyViewHolder>{
    private LayoutInflater inflater;
    private Context context;
    private List<ScanOwnerHistoryModel> scanOwnerHistoryModels= Collections.emptyList();
    public ScanOwnerHistoryAdapter(Context context, List<ScanOwnerHistoryModel> scanOwnerHistoryModelList) {
        this.context=context;
        inflater=LayoutInflater.from(context);
        this.scanOwnerHistoryModels=scanOwnerHistoryModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=inflater.inflate(R.layout.row_scan_history_owner,parent,false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ScanOwnerHistoryModel scanOwnerHistoryModel=scanOwnerHistoryModels.get(position);
        holder.tvDate.setText(scanOwnerHistoryModel.getTvDate());
        holder.tvMilk.setText(scanOwnerHistoryModel.getTvMilk());
        holder.tvTime.setText(scanOwnerHistoryModel.getTvTime());
    }

    @Override
    public int getItemCount() {
        return scanOwnerHistoryModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTime,tvMilk,tvDate;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvDate=(TextView) itemView.findViewById(R.id.tvDate);
            tvTime=(TextView) itemView.findViewById(R.id.tvTime);
            tvMilk=(TextView) itemView.findViewById(R.id.tvMilk);
        }
    }
}
