package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.SearchHistory;
import com.srinivas.restaurantowner.activity.enduser.ScanResultsActivity;

import java.util.Collections;
import java.util.List;

public class RecentlySearchedAdapter extends RecyclerView.Adapter<RecentlySearchedAdapter.MyViewHolder> {
    List<SearchHistory> searchHistoryList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private String searchedProduct;
    private Intent intent;
    private boolean isCrossIcon = false;
    private View.OnClickListener yesButtonListener;
    private Dialog mDialog;
    private String TAG = RecentlySearchedAdapter.class.getSimpleName();

    public RecentlySearchedAdapter(Context context, List<SearchHistory> searchHistoryList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.searchHistoryList = searchHistoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_product, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (searchHistoryList.size() > 0) {

            final SearchHistory searchHistoryModel = searchHistoryList.get(position);

            if (searchHistoryModel.getProduct_name() != null &&
                    searchHistoryModel.getProduct_name().length() > 0) {
                holder.tvProductName.setText(searchHistoryModel.getProduct_name());
            }
            if (searchHistoryModel.getImage() != null && searchHistoryModel.getImage().length() > 0) {

                Picasso.with(context).load(searchHistoryModel.getImage()).
                        placeholder(R.drawable.img).into(holder.ivProductImg);
            }


            if (searchHistoryModel.upc_code != null && searchHistoryModel.upc_code.length() > 0) {


                holder.lllist.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.startActivity(new Intent(context, ScanResultsActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("ingredientName",
                                        searchHistoryModel.upc_code).putExtra("fromAdapter","fromAdapter"));
                    }
                });

            }


        }

    }

    @Override
    public int getItemCount()


    {
        return searchHistoryList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvProductName;
        private ImageView ivProductImg, ivCross;
        private LinearLayout lllist;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductsName);
            ivProductImg = (ImageView) itemView.findViewById(R.id.ivProductsImg);
            lllist = (LinearLayout) itemView.findViewById(R.id.lllist);
        }
    }

}

