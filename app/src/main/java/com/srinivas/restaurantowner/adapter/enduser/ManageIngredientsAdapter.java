package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.ManageIngredientsModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 21/9/15.
 */
public class ManageIngredientsAdapter extends RecyclerView.Adapter<ManageIngredientsAdapter.MyViewHolder>{
    List<ManageIngredientsModel> manageIngredientsModelList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private View.OnClickListener yesButtonListener;
    private Dialog mDialog;
    private boolean isCrossIcon=false;

    public ManageIngredientsAdapter(Context context, List<ManageIngredientsModel> manageIngredientsModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.manageIngredientsModelList = manageIngredientsModels;
    }

/*
    public ManageIngredientsAdapter(Activity context, List<ManageIngredientsModel> manageIngredientsModels,boolean isCrossIcon,View.OnClickListener yesButtonListener,Dialog mDialog) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.manageIngredientsModelList = manageIngredientsModels;
        this.isCrossIcon=isCrossIcon;
        this.yesButtonListener=yesButtonListener;
        this.mDialog=mDialog;
    }
*/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= inflater.inflate(R.layout.row_manage_ingredients,parent,false);
        MyViewHolder viewHolder=new MyViewHolder(view);
        viewHolder.ivManageIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"work on Progress",Toast.LENGTH_SHORT).show();
            }
        });
        /*if(isCrossIcon)
        {
            viewHolder.ivManageIngredients.setVisibility(View.VISIBLE);
        }

        else
        if(!isCrossIcon)
        {
            viewHolder.ivManageIngredients.setVisibility(View.VISIBLE);
        }*/

        return viewHolder;
    }

    public void delete(int position) {
        manageIngredientsModelList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
        ManageIngredientsModel current=manageIngredientsModelList.get(position);
        holder.tvManageIngredients.setText(current.getTvManageIngredients());
        holder.ivManageIngredients.setImageResource(current.getIvDeleteItem());
/*
        holder.ivManageIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.POSITION_DELETE = position;
                CommonUtils.showCustomDialogLogout(context, "Do you want to delete\nthis Preventive ?", null, yesButtonListener, View.VISIBLE, mDialog);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return manageIngredientsModelList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView tvManageIngredients;
        private ImageView ivManageIngredients;
        public MyViewHolder(View itemView){
            super(itemView);
            tvManageIngredients=(TextView) itemView.findViewById(R.id.tvManageIngredients);
            ivManageIngredients=(ImageView) itemView.findViewById(R.id.ivManageIngredients);
        }
    }
}
