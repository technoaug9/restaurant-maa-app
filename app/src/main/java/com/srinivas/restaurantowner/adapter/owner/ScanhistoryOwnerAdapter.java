package com.srinivas.restaurantowner.adapter.owner;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class ScanhistoryOwnerAdapter extends RecyclerView.Adapter<ScanhistoryOwnerAdapter.MyViewHolder> {

    private Context context;
  //  private List<ScanHistoryModel> model;

    private static String time = "";
    private List<Product>productList;
    private View view;
    private static String TAG = ScanhistoryOwnerAdapter.class.getSimpleName();
    public ScanhistoryOwnerAdapter(Context context, List<Product> productList){
        this.context=context;
        this.productList=productList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(context).inflate(R.layout.list_row_item_scan_history,viewGroup,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        if (productList.size() > 0) {
            Product scanHistoryListModel = productList.get(position);
            myViewHolder.llUnsafeName.setVisibility(View.GONE);

            if (scanHistoryListModel.created_at != null && scanHistoryListModel.created_at.length() > 0) {

                String date = convertTimeStamp(scanHistoryListModel.created_at);
                myViewHolder.tvDate.setText(time + "\n" + date);

            }
            if (scanHistoryListModel.product_name != null && scanHistoryListModel.product_name.length() > 0) {
                myViewHolder.tvProduct.setText(scanHistoryListModel.product_name);
            }
            if (scanHistoryListModel.user.name != null && scanHistoryListModel.user.name.length() > 0) {
                myViewHolder.tvChefName.setText(scanHistoryListModel.user.name);
            }

            if (scanHistoryListModel.history_type != null && scanHistoryListModel.history_type.length() > 0) {
                myViewHolder.tvAssignlist.setText(scanHistoryListModel.history_type);
            }
           /* if (scanHistoryListModel.unsafe_users != null && scanHistoryListModel.unsafe_users.size() > 0) {
                myViewHolder.tvUnsafeName.setText((CharSequence) scanHistoryListModel.unsafe_users);

            }

            if (scanHistoryListModel.user_id != null && scanHistoryListModel.user_id.length() > 0) {


            }*/
        }

    }

    /*    myViewHolder.tvTime.setText(model.get(position).getTime());
        myViewHolder.tvChefName.setText(model.get(position).getShefName());
      //  myViewHolder.tvSafe.setText(model.get(position).getShefName());
        myViewHolder.tvProduct.setText(model.get(position).getNameOfProduct());
        myViewHolder.tvAssign.setText(model.get(position).getAssign());
        myViewHolder.tvDate.setText(model.get(position).getDate());



        if(position==model.size()-1)
        {
            myViewHolder.vLine.setVisibility(View.GONE);
        }
        if(model.get(position).getScanResult().equalsIgnoreCase("UnSafe"))
        {
            myViewHolder.llUnsafeName.setVisibility(View.VISIBLE);

        }else {
            myViewHolder.llUnsafeName.setVisibility(View.GONE);
        }

    }

*/

    @Override
    public int getItemCount()
    {
        return productList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        private TextView tvChefName,tvProduct,tvTime,tvSafe,tvUnsafeName,tvAssign,tvDate,tvAssignlist;
        private LinearLayout llUnsafeName;
        private View vLine;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTime=(TextView)view.findViewById(R.id.tvTime);
            tvAssignlist=(TextView)view.findViewById(R.id.tvAssignlist);
            tvChefName=(TextView)view.findViewById(R.id.tvChefName);
            tvAssign=(TextView)view.findViewById(R.id.tvAssign);
           // tvSafe=(TextView)view.findViewById(R.id.tvSafe);
            tvProduct= (TextView) view.findViewById(R.id.tvProduct);
            vLine=(View)view.findViewById(R.id.vLine);
            tvUnsafeName=(TextView)view.findViewById(R.id.tvUnsafeName);
            llUnsafeName=(LinearLayout)view.findViewById(R.id.llUnsafeName);

            tvDate= (TextView) view.findViewById(R.id.tvDate);

        }
    }
   /* public static String convertTimeStamp(String date)
    {
        try {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            serverFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date d = serverFormat.parse(date);
            long currentTimeStamp = System.currentTimeMillis();
            long server = d.getTime();
            Log.e("current" + currentTimeStamp, "Server" + server);
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy");
            SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");
            format1.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
            time = format1.format(d);
            Log.e(TAG, "<<<<<<time>>>>>" + time);
            return formatter.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
*/
    public static String convertTimeStamp(String date)
    {
        if(date.contains("T")||date.contains("Z")) {
            try {
                SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                serverFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date d = serverFormat.parse(date);
                long currentTimeStamp = System.currentTimeMillis();
                long server = d.getTime();
                Log.e("current" + currentTimeStamp, "Server" + server);
                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy");
                SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");
                format1.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                time = format1.format(d);
                Log.e(TAG, "<<<<<<time>>>>>" + time);
                return formatter.format(d);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("date  scanhistory", e.toString());

            }
        }
        else {
            try {
                Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        .parse(date);
                long currentTimeStamp = System.currentTimeMillis();
                long server = d.getTime();
                Log.e("current" + currentTimeStamp, "Server" + server);
                SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy");
                SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");
                format1.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                time = format1.format(d);
                Log.e(TAG, "<<<<<<time>>>>>" + time);
                return formatter.format(d);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
