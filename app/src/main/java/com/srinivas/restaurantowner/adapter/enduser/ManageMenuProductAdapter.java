package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Activity;
import android.app.Dialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 22/9/15.
 */
public class ManageMenuProductAdapter extends BaseAdapter {
    private List<IngredientList> ingredientList = Collections.emptyList();
    private LayoutInflater inflater;
    private Activity context;
    private boolean isCrossIcon = false;

    public ManageMenuProductAdapter(Activity context, List<IngredientList> ingredientList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.ingredientList = ingredientList;
    }

    @Override
    public int getCount() {
        return ingredientList.size();
    }

    @Override
    public Object getItem(int i) {
        return ingredientList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        MyViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.row_manage_menu_product, parent, false);
            viewHolder = new MyViewHolder();
            viewHolder.tvIngredientName = (TextView) view.findViewById(R.id.tvIngredientName);
            viewHolder.ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
            view.setTag(viewHolder);
        } else {
            viewHolder = (MyViewHolder) view.getTag();
        }

        if (ingredientList != null && ingredientList.size() > 0) {
            IngredientList ingredientListModel = ingredientList.get(position);

            if (ingredientListModel.ingredient_name != null && ingredientListModel.ingredient_name.length() > 0) {
                viewHolder.tvIngredientName.setText(ingredientListModel.ingredient_name);
            } else {
                viewHolder.tvIngredientName.setText("");
            }

            if (ingredientList.get(position).isCrossVisible) {
                viewHolder.ivDelete.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ivDelete.setVisibility(View.INVISIBLE);
            }

            viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (ingredientList.size() > 0) {
                        deleteDialog(position);
                    } else {
                        CommonUtils.showToast(context, "Ingredients can't be zero, please add some ingredients");
                    }

                }
            });


        }


        return view;

    }


    public class MyViewHolder {
        private TextView tvIngredientName;
        private ImageView ivDelete;

    }


    private void deleteDialog(final int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.delete_dialog, null);
        mDialog.setContentView(dialoglayout);


        TextView tvYes = (TextView) dialoglayout.findViewById(R.id.tvYes);
        TextView tvNo = (TextView) dialoglayout.findViewById(R.id.tvNo);
        TextView tvAlert = (TextView) dialoglayout.findViewById(R.id.tvAlert);
        TextView tvDelete = (TextView) dialoglayout.findViewById(R.id.tvDelete);
        tvDelete.setText("Do you want to delete\nthis Preventive ?");
        tvAlert.setVisibility(View.GONE);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDialog.dismiss();
            }
        });
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingredientList.remove(position);
                notifyDataSetChanged();
                mDialog.dismiss();
            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }


   /* @Override
    public ManageMenuProductAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_manage_menu_product, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ManageMenuProductAdapter.MyViewHolder holder, final int position) {

        if (ingredientList != null && ingredientList.size() > 0) {
            IngredientList ingredientListModel = ingredientList.get(position);

            if (ingredientListModel.ingredient_name != null && ingredientListModel.ingredient_name.length() > 0) {
                holder.tvIngredientName.setText(ingredientListModel.ingredient_name);
            } else {
                holder.tvIngredientName.setText("");
            }

            if (ingredientList.get(position).isCrossVisible) {
                holder.ivDelete.setVisibility(View.VISIBLE);
            } else {
                holder.ivDelete.setVisibility(View.INVISIBLE);
            }

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (ingredientList.size() > 1) {
                        deleteDialog(position);
                    } else {
                        CommonUtils.showToast(context, "Ingredients can't be zero, please add some ingredients");
                    }

                }
            });


        }


    }

    @Override
    public int getItemCount() {
        return ingredientList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvIngredientName;
        private ImageView ivDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvIngredientName = (TextView) itemView.findViewById(R.id.tvIngredientName);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
        }
    }



*/
}
