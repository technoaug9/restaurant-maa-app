package com.srinivas.restaurantowner.adapter.owner;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.ScanHistoryModel;

import java.util.List;

/**
 * Created by vinay.tripathi on 23/9/15.
 */
public class ScanResturantAdapter extends RecyclerView.Adapter<ScanResturantAdapter.MyViewHolder> {

    private Context context;
    private List<ScanHistoryModel> model;
    private View view;

    public ScanResturantAdapter(Context context, List<ScanHistoryModel> model){
        this.context=context;
        this.model=model;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(context).inflate(R.layout.row_resturant_scan,viewGroup,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {

        myViewHolder.tvTime.setText(model.get(position).getTime());
        myViewHolder.tvDate.setText(model.get(position).getDate());
        myViewHolder.tvSafe.setText(model.get(position).getScanResult());
        myViewHolder.tvMilk.setText(model.get(position).getNameOfProduct());


        if(position==model.size()-1)
        {
            myViewHolder.vLine.setVisibility(View.GONE);
        }
        if(model.get(position).getScanResult().equalsIgnoreCase("UnSafe"))
        {
            myViewHolder.llUnsafeName.setVisibility(View.VISIBLE);

        }else {
            myViewHolder.llUnsafeName.setVisibility(View.GONE);
        }

    }



    @Override
    public int getItemCount()
    {
        return model.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTime,tvDate,tvMilk,tvSafe,tvUnsafeName;
        private LinearLayout llUnsafeName;
        private View vLine;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTime=(TextView)view.findViewById(R.id.tvTime);
            tvDate=(TextView)view.findViewById(R.id.tvDate);
            tvMilk=(TextView)view.findViewById(R.id.tvMilk);
            tvSafe=(TextView)view.findViewById(R.id.tvSafe);
            vLine=(View)view.findViewById(R.id.vLine);
          /*  tvUnsafeName=(TextView)view.findViewById(R.id.tvUnsafeName);
            llUnsafeName=(LinearLayout)view.findViewById(R.id.llUnsafeName);*/


        }
    }
}
