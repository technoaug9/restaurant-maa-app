package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.ManageMenuModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 21/9/15.
 */
public class ManageMenuAdapter extends RecyclerView.Adapter<ManageMenuAdapter.MyViewHolder> {
    List<ManageMenuModel> manageMenuModels = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public ManageMenuAdapter(Context context, List<ManageMenuModel> manageMenuModelList){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.manageMenuModels = manageMenuModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_manage_menu, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        ManageMenuModel current=manageMenuModels.get(position);
        holder.tvMenuIngredients.setText(current.getTvMenuIngredients());
        holder.ivCross.setImageResource(current.getIvCross());
    }

    @Override
    public int getItemCount() {
        return manageMenuModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvMenuIngredients;
        private ImageView ivCross;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvMenuIngredients=(TextView) itemView.findViewById(R.id.tvMenuIngredients);
            ivCross=(ImageView) itemView.findViewById(R.id.ivCross);
        }
    }
}
