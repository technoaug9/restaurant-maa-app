package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.MemberList;
import com.srinivas.restaurantowner.activity.enduser.FriendProfileActivity;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class PreventiveListAdapter extends RecyclerView.Adapter<PreventiveListAdapter.MyViewHolder> {

    private LayoutInflater inflater;

    public List<MemberList> memberlist;
    public List<String> image;
    private Context context;
    Intent intent;


    public PreventiveListAdapter(Context context, List<MemberList> memberlist) {
        this.context = context;

        this.memberlist = memberlist;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserAge;
        TextView tvUserName;
        ImageView ivUserImg;
        LinearLayout llPreventList;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvUserAge = (TextView) itemView.findViewById(R.id.tvUserAge);
            tvUserName = (TextView) itemView.findViewById(R.id.tvuserName);
            ivUserImg = (ImageView) itemView.findViewById(R.id.ivUserImg);
            llPreventList = (LinearLayout) itemView.findViewById(R.id.llPreventList);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_preventive_list, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Context context = this.context;
        final MemberList memberListModel = memberlist.get(position);

        if (memberlist != null && memberListModel.image != null) {
            String imageValues = memberListModel.image;
            Ion.with(context)
                    .load(imageValues)
                    .withBitmap()
                    .placeholder(R.drawable.img)
                    .error(R.drawable.img)
                    .intoImageView(holder.ivUserImg);

        }
        if (memberlist != null && memberlist.size() > 0) {

            if (memberlist.get(position).name != null && memberlist.get(position).name.length() > 0) {
                holder.tvUserName.setText(memberlist.get(position).name);
            } else {
                holder.tvUserName.setText(" ");
            }
            if (memberlist.get(position).dob != null && memberlist.get(position).dob.length() > 0) {
                try {

                    Log.e("TAG", "serverTimeStamp>>>>" + memberlist.get(position).dob);
                    long timestampLong = Long.parseLong(memberlist.get(position).dob);

                    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                    cal.setTimeInMillis(timestampLong * 1000);
                    String date = DateFormat.format("dd-MM-yyyy", cal).toString();

                    String dateArray[] = date.split("-");

                    Log.e("TAG", "serverTimeStampin_YEARS>>>>" + dateArray[2]);
                    final Calendar currentCalender = Calendar.getInstance();
                    int mYear = currentCalender.get(Calendar.YEAR);

                    int age = mYear - Integer.valueOf(dateArray[2]);

                    holder.tvUserAge.setText(String.valueOf(String.valueOf(age)));


                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            } else {
                holder.tvUserAge.setText(" ");
            }

            holder.llPreventList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (memberListModel.id != null && memberListModel.id.length() > 0) {

                        if (memberListModel.id.equalsIgnoreCase(CommonUtils.getPreferencesString(context, AppConstants.USER_ID))) {
                            context.startActivity(new Intent(context, FriendProfileActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        } else {
                            context.startActivity(new Intent(context, FriendProfileActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .putExtra("memberID", memberListModel.id));
                        }
                    }

                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return memberlist.size();
    }


}