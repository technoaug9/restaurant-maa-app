package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import java.util.List;

/**
 * Created by rahul.kumar on 21/8/15.
 */
public class SearchProductAdapter extends ArrayAdapter<String>{

    private Context context;

    private List<String> productList;

    private LayoutInflater inflater;


    public SearchProductAdapter(Context context,int resource,List<String> productList)
    {
        super(context,resource,productList);
        this.context=context;
        this.productList=productList;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    class ViewHolder
    {
        private TextView tvSearchProduct;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView==null)
        {
            viewHolder=new ViewHolder();
            convertView=inflater.inflate(R.layout.row_product_search,null);
            viewHolder.tvSearchProduct=(TextView)convertView.findViewById(R.id.tvSearchProduct);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.tvSearchProduct.setText(productList.get(position));

        return convertView;
    }


}
