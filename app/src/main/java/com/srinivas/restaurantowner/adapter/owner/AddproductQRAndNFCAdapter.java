package com.srinivas.restaurantowner.adapter.owner;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.SwipAdapter;

import java.util.ArrayList;
import java.util.List;


public class AddproductQRAndNFCAdapter extends SwipAdapter implements Filterable {
    private static final String TAG = AddproductQRAndNFCAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private Context context;
    private Intent intent;
    private LinearLayout llproductlist;

    private List<Product> modelList;
    private List<Product> mStringSearch;



    public AddproductQRAndNFCAdapter(Context context, List<Product> modelList) {
        super(context);
        this.context = context;
        this.modelList = modelList;
        this.mStringSearch = modelList;

        mInflater = LayoutInflater.from(context);
    }

    @Override
    protected View generateLeftView(final int position, View convertView) {
        final ViewHolderForm viewHolderForm = new ViewHolderForm();
        convertView = mInflater.inflate(R.layout.row_add_product_list, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        convertView.setLayoutParams(lp);
        viewHolderForm.tvProductName = (TextView) convertView.findViewById(R.id.tvProductName);
        viewHolderForm.llproductlist = (LinearLayout) convertView.findViewById(R.id.llproductlist);
        viewHolderForm.ivProductImage = (ImageView) convertView.findViewById(R.id.ivProductImage);
        viewHolderForm.ivTool = (ImageView) convertView.findViewById(R.id.ivTool);
        viewHolderForm.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
        convertView.setTag(viewHolderForm);

            if (modelList.get(position).name != null && modelList.get(position).name.length() > 0) {
                viewHolderForm.tvProductName.setText(modelList.get(position).name);
            } else {
                viewHolderForm.tvProductName.setText("");
            }

            if (modelList.get(position).image != null && modelList.get(position).image!= null && modelList.get(position).image.length() > 0) {
                Picasso.with(context).load(modelList.get(position).image).into(viewHolderForm.ivProductImage);

            }

        return convertView;
    }


    @Override
    protected View generateRightView(final int position, View convertView) {
        final ViewHolderForm viewHolderForm = new ViewHolderForm();
        convertView = mInflater.inflate(R.layout.row_delete, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120, LinearLayout.LayoutParams.MATCH_PARENT);
        convertView.setLayoutParams(lp);
        viewHolderForm.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
        convertView.setTag(viewHolderForm);

            viewHolderForm.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog(position);
                }
            });


        return convertView;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }




    class ViewHolderForm {
        private TextView tvProductName;
        private ImageView ivProductImage, ivTool, ivDelete;
        private LinearLayout llproductlist;
    }

    private void deleteDialog(final int positionToDelete) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.delete_dialog, null);
        mDialog.setContentView(dialoglayout);

        TextView tvYes = (TextView) dialoglayout.findViewById(R.id.tvYes);
        TextView tvNo = (TextView) dialoglayout.findViewById(R.id.tvNo);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDialog.dismiss();
            }
        });
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                    request.id = modelList.get(positionToDelete).id;
                    deleteProductAPI(request, positionToDelete);
                } else {
                    CommonUtils.showAlert("Please check your internet connection.", context);
                }


                mDialog.dismiss();
            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    private void deleteProductAPI(ServiceRequest request, final int position) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.DELETE_PRODUCT;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        android.util.Log.e(TAG, "URL>>>>>>" + url);
        android.util.Log.e(TAG, "req data " + new Gson().toJson(requestJson));

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                android.util.Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        modelList.remove(position);
                        notifyDataSetChanged();
                        CommonUtils.showToast(context, "Product deleted successfully");
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, context.getString(R.string.server_error));
                    }
                }
            }
        });


    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results.values!=null){
                    modelList = (ArrayList<Product>) results.values;
                    Log.e("search", "search");
                    notifyDataSetChanged();
                }

            }

            @SuppressLint("DefaultLocale")
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<Product> arlFilteredArrayNames = new ArrayList<Product>();
                constraint = constraint.toString().toLowerCase();
                Log.e("","mStringSearch size "+ mStringSearch.size());
                for (int i = 0; i < mStringSearch.size(); i++) {
                    Product dataNames = new Product();
                    dataNames.name = mStringSearch.get(i).name;
                    dataNames.id = mStringSearch.get(i).id;
                    dataNames.image= mStringSearch.get(i).image;
                    dataNames.ingredients = mStringSearch.get(i).ingredients;

                    String searchData = dataNames.name;


                    if (searchData.toLowerCase().contains(constraint.toString()))  {
                        arlFilteredArrayNames.add(dataNames);
                    }
                }
                results.values = arlFilteredArrayNames;
                return results;
            }
        };

        return filter;
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return modelList.indexOf(getItem(position));
    }


    /*@Override
    public Filter getFilter() {
        if (valueFilter == null) {

            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                List<Product> filteredList = new ArrayList<>();
                for (int i = 0; i < modelList.size(); i++) {
                    if (modelList.get(i).name != null && modelList.get(i).name.length() > 0) {
                        if (modelList.get(i).name.toUpperCase().contains(constraint.toString().toUpperCase())) {
                            Product product = new Product();
                            product.name = modelList.get(i).name;

                            if (modelList.get(i).id != null && modelList.get(i).id.length() > 0) {
                                product.id = modelList.get(i).id;
                            }

                            if (modelList.get(i).image != null && modelList.get(i).image.url != null) {
                                product.image.url = modelList.get(i).image.url;
                            }

                            if (modelList.get(i).ingredients != null && modelList.get(i).ingredients.size() > 0) {
                                product.ingredients = modelList.get(i).ingredients;
                            }

                            filteredList.add(product);

                        }
                    }


                }
                results.count = filteredList.size();
                results.values = filteredList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            modelList = (ArrayList<Product>) filterResults.values;
            notifyDataSetChanged();

        }
    }*/


}
