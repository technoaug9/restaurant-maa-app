package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;

import java.util.List;


public class FlashTextAdapter extends BaseAdapter {

    private final Context context;
    private View holder;
    private List<IngredientList> arlItem;
    private String TAG = FlashTextAdapter.class.getSimpleName();

    public FlashTextAdapter(Context context, List<IngredientList> arlItem) {
        this.context = context;
        this.arlItem = arlItem;
    }

    @Override
    public int getCount() {
        //  return arlItem.size();
        return arlItem.size();
    }

    @Override
    public Object getItem(int position) {
        return arlItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertview, ViewGroup parent) {
        ViewHolder holder;
        if (convertview == null) {
            holder = new ViewHolder();
            LayoutInflater diInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertview = diInflater.inflate(R.layout.row_product, null);
            holder.ivCross = (ImageView) convertview.findViewById(R.id.ivCross);
            holder.tvProductsName = (TextView) convertview.findViewById(R.id.tvProductsName);
            convertview.setTag(holder);
        } else {
            holder = (ViewHolder) convertview.getTag();
        }


        holder.ivCross.setVisibility(View.VISIBLE);
        if (arlItem.get(position) != null) {
            IngredientList current = arlItem.get(position);

            if (current.ingredient_name != null && current.ingredient_name.length() > 0) {

                holder.tvProductsName.setText(current.ingredient_name);
            }

            holder.ivCross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteDialog(position);
                }
            });

        }

        return convertview;
    }

    private class ViewHolder {
        ImageView ivCross;
        TextView tvProductsName;

    }

    private void deleteDialog(final int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.delete_dialog, null);
        TextView tvDelete = (TextView) dialoglayout.findViewById(R.id.tvDelete);
        tvDelete.setText("Do you want to delete this\nIngredient ?");
        mDialog.setContentView(dialoglayout);


        TextView tvYes = (TextView) dialoglayout.findViewById(R.id.tvYes);
        TextView tvNo = (TextView) dialoglayout.findViewById(R.id.tvNo);
        TextView tvAlert = (TextView) dialoglayout.findViewById(R.id.tvAlert);
        tvAlert.setVisibility(View.GONE);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDialog.dismiss();
            }
        });
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arlItem.remove(position);
                notifyDataSetChanged();
                mDialog.dismiss();
            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

}
