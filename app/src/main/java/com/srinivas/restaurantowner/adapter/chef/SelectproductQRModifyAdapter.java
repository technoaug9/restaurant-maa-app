package com.srinivas.restaurantowner.adapter.chef;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.chef.ProgramScanQrActivity;
import com.srinivas.restaurantowner.model.SelectNfcChipModel;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.List;

/**
 * Created by vinay.tripathi on 17/10/15.
 */
public class SelectproductQRModifyAdapter extends RecyclerView.Adapter<SelectproductQRModifyAdapter.MyViewHolder> {
    List<SelectNfcChipModel> selectNfcChipModels;
    private LayoutInflater inflater;
    private Context context;
    private String searchedProduct;
    private Intent intent;


    public SelectproductQRModifyAdapter(Context context, List<SelectNfcChipModel> selectNfcChipModels) {
        this.context = context;
        this.selectNfcChipModels = selectNfcChipModels;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_add_ingredients, viewGroup, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {


        SelectNfcChipModel current = selectNfcChipModels.get(i);

        myViewHolder.tvProductsName.setText(current.getRowHeader());

        Log.e("bgchdshc", "<<<<<<<<<>>>>>>>>" + CommonUtils.getPreferencesBoolean(context, AppConstants.IS_QRSCANNER));

        myViewHolder.tvProductsName.setText(current.getRowHeader());
        myViewHolder.iv_product.setVisibility(View.VISIBLE);
        myViewHolder.llNfcList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showCustomDialog(context);
            }
        });
    }



    private void showCustomDialog(final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
//        mDialog.getWindow().getAttributes().windowAnimations = R.anim;
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        View dialoglayout = inflater.inflate(R.layout.test_chip_dialog, null);
        mDialog.setContentView(dialoglayout);
        TextView tvMsg = (TextView) mDialog.findViewById(R.id.tvMsg);
        TextView tvOk = (TextView) mDialog.findViewById(R.id.tvOk);
        TextView tvCode = (TextView) mDialog.findViewById(R.id.tvCode);
        tvCode.setVisibility(View.GONE);
        View view = (View) mDialog.findViewById(R.id.view);
        TextView tvTestchip = (TextView) mDialog.findViewById(R.id.tvTestchip);
        tvMsg.setText(context.getString(R.string.edit_Qr));
        view.setVisibility(View.GONE);
        tvOk.setVisibility(View.GONE);
        tvTestchip.setGravity(Gravity.CENTER);
        tvTestchip.setText("Test The QR");


        tvTestchip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(context, ProgramScanQrActivity.class);
               intent.putExtra("from", "AddAddapter");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                mDialog.dismiss();

            }
        });


        mDialog.show();
    }


    @Override
    public int getItemCount() {
        return selectNfcChipModels.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductsName;
        LinearLayout llNfcList;
        ImageView iv_product;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvProductsName = (TextView) itemView.findViewById(R.id.tvProductsName);
            llNfcList = (LinearLayout) itemView.findViewById(R.id.llNfcList);
            iv_product = (ImageView) itemView.findViewById(R.id.iv_product);
        }
    }
}
