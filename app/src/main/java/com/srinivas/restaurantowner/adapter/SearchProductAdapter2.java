package com.srinivas.restaurantowner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import java.util.List;

/**
 * Created by shashank.kapsime on 3/11/15.
 */
public class SearchProductAdapter2 extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private List<String> ingredientLists;
    private ViewHolder holder;


    public SearchProductAdapter2(Context context, List<String> ingredientLists) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.ingredientLists = ingredientLists;
    }


    @Override
    public int getCount() {
        return ingredientLists.size();
    }

    @Override
    public Object getItem(int position)

    {
        return ingredientLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_product_search, parent, false);
            holder.tvSearchProduct = (TextView) convertView.findViewById(R.id.tvSearchProduct);
            convertView.setTag(holder);
        }


        // ProductListModel ingredientListModel = (ProductListModel) getItem(position);
        if (ingredientLists.get(position) != null && ingredientLists.get(position).length() > 0) {
            holder.tvSearchProduct.setText(ingredientLists.get(position));
        }
        return convertView;
    }


    private class ViewHolder {

        private TextView tvSearchProduct;

    }
}