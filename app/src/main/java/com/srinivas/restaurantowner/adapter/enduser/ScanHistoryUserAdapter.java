package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.SearchHistory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by vinay.tripathi on 10/10/15.
 */
public class ScanHistoryUserAdapter extends RecyclerView.Adapter<ScanHistoryUserAdapter.MyViewHolder> {
    private static String time = "";
    private Context context;
    private List<SearchHistory> scanHistoryList;
    private View view;
    private static String TAG = ScanHistoryUserAdapter.class.getSimpleName();

    public ScanHistoryUserAdapter(Context context, List<SearchHistory> scanHistoryList) {
        this.context = context;
        this.scanHistoryList = scanHistoryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(context).inflate(R.layout.row_scan_history_user, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        if (scanHistoryList.size() > 0) {
            SearchHistory scanHistoryListModel = scanHistoryList.get(position);

            if (scanHistoryListModel.created_at != null && scanHistoryListModel.created_at.length() > 0) {

                String date = convertTimeStamp(scanHistoryListModel.created_at);
                myViewHolder.tvDate.setText(time + "\n" + date);

            } else {
                myViewHolder.tvDate.setText("");
            }
            if (scanHistoryListModel.product_name != null && scanHistoryListModel.product_name.length() > 0) {
                myViewHolder.tvProductName.setText(scanHistoryListModel.product_name);
            } else {
                myViewHolder.tvProductName.setText("");
            }
            if (scanHistoryListModel.result != null && scanHistoryListModel.result.length() > 0) {
                myViewHolder.tvResult.setText(scanHistoryListModel.result);
            } else {
                myViewHolder.tvResult.setText("");
            }
            if (scanHistoryListModel.unsafe_users != null && scanHistoryListModel.unsafe_users.size() > 0) {
                //  myViewHolder.tvUnsafeName.setText((CharSequence) scanHistoryListModel.unsafe_users);

                String strListOfUsers = "";
                for (int i = 0; i < scanHistoryListModel.unsafe_users.size(); i++) {
                    strListOfUsers = strListOfUsers + scanHistoryListModel.unsafe_users.get(i) + "\n";
                }
                scanHistoryListModel.strListOfUsers = strListOfUsers;
                //    myViewHolder.tvUnsafeName.setText(strListOfUsers);
                myViewHolder.tvUnsafeName.setText(scanHistoryListModel.strListOfUsers);

            } else {
                myViewHolder.tvUnsafeName.setText("");
            }

            if (scanHistoryListModel.user_id != null && scanHistoryListModel.user_id.length() > 0) {


            }
        }

    }


    @Override
    public int getItemCount() {
        return scanHistoryList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTime, tvDate, tvProductName, tvResult, tvUnsafeName;
        private LinearLayout llUnsafeName;
        private View vLine;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvProductName = (TextView) view.findViewById(R.id.tvProductName);
            tvResult = (TextView) view.findViewById(R.id.tvResult);
            vLine = (View) view.findViewById(R.id.vLine);
            tvUnsafeName = (TextView) view.findViewById(R.id.tvUnsafeName);
            llUnsafeName = (LinearLayout) view.findViewById(R.id.llUnsafeName);

        }
    }

    public static String convertTimeStamp(String date)
    {
        try
        {
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            serverFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date d = serverFormat.parse(date);
            long currentTimeStamp = System.currentTimeMillis();
            long server = d.getTime();
            Log.e("current" + currentTimeStamp, "Server" + server);
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy");
            SimpleDateFormat format1 = new SimpleDateFormat("hh:mm a");
            format1.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
            time = format1.format(d);
            Log.e(TAG, "<<<<<<time>>>>>" + time);
            return formatter.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
