package com.srinivas.restaurantowner.adapter.owner;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.srinivas.restaurantowner.R;

import java.util.List;

/**
 * Created by vinay.tripathi on 16/10/15.
 */
public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.ViewHolderEvents> {
    private List<String> list;
    private Activity context;

    public IngredientAdapter(Activity context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolderEvents onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_product, parent, false);
        return new ViewHolderEvents(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderEvents holder, int position) {

        holder.tvProductsName.setText(list.get(position));

        holder.tvProductsName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"ghgjkjklkl,k",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class ViewHolderEvents extends RecyclerView.ViewHolder
    {
        protected TextView tvProductsName;

        public ViewHolderEvents(View itemView) {
            super(itemView);
            tvProductsName = (TextView) itemView.findViewById(R.id.tvProductsName);

        }
    }

}
