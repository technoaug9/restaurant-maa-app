package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;

import java.util.List;

/**
 * Created by vinay.tripathi on 5/10/15.
 */
public class SearchAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private List<IngredientList> ingredientLists;
    private ViewHolder holder;
    private int matchPosition;
    private boolean isMatched = false;


    public SearchAdapter(Context context, List<IngredientList> ingredientLists) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.ingredientLists = ingredientLists;
    }

    public SearchAdapter(Context context, List<IngredientList> ingredientLists, boolean isMatched, int matchPosition) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.ingredientLists = ingredientLists;
        this.matchPosition = matchPosition;
        this.isMatched = isMatched;
    }

    @Override
    public int getCount() {
        return ingredientLists.size();
    }

    @Override
    public Object getItem(int position)

    {
        return ingredientLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_product_search, parent, false);
            holder.tvSearchProduct = (TextView) convertView.findViewById(R.id.tvSearchProduct);
            holder.ivSelected = (ImageView) convertView.findViewById(R.id.ivSelected);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        IngredientList ingredientList = (IngredientList) getItem(position);

        holder.tvSearchProduct.setText(ingredientList.ingredient_name);

        if (isMatched) {
            if (position == matchPosition) {
                holder.ivSelected.setVisibility(View.VISIBLE);
            } else {
                holder.ivSelected.setVisibility(View.GONE);
            }
        } else {
            holder.ivSelected.setVisibility(View.GONE);
        }

        return convertView;
    }


    private class ViewHolder {

        private TextView tvSearchProduct;
        private ImageView ivSelected;

    }
}