package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SearchedProductAdapter extends RecyclerView.Adapter<SearchedProductAdapter.MyViewHolder> {
    List<String> searchedProductModels = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;


    public SearchedProductAdapter(Context context, List<String> friendProfileModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.searchedProductModels = friendProfileModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_search_product_ingredient, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (searchedProductModels.get(position) != null && searchedProductModels.get(position).length() > 0) {

            holder.tvSearchProductIngredients.setText(searchedProductModels.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return searchedProductModels.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSearchProductIngredients;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvSearchProductIngredients = (TextView) itemView.findViewById(R.id.tvSearchProductIngredients);
        }
    }
}