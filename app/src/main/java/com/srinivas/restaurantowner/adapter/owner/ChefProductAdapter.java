package com.srinivas.restaurantowner.adapter.owner;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.owner.AddProductActivity;
import com.srinivas.restaurantowner.model.AddChefModel;
import com.srinivas.restaurantowner.utils.AppConstants;

import java.util.Collections;
import java.util.List;

/**
 * Created by vinay.tripathi on 8/10/15.
 */
public class ChefProductAdapter extends RecyclerView.Adapter<ChefProductAdapter.MyViewHolder> {
        List<AddChefModel> preventiveListModels = Collections.emptyList();
private LayoutInflater inflater;
private Context context;
private Intent intent;

public ChefProductAdapter(Context context, List<AddChefModel> preventiveListModelList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.preventiveListModels = preventiveListModelList;

        }

public void delete(int position)
        {
        preventiveListModels.remove(position);
        notifyItemRemoved(position);
        }

@Override
public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_add_chef, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
        }

@Override
public void onBindViewHolder(final MyViewHolder holder, final int position) {
        AddChefModel current = preventiveListModels.get(position);
final Context context=this.context;
        holder.tvUserFrndName.setText(current.getTvuserfrndName());
        holder.ivuserFrndImage.setImageResource(current.getIvUserFrndImage());
        holder.ivuserFrndImage.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v)
        {
        intent=new Intent(context,AddProductActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("chefproductadapter", AppConstants.CHEFPRODUCTADAPTER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);
        }
        });
        }

@Override
public int getItemCount()
        {
        return preventiveListModels.size();
        }


class MyViewHolder extends RecyclerView.ViewHolder
{
    private TextView tvUserFrndName;
    private ImageView ivuserFrndImage;
    private LinearLayout llAddChef;

    public MyViewHolder(View itemView)
    {
        super(itemView);
        tvUserFrndName=(TextView)itemView.findViewById(R.id.tvUserFrndName);
        ivuserFrndImage=(ImageView)itemView.findViewById(R.id.ivuserFrndImage);
        llAddChef=(LinearLayout)itemView.findViewById(R.id.llAddChef);
    }
}
}