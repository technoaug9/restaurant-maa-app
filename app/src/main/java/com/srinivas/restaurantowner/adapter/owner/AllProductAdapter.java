package com.srinivas.restaurantowner.adapter.owner;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.IngredientList;
import com.srinivas.restaurantowner.model.Product;

import java.util.ArrayList;

/**
 * Created by surya on 8/10/15.
 */
public class AllProductAdapter extends BaseExpandableListAdapter implements View.OnClickListener {
    private Context context;
    private ArrayList<Product> productArrayList;
    private TextView tvProduct, tvMike, tvchildProduct;
    private String TAG = AllProductAdapter.class.getSimpleName();
    private String ingredients = "";

    public AllProductAdapter(Context context, ArrayList<Product> productArrayList) {
        this.context = context;
        this.productArrayList = productArrayList;
    }

    @Override
    public int getGroupCount() {
        return productArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return productArrayList.get(groupPosition).ingredients.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.headerproductrow, null);
        }
        tvProduct = (TextView) convertView.findViewById(R.id.tvProduct);
        tvMike = (TextView) convertView.findViewById(R.id.tvMike);
        final TextView tv_line = (TextView) convertView.findViewById(R.id.tv_line);
        final TextView tv_line_top = (TextView) convertView.findViewById(R.id.tv_line_top);

        if (productArrayList.size() > 0) {
            Product scanHistoryListModel = productArrayList.get(groupPosition);
            if (scanHistoryListModel.name != null && scanHistoryListModel.name.length() > 0) {
                tvProduct.setText(scanHistoryListModel.name);
            }
            if (scanHistoryListModel.user.name != null && scanHistoryListModel.user.name.length() > 0) {
                tvMike.setText(scanHistoryListModel.user.name);
            }
            if (scanHistoryListModel.ingredients != null && scanHistoryListModel.ingredients.size() > 0) {
            }
            if (productArrayList.size() > 0) {
                if (groupPosition == 0) {
                    tv_line_top.setVisibility(View.GONE);
                    tv_line.setVisibility(View.GONE);
                } else if (groupPosition == productArrayList.size() - 1) {
                    tv_line_top.setVisibility(View.VISIBLE);
                    tv_line.setVisibility(View.VISIBLE);
                } else {
                    tv_line_top.setVisibility(View.VISIBLE);
                    tv_line.setVisibility(View.GONE);
                }
            } else {
                tv_line.setVisibility(View.GONE);
                if (groupPosition == 0) {
                    tv_line_top.setVisibility(View.GONE);
                } else {
                    tv_line_top.setVisibility(View.VISIBLE);
                }
            }
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        int last = ingredients.length() - 1;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.childrenproductsinglerow, null);
        }
        tvchildProduct = (TextView) convertView.findViewById(R.id.tvchildProduct);
        TextView tv_line = (TextView) convertView.findViewById(R.id.tv_line);

        if (childPosition == last) {
            tv_line.setVisibility(View.VISIBLE);
        } else {
            tv_line.setVisibility(View.GONE);
        }
        if (productArrayList.size() > 0) {
            IngredientList ingredientList = productArrayList.get(groupPosition).ingredients.get(childPosition);
            if (ingredientList.ingredient_name != null && ingredientList.ingredient_name.length() > 0) {
                tvchildProduct.setText(ingredientList.ingredient_name);
            }
            if (ingredients.length() > 0) {
                if (childPosition == 0) {
                    tv_line.setVisibility(View.GONE);
                } else if (childPosition == productArrayList.size()) {
                    tv_line.setVisibility(View.VISIBLE);
                } else {
                    tv_line.setVisibility(View.GONE);
                }
            }
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onClick(View v) {
    }
}
