package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.SearchProductUserVisibilityModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SearchproductUserVisibilityAdapter extends RecyclerView.Adapter<SearchproductUserVisibilityAdapter.MyViewHolder> {
    List<SearchProductUserVisibilityModel> preventiveListModels = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;


    public SearchproductUserVisibilityAdapter(Context context, List<SearchProductUserVisibilityModel> preventiveListModelList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.preventiveListModels = preventiveListModelList;

    }

    public void delete(int position) {
        preventiveListModels.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_search_product_visibility_single, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SearchProductUserVisibilityModel current = preventiveListModels.get(position);
        holder.tvSearchproductVisibleSingleRow.setText(current.getTvSearchProductVisibility());
    }

    @Override
    public int getItemCount() {
        return preventiveListModels.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSearchproductVisibleSingleRow;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvSearchproductVisibleSingleRow = (TextView) itemView.findViewById(R.id.tvSearchproductVisibleSingleRow);
        }
    }
}