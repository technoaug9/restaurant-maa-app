package com.srinivas.restaurantowner.adapter.owner;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.activity.owner.ManageMenuProductActivity;
import com.srinivas.restaurantowner.model.Product;
import com.srinivas.restaurantowner.utils.AppConstants;
import com.srinivas.restaurantowner.utils.CommonUtils;
import com.srinivas.restaurantowner.utils.SwipAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinay.tripathi on 21/9/15.
 */
public class AddproductAdapter extends SwipAdapter implements Filterable {
    private static final String TAG = AddproductAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private Context context;
    private List<Product> modelList;
    private List<Product> arrayList;
    private Intent intent;
    private LinearLayout llproductlist;
    private ValueFilter valueFilter;
    private List<Product> mStringFilterList;

    public AddproductAdapter(Context context, List<Product> modelList) {
        super(context);
        this.context = context;
        this.modelList = modelList;
        arrayList = new ArrayList<Product>();
        arrayList.addAll(modelList);
        mStringFilterList = modelList;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    protected View generateLeftView(final int position, View convertView) {
        final ViewHolderForm viewHolderForm = new ViewHolderForm();
        convertView = mInflater.inflate(R.layout.row_add_product_list, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        convertView.setLayoutParams(lp);


        viewHolderForm.tvProductName = (TextView) convertView.findViewById(R.id.tvProductName);
        viewHolderForm.llproductlist = (LinearLayout) convertView.findViewById(R.id.llproductlist);
        viewHolderForm.ivProductImage = (ImageView) convertView.findViewById(R.id.ivProductImage);
        viewHolderForm.ivTool = (ImageView) convertView.findViewById(R.id.ivTool);
        viewHolderForm.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
        convertView.setTag(viewHolderForm);


        if (modelList != null && modelList.size() > 0) {
            if (modelList.get(position).name != null && modelList.get(position).name.length() > 0) {
                viewHolderForm.tvProductName.setText(modelList.get(position).name);
                } else {
                viewHolderForm.tvProductName.setText("");
            }

            if (modelList.get(position).image != null && modelList.get(position).image != null && modelList.get(position).image.length() > 0) {
                Picasso.with(context).load(modelList.get(position).image).into(viewHolderForm.ivProductImage);

            }

            if (modelList.get(position).id != null && modelList.get(position).id.length() > 0) {
               }

        }


        viewHolderForm.tvProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (modelList != null && modelList.size() > 0 && modelList.get(position).ingredients != null) {
                    intent = new Intent(context, ManageMenuProductActivity.class);
                    intent.putExtra("ingredients", (Serializable) modelList.get(position).ingredients);
                    intent.putExtra("productName", modelList.get(position).name);
                    intent.putExtra("productImage", modelList.get(position).image);
                    intent.putExtra("productID", modelList.get(position).id);
                    intent.putExtra("user_id", CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
                    intent.putExtra("user_type", CommonUtils.getPreferencesString(context, AppConstants.USER_TYPE));
                    context.startActivity(intent);
                }
            }
        });
        return convertView;
    }


    @Override
    protected View generateRightView(final int position, View convertView) {
        final ViewHolderForm viewHolderForm = new ViewHolderForm();
        convertView = mInflater.inflate(R.layout.row_delete, null);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120, LinearLayout.LayoutParams.MATCH_PARENT);
        convertView.setLayoutParams(lp);
        viewHolderForm.ivDelete = (ImageView) convertView.findViewById(R.id.ivDelete);
        convertView.setTag(viewHolderForm);

        if (modelList != null && modelList.size() > 0) {
            viewHolderForm.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog(position);
                }
            });
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolderForm {
        private TextView tvProductName;
        private ImageView ivProductImage, ivTool, ivDelete;
        private LinearLayout llproductlist;
    }

    private void deleteDialog(final int positionToDelete) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.delete_dialog, null);
        mDialog.setContentView(dialoglayout);

        TextView tvYes = (TextView) dialoglayout.findViewById(R.id.tvYes);
        TextView tvNo = (TextView) dialoglayout.findViewById(R.id.tvNo);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDialog.dismiss();
            }
        });
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    request.user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
                    request.id = modelList.get(positionToDelete).id;
                    deleteProductAPI(request, positionToDelete);
                } else {
                    CommonUtils.showAlert("Please check your internet connection.", context);
                }


                mDialog.dismiss();
            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    private void deleteProductAPI(ServiceRequest request, final int position) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.DELETE_PRODUCT;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        android.util.Log.e(TAG, "URL>>>>>>" + url);
        android.util.Log.e(TAG, "req data " + new Gson().toJson(requestJson));

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                android.util.Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        modelList.remove(position);
                        notifyDataSetChanged();
                        CommonUtils.showToast(context, "Product deleted successfully");
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, context.getString(R.string.server_error));
                    }
                }
            }
        });


    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {

            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {


        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                Log.e(TAG, "constraint   " +constraint.toString());
                List<Product> filteredList = new ArrayList<>();
                Log.e(TAG, "length " +modelList.size());

                    modelList=mStringFilterList;


                for (int i = 0; i < modelList.size(); i++) {
                    if (modelList.get(i).name != null && modelList.get(i).name.length() > 0) {
                        Log.e(TAG, "Inside If   " +constraint.toString());
                        if (modelList.get(i).name.replaceAll("\\s+","").toUpperCase().contains(constraint.toString().replaceAll("\\s+","").toUpperCase())) {
                            Product product = new Product();

                            product.name = modelList.get(i).name;

                            if (modelList.get(i).id != null && modelList.get(i).id.length() > 0) {
                                product.id = modelList.get(i).id;
                            }

                            if (modelList.get(i).image != null && modelList.get(i).image != null) {
                                product.image = modelList.get(i).image;
                            }

                            if (modelList.get(i).ingredients != null && modelList.get(i).ingredients.size() > 0) {
                                product.ingredients = modelList.get(i).ingredients;
                            }

                            filteredList.add(product);

                        }
                    }


                }
                results.count = filteredList.size();
                results.values = filteredList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            modelList = (ArrayList<Product>) filterResults.values;
            notifyDataSetChanged();

        }
    }


}
