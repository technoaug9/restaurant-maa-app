package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.activity.owner.AddProductActivity;
import com.srinivas.restaurantowner.model.SelectNfcChipModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 21/9/15.
 */
public class SelectNfcChipAdapter extends RecyclerView.Adapter<SelectNfcChipAdapter.MyViewHolder> {
    List<SelectNfcChipModel> selectNfcChipModelList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private Intent intent;

    public SelectNfcChipAdapter(Context context, List<SelectNfcChipModel> selectNfcChipListModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.selectNfcChipModelList = selectNfcChipListModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.row_select_nfc_chip, parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SelectNfcChipModel selectNfcChipModel=selectNfcChipModelList.get(position);
        holder.ivProductImg.setImageResource(selectNfcChipModel.getIvProductImg());
        holder.tvProductUniqueCode.setText(selectNfcChipModel.getTvProductUniqueCode());
        holder.tvProductName.setText(selectNfcChipModel.getTvProductName());
        holder.tvProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(context, AddProductActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return selectNfcChipModelList.size();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder{
        private TextView tvProductName,tvProductUniqueCode;
        private ImageView ivProductImg;
        public MyViewHolder(View itemView) {
            super(itemView);
            ivProductImg=(ImageView) itemView.findViewById(R.id.ivProductImg);
            tvProductName=(TextView) itemView.findViewById(R.id.tvProductName);
            tvProductUniqueCode=(TextView) itemView.findViewById(R.id.tvProductUniqueCode);

        }
    }
}
