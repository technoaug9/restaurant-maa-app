package com.srinivas.restaurantowner.adapter.enduser;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.SpinnerRecyclerModel;

import java.util.Collections;
import java.util.List;

/**
 * Created by priya.singh on 18/8/15.
 */
public class SpinnerRecyclerAdapter extends RecyclerView.Adapter<SpinnerRecyclerAdapter.MyViewHolder> {
    List<SpinnerRecyclerModel> productModel = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private String searchedProduct;
    private Intent intent;

    public SpinnerRecyclerAdapter(Context context, List<SpinnerRecyclerModel> productModels) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.productModel = productModels;
    }

    public void delete(int position) {
        productModel.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_spinner_data, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SpinnerRecyclerModel current = productModel.get(position);
        holder.tvSpinnerData.setText(current.getSelectUser());
    }


    @Override
    public int getItemCount() {
        return productModel.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSpinnerData;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvSpinnerData = (TextView) itemView.findViewById(R.id.tvSpinnerData);
        }
    }
}