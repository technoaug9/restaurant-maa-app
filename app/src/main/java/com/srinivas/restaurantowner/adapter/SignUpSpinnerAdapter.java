package com.srinivas.restaurantowner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;

import java.util.ArrayList;

/**
 * Created by rahul.kumar on 18/8/15.
 */
public class SignUpSpinnerAdapter extends BaseAdapter
{

    private Context context;

    private LayoutInflater inflater;

    private ArrayList<String> spinnerData;

    public SignUpSpinnerAdapter(Context context,ArrayList<String> spinnerData)
    {
        this.context=context;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.spinnerData=spinnerData;
    }

    @Override
    public int getCount()
    {
        return spinnerData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHolder
    {
        TextView tvSpinnerData;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView==null)
        {
            viewHolder=new ViewHolder();

            convertView=inflater.inflate(R.layout.row_spinner_data,null);

            viewHolder.tvSpinnerData=(TextView)convertView.findViewById(R.id.tvSpinnerData);

            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder=(ViewHolder)convertView.getTag();
        }

        viewHolder.tvSpinnerData.setText(spinnerData.get(position).toString());

        return convertView;
    }
}
