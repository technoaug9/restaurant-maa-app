package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.model.ProductModel;
import com.srinivas.restaurantowner.utils.Log;

import java.util.Collections;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    List<ProductModel> productModelList = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    private String searchedProduct;
    private Intent intent;
    private boolean isCrossIcon=false;
    private View.OnClickListener yesButtonListener;
    private Dialog mDialog;
    private String TAG=ProductAdapter.class.getSimpleName();
    public ProductAdapter(Context context, List<ProductModel> productModels)
    {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.productModelList = productModels;
    }

    public void delete(int position)
    {
        productModelList.remove(position);
        notifyItemRemoved(position);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
       View  view = inflater.inflate(R.layout.row_product, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,  final int position)
    {

        ProductModel current = productModelList.get(position);
        holder.tvProductName.setText(current.getTvProductName());
      //  holder.ivProductImg.setImageResource(current.getIvProductImg());
        holder.ivProductImg.setVisibility(View.GONE);
        holder.ivCross.setImageResource(current.getIvCross());
        holder.ivCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context,"Delete",Toast.LENGTH_SHORT).show();
            }
        });

        Log.e(TAG,"******==============  7  ==============*********");

        holder.tvProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"******8*********");
              //  Toast.makeText(context,"Delete",Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount()


    {
        return productModelList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvProductName;
        private ImageView ivProductImg,ivCross;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductsName);
             ivProductImg=(ImageView) itemView.findViewById(R.id.ivProductsImg);
            ivCross=(ImageView) itemView.findViewById(R.id.ivCross);
        }
    }

}