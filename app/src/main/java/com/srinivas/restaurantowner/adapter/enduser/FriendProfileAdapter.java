package com.srinivas.restaurantowner.adapter.enduser;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;
import com.srinivas.restaurantowner.R;
import com.srinivas.restaurantowner.ServiceModel.PreventiveList;
import com.srinivas.restaurantowner.Services.RequestURL;
import com.srinivas.restaurantowner.Services.ServiceRequest;
import com.srinivas.restaurantowner.Services.ServiceResponse;
import com.srinivas.restaurantowner.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class FriendProfileAdapter extends RecyclerView.Adapter {
    private static final String TAG = FriendProfileAdapter.class.getSimpleName();
    List<PreventiveList> friendProfileModelList;
    private LayoutInflater inflater;
    private Context context;
    private Dialog mDialog;
    private final int HEADER = 1, BODY = 2;
    private String profileName, age, profileImage, ID = "", userType = "";


    public FriendProfileAdapter(Context context, List<PreventiveList> friendProfileModelList,
                                String profileName, String age, String profileImage, String ID, String userType) {

        this.context = context;
        Log.e(TAG, "list_size_adapter" + friendProfileModelList.size());

        this.friendProfileModelList = friendProfileModelList;
        this.profileImage = profileImage;
        this.profileName = profileName;
        this.age = age;
        mDialog = new Dialog(context);
        this.ID = ID;
        this.userType = userType;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_profile_row_header, parent, false);
                return new ViewHolderHeader(view);
            case BODY:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_frnd_profile, parent, false);
                return new ViewHolderBody(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        switch (holder.getItemViewType()) {
            case HEADER:
                ViewHolderHeader viewHolderHeader = (ViewHolderHeader) holder;

                if (profileName != null && profileName.length() > 0) {
                    viewHolderHeader.tvProfileName.setText(profileName);
                }
                if (age != null && age.length() > 0) {


                    try {

                        Log.e(TAG, "serverTimeStamp>>>>" + age);
                        long timestampLong = Long.parseLong(age);

                        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                        cal.setTimeInMillis(timestampLong * 1000);
                        String date = DateFormat.format("dd-MM-yyyy", cal).toString();

                        String dateArray[] = date.split("-");

                        Log.e(TAG, "serverTimeStampin_YEARS>>>>" + dateArray[2]);
                        final Calendar currentCalender = Calendar.getInstance();
                        int mYear = currentCalender.get(Calendar.YEAR);

                        int age = mYear - Integer.valueOf(dateArray[2]);

                        viewHolderHeader.tvAge.setText("Age: " + age);


                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }


                }

                if (profileImage != null && profileImage.length() > 0) {
                    Picasso.with(context).load(profileImage).placeholder(R.drawable.img).into(viewHolderHeader.ivProfile);
                } else {
                    viewHolderHeader.ivProfile.setImageResource(R.drawable.img);
                }
                break;
            case BODY:
                ViewHolderBody viewHolderBody = (ViewHolderBody) holder;

                if (friendProfileModelList.get(position - 1) != null && friendProfileModelList.size() > 0) {
                    PreventiveList preventiveListModel = friendProfileModelList.get(position - 1);

                    viewHolderBody.ivProductImg.setVisibility(View.GONE);
                    if (preventiveListModel.name != null && preventiveListModel.name.length() > 0) {
                        viewHolderBody.tvProductName.setText(preventiveListModel.name);
                    }

                    if (preventiveListModel.isCrossVisible) {
                        viewHolderBody.ivDeleteProduct.setVisibility(View.VISIBLE);
                    } else {
                        viewHolderBody.ivDeleteProduct.setVisibility(View.INVISIBLE);
                    }

                    viewHolderBody.ivDeleteProduct.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //showCustomDialogLogout( position -1,context, "Do you want to delete\nthis Preventive ?", null, View.VISIBLE, mDialog);
                            deleteDialog(position - 1);
                        }
                    });

                }


                break;
        }

    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return HEADER;
        } else {
            return BODY;
        }
    }

    @Override
    public int getItemCount() {
        return friendProfileModelList.size() + 1;
    }


    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        private ImageView ivProfile;
        private TextView tvProfileName, tvAge;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
            tvProfileName = (TextView) itemView.findViewById(R.id.tvProfileName);
            tvAge = (TextView) itemView.findViewById(R.id.tvAge);

        }
    }

    public class ViewHolderBody extends RecyclerView.ViewHolder {
        TextView tvProductName;
        ImageView ivProductImg, ivDeleteProduct;

        public ViewHolderBody(View itemView) {
            super(itemView);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            ivProductImg = (ImageView) itemView.findViewById(R.id.ivProductImg);
            ivDeleteProduct = (ImageView) itemView.findViewById(R.id.ivDeleteProduct);
        }
    }


    private void deleteDialog(final int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        final Dialog mDialog = new Dialog(context,
                android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        mDialog.getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow()
                .addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();

        View dialoglayout = inflater.inflate(R.layout.delete_dialog, null);
        mDialog.setContentView(dialoglayout);


        TextView tvYes = (TextView) dialoglayout.findViewById(R.id.tvYes);
        TextView tvNo = (TextView) dialoglayout.findViewById(R.id.tvNo);
        TextView tvAlert = (TextView) dialoglayout.findViewById(R.id.tvAlert);
        TextView tvDelete = (TextView) dialoglayout.findViewById(R.id.tvDelete);
        tvDelete.setText("Do you want to delete\nthis Preventive ?");
        tvAlert.setVisibility(View.GONE);

        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDialog.dismiss();
            }
        });
        tvYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isOnline(context)) {
                    ServiceRequest request = new ServiceRequest();
                    request.preventive_id = friendProfileModelList.get(position).id;

                    if (userType.equalsIgnoreCase("user")) {
                        request.user_id = ID;
                    } else {
                        request.user_id = ID;
                    }
                    request.user_type = userType;
                    removePreventiveAPI(request, position);
                } else {
                    CommonUtils.showAlert("Please check your internet connection.", context);
                }
                mDialog.dismiss();
            }
        });
        if (!mDialog.isShowing()) {
            mDialog.show();
        }

    }

    private void removePreventiveAPI(ServiceRequest request, final int position) {

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", context.getString(R.string.please_wait));
        String url = RequestURL.Remove_Preventive;
        Gson gson = new Gson();
        JsonObject requestJson = (JsonObject) gson.toJsonTree(request);
        Log.e(TAG, "URL>>>>>>" + url);
        Log.e(TAG, "req data " + new Gson().toJson(requestJson));

        Ion.with(context)
                .load(url)
                .setTimeout(RequestURL.CONNECTION_TIME_OUT)
                .addHeader(RequestURL.CONTENT_TYPE, RequestURL.APPLICATION_JSON)
                .setJsonObjectBody(requestJson)
                .as(new TypeToken<ServiceResponse>() {
                }).setCallback(new FutureCallback<ServiceResponse>() {
            public void onCompleted(Exception e, ServiceResponse response) {
                Log.e("Tag", " response data" + new Gson().toJson(response));
                progressDialog.dismiss();
                if (response != null) {
                    if (response.response_code != null && response.response_code.equalsIgnoreCase(RequestURL.SUCCESS_CODE)) {
                        friendProfileModelList.remove(position);
                        notifyDataSetChanged();
                        CommonUtils.showToast(context, response.response_message);
                    } else {
                        if (response.response_message != null) {
                            CommonUtils.showToast(context, response.response_message);
                        }
                    }
                } else {
                    if (e != null) {
                        e.printStackTrace();
                        CommonUtils.showToast(context, context.getString(R.string.server_error));
                    }
                }
            }
        });
    }


}