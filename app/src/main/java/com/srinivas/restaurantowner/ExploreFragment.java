/*
package com.srinivas.barcodescanner;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.hangto.R;
import com.hangto.activity.FeaturedHangsterActivity;
import com.hangto.adapter.ExplorePeopleAdapter;
import com.hangto.adapter.ExplorePlaceAdapter;
import com.hangto.model.ExplorePeopleData;
import com.hangto.model.ExplorePeopleResposne;
import com.hangto.model.ExplorePlaceData;
import com.hangto.model.ExplorePlaceResposne;
import com.hangto.services.RequestURL;
import com.hangto.utils.CircleTransformation;
import com.hangto.utils.CommonUtils;
import com.hangto.utils.Constants;
import com.hangto.utils.EndlessRecyclerOnScrollListener;
import com.hangto.utils.HangtoSession;
import com.hangto.utils.HangtoUtils;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ExploreFragment extends Fragment implements  OnClickListener{

	private TextView tvHeader,tvPeopleBar,tvPlaceBar,tv_places,tv_people, tvTellFriends,tvFeaturedHangsters;
	private Activity context;
	private RecyclerView rvPlace,rvPeople;
	private SwipeRefreshLayout placeSwipeRefreshLayout,peopleSwipeRefreshLayout;
	private ExplorePeopleAdapter explorePeopleAdapter;
	private ExplorePlaceAdapter explorePlaceAdapter;
	private ArrayList<ExplorePlaceData> explorePlaceDatas;
	private  ArrayList<ExplorePeopleData> explorePeopleDatas;
	private LinearLayout linearLayoutPlace,linearLayoutPeople;
    private EditText etSearch;
    private ImageView ivSearch, ivCross;
	private int peoplePageNo=1,peoplePageSize;
	private int placePageNo=1,placePageSize;

    private HangtoSession hangtoSession;
    private String keyword = "";
    private boolean isPeopleSelected;
    private LinearLayout llHelp, llSearchView;
    private ImageView ivGotIt;
	private LinearLayout llFeaturedHangsters;

    private EndlessRecyclerOnScrollListener placeEndlessRecyclerOnScrollListener;
    private EndlessRecyclerOnScrollListener peopleEndlessRecyclerOnScrollListener;

	private ImageView ivFirstUser, ivSecondUser, ivThirdUser;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.context=activity;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_explore, container, false);

        FacebookSdk.sdkInitialize(context);

		tvHeader=(TextView)view.findViewById(R.id.tv_title);
		linearLayoutPlace=(LinearLayout)view.findViewById(R.id.layout_place);
		linearLayoutPeople=(LinearLayout)view.findViewById(R.id.layout_people);
        llSearchView=(LinearLayout)view.findViewById(R.id.llSearchView);

        hangtoSession = new HangtoSession(context);
		tv_places = (TextView) view.findViewById(R.id.tv_places);
        tvTellFriends = (TextView) view.findViewById(R.id.tvTellFriends);
		tvFeaturedHangsters = (TextView) view.findViewById(R.id.tvFeaturedHangsters);
		llFeaturedHangsters = (LinearLayout) view.findViewById(R.id.llFeaturedHangsters);
		tv_people = (TextView) view.findViewById(R.id.tv_people);
		tvPeopleBar=(TextView)view.findViewById(R.id.tv_people_bar);
		tvPlaceBar=(TextView)view.findViewById(R.id.tv_place_bar);
        ivSearch = (ImageView)view.findViewById(R.id.ivSearch);
        ivCross = (ImageView)view.findViewById(R.id.ivCross);
		ivFirstUser = (ImageView)view.findViewById(R.id.ivFirstUser);
		ivSecondUser = (ImageView)view.findViewById(R.id.ivSecondUser);
		ivThirdUser = (ImageView)view.findViewById(R.id.ivThirdUser);

        etSearch = (EditText)view.findViewById(R.id.etSearch);



        tv_places.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvTellFriends.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
		tvFeaturedHangsters.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA_BOLD));
        tvHeader.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA_BOLD));
        tv_people.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvPeopleBar.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvPlaceBar.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
		placeSwipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.place_swipeRefreshLayout);
		peopleSwipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.people_swipeRefreshLayout);
		rvPlace= (RecyclerView)view.findViewById(R.id.lvPlace);
        llHelp = (LinearLayout)view.findViewById(R.id.llHelp);
        ivGotIt = (ImageView)view.findViewById(R.id.ivGotIt);

		final LinearLayoutManager placeLayoutManager = new LinearLayoutManager(context);
		rvPlace.setLayoutManager(placeLayoutManager);
		rvPeople= (RecyclerView)view.findViewById(R.id.lvPeople);
		LinearLayoutManager peopleLayoutManager = new LinearLayoutManager(context);
		rvPeople.setLayoutManager(peopleLayoutManager);
		tvHeader.setText("Discover");


		linearLayoutPlace.setVisibility(View.VISIBLE);
		linearLayoutPeople.setVisibility(View.GONE);
		tvPeopleBar.setVisibility(View.INVISIBLE);
		tvPlaceBar.setVisibility(View.VISIBLE);

        tv_places.setTextColor(context.getResources().getColor(R.color.violet));
        tv_people.setTextColor(context.getResources().getColor(R.color.black));

		tv_people.setOnClickListener(this);
		tv_places.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        ivGotIt.setOnClickListener(this);
        tvTellFriends.setOnClickListener(this);
		llFeaturedHangsters.setOnClickListener(this);
        ivCross.setOnClickListener(this);

        if(CommonUtils.getPreferencesBoolean(getActivity(), Constants.DISCOVER_HELP)){
            llHelp.setVisibility(View.GONE);
        }else{
            llHelp.setVisibility(View.VISIBLE);
        }

		explorePeopleDatas = new ArrayList<ExplorePeopleData>();
		explorePlaceDatas= new ArrayList<ExplorePlaceData>();

		explorePlaceAdapter = new ExplorePlaceAdapter(context,explorePlaceDatas);
		explorePeopleAdapter= new ExplorePeopleAdapter(context,explorePeopleDatas);

		rvPeople.setAdapter(explorePeopleAdapter);
		rvPlace.setAdapter(explorePlaceAdapter);


		placeEndlessRecyclerOnScrollListener= new EndlessRecyclerOnScrollListener(placeLayoutManager) {
			@Override
			public void onLoadMore(int current_page) {

				Log.e("Load more ", "current page" + current_page);

				if(explorePlaceAdapter.getItemViewType(explorePlaceDatas.size()-1)==1){
					explorePlaceDatas.add(null);
					explorePlaceAdapter.notifyItemInserted(explorePlaceDatas.size());
					getPlaceList(placePageNo, keyword);
				}

			}
		};


		 peopleEndlessRecyclerOnScrollListener= new EndlessRecyclerOnScrollListener(peopleLayoutManager) {
			@Override
			public void onLoadMore(int current_page) {
				Log.e("Load more ", "current page" + current_page);
				if(explorePeopleAdapter.getItemViewType(explorePeopleDatas.size()-1)==1){
					explorePeopleDatas.add(null);
					explorePeopleAdapter.notifyItemInserted(explorePeopleDatas.size());
					getPeopleList(peoplePageNo, keyword);
				}
			}
		};


		rvPlace.setOnScrollListener(placeEndlessRecyclerOnScrollListener);
		rvPeople.setOnScrollListener(peopleEndlessRecyclerOnScrollListener);




		placeSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				if (CommonUtils.isOnline(context)) {
					placeEndlessRecyclerOnScrollListener.reset(0,true);
					placeSwipeRefreshLayout.setRefreshing(true);
					placePageNo=1;
					getPlaceList(placePageNo, keyword);

				} else {
					Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
				}
			}
		});


		peopleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				if (CommonUtils.isOnline(context)) {
					peopleSwipeRefreshLayout.setRefreshing(true);
					peopleEndlessRecyclerOnScrollListener.reset(0,true);
					peoplePageNo=1;
					getPeopleList(peoplePageNo, keyword);
				} else {
					Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
				}
			}
		});


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(etSearch.getText().toString().trim().length()==0){
                    */
/*etSearch.setVisibility(View.GONE);
                    ivCross.setVisibility(View.GONE);
                    tvHeader.setVisibility(View.VISIBLE);
                    ivSearch.setVisibility(View.VISIBLE);*//*

                    keyword = etSearch.getText().toString().trim();
                    if(isPeopleSelected){
                        if (CommonUtils.isOnline(context)) {
                            peopleSwipeRefreshLayout.setRefreshing(true);
                            peopleEndlessRecyclerOnScrollListener.reset(0,true);
                            peoplePageNo=1;
                            getPeopleList(peoplePageNo, keyword);
                        } else {
                            Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        if (CommonUtils.isOnline(context)) {
                            placeEndlessRecyclerOnScrollListener.reset(0,true);
                            placeSwipeRefreshLayout.setRefreshing(true);
                            placePageNo=1;
                            getPlaceList(placePageNo, keyword);

                        } else {
                            Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
                        }
                    }
                    CommonUtils.hideSoftKeyboard(context);


                }else{
                    ivCross.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                        if(isPeopleSelected){
                            keyword = etSearch.getText().toString().trim();
                            if (CommonUtils.isOnline(context)) {
                                peopleSwipeRefreshLayout.setRefreshing(true);
                                peopleEndlessRecyclerOnScrollListener.reset(0,true);
                                peoplePageNo=1;
                                getPeopleList(peoplePageNo, keyword);
                            } else {
                                Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            keyword = etSearch.getText().toString().trim();
                            if (CommonUtils.isOnline(context)) {
                                placeEndlessRecyclerOnScrollListener.reset(0,true);
                                placeSwipeRefreshLayout.setRefreshing(true);
                                placePageNo=1;
                                getPlaceList(placePageNo, keyword);

                            } else {
                                Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
                            }

                        }

                }
                return false;
            }
        });





		if(CommonUtils.isOnline(context)){
			placePageNo=1;
			getPlaceList(placePageNo, keyword);

		}else{
			Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
		}


		return view;

	}

    @Override
    public void onStart() {
        super.onStart();
        CommonUtils.setScreenTrack("Discover Screen", getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(getActivity()).reportActivityStop(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        if(CommonUtils.isOnline(context)){
            peoplePageNo=1;
            getPeopleList(peoplePageNo, keyword);
        }else{
            //Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
        }
    }

    private void getPeopleList(int PageNo, String keyword) {
		JsonObject jsonRequest = new JsonObject();
		jsonRequest.addProperty("userID", hangtoSession.getUserId());
		jsonRequest.addProperty("fbid", hangtoSession.getFacebookId());
		jsonRequest.addProperty("access_token",hangtoSession.getAccessToken());
		jsonRequest.addProperty("keyword", keyword);
		jsonRequest.addProperty("latitude", hangtoSession.getLatitude());
		jsonRequest.addProperty("longitude", hangtoSession.getLongitude());
		jsonRequest.addProperty("pageNumber",String.valueOf(PageNo));
		jsonRequest.addProperty("pageSize", "10");


		Log.e("", "jsonRequest " + jsonRequest.toString());

		String url = RequestURL.BASE_URL + RequestURL.EXPLORE_PEOPLE;
		Log.e("", "url " + url);

		Ion.with(context)
				.load(url)
				.setHeader("Content-Type", "application/json")
				.setJsonObjectBody(jsonRequest)
				.as(new TypeToken<ExplorePeopleResposne>() {
				})

				.setCallback(new FutureCallback<ExplorePeopleResposne>() {
					@Override
					public void onCompleted(Exception e, final ExplorePeopleResposne result) {

					peopleSwipeRefreshLayout.setRefreshing(false);

						if (result != null) {
							if (result.responseCode.equalsIgnoreCase("200")) {
								if (result.exploreList != null) {

									if(peoplePageNo==1) {
										explorePeopleDatas.clear();
									}

									if(explorePeopleDatas.size()>0) {
										explorePeopleDatas.remove(explorePeopleDatas.size() - 1);
										explorePeopleAdapter.notifyItemRemoved(explorePeopleDatas.size());
									}

									peoplePageNo= Integer.parseInt(result.pagination.pageNumber);
									explorePeopleDatas.addAll(result.exploreList);
									explorePeopleAdapter.notifyDataSetChanged();
									peoplePageNo= peoplePageNo+1;

								}


							} else {
								//Toast.makeText(context, result.responseMessage, Toast.LENGTH_LONG).show();
							}


						} else {
                            if(HangtoUtils.DEBUG) {
                                Toast.makeText(context, context.getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            }
						}
					}
				});
	}



	private void getPlaceList(int pageNo, String keyword) {
		JsonObject jsonRequest = new JsonObject();
		jsonRequest.addProperty("userID", hangtoSession.getUserId());
        jsonRequest.addProperty("latitude", hangtoSession.getLatitude());
        jsonRequest.addProperty("longitude", hangtoSession.getLongitude());
		jsonRequest.addProperty("keyword",keyword);
		jsonRequest.addProperty("timeZone", HangtoUtils.getTimeZone());
    	jsonRequest.addProperty("pageNumber",String.valueOf(pageNo));
		jsonRequest.addProperty("pageSize", "10");


		Log.e("", "jsonRequest " + jsonRequest.toString());

		String url = RequestURL.BASE_URL + RequestURL.EXPLORE_PLACES;
		Log.e("", "url " + url);

		Ion.with(context)
				.load(url)
				.setHeader("Content-Type", "application/json")
				.setJsonObjectBody(jsonRequest)
				.as(new TypeToken<ExplorePlaceResposne> () {
				})

				.setCallback(new FutureCallback<ExplorePlaceResposne>() {
					@Override
					public void onCompleted(Exception e, final ExplorePlaceResposne result) {

					placeSwipeRefreshLayout.setRefreshing(false);

						if (result != null) {
							if (result.responseCode.equalsIgnoreCase("200")) {
								if (result.exploreList != null) {
									if(placePageNo==1) {
									explorePlaceDatas.clear();
									}
									if(explorePlaceDatas.size()>0) {
										explorePlaceDatas.remove(explorePlaceDatas.size() - 1);
										explorePlaceAdapter.notifyItemRemoved(explorePlaceDatas.size());
									}

									placePageNo =Integer.parseInt(result.pagination.pageNumber);

									explorePlaceDatas.addAll(result.exploreList);
									explorePlaceAdapter.notifyDataSetChanged();
									placePageNo=placePageNo+1;

								}

								if(result.featured_image!=null){
									if(result.featured_image.size()>0){
										Picasso.with(context).load(result.featured_image.get(0)).placeholder(R.mipmap.demo).transform(new CircleTransformation()).into(ivFirstUser);
									}

									if(result.featured_image.size()>1){
										Picasso.with(context).load(result.featured_image.get(1)).placeholder(R.mipmap.demo).transform(new CircleTransformation()).into(ivSecondUser);
									}

									if(result.featured_image.size()>1){
										Picasso.with(context).load(result.featured_image.get(2)).placeholder(R.mipmap.demo).transform(new CircleTransformation()).into(ivThirdUser);
									}
								}
							} else {
								//Toast.makeText(context, result.responseMessage, Toast.LENGTH_LONG).show();
							}


						} else {
                            if(HangtoUtils.DEBUG) {
                                Toast.makeText(context, getString(R.string.error_connection), Toast.LENGTH_LONG).show();
                            }
						}
					}
				});
	}

	@Override
	public void onClick(View v) {

			switch (v.getId()) {
				case R.id.tv_places:
					linearLayoutPlace.setVisibility(View.VISIBLE);
					linearLayoutPeople.setVisibility(View.GONE);
					tvPeopleBar.setVisibility(View.INVISIBLE);
					tvPlaceBar.setVisibility(View.VISIBLE);
                    tv_places.setTextColor(context.getResources().getColor(R.color.violet));
                    tv_people.setTextColor(context.getResources().getColor(R.color.black));
                    isPeopleSelected = false;

                    keyword = etSearch.getText().toString().trim();
                    if (CommonUtils.isOnline(context)) {
                        placeEndlessRecyclerOnScrollListener.reset(0,true);
                        placeSwipeRefreshLayout.setRefreshing(true);
                        placePageNo=1;
                        getPlaceList(placePageNo, keyword);
                    } else {
                        Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
                    }

					break;

				case R.id.tv_people:
					linearLayoutPeople.setVisibility(View.VISIBLE);
					linearLayoutPlace.setVisibility(View.GONE);
					tvPeopleBar.setVisibility(View.VISIBLE);
					tvPlaceBar.setVisibility(View.INVISIBLE);
                    tv_people.setTextColor(context.getResources().getColor(R.color.violet));
                    tv_places.setTextColor(context.getResources().getColor(R.color.black));
                    isPeopleSelected = true;

                    keyword = etSearch.getText().toString().trim();
                    if (CommonUtils.isOnline(context)) {
                        peopleSwipeRefreshLayout.setRefreshing(true);
                        peopleEndlessRecyclerOnScrollListener.reset(0,true);
                        peoplePageNo=1;
                        getPeopleList(peoplePageNo, keyword);
                    } else {
                        Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();
                    }
					break;

                case R.id.ivSearch:
                    llSearchView.setVisibility(View.VISIBLE);
                    tvHeader.setVisibility(View.GONE);
                    ivSearch.setVisibility(View.GONE);
                    break;

                case R.id.ivGotIt:
                    llHelp.setVisibility(View.GONE);
                    CommonUtils.saveBooleanPreferences(context, Constants.DISCOVER_HELP, true);
                    break;

                case R.id.tvTellFriends:
                    openInviteDialog();
                    break;

				case R.id.llFeaturedHangsters:
						Intent intent = new Intent(context, FeaturedHangsterActivity.class);
						startActivity(intent);
                    break;

                case R.id.ivCross:
                    etSearch.setText("");
                    llSearchView.setVisibility(View.GONE);
                    tvHeader.setVisibility(View.VISIBLE);
                    ivSearch.setVisibility(View.VISIBLE);
                    break;
			}
		}


    public void openInviteDialog() {

        final Dialog dialog = new Dialog(context);
        LinearLayout llTakeAPhoto, llFromGallery,llFacebookImage;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_tell_friends);

        TextView tvFbInvite, tvFbMessengerInvite, tvWhatsAppInvite, tvMaybeLater, tvMsg;

        tvFbInvite = (TextView)dialog.findViewById(R.id.tvFbInvite);
        tvMsg = (TextView)dialog.findViewById(R.id.tvMsg);
        tvFbMessengerInvite = (TextView)dialog.findViewById(R.id.tvFbMessengerInvite);
        tvWhatsAppInvite = (TextView)dialog.findViewById(R.id.tvWhatsAppInvite);
        tvMaybeLater = (TextView)dialog.findViewById(R.id.tvMaybeLater);
        //Typeface

        tvFbInvite.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvMsg.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvFbMessengerInvite.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvWhatsAppInvite.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));
        tvMaybeLater.setTypeface(Typeface.createFromAsset(context.getAssets(), Constants.FONT_CANDARA));

        tvFbInvite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                    dialog.dismiss();
                if(CommonUtils.isOnline(context)) {
                    if (AppInviteDialog.canShow()) {
                        AppInviteContent content = new AppInviteContent.Builder().setApplinkUrl(Constants.APP_LINK).setPreviewImageUrl(Constants.PREVIEW_URL).build();
                        AppInviteDialog.show(context, content);
                    }
                }else{
                    Toast.makeText(context, getString(R.string.network_connection_error_msg), Toast.LENGTH_SHORT).show();

                }

            }
        });

        tvFbMessengerInvite.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_text));
                        sendIntent.setType("text/plain");
                        sendIntent.setPackage("com.facebook.orca");
                        try
                        {
                            startActivity(sendIntent);
                        }
                        catch (android.content.ActivityNotFoundException ex)
                        {
                            Toast.makeText(context,"Please Install Facebook Messenger.", Toast.LENGTH_SHORT).show();
                        }


                    }
                });

        tvWhatsAppInvite.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.share_text));
                        sendIntent.setType("text/plain");
                        sendIntent.setPackage("com.whatsapp");
                        try
                        {
                            startActivity(sendIntent);
                        }
                        catch (android.content.ActivityNotFoundException ex)
                        {
                            Toast.makeText(context,"Please Install WhatsApp.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

        tvMaybeLater.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });



        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
    }

}
*/
